package ru.itis.emulator;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Clients {

    private static final int TO_MANY_TASKS = 10;
    private static final Logger logger = Logger.getLogger(Clients.class);
    private final ExecutorService executorService;

    private List<Runnable> tasks;

    public Clients() {
        this.executorService = Executors.newCachedThreadPool();
        this.tasks = new ArrayList<>();
        logger.info("Initialized thread pool");
    }

    public void newClient(Runnable task) {
        this.tasks.add(task);
        logger.debug("Added task");

        if (tasks.size() > TO_MANY_TASKS) {
            logger.warn("To many tasks");
        }
    }

    public void run() {
        for (Runnable task : tasks) {
            executorService.submit(task);
        }
        logger.info("Run clients");
    }

    public void stop() {
        executorService.shutdown();
        logger.info("Shutdown thread pool");
    }
}
