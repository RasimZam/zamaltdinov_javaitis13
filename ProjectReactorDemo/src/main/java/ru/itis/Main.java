package ru.itis;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Mono.empty();
        Flux.empty();
        Mono<Integer> mono = Mono.just(1);
        Flux<Integer> flux = Flux.just(1, 2, 3);

        Flux<Integer> fluxFromMono = mono.flux();

//        Flux.range(1, 5).subscribe(System.out::println);

        Flux.<String>generate(sink -> {
            sink.next("hello");
        })
                .delayElements(Duration.ofMillis(500))
                .take(5)
                .subscribe(System.out::println);

        Flux<String> second = Flux
                .just("world", "coder")
                .repeat();

        Flux<String> sumFlux = Flux
                .just("hello", "dru", "Linus", "Asia", "java")
                .zipWith(second, (f, s) -> String.format("%s %s", f, s));

        sumFlux
                .delayElements(Duration.ofMillis(1300))
                .timeout(Duration.ofSeconds(1))
                .onErrorReturn("Too slow")
                .subscribe(System.out::println);

//        Thread.sleep(40001);

    }
}
