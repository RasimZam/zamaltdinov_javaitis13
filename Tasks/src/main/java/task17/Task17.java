package task17;

public class Task17 {
    public static void main(String[] args) {
        int y = 0, x, tempNumber;
        int currentNumber = 371;
        tempNumber = currentNumber;

        while (currentNumber > 0) {
            x = currentNumber % 10;
            currentNumber = currentNumber / 10;
            y = y + (x * x * x);
        }

        if (tempNumber == y) {
            System.out.println("Данное число является числом Армстронга");
        } else {
            System.out.println("Данное число не является числом Армстронга");
        }
    }
}
