package task8;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        String inputString, reversedString = "";
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число или строку: ");
        inputString = scanner.nextLine();
        int stringLength = inputString.length();
        for (int i = stringLength  - 1; i >= 0 ; i--) {
            reversedString = reversedString + inputString.charAt(i);
        }
        System.out.println("перевернутое значение: " + reversedString);

        if (inputString.equals(reversedString)) {
            System.out.println("Введенное значение является палиндромом");
        } else {
            System.out.println("Введенное значение не является палиндромом");
        }

    }
}
