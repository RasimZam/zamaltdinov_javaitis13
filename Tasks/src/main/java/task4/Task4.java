package task4;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        int a,b;
        System.out.println("Enter numbers: ");
        Scanner scanner = new Scanner(System.in);

        a = scanner.nextInt();
        b = scanner.nextInt();

        System.out.println("Before change \na = " + a + "\nb = " + b);

        a = a + b;
        b = a - b;
        a = a - b;

        System.out.println("After change \na = " + a + "\nb = " + b);

    }
}
