package task6;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Task6 {
    public static void main(String[] args) {
        Map<Integer, String> keyValue = new HashMap<>();
        keyValue.put(1, "Hello");
        keyValue.put(2, "World");
        keyValue.put(3, "Have a nice day!");
        System.out.println(keyValue.size());
        System.out.println("Cycle While: ");

        Iterator iter = keyValue.entrySet().iterator();

        while(iter.hasNext()) {
            Map.Entry qurentMe = (Map.Entry)iter.next();
            System.out.println("Ключ это " + qurentMe.getKey() + " Значение это " + qurentMe.getValue());
        }

        System.out.println("Cycle for: ");

        for (Map.Entry qurentMe2: keyValue.entrySet()) {
            System.out.println("Ключ это " + qurentMe2.getKey() + " Значение это " + qurentMe2.getValue());

        }
    }
}
