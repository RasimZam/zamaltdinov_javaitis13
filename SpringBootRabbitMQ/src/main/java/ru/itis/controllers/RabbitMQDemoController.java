package ru.itis.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.models.MenuOrder;
import ru.itis.service.RabbitMQSender;

@RestController
public class RabbitMQDemoController {

    private RabbitMQSender rabbitMQSender;

    public RabbitMQDemoController(RabbitMQSender rabbitMQSender) {
        this.rabbitMQSender = rabbitMQSender;
    }

    @PostMapping(value = "/rabbitmq/sender")
    public String producer(@RequestBody MenuOrder menuOrder) {
        rabbitMQSender.send(menuOrder);
        return "Message sent to the RabbitMQ Queue successfully";
    }
}
