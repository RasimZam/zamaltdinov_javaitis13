package ru.itis.service;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import ru.itis.models.MenuOrder;

import java.util.logging.LogManager;
import java.util.logging.Logger;

@Component
@RabbitListener(queues = "dev-example")
public class RabbitMQReceiver {

    private static final Logger logger = LogManager.getLogManager().getLogger(RabbitMQReceiver.class.toString());

    @RabbitHandler
    public void receiver(MenuOrder menuOrder) {
        logger.info("MenuOrder listener invoked - Consuming Message with MenuOrder Identifier : " +
                menuOrder.getOrderIdentifier());
    }
}
