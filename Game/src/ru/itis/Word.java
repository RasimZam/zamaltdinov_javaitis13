package ru.itis;

public class Word {

    private String value;
    private String changedValue;

    public Word(String value) {
        this.value = value;
    }

    public Word(String value, String changedValue) {
        this.value = value;
        this.changedValue = changedValue;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getChangedValue() {
        return changedValue;
    }

    public void setChangedValue(String changedValue) {
        this.changedValue = changedValue;
    }
}
