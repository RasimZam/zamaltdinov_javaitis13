package ru.itis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WordsChanger {

    public List<Word> change(List<Word> source) {
        List<Word> changedWords = new ArrayList<>();

        for (Word word: source) {
            Word changedWord = change(word);
            changedWords.add(changedWord);
        }
        return changedWords;
    }

    public Word change(Word word) {
        String oldWord = word.getValue();
        char[] chars = oldWord.toCharArray();
        List<Character> changedCharacters = new ArrayList<>();
        for (char character: chars) {
            changedCharacters.add(character);
        }
        Collections.shuffle(changedCharacters);
        char[] newChars = new char[changedCharacters.size()];
        for (int i = 0; i < newChars.length - 1; i++) {
            newChars[i] = changedCharacters.get(i);
        }
        String newWord = new String(newChars);
        return new Word(oldWord, newWord);
    }
}
