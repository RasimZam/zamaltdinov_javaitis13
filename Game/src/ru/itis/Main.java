package ru.itis;

import java.util.List;

public class Main {

    public static void main(String[] args) {
	    WordReader wordReader = new WordReader("words.txt");
        List<Word> words = wordReader.getWords();
        WordsChanger changer = new WordsChanger();
        List<Word> changedWords = changer.change(words);
    }
}
