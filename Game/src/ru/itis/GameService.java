package ru.itis;

import java.util.List;
import java.util.Scanner;

import static com.sun.corba.se.impl.activation.ServerMain.printResult;

public class GameService {

    private int wordsCount;
    private int currentWordPosition;
    private int playerScore;
    private List<Word> words;
    private Scanner scanner;

    public void load(String fileName) {
        WordReader wordReader = new WordReader(fileName);
        List<Word> rawWords = wordReader.getWords();
        WordsChanger changer = new WordsChanger();
        this.words = changer.change(rawWords);

        this.playerScore = 0;
        this.currentWordPosition = 0;
        this.wordsCount = words.size();

        scanner = new Scanner(System.in);
    }

    public void run() {
        while (hasNextWords()) {
            System.out.println(words.get(currentWordPosition).getChangedValue());
            String playerWord = scanner.nextLine();
            if (playerWord.equals(words.get(currentWordPosition).getValue())) {
                System.out.println("YES");
                playerScore++;
            } else {
                System.err.println("MISTAKE");
            }
            currentWordPosition++;
        }
        
        printResult();
    }

    private void printResult() {
        System.out.println("КОЛИЧЕСТВО СЛОВ - " + wordsCount);
        System.out.println("КОЛИЧЕСТВО ПРАВИЛЬНЫХ ОТВЕТОВ - " + playerScore);
        System.out.println("КОЛИЧЕСТВО НЕВЕРНЫХ ОТВЕТОВ - " + (wordsCount - playerScore));
    }

    private boolean hasNextWords() {
        return currentWordPosition < wordsCount;
    }
}
