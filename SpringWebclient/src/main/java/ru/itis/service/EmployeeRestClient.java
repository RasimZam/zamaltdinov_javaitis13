package ru.itis.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;
import ru.itis.dto.Employee;
import ru.itis.exception.ClientDataException;
import ru.itis.exception.EmployeeServiceException;

import java.util.List;

import static ru.itis.constants.EmployeeConstants.*;

@Slf4j
public class EmployeeRestClient {

    private final WebClient webClient;

    public EmployeeRestClient(WebClient webClient) {
        this.webClient = webClient;
    }

    //http://localhost:8081/employeeservice/v1/allEmployees

    public List<Employee> retrieveAllEmployees() {
        return webClient.get().uri(GET_ALL_EMPLOYEES_V1)
                .retrieve()
                .bodyToFlux(Employee.class)
                .collectList()
                .block();
    }

    //http://localhost:8081/employeeservice/v1/employee/10

    public Employee retrieveEmployeeById(Long employeeId) {
        try {
            return webClient.get().uri(EMPLOYEE_BY_ID_V1, employeeId)
                    .retrieve()
                    .bodyToMono(Employee.class)
                    .block();
        } catch (WebClientResponseException ex) {
            log.error("Error response code is {} and the response body is {}",
                    ex.getRawStatusCode(), ex.getResponseBodyAsString());
            log.error("WebClientResponseException in retrieveEmployeeById ", ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Exception in retrieveEmployeeById ", ex);
            throw ex;
        }
    }

    public Employee retrieveEmployeeById_custom_error_handling(Long employeeId) {
            return webClient.get().uri(EMPLOYEE_BY_ID_V1, employeeId)
                    .retrieve()
                    .onStatus(HttpStatus::is4xxClientError, clientResponse -> handle4xxError(clientResponse))
                    .onStatus(HttpStatus::is5xxServerError, clientResponse -> handle5xxError(clientResponse))
                    .bodyToMono(Employee.class)
                    .block();
    }

    //http://localhost:8081/employeeservice/v1/employeeName?employee_name=ABC

    public List<Employee> retrieveEmployeeByName(String employeeName) {
        String uri = UriComponentsBuilder.fromUriString(EMPLOYEE_BY_NAME_V1)
                .queryParam("employeeName", employeeName)
                .build().toString();
        try {
            return webClient.get().uri(uri)
                    .retrieve()
                    .bodyToFlux(Employee.class)
                    .collectList()
                    .block();
        } catch (WebClientResponseException ex) {
            log.error("Error response code is {} and the response body is {}",
                    ex.getRawStatusCode(), ex.getResponseBodyAsString());
            log.error("WebClientResponseException in retrieveEmployeeByName ", ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Exception in retrieveEmployeeByName ", ex);
            throw ex;
        }
    }

    public Employee addNewEmployee(Employee employee) {
        try {
            return webClient.post().uri(ADD_NEW_EMPLOYEE_V1)
                    .bodyValue(employee)
                    .retrieve()
                    .bodyToMono(Employee.class)
                    .block();
        } catch (WebClientResponseException ex) {
            log.error("Error response code is {} and the response body is {}",
                    ex.getRawStatusCode(), ex.getResponseBodyAsString());
            log.error("WebClientResponseException in addNewEmployee ", ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Exception in addNewEmployee ", ex);
            throw ex;
        }
    }

    public Employee updateEmployee(Long employeeId, Employee employee) {
        try {
            return webClient.put().uri(EMPLOYEE_BY_ID_V1, employeeId)
                    .bodyValue(employee)
                    .retrieve()
                    .bodyToMono(Employee.class)
                    .block();
        } catch (WebClientResponseException ex) {
            log.error("Error response code is {} and the response body is {}",
                    ex.getRawStatusCode(), ex.getResponseBodyAsString());
            log.error("WebClientResponseException in updateEmployee ", ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Exception in updateEmployee ", ex);
            throw ex;
        }
    }

    public String deleteEmployee(Long employeeId) {
        return webClient.delete().uri(EMPLOYEE_BY_ID_V1, employeeId)
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    private Mono<? extends Throwable> handle5xxError(ClientResponse clientResponse) {
        Mono<String> errorMessage = clientResponse.bodyToMono(String.class);
        return errorMessage.flatMap((message) -> {
            log.error("Error response code is " + clientResponse.rawStatusCode() +
                    " and the message is " + message);
            throw new EmployeeServiceException(message);
        });
    }

    private Mono<? extends Throwable> handle4xxError(ClientResponse clientResponse) {
        Mono<String> errorMessage = clientResponse.bodyToMono(String.class);
        return errorMessage.flatMap((message) -> {
            log.error("Error response code is " + clientResponse.rawStatusCode() +
                    " and the message is " + message);
            throw new ClientDataException(message);
        });
    }
}
