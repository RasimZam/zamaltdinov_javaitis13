package ru.itis;

import org.junit.jupiter.api.Test;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import ru.itis.dto.Employee;
import ru.itis.exception.ClientDataException;
import ru.itis.service.EmployeeRestClient;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class EmployeeRestClientTest {

    private static final String baseUrl = "http://localhost:8081/employeeservice";
    private final WebClient webClient = WebClient.create(baseUrl);

    EmployeeRestClient employeeRestClient = new EmployeeRestClient(webClient);

    @Test
    void retrieveAllEmployees() {
        List<Employee> employeesList = employeeRestClient.retrieveAllEmployees();
        assertTrue(employeesList.size() > 0);
    }

    @Test
    void retrieveById() {
        long employeeId = 1;
        Employee employee = employeeRestClient.retrieveEmployeeById(employeeId);
        assertEquals("Chris", employee.getFirstName());
    }

    @Test
    void retrieveEmployeeById_notFound() {
        Long employeeId = 10L;
        assertThrows(WebClientResponseException.class,
                () -> employeeRestClient.retrieveEmployeeById(employeeId));
    }

    @Test
    void retrieveEmployeeById_custom_error_handling() {
        Long employeeId = 10L;
        assertThrows(ClientDataException.class,
                () -> employeeRestClient.retrieveEmployeeById_custom_error_handling(employeeId));
    }

    @Test
    void retrieveEmployeeByName() {
        String name = "Chris";
        List<Employee> employees = employeeRestClient.retrieveEmployeeByName(name);
        assertTrue(employees.size() > 0);
        Employee employee = employees.get(0);
        assertEquals("Chris", employee.getFirstName());
    }

    @Test
    void retrieveEmployeeByName_notFound() {
        String name = "ABC";
        assertThrows(WebClientResponseException.class,
                () -> employeeRestClient.retrieveEmployeeByName(name));
    }

    @Test
    void addNewEmployee() {
        Employee employee =
                new Employee(null, 54, "Iron", "Man", "male", "engineer");
        Employee addEmployee = employeeRestClient.addNewEmployee(employee);
        assertTrue(addEmployee.getId() != null);
    }

    @Test
    void addNewEmployee_BadRequest() {
        Employee employee =
                new Employee(null, 54, null, "Man", "male", "engineer");
        assertThrows(WebClientResponseException.class,
                () -> employeeRestClient.addNewEmployee(employee));
    }

    @Test
    void updateEmployee() {
        Employee employee =
                new Employee(null, 54, "Adam1", "Sandler1", "male", null);
        Employee updateEmployee = employeeRestClient.updateEmployee(2L, employee);
        assertEquals("Adam1", updateEmployee.getFirstName());
        assertEquals("Sandler1", updateEmployee.getLastName());
    }

    @Test
    void updateEmployee_NotFound() {
        Employee employee =
                new Employee(null, 54, "Adam1", "Sandler1", "male", null);
        assertThrows(WebClientResponseException.class,
                () -> employeeRestClient.updateEmployee(200L, employee));
    }

    @Test
    void deleteEmployeeById() {
        Employee employee =
                new Employee(null, 54, "Iron", "Man", "male", "engineer");
        Employee addEmployee = employeeRestClient.addNewEmployee(employee);
        String response = employeeRestClient.deleteEmployee(addEmployee.getId());
        String expectedMessage = "Employee deleted successfully.";
        assertEquals(expectedMessage, response);
    }
}
