package reflectionExample5;

import java.io.FileReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Properties;

public class PasswordFactory {
    public static String getPassword() {
        try {
            String clazz = getGenerator();
            Class<?> genClass = Class.forName(clazz);

            Method[] methods = genClass.getDeclaredMethods();
            for (Method method: methods) {
                System.out.println("Check method: " + method.getName());
                Generator ann = method.getAnnotation(Generator.class);
                if (ann != null) {
                    Object generator = createObject(genClass);
                    method.setAccessible(true);
                    return (String) method.invoke(generator);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return "";
    }

    private static String getGenerator() throws Exception {
        Properties properties = new Properties();
        properties.load(new FileReader("generator.properties"));
        return properties.getProperty("generator.class");
    }

    private static Object createObject(Class clazz) throws Exception {
        Constructor constructor = clazz.getConstructor(String.class, String.class);
        Object obj = constructor.newInstance("FromConstructorAlg", "FromConstructorName");
//        return obj;
        return fillProperties(obj);
    }

    private static Object fillProperties(Object obj) throws Exception {
        Properties properties = new Properties();
        properties.load(new FileReader("generator.properties"));

        for (String name: properties.stringPropertyNames()) {
            if (!"generator.class".equals(name)) {
                String value = properties.getProperty(name);
                System.out.println("Property: " + name + " = " + value);

                Field field = obj.getClass().getDeclaredField(name);
                field.setAccessible(true);
                field.set(obj, value);
            }
        }
        return obj;
    }
}
