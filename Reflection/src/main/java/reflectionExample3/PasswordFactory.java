package reflectionExample3;

import java.io.FileReader;
import java.lang.reflect.Method;
import java.util.Properties;

public class PasswordFactory {
    public static String getPassword() {
        try {
            String clazz = getGenerator();
            Class<?> genClass = Class.forName(clazz);
            Object generator =  genClass.newInstance();

            Method[] methods = genClass.getMethods();
            for (Method method: methods) {
                System.out.println("Check method: " + method.getName());
                Generator ann = method.getAnnotation(Generator.class);
                if (ann != null) {
                    return (String) method.invoke(generator);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return "";
    }

    private static String getGenerator() throws Exception {
        Properties properties = new Properties();
        properties.load(new FileReader("generator.properties"));
        return properties.getProperty("generator");
    }
}
