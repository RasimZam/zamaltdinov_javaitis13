package reflectionExample1;

import java.io.FileReader;
import java.util.Properties;

public class PasswordGeneratorFactory {
    public static PasswordGenerator getPasswordGenerator() {
        try {
            String clazz = getGenerator();
            Class<?> genClass = Class.forName(clazz);
//            System.out.println(genClass.getCanonicalName());
            PasswordGenerator generator = (PasswordGenerator) genClass.newInstance();
            return generator;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static String getGenerator() throws Exception {
        Properties properties = new Properties();
        properties.load(new FileReader("generator.properties"));
        return properties.getProperty("generator");
    }
}
