package reflectionExample1;

public class Main {
    public static void main(String[] args) {
        PasswordGenerator pgf = PasswordGeneratorFactory.getPasswordGenerator();
        String password = pgf.generate();
        System.out.println(password);
    }
}
