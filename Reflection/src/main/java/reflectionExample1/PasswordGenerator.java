package reflectionExample1;

public interface PasswordGenerator {
    String generate();
}
