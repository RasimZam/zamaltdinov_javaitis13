package reflectionExample2;

public class PasswordGeneratorSecond {

    static {
        System.out.println("PasswordGeneratorSecond");
    }

    public String generate() {
        return "Second Password";
    }
}
