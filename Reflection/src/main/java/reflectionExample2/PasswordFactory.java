package reflectionExample2;

import java.io.FileReader;
import java.lang.reflect.Method;
import java.util.Properties;

public class PasswordFactory {
    public static String getPassword() {
        try {
            String clazz = getGenerator();
            Class<?> genClass = Class.forName(clazz);
            Object generator =  genClass.newInstance();
            System.out.println(generator.getClass().getCanonicalName());

            String methodStr = getMethod();
            Method method = genClass.getMethod(methodStr);
            System.out.println(method.getName());

            String pswd = (String) method.invoke(generator);
            return pswd;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
//        return new PasswordGeneratorFirst().generate();
    }

    private static String getGenerator() throws Exception {
        Properties properties = new Properties();
        properties.load(new FileReader("generator.properties"));
        return properties.getProperty("generator");
    }

    private static String getMethod() throws Exception {
        Properties properties = new Properties();
        properties.load(new FileReader("generator.properties"));
        return properties.getProperty("method");
    }
}
