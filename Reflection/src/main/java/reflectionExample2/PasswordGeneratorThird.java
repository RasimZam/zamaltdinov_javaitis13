package reflectionExample2;

public class PasswordGeneratorThird {

    static {
        System.out.println("PasswordGeneratorThird");
    }

    public String generate() {
        return "Third Password";
    }
}
