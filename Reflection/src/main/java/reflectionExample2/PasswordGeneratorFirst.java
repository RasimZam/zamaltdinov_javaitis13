package reflectionExample2;

public class PasswordGeneratorFirst {

    static {
        System.out.println("PasswordGeneratorFirst");
    }

    public String generate() {
        return "First Password";
    }
}
