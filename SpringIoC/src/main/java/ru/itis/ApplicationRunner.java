package ru.itis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itis.config.ApplicationConfiguration;
import ru.itis.config.DataBaseProperties;
import ru.itis.dataBase.pool.ConnectionPool;
import ru.itis.dataBase.repository.CompanyRepository;

@SpringBootApplication
public class ApplicationRunner {
    public static void main(String[] args) {
//        CompanyRepository companyRepository;
//        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class)) {
//
//            ConnectionPool pool = context.getBean("pool1", ConnectionPool.class);
//            System.out.println(pool);
//
//            companyRepository = context.getBean("companyRepository", CompanyRepository.class);
//            System.out.println(companyRepository);

       ApplicationContext context = SpringApplication.run(ApplicationRunner.class, args);
       context.getBean(DataBaseProperties.class);
       int i = 0;
    }
}

