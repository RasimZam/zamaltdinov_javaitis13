package ru.itis.kafkaspringdemo.utils;

public final class Constant {

    public static final String TOPIC = "test";
    public static final String GROUP_ID = "group_id";

    private Constant() {
    }
}
