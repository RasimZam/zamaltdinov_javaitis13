package ru.itis.kafkaspringdemo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import ru.itis.kafkaspringdemo.utils.Constant;

import static ru.itis.kafkaspringdemo.utils.Constant.TOPIC;

@Slf4j
@Service
public class KafkaProducerService {

    private final KafkaTemplate<String, String> kafkaTemplate;

    public KafkaProducerService(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendMessage(String message) {
        log.info("Sent message: {}", message);
        kafkaTemplate.send(TOPIC, message);
    }
}
