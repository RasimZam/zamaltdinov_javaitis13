package ru.itis.kafkaspringdemo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import ru.itis.kafkaspringdemo.utils.Constant;

import static ru.itis.kafkaspringdemo.utils.Constant.GROUP_ID;
import static ru.itis.kafkaspringdemo.utils.Constant.TOPIC;

@Slf4j
@Service
public class KafkaConsumerService {

    @KafkaListener(topics = TOPIC, groupId = GROUP_ID)
    private void consume(String message) {
        log.info("Got message: {}", message);
    }
}
