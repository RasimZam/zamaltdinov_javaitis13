package ru.itis.kafkaspringdemo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.kafkaspringdemo.service.KafkaProducerService;

@Slf4j
@RestController
public class KafkaController {

    private final KafkaProducerService service;

    public KafkaController(KafkaProducerService service) {
        this.service = service;
    }

    @PostMapping(value = "/publish")
    private void sendMessageKafka(@RequestBody String message) {
        log.info("Controller got message: {}", message);
        service.sendMessage(message);
    }
}
