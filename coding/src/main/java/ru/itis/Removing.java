package ru.itis;

public class Removing {
    public static void main(String[] args) {
        int[] nums = new int[]{3, 2, 2, 3};
        int target = 3;

        int number = removeElement(nums, target);
    }

    public static int removeElement(int[] nums, int target) {
        int index = 0;
        for (int i : nums) {
            if (i != target) {
                nums[index++] = i;
            }
        }
        return index;
    }
}
