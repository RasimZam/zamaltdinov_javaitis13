package ru.itis;

public class EvenNumberOfDigits {

    public static void main(String[] args) {
        int number = findNumbersWithEvenNumberOfDigits(new int[] {12,345,2,6,7896});
        System.out.println(number);
    }

    private static int findNumbersWithEvenNumberOfDigits(int[] nums) {
        int count = 0;
        for (int i : nums) {
            int length = String.valueOf(i).length();
            if(length % 2 == 0) {
                count++;
            }
        }
        return count;
    }
}
