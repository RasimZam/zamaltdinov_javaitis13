package ru.itis;

public class MaxConsecutiveOnes {
    public static void main(String[] args) {
        int max = findMaxConsecutiveOnes(new int[] {1, 1, 1, 1, 0, 1});
        System.out.println(max);
    }

    public static int findMaxConsecutiveOnes(int[] nums) {
        int count = 0;
        int maxAmount = 0;
        for(int i = 0; i <= nums.length - 1; i++) {
            if (nums[i] == 1) {
                count++;
            } else {
                count = 0;
            }

            maxAmount = Math.max(maxAmount, count);
        }

        return maxAmount;
    }

}
