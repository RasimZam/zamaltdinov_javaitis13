package ru.itis;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum {
    public static void main(String[] args) {
        int[] array = new int[]{1, 7, 2, 15};
        int target = 9;

//        int[] indexArray = sumTwoNumber(array, target);
        int[] indexArray1 = sumTwoNumber1(array, target);
//        System.out.println(Arrays.toString(indexArray));
        System.out.println(Arrays.toString(indexArray1));

    }

    private static int[] sumTwoNumber(int[] array, int target) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] + array[j] == target) {
                    return new int[]{i, j};
                }
            }
        }
        throw new IllegalArgumentException("No solution for defined input data!!!");
    }

    private static int[] sumTwoNumber1(int[] array, int target) {
        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < array.length; i++) {
            map.put(array[i], i);
        }

        for (int i = 0; i < array.length; i++) {
            int requiredNumber = target - array[i];

            if (map.containsKey(requiredNumber) && map.get(requiredNumber) != i) {
                return new int[]{i, map.get(requiredNumber)};
            }
        }
        throw new IllegalArgumentException("No solution for defined input data!!!");
    }
}

