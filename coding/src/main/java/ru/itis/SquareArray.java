package ru.itis;

import java.util.Arrays;

public class SquareArray {
    public static void main(String[] args) {
        int[] sortedArray = sortedSquares(new int[]{-4,-1,0,3,10});
        System.out.println(Arrays.toString(sortedArray));
    }

    private static int[] sortedSquares(int[] nums) {
        for (int i = 0; i <= nums.length - 1; i++) {
            nums[i] = (int) Math.pow(nums[i], 2);
        }
        Arrays.sort(nums);
        return nums;
    }
}
