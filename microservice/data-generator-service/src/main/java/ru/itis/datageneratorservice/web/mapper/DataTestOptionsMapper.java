package ru.itis.datageneratorservice.web.mapper;

import org.mapstruct.Mapper;
import ru.itis.datageneratorservice.model.test.DataTestOptions;
import ru.itis.datageneratorservice.web.dto.DataTestOptionsDto;

@Mapper(componentModel = "spring")
public interface DataTestOptionsMapper extends Mappable<DataTestOptions, DataTestOptionsDto> {
}
