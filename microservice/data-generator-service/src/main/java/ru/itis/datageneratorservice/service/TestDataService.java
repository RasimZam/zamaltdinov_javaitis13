package ru.itis.datageneratorservice.service;

import ru.itis.datageneratorservice.model.test.DataTestOptions;

public interface TestDataService {
    void sendMessages(DataTestOptions testOptions);
}
