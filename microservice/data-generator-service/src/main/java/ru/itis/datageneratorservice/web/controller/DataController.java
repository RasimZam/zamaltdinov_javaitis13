package ru.itis.datageneratorservice.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.datageneratorservice.model.Data;
import ru.itis.datageneratorservice.model.test.DataTestOptions;
import ru.itis.datageneratorservice.service.KafkaDataService;
import ru.itis.datageneratorservice.service.TestDataService;
import ru.itis.datageneratorservice.web.dto.DataDto;
import ru.itis.datageneratorservice.web.dto.DataTestOptionsDto;
import ru.itis.datageneratorservice.web.mapper.DataMapper;
import ru.itis.datageneratorservice.web.mapper.DataTestOptionsMapper;

@RestController
@RequestMapping("/api/v1/data")
@RequiredArgsConstructor
public class DataController {
    private final KafkaDataService kafkaDataService;
    private final DataMapper dataMapper;
    private final DataTestOptionsMapper dataTestOptionsMapper;
    private final TestDataService testDataService;

    @PostMapping("/send")
    public void send(@RequestBody DataDto dto) {
        Data data = dataMapper.toEntity(dto);
        kafkaDataService.send(data);
    }

    @PostMapping("/test/send")
    public void testSend(@RequestBody DataTestOptionsDto testOptionsDto) {
        DataTestOptions testOptions = dataTestOptionsMapper.toEntity(testOptionsDto);
        testDataService.sendMessages(testOptions);
    }
}
