package ru.itis.datageneratorservice.service;

import ru.itis.datageneratorservice.model.Data;

public interface KafkaDataService {
    void send(Data data);
}
