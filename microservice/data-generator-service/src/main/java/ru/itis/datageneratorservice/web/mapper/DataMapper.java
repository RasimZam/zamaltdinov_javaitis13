package ru.itis.datageneratorservice.web.mapper;

import org.mapstruct.Mapper;
import ru.itis.datageneratorservice.model.Data;
import ru.itis.datageneratorservice.web.dto.DataDto;

@Mapper(componentModel = "spring")
public interface DataMapper extends Mappable<Data, DataDto> {
}
