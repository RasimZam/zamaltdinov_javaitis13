package ru.itis;

import ru.itis.generic.Info;
import ru.itis.generic.Pair;

public class Main {
    public static void main(String[] args) {
        Info<String> info = new Info<>("Hello");
        System.out.println(info);
        String str = info.getValue();

        Pair<String, Integer> pair = new Pair<>("hello", 20);
        System.out.println("Values pairs: value1 = " + pair.getFirstValue() +
                ", value2  = " + pair.getSecondValue());
    }
}
