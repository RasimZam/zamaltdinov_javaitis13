package ru.itis.generic;

import java.util.ArrayList;
import java.util.List;

public class GenericMethod {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(13);
        list.add(45);
        int a = GenMethod.getSecondElement(list);
        System.out.println(a);
    }
}
