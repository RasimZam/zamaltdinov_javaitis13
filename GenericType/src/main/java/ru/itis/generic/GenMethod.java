package ru.itis.generic;

import java.util.List;

public class GenMethod {

    public static <T> T getSecondElement(List<T> al) {
        return al.get(1);
    }
}
