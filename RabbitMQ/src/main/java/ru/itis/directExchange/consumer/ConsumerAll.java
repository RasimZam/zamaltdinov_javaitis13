package ru.itis.directExchange.consumer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.springframework.amqp.core.ExchangeTypes;

import java.nio.charset.StandardCharsets;

public class ConsumerAll {
    public static void main(String[] args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("direct_logs", ExchangeTypes.DIRECT);
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, "direct_logs", "error");
        channel.queueBind(queueName, "direct_logs", "info");
        channel.queueBind(queueName, "direct_logs", "warning");
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            System.out.println("Received message ' " + message + "'");
        };
        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });
        System.out.println("Subscribed to the queue " + queueName);
    }
}
