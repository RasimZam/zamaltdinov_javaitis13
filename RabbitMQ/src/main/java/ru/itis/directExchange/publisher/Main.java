package ru.itis.directExchange.publisher;

import static ru.itis.directExchange.publisher.Task.createTask;

public class Main {
    public static void main(String[] args) throws Exception {
         new Thread(() -> {
             try {
                 createTask(3000, "error");
             } catch (Exception e) {
                 e.printStackTrace();
             }
         }).start();

        new Thread(() -> {
            try {
                createTask(5000, "info");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(() -> {
            try {
                createTask(7000, "warning");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();

    }
}
