package ru.itis.defultExchange.publisher;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.nio.charset.StandardCharsets;

public class Main {

    private final static String QUEUE_NAME = "dev-queue";

    public static void main(String[] args) throws Exception {
        int counter = 0;

        do {
            Thread.sleep(3000);
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            try (Connection connection = factory.newConnection();
                 Channel channel = connection.createChannel()) {

                channel.queueDeclare(QUEUE_NAME, false, false, false, null);
                String message = "Message from publisher N " + counter++;
                channel.basicPublish("", QUEUE_NAME, null, message.getBytes(StandardCharsets.UTF_8));
                System.out.println("Message is sent into Default Exchange " + counter);
            }
        } while (true);
    }
}
