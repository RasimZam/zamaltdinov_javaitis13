package ru.itis.structural.proxy;

public interface Project {

    void run();
}
