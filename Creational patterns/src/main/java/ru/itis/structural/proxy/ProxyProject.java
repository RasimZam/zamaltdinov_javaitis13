package ru.itis.structural.proxy;

import java.util.Optional;

public class ProxyProject implements Project {
    private String url;
    private RealProject realProject;

    public ProxyProject(String url) {
        this.url = url;
    }

    @Override
    public void run() {
        realProject = Optional.ofNullable(realProject).orElse(new RealProject(url));
        realProject.run();
    }
}
