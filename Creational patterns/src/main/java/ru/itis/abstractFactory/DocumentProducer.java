package ru.itis.abstractFactory;

public interface DocumentProducer {
    Act createAct();
    Statement createStatement();
}
