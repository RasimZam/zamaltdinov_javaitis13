package ru.itis.abstractFactory;

public interface Act {
    String getDescription();
}
