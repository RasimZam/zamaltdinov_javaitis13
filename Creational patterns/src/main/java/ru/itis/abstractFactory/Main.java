package ru.itis.abstractFactory;

public class Main {
    public static void main(String[] args) {
        DocumentProducer producer = new DocumentProducerWithDateImpl();
        Statement statement = producer.createStatement();
        Act act = producer.createAct();

        System.out.println(statement.getDescription());
        System.out.println(act.getDescription());

    }
}
