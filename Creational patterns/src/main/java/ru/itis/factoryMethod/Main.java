package ru.itis.factoryMethod;

public class Main {
    public static void main(String[] args) {
        DocumentsProducer producer = new ActsProducer();
        DocumentDispatcher dispatcher = new DocumentDispatcher(producer);
        dispatcher.enterDocumentInformation();
    }
}
