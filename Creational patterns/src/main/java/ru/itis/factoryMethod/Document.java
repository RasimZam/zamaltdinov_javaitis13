package ru.itis.factoryMethod;

public interface Document {
    String getType();
    String getFrom();
}
