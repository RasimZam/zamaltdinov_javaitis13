package ru.itis.factoryMethod;

public interface DocumentsProducer {

    Document create(Information information);
}
