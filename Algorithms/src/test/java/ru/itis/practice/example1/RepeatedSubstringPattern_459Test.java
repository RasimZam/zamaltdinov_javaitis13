package ru.itis.practice.example1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RepeatedSubstringPattern_459Test {

    @Test
    void whenRepeatedSubstringThenTrue() {
        String str = "abcabc";
        boolean result = RepeatedSubstringPattern_459.repeatedSubstring(str);
        Assertions.assertTrue(result);
    }

    @Test
    void whenRepeatedSubstringThenFalse() {
        String str = "aba";
        boolean result = RepeatedSubstringPattern_459.repeatedSubstring(str);
        Assertions.assertFalse(result);
    }

    @Test
    void whenRepeatedSubstringThen_True() {
        String str = "abcabcabcabc";
        boolean result = RepeatedSubstringPattern_459.repeatedSubstring(str);
        Assertions.assertTrue(result);
    }
}