package ru.itis.lambda.functional;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Task178Test {

    @Test
    void rndStringFromStrings() {
        String str1 = "Some";
        String str2 = "random";
        String str3 = "text";

        String result1 = Task178.extractorCharacter(str1);
        String result2 = Task178.extractorCharacter(str2);
        String result3 = Task178.extractorCharacter(str3);

        assertEquals(result1.length(), 1);
        assertEquals(result2.length(), 1);
        assertEquals(result3.length(), 1);
    }
}