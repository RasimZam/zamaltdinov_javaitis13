package ru.itis.json.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.itis.json.JacksonJsonProcessor;
import ru.itis.json.JsonProcessor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

class JacksonJsonProcessorTest {

    private User user;
    private JsonProcessor processor;

    @BeforeEach
    void init() {
        user = new User();
        user.setName("Ivan");
        user.setLastName("Ivanov");
        processor = new JacksonJsonProcessor();
    }

    @Test
    void toJsonValidObjectSuccess() {
        String json = processor.toJson(user);
    }

    @Test
    void fromJsonValidJsonSuccess() throws IOException {
        String json = Files.readString(Paths.get("src/test/resources/sample.json"));
        User user = processor.fromJson(json);
        Assertions.assertEquals("Peter", user.getName());
        Assertions.assertEquals("Jones", user.getLastName());
    }

    @Test
    void getUserNameValidJsonSuccess() throws IOException {
        String json = Files.readString(Paths.get("src/test/resources/sample.json"));
        String name = processor.getUserName(json);
        Assertions.assertEquals("Peter", name);
    }
}