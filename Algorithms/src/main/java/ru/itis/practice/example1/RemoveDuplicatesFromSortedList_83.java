package ru.itis.practice.example1;

// 83. Remove Duplicates from Sorted List

public class RemoveDuplicatesFromSortedList_83 {

    static class ListNode {
        int val;
        ListNode next;
        ListNode() {

        }

        ListNode(int val) {
            this.val = val;
        }
    }

    private static ListNode deleteDuplicates(ListNode head) {
        ListNode currentNode = head;

        while (currentNode != null && currentNode.next != null) {
           if (currentNode.next.val == currentNode.val) {
               currentNode.next = currentNode.next.next;
           } else {
               currentNode = currentNode.next;
           }
        }
        return head;
    }
}
