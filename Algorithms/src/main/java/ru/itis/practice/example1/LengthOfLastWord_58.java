package ru.itis.practice.example1;

// 58. Length of Last Words

public class LengthOfLastWord_58 {
    public static void main(String[] args) {
        String str = "Hello big world     ";
        System.out.println(lengthOfLastWord(str));
//        System.out.println(lengthOfLastWord1(str));
        System.out.println(lengthOfLastWord2(str));
    }

    private static int lengthOfLastWord(String s) {
        String[] word = s.split(" ");
        return word[word.length -1].length();
    }

    private static int lengthOfLastWord1(String s) {
        s = s.trim();
        int lastIndex = s.lastIndexOf(' ') + 1;
        return s.length() - lastIndex;
    }

    private static int lengthOfLastWord2(String s) {
        int i = s.length() - 1;

        while (i >= 0 && s.charAt(i) == ' ')
            --i;
        final int lastIndex = i;
        while (i >= 0 && s.charAt(i) != ' ')
            --i;

        return lastIndex - i;
    }
}
