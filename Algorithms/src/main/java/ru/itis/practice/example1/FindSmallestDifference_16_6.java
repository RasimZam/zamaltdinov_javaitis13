package ru.itis.practice.example1;

import java.util.Arrays;

public class FindSmallestDifference_16_6 {
    public static void main(String[] args) {
        int[] array1 = {1, 3, 15, 11, 2};
        int[] array2 = {23, 127, 235, 19, 8};
        int result = findSmallestDifference1(array1, array2);
        System.out.println(result);
    }

    public static int findSmallestDifference(int[] array1, int[] array2) {
        if (array1.length == 0 || array2.length == 0) {
            return -1;
        }
        int min = Integer.MAX_VALUE;
        // 1, 3, 15, 11, 2
        for (int i = 0; i < array1.length; i++) {
            // 23, 127, 235, 19, 8
            for (int j = 0; j < array2.length; j++) {
                // 1 - 23 = -22 < min
                if (Math.abs(array1[i] - array2[j]) < min) {
                    // 22
                    min = Math.abs(array1[i] - array2[j]);
                }
            }
        }
        return min;
    }

    public static int findSmallestDifference1(int[] array1, int[] array2) {
        Arrays.sort(array1);
        Arrays.sort(array2);
        int array1Pointer = 0;
        int array2Pointer = 0;
        int difference = Integer.MAX_VALUE;
        while (array1Pointer < array1.length && array2Pointer < array2.length) {
            if (Math.abs(array1[array1Pointer] - array2[array2Pointer]) < difference) {
                difference = Math.abs(array1[array1Pointer] - array2[array2Pointer]);
            }

            if (array1[array1Pointer] < array2[array2Pointer]) {
                array1Pointer++;
            } else {
                array2Pointer++;
            }
        }
        return difference;
    }
}
