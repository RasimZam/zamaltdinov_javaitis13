package ru.itis.practice.example1;

import java.util.ArrayList;

public class ComputePondSize_16_19 {
    public static void main(String[] args) {
        int[][] array = {{0, 2, 1, 0}, {0, 1, 0, 1}, {1, 1, 0, 1}, {0, 1, 0, 1}};
        ArrayList<Integer> result = computePondSize(array);
        System.out.println(result);

    }


    public static ArrayList<Integer> computePondSize(int[][] land) {
        ArrayList<Integer> pondSizes = new ArrayList<>();
        for (int row = 0; row < land.length; row++) {
            for (int col = 0; col < land[row].length; col++) {
                if (land[row][col] == 0) {
                    int size = computeSize(land, row, col);
                    pondSizes.add(size);
                }
            }
        }
        return pondSizes;
    }

    private static int computeSize(int[][] land, int row, int col) {
        /* Если ячейка выходит за границы участка или уже посещалось*/
        if (row < 0 || col < 0 || row >= land.length || col >= land[row].length ||
                land[row][col] != 0) { //Посещалось или не содержит воду
            return 0;
        }
        int size = 1;
        land[row][col] = -1; // ячейка помечается как посещенная
        for (int dr = -1; dr <= 1; dr++) {
            for (int dc = 0; dc <= 1; dc++) {
                size += computeSize(land, row + dr, col + dc);
            }
        }
        return size;
    }
}
