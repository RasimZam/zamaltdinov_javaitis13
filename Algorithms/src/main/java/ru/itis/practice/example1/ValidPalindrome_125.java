package ru.itis.practice.example1;

// 125. Valid Palindrome
// time O(n)
// space O(n)

public class ValidPalindrome_125 {
    public static void main(String[] args) {
        System.out.println(isPalindrome("A man, a plan, a canal: Panama"));
        System.out.println("-------------");
        System.out.println(isPalindrome("race a car"));
        System.out.println("-------------");
    }

    public static boolean isPalindrome(String s) {
        StringBuilder fixedString = new StringBuilder();
        for (char ch : s.toCharArray()) {
            if (Character.isDigit(ch) || Character.isLetter(ch)) {
                fixedString.append(ch);
            }
        }
        fixedString = new StringBuilder(fixedString.toString().toLowerCase());
        int leftPointer = 0;
        int rightPointer = fixedString.length() - 1;

        while (leftPointer <= rightPointer) {
            if (fixedString.charAt(leftPointer) != fixedString.charAt(rightPointer)) {
                return false;
            }
            leftPointer++;
            rightPointer--;
        }
        return true;
    }
}
