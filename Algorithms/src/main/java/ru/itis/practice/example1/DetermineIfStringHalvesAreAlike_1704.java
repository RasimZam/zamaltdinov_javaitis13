package ru.itis.practice.example1;

import java.util.Set;

public class DetermineIfStringHalvesAreAlike_1704 {
    public static void main(String[] args) {
        String strTrue = "book";
        String strFalse = "textbook";
        System.out.println(halvesAreAlike(strTrue));
        System.out.println("------------------------");
        System.out.println(halvesAreAlike(strFalse));
    }

    public static boolean halvesAreAlike(String str) {
        char[] ch = str.toCharArray();
        int leftPointer = 0, rightPointer = ch.length - 1;
        int leftCount = 0, rightCount = 0;
        Set vowels = Set.of('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U');

        while(leftPointer < rightPointer) {
            leftCount += vowels.contains(ch[leftPointer]) ? 1 : 0;
            rightCount += vowels.contains(ch[rightPointer]) ? 1 : 0;
            leftPointer++;
            rightPointer--;
        }
        return leftCount == rightCount;
    }
}
