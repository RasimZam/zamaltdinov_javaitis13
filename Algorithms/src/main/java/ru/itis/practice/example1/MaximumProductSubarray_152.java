package ru.itis.practice.example1;

public class MaximumProductSubarray_152 {
    public static void main(String[] args) {
        int[] array = new int[]{2, 3, -2 , 4};
        System.out.println(maxProduct(array));
    }

    private static int maxProduct(int[] nums) {
        int maxSoFar = nums[0], minSoFar = nums[0], res = nums[0];
        for (int i = 0; i < nums.length; i++) {
            int tempMax = maxSoFar;
            maxSoFar = Math.max(nums[i], Math.max(maxSoFar * nums[i], minSoFar * nums[i]));
            minSoFar = Math.min(nums[i], Math.min(maxSoFar * nums[i], minSoFar * nums[i]));
            res = Math.max(res, maxSoFar);
        }
        return  res;
    }
}
