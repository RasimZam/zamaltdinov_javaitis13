package ru.itis.practice.example1;

import java.util.HashMap;
import java.util.Map;

public class Get_Frequency_16_2 {

    public int getFrequency(String[] book, String word) {
        word = word.toLowerCase().trim();
        int count = 0;
        for (String w: book) {
            if (w.toLowerCase().trim().equals(word)) {
                count++;
            }
        }
        return count;
    }

    public Map<String, Integer> setupDictionary(String[] book) {
        Map<String, Integer> table = new HashMap<>();
        for (String word : book) {
            word = word.toLowerCase();
            if (word.trim() != "") {
                if (!table.containsKey(word)) {
                    table.put(word, 0);
                }
                table.put(word, table.get(word) + 1);
            }
        }
        return table;
    }

    public int getFrequency1(Map<String, Integer> table, String word) {
        if (table == null || word == null) {
            return -1;
        }
        word = word.toLowerCase();
        if (table.containsKey(word)) {
            return table.get(word);
        }
        return 0;
    }
}
