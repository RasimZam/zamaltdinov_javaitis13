package ru.itis.practice.example1;

import java.util.HashSet;

// 653. Two Sum IV - Input is a BST

public class TwoSumIV_InputIsaBST_653 {
    public static void main(String[] args) {

    }

    public static boolean findTarget(TreeNode root, int k) {
        HashSet<Integer> set = new HashSet<>();
        return findTarget(root, k, set);
    }

    public static boolean findTarget(TreeNode root, int k, HashSet<Integer> set) {
        if (root == null) {
            return false;
        }
        if (set.contains(k - root.val)) {
            return true;
        }
        set.add(root.val);
        return findTarget(root.left, k, set) || findTarget(root.right, k, set);
    }
}
