package ru.itis.practice.example1;

//168. Excel Sheet Column Title

public class ExcelSheetColumnTitle_168 {
    public static void main(String[] args) {
        System.out.println(convertToTitle(28));
    }

    private static String convertToTitle(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("Input is not valid");
        }

        StringBuilder sb = new StringBuilder();
        while (n > 0) {
            n--;
            char ch = (char) (n % 26 + 'A');
            n /= 26;
            sb.append(ch);
        }
        return sb.reverse().toString();
    }
}
