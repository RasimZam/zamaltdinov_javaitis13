package ru.itis.practice.example1;

// 844. Backspace String Compare

public class Backspace_String_Compare_844 {
    public static void main(String[] args) {
        String str1 = "ab#c";
        String str2 = "ad#c";
        System.out.println(backspaceCompareUsingPointer(str1, str2));
    }

    public static boolean backspaceCompareUsingPointer(String firstStr, String secondStr) {
        int firstIdx = firstStr.length() - 1;
        int secondIdx = secondStr.length() - 1;

        while (firstIdx >= 0 || secondIdx >= 0) {

            firstIdx = backspaceOperationWithPointer(firstStr, firstIdx);
            secondIdx = backspaceOperationWithPointer(secondStr, secondIdx);

            if (firstIdx >= 0 && secondIdx >= 0
                    && firstStr.charAt(firstIdx) != secondStr.charAt(secondIdx)) {
                return false;
            }

            if ((firstIdx >= 0) != (secondIdx >= 0)) {
                return false;
            }
            firstIdx--;
            secondIdx--;
        }
        return true;
    }

    public static int backspaceOperationWithPointer(String str, int idx) {
        int skip = 0;
        while (idx >= 0) {
            if (str.charAt(idx) == '#') {
                skip++;
                idx--;
            } else if (skip > 0) {
                skip--;
                idx--;
            } else {
                break;
            }
        }
        return idx;
    }
}
