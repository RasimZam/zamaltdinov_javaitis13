package ru.itis.practice.example1;

public class SearchInsertPosition_35 {
    public static void main(String[] args) {
        int[] nums = new int[]{1, 3, 5, 6};
        int target = 7;
        System.out.println(searchInsert(nums, target));
    }

    public static int searchInsert(int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1;

        while (left <= right) {
            int middleIndex = (left + right) / 2;
            if (nums[middleIndex] == target) {
                return middleIndex;
            } else if (nums[middleIndex] > target) {
                right = middleIndex - 1;
            } else {
                left = middleIndex + 1;
            }
        }
        return left;
    }
}
