package ru.itis.practice.example1;

public class MinimumSizeSubarraySum_209 {
    public static void main(String[] args) {
        int[] array = new int[]{2, 3, 1, 2, 4, 3};
        int target = 7;
        System.out.println(minSubarrayLen(target, array));
    }

    private static int minSubarrayLen(int target, int[] nums) {
        if (nums.length == 0) {
            return -1;
        }

        int result = Integer.MAX_VALUE;
        int left = 0;
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];

            while (sum >= target) {
                result = Math.min(result, i + 1 - left);
                sum -= nums[left];
                left++;
            }
        }
        return (result != Integer.MAX_VALUE) ? result : 0;
    }
}
