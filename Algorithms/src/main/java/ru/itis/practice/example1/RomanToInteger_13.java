package ru.itis.practice.example1;

import java.util.HashMap;
import java.util.Map;

public class RomanToInteger_13 {
    public static void main(String[] args) {
        Map<Character, Integer> map = new HashMap<>();
        map.put('I', 1);
        map.put('V', 5);
        map.put('X', 10);
        map.put('L', 50);
        map.put('C', 100);
        map.put('D', 500);
        map.put('M', 1000);

        System.out.println(romanToInt("LVIII", map));
    }

    public static int romanToInt(String s, Map<Character, Integer> map) {
        int result = 0;
        int prev = 0;
        for (int i = s.length() - 1; i >= 0 ; i--) {
            int current = map.get(s.charAt(i));
            if(current < prev) {
                result -= current;
            } else {
                result += current;
            }
            prev = current;
        }
        return result;
    }
}
