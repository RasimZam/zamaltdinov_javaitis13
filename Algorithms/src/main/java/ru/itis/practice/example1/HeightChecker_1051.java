package ru.itis.practice.example1;

import java.util.Arrays;

public class HeightChecker_1051 {
    public static void main(String[] args) {
        int[] heights = new int[]{1,1,4,2,1,3};
        System.out.println(heightChecker(heights));
    }

    public static int heightChecker(int[] heights) {
        int[] sortedArray = heights.clone();
        Arrays.sort(sortedArray);
        int counter = 0;

        for (int i = 0; i < heights.length; i++) {
            if (heights[i] != sortedArray[i]) {
                counter++;
            }
        }
        return counter;
    }
}
