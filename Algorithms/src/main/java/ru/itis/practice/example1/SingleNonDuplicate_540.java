package ru.itis.practice.example1;

public class SingleNonDuplicate_540 {
    public static void main(String[] args) {
        int[] nums = new int [] {1, 1, 2, 3, 3, 4, 4, 8, 8};
        System.out.println(singleNonDuplicate(nums));
    }

    private static int singleNonDuplicate(int[] nums) {
        int left = 0;
        int right = nums.length - 1;
        int midIndex;

        while(left < right) {
            midIndex = (left + right) / 2;
            if (midIndex % 2 == 1) {
                --midIndex;
            } if (nums[midIndex] == nums[midIndex + 1]) {
                left = midIndex + 2;
            } else {
                right = midIndex;
            }
        }
        return nums[left];
    }
}
