package ru.itis.practice.example1;

import java.util.HashMap;
import java.util.Map;

public class WordPattern_290 {
    public static void main(String[] args) {
//        String pattern = "abba";
//        String str = "dog cat cat dog";
//        System.out.println(wordPattern(pattern, str));
//        System.out.println("-------------");
//        String pattern1 = "abba";
//        String str1 = "dog cat cat fish";
//        System.out.println(wordPattern(pattern1, str1));
//        System.out.println("-------------");
//        String pattern2 = "aaaa";
//        String str2 = "dog cat cat dog";
//        System.out.println(wordPattern(pattern2, str2));
        System.out.println("-------------");
        String pattern3 = "abba";
        String str3 = "dog dog dog dog";
        System.out.println(wordPattern(pattern3, str3));
    }

    private static boolean wordPattern(String pattern, String str) {
        String[] words = str.split(" ");
        if (words.length != pattern.length()) {
            return false;
        }

        Map<Character, String> map = new HashMap();
        for (int i = 0; i < pattern.length(); i++) {
            char currentChar = pattern.charAt(i);
            if (map.containsKey(currentChar)) {
                if (!map.get(currentChar).equals(words[i])) {
                    return false;
                }
            } else {
                if (map.containsValue(words[i])) {
                    return false;
                }
                map.put(currentChar, words[i]);
            }
        }
        return true;
    }
}
