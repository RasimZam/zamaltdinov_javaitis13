package ru.itis.practice.example1;

public class Maximum69Number_1323 {
    public static void main(String[] args) {
        int number = 9669;
        int numberV2 = 9996;
        System.out.println(maximum69Number(number));
        System.out.println("_________________________");
        System.out.println(maximum69NumberV2(numberV2));
    }

    public static int maximum69Number(int num) {
        return Integer.parseInt(String.valueOf(num).replaceFirst("6", "9"));
    }

    public static int maximum69NumberV2(int num) {
        char[] chars = String.valueOf(num).toCharArray();

        for (int i = 0; i <chars.length; i++) {
            if (chars[i] == '6') {
                chars[i] = '9';
                break;
            }
        }
        return Integer.valueOf(String.valueOf(chars));
    }
}
