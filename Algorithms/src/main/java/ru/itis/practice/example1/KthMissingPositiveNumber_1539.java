package ru.itis.practice.example1;

// 1539. Kth Missing Positive Number

public class KthMissingPositiveNumber_1539 {
    public static void main(String[] args) {
        int[] arr = new int[]{2, 3, 4, 7, 11};
        int k = 5;
        System.out.println(findKthPositive(arr, k));
    }

    public static int findKthPositive(int[] arr, int k) {
        int left = 0;
        int right = arr.length;
        while (left < right) {
            int middle = (left + right) / 2;
            if (arr[middle] - middle - 1 >= k) {
                right = middle;
            } else {
                left = middle + 1;
            }
        }
        return left + k;
    }
}
