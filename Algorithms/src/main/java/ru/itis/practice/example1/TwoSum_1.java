package ru.itis.practice.example1;

// 1. Two Sum

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum_1 {
    public static void main(String[] args) {
        int[] arrays = new int[]{2, 7, 11, 15};
        int target = 9;

        System.out.println(Arrays.toString(twoSum(arrays, target)));

    }

    public static int[] twoSum(int[] arrays, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < arrays.length; i++) {
            map.put(arrays[i], i);
        }

        for (int i = 0; i < arrays.length; i++) {
            int requiredNumber = target - arrays[i];

            if (map.containsKey(requiredNumber) && map.get(requiredNumber) != i) {
                return new int[]{i, map.get(requiredNumber)};
            }
        }
        throw new IllegalArgumentException("No solution for defined input data");
    }
}
