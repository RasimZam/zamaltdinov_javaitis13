package ru.itis.practice.example1;

public class ValidPerfectSquare_367 {
    public static void main(String[] args) {
        System.out.println(isPerfectSquare(16));
        System.out.println(isPerfectSquare(14));
    }

    private static boolean isPerfectSquare(int num) {
        int left = 1;
        int right = num;
        while (left <= right) {
            long mid = (left + right) / 2;
            long squaredMid = mid * mid;

            if (squaredMid == num) {
                return true;
            } else if (squaredMid > num) {
                right = (int) mid - 1;
            } else {
                left = (int) mid + 1;
            }
        }
        return false;
    }
}
