package ru.itis.practice.example1;

import java.util.Arrays;

public class ReplaceElementsWithGreatestElementOnRightSide_1299 {
    public static void main(String[] args) {
        int[] arr = new int[]{17, 18, 5, 4, 6, 1};
        System.out.println(Arrays.toString(replaceElements(arr)));
    }

    private static int[] replaceElements(int[] arr) {
        int maxOfRight = -1;
        for (int i = arr.length - 1; i >= 0 ; --i) {
            int a = arr[i];
            arr[i] = maxOfRight;
            maxOfRight = Math.max(maxOfRight, a);
        }
        return arr;
    }
}
