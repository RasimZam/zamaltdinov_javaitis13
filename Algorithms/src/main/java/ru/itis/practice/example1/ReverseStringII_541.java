package ru.itis.practice.example1;

public class ReverseStringII_541 {
    public static void main(String[] args) {
        String str = "abcdefg";
        System.out.println(reverseStr(str, 2));
    }

    public static String reverseStr(String s, int k) {
        char[] array = s.toCharArray();
        for (int i = 0; i < s.length(); i = i + (2 * k)) {
            int start = i;
            int end = Math.min(i + k, s.length()) - 1;
            char temp;
            while (start < end) {
                temp = array[start];
                array[start] = array[end];
                array[end] = temp;
                start++;
                end--;
            }
        }
        return new String(array);
    }
}
