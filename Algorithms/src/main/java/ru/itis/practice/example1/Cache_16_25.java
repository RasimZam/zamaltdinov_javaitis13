package ru.itis.practice.example1;

import javax.swing.*;
import java.util.HashMap;
import java.util.LinkedList;

public class Cache_16_25 {
    private int maxCacheSize;
    private HashMap<Integer, LinkedListNode> map = new HashMap<>();
    private LinkedListNode listHead = null;
    private LinkedListNode listTail = null;

    public Cache_16_25(int maxCacheSize) {
        this.maxCacheSize = maxCacheSize;
    }

    /* Получение значения ключа и пометка недавнего использования */
    public String getValue(int key) {
        LinkedListNode item = map.get(key);
        if (item == null) {
            return null;
        }

        /* Перемещение в начало списка */
        if (item != listHead) {
            removeFromLinkedList(item);
            insertAtFrontOfLinkedList(item);
        }
        return item.value;
    }

    /* Удаление узла из списка */
    private void removeFromLinkedList(LinkedListNode node) {
        if (node == null) {
            return;
        }
        if (node.prev != null) {
            node.prev.next = node.next;
        }
        if (node.next != null) {
            node.next.prev = node.prev;
        }
        if (node == listTail) {
            listTail = node.prev;
        }
        if (node == listHead) {
            listHead = node.next;
        }
    }

    /* Вставка узла в начало связного списка */
    private void insertAtFrontOfLinkedList(LinkedListNode node) {
        if (listHead == null) {
            listHead = node;
            listTail = node;
        } else {
            listHead.prev = node;
            node.next = listHead;
            listHead = node;
        }
    }

    /* Удаление пары ключ/значение из кеша */
    public boolean removeKey(int key) {
        LinkedListNode node = map.get(key);
        removeFromLinkedList(node);
        map.remove(key);
        return true;
    }

    /* Занесение пары ключ/значение в кеш (с удалением старого значение, если потребуется)*/
    public void setKeyValue(int key, String value) {
        /* Удалить, если уже присутствует*/
        removeKey(key);

        /* Если кеш заполнен, удалить самый старый элемент*/
        if (map.size() >= maxCacheSize && listTail != null) {
            removeKey(listTail.key);
        }

        /* Вставка новой ноды*/
        LinkedListNode node = new LinkedListNode(key, value);
        insertAtFrontOfLinkedList(node);
        map.put(key, node);
    }

    private static class LinkedListNode {
        private LinkedListNode next, prev;
        public int key;
        public String value;

        public LinkedListNode(int k, String v) {
            this.key = k;
            this.value = v;
        }
    }
}
