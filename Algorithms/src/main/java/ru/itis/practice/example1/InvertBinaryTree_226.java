package ru.itis.practice.example1;

import java.util.LinkedList;
import java.util.Queue;

public class InvertBinaryTree_226 {
    private TreeNode root;

    private static class TreeNode {
        private TreeNode left;
        private TreeNode right;
        private int data;

        public TreeNode(int data) {
            this.data = data;
        }
    }

    public TreeNode createBinaryTree() {
        TreeNode first = new TreeNode(4);
        TreeNode second = new TreeNode(2);
        TreeNode third = new TreeNode(1);
        TreeNode fourth = new TreeNode(3);
        TreeNode fifth = new TreeNode(7);
        TreeNode sixth = new TreeNode(6);
        TreeNode seventh = new TreeNode(9);
        root = first;
        first.left = second;
        first.right = fifth;
        second.left = third;
        second.right = fourth;
        fifth.left = sixth;
        fifth.right = seventh;
        return root;
    }
    public static void main(String[] args) {
        InvertBinaryTree_226 bsTree = new InvertBinaryTree_226();
        TreeNode binaryTree = bsTree.createBinaryTree();
        invertTree(binaryTree);
    }

    public static TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return root;
        }
        TreeNode left = invertTree(root.left);
        TreeNode right = invertTree(root.right);
        // swap
        root.right = left;
        root.left = right;
        return root;
    }

    // time O(n)
    // space O(n)
    public static TreeNode invertTree1(TreeNode root) {
        if(root == null) {
            return null;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        while(!queue.isEmpty()) {
            TreeNode node = queue.poll();

            TreeNode temp = node.left;
            node.left = node.right;
            node.right = temp;

            if(node.left != null) queue.add(node.left);
            if(node.right != null) queue.add(node.right);
        }
        return root;
    }
}
