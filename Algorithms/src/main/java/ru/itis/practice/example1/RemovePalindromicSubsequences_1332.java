package ru.itis.practice.example1;

public class RemovePalindromicSubsequences_1332 {
    public static void main(String[] args) {
        String str = "ababa";
        int result = removePalindromicSubsequences(str);
        System.out.println(result);

    }

    public static int removePalindromicSubsequences(String s) {
        if (s.isBlank()) {
            return 0;
        } else if(isPalindrome(s)) {
            return 1;
        }
        return 2;
    }

    private static boolean isPalindrome(String s) {
        int leftPointer = 0;
        int rightPointer = s.length() - 1;
        while (leftPointer < rightPointer) {
            if (s.charAt(leftPointer) != s.charAt(rightPointer)) {
                return false;
            }
            leftPointer++;
            rightPointer--;
        }
        return true;
    }
}
