package ru.itis.practice.example1;

public class LongestCommonPrefix_14 {
    public static void main(String[] args) {
        String[] strs = {"flower", "flow", "flight"};
        System.out.println(longestCommonPrefix(strs));
    }

    public static String longestCommonPrefix(String[] strs) {
        String common = "";
        if (strs.length == 0) {
            return common;
        }
        common = strs[0];
        for (int i = 1; i < strs.length; i++) {
            while (strs[i].indexOf(common) != 0) {
                common = common.substring(0, common.length() - 1);
            }
        }
        return common;
    }
}
