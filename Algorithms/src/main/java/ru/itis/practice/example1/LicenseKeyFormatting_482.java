package ru.itis.practice.example1;

public class LicenseKeyFormatting_482 {
    public static void main(String[] args) {
        String str = "5F3Z-2e-9-w";
        int k = 4;
        String resultString = licenseKeyFormatting(str, k);
        System.out.println(resultString);
    }

    public static String licenseKeyFormatting(String s, int k) {
        s = s.toUpperCase();
        s = s.replaceAll("-","");

        StringBuilder sb = new StringBuilder(s);

        for (int i = s.length() - k; i > 0 ; i = i - k) {
            sb.insert(i, "-");
        }
        return sb.toString();
    }
}
