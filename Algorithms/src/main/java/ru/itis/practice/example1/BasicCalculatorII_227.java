package ru.itis.practice.example1;

import java.util.Stack;

public class BasicCalculatorII_227 {
    public static void main(String[] args) {
        String str = "3+2*2";
        System.out.println(calculate(str));
        System.out.println("-------------------------");
        String str1 = "3/2";
        System.out.println(calculate(str1));
    }

    public static int calculate(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }

        Stack<Integer> st = new Stack<>();
        int current = 0;
        int sum = 0;
        char op = '+';
        char[] ch = s.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            if (Character.isDigit(ch[i])) {
                current = current * 10 + ch[i] - '0';
            }

            if (!Character.isDigit(ch[i]) && ch[i] != ' ' ||  i == ch.length - 1) {
                if (op == '+') {
                    st.push(current);
                } else if (op == '-') {
                    st.push(-current);
                } else if (op == '*') {
                    st.push(st.pop() * current);
                } else if (op == '/') {
                    st.push(st.pop() / current);
                }
                op = ch[i];
                current = 0;
            }
        }

        while (!st.isEmpty()) {
            sum += st.pop();
        }
        return sum;
    }
}
