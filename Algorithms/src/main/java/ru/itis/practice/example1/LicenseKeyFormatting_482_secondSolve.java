package ru.itis.practice.example1;

public class LicenseKeyFormatting_482_secondSolve {
    public static void main(String[] args) {
        String str = "5F3Z-2e-9-w";
        int k = 4;
        String resultString = licenseKeyFormatting(str, k);
        System.out.println(resultString);
    }

    public static String licenseKeyFormatting(String s, int k) {
        StringBuilder result = new StringBuilder();
        int count = 0;
        int i = s.length() - 1;
        while (i >= 0) {
            char current = Character.toUpperCase(s.charAt(i));
            if (current == '-') {
                i--;
            } else if (count != 0 && count % k == 0) {
                result.insert(0, '-');
                count = 0;
            } else {
                result.insert(0, current);
                count++;
                i--;
            }
        }
        return result.toString();
    }
}
