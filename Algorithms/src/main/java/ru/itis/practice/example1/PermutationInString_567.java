package ru.itis.practice.example1;

public class PermutationInString_567 {
    public static void main(String[] args) {
        String s1 = "ab", s2 = "eidbaooo";
        System.out.println(checkInclusion(s1, s2));
    }

    private static boolean checkInclusion(String s1, String s2) {
        int[] arr = new int[128];
        int leftPointer = 0;
        int rightPointer = 0;

        char[] s1Array = s1.toCharArray();
        char[] s2Array = s2.toCharArray();

        for (char current : s1Array) {
            arr[current]++;
        }

        int minLength = Integer.MAX_VALUE;
        int counter = 0;

        while (rightPointer < s2Array.length) {
            char current = s2Array[rightPointer];
            if (--arr[current] >= 0) {
                counter++;
            }

            while (counter == s1.length()) {
                int currentLength = rightPointer - leftPointer + 1;
                minLength = Math.min(currentLength, minLength);
                char leftChar = s2Array[leftPointer];
                if (++arr[leftChar] > 0) {
                    counter--;
                }
                leftPointer++;
            }
            rightPointer++;
        }
        return minLength == s1.length();
    }
}
