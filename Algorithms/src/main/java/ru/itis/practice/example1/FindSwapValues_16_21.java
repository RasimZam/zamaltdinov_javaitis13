package ru.itis.practice.example1;

import java.util.Arrays;

public class FindSwapValues_16_21 {
    public static void main(String[] args) {
        int[] array1 = {4, 1, 2, 1, 1, 2};
        int[] array2 = {3, 6, 3, 3};
        int[] result = findSwapValues1(array1, array2);
        System.out.println(Arrays.toString(result));
    }

    private static int[] findSwapValues(int[] array1, int[] array2) {
        int sum1 = sum(array1);
        int sum2 = sum(array2);

        for (int one : array1) {
            for (int two : array2) {
                int newSum1 = sum1 - one + two;
                int newSum2 = sum2 - two + one;
                if (newSum1 == newSum2) {
                    return new int[]{one, two};
                }
            }
        }
        return null;
    }

    private static int[] findSwapValues1(int[] array1, int[] array2) {
        Integer target = getTarget(array1, array2);
        if (target == null) {
            return null;
        }

        for (int one : array1) {
            for (int two : array2) {
                if (one - two == target) {
                    return new int[]{one, two};
                }
            }
        }

        return null;
    }

    private static Integer getTarget(int[] array1, int[] array2) {
        int sum1 = sum(array1);
        int sum2 = sum(array2);
        if ((sum1 - sum2) % 2 != 0) {
            return null;
        }
        return (sum1 - sum2) / 2;
    }

    private static int sum(int[] array) {
        return Arrays.stream(array)
                .sum();
    }
}
