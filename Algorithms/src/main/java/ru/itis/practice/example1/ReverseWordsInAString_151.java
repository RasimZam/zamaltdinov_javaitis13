package ru.itis.practice.example1;

import java.util.*;

public class ReverseWordsInAString_151 {
    public static void main(String[] args) {
        String s = "the sky is blue";
        System.out.println(reverseWords(s));
    }

    public static String reverseWords(String s) {
        StringTokenizer st = new StringTokenizer(s, " ");
        List<String> result = new ArrayList<>();

        while (st.hasMoreTokens()) {
            String curr = st.nextToken();
            result.add(curr);
        }
        Collections.reverse(result);
        String str = "";
        for (String curr: result) {
            str += curr + " ";
        }
        return str.trim();
    }

    public static String reverseWords1(String s) {
        String[] wordsArray = s.trim().split("\\s+");

        List<String> wordList = new ArrayList<>(Arrays.asList(wordsArray));
        Collections.reverse(wordList);
        String reversed = String.join(" ", wordList);
        return reversed;
    }

    public static String reverseWords2(String s) {
        String[] arr = s.trim().split("\\s+");
        int i=0,j=arr.length-1;
        while(i<j) {
            String t = arr[i];
            arr[i] = arr[j];
            arr[j] = t;
            i++;
            j--;
        }
        return String.join(" ", arr);
    }
}
