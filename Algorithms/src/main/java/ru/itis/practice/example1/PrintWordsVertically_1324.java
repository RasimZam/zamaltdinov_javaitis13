package ru.itis.practice.example1;

import java.util.ArrayList;
import java.util.List;

public class PrintWordsVertically_1324 {
    public static void main(String[] args) {
        String str = "HOW ARE YOU";
        System.out.println(printVertically(str));
        System.out.println("-------------------------");
        String str1 = "TO BE OR NOT TO BE";
        System.out.println(printVertically(str1));

    }

    public static List<String> printVertically(String s) {
        List<String> result = new ArrayList<>();
        String[] words = s.split(" ");
        int maxWordLength = getMaxLength(words);
        for (int i = 0; i < maxWordLength; i++) {
            StringBuilder str = new StringBuilder();
            for (String word: words) {
                if (i >= word.length()) {
                    str.append(" ");
                } else {
                    str.append(word.charAt(i));
                }
            }
            result.add(trimRight(str.toString()));
        }
        return result;
    }

    private static String trimRight(String word) {
        int i = word.length();
        while (--i > -1) {
            if (word.charAt(i) != ' ') {
                return word.substring(0, i + 1);
            }
        }
        return null;
    }

    private static int getMaxLength(String[] words) {
        int max = 0;
        for (String word: words) {
            max = Math.max(max, word.length());
        }
        return max;
    }
}
