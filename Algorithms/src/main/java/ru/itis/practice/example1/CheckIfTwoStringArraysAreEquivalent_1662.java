package ru.itis.practice.example1;

public class CheckIfTwoStringArraysAreEquivalent_1662 {
    public static void main(String[] args) {
        String[] str1 = {"ab", "c"};
        String[] str2 = {"a", "bc"};

        System.out.println(isEqualsWords(str1, str2));
    }
    private static boolean isEqualsWords(String[] str1, String[] str2) {
        StringBuilder sb1 = new StringBuilder("");
        StringBuilder sb2 = new StringBuilder("");
        for (String st:str1) {
            sb1.append(st);
        }
        for (String st:str2) {
            sb2.append(st);
        }
        return sb1.toString().equals(sb2.toString());
    }
}
