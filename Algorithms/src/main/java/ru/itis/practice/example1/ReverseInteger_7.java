package ru.itis.practice.example1;

public class ReverseInteger_7 {
    public static void main(String[] args) {
        int number = 123;
        System.out.println(reverseInteger(number));
    }

    public static int reverseInteger(int x) {
        long result = 0;

        while (x != 0) {
            result = result * 10 + x % 10;
            x /= 10;
        }
        return (result < Integer.MIN_VALUE || result > Integer.MAX_VALUE) ? 0
                : (int) result;
    }
}
