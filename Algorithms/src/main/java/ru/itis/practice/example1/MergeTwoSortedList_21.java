package ru.itis.practice.example1;

import ru.itis.model.ListNode1;

public class MergeTwoSortedList_21 {
    public static void main(String[] args) {

    }

    public ListNode1 mergeTwoLists(ListNode1 l1, ListNode1 l2) {
        ListNode1 tempNode = new ListNode1(0);
        ListNode1 currentNode = tempNode;

        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                currentNode.next = l1;
                l1 = l1.next;
            } else {
                currentNode.next = l2;
                l2 = l2.next;
            }
            currentNode = currentNode.next;
        }

        if (l1 != null) {
            currentNode.next = l1;
            l1 = l1.next;
        }

        if (l2 != null) {
            currentNode.next = l2;
            l2 = l2.next;
        }
        return tempNode.next;
    }
}
