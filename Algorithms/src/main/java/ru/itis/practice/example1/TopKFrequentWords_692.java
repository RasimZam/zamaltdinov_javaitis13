package ru.itis.practice.example1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TopKFrequentWords_692 {
    public static void main(String[] args) {
        String[] words = {"i", "love", "leetcode", "i", "love", "coding"};
        int k = 2;
        List<String> frequentWords = topKFrequent(words, k);
        System.out.println(frequentWords.toString());

    }

    public static List<String> topKFrequent(String[] words, int k) {
        Map<String, Element> map = new HashMap<>();
        List<Element> list = new ArrayList<>();

        for (int i = 0; i < words.length; i++) {
            Element el = map.get(words[i]);
            if (el == null) {
                el = new Element(words);
                map.put(words[i], el);
                list.add(el);
            }
            el.count++;
            el.index = i;
        }
        list.sort(null);
        List<String> result = new ArrayList<>();
        for(int i = 0; i < k; i++) {
            int index = list.get(i).index;
            result.add(words[index]);
        }
        return result;
    }

    static class Element implements Comparable<Element> {
        int count;
        int index;
        String[] words;

        public Element(String[] words) {
            this.words = words;
            this.index = -1;
            this.count = 0;
        }

        @Override
        public int compareTo(Element el) {
            if (this.count < el.count) {
                return 1;
            } else if (this.count > el.count) {
                return -1;
            } else {
                return words[index].compareTo(words[el.index]);
            }
        }
    }
}
