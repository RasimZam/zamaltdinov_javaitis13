package ru.itis.practice.example1;

public class ReverseString_344 {
    public static void main(String[] args) {
        char[] s = new char[]{'h','e','l','l','o'};
        int leftPointer = 0;
        int rightPointer = s.length - 1;

        while (leftPointer <= rightPointer) {
            char temp = s[leftPointer];
            s[leftPointer] = s[rightPointer];
            s[rightPointer] = temp;

            leftPointer++;
            rightPointer--;
        }
    }
}
