package ru.itis.practice.example1;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class SortCharactersByFrequency_451 {
    public static void main(String[] args) {
        String str = "tree";
        System.out.println(sortCharactersByFrequency(str));
    }

    public static String sortCharactersByFrequency(String s) {
        Map<Character, Integer> map = new HashMap<>();
        PriorityQueue<Character> pq = new PriorityQueue<>((x ,y) -> map.get(y) - map.get(x));

        for (char ch: s.toCharArray()) {
            map.put(ch, (map.getOrDefault(ch, 0)) + 1);
        }

        for (char ch: map.keySet()) {
            pq.offer(ch);
        }

        StringBuilder sb = new StringBuilder();
        while(!pq.isEmpty()) {
            char ch = pq.poll();

            for (int i = 0; i < map.get(ch); i++) {
                sb.append(ch);
            }
        }
        return sb.toString();
    }
}
