package ru.itis.practice.example1;

// 41. First Missing Positive

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class FirstMissingPositive_41 {
    public int firstMissingPositive(int[] nums) {
        Map<Integer, Boolean> map = new HashMap<>();
        for (int i = 1; i <= nums.length + 1; i++) {
            map.put(i, false);
        }

        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                map.put(nums[i], true);
            }
        }

        int result = Integer.MAX_VALUE;
        for (int key: map.keySet()) {
            if (!map.get(key)) {
                result = Math.min(result, key);
            }
        }
        return result;
    }

    public int firstMissingPositive1(int[] nums) {
        Map<Integer, Boolean> map = new LinkedHashMap<>();
        for (int i = 1; i <= nums.length + 1; i++) {
            map.put(i, false);
        }

        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                map.put(nums[i], true);
            }
        }
        for (int key: map.keySet()) {
            if (!map.get(key)) {
                return key;
            }
        }
        return -1;
    }

    public int firstMissingPositive2(int[] nums) {
        boolean[] map = new boolean[nums.length];
        for (int i = 0; i< nums.length; i++) {
            if (nums[i] >= 1 && nums[i] <= nums.length + 1) {
                map[nums[i] - 1] = true;
            }
        }

        for (int i = 0; i < nums.length + 1; i++) {
            if (!map[i]) {
                return i + 1;
            }
        }
        return -1;
    }

    // cycle sorting
    public void sort(int[] nums) {
        int i = 0;
        while (i < nums.length) {
            int correct_index = nums[i] - 1;
            if (i != correct_index) {
                swap(nums, i, correct_index);
            } else {
                i++;
            }
        }
    }

    public int firstMissingPositive3(int[] nums) {
        int i = 0;
        while (i < nums.length) {
            int correct_index = nums[i] - 1;
            if (i != correct_index  && nums[i] > 0
                    && nums[i] <= nums.length
                    && nums[i] != nums[correct_index]) {
                swap(nums, i, correct_index);
            } else {
                i++;
            }
        }
        for (i = 0; i < nums.length; i++) {
            int correct_index = nums[i] - 1;
            if (i != correct_index) {
                return i + 1;
            }
        }
        return nums.length + 1;
    }

    public void swap (int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }
}
