package ru.itis.practice.example1;

public class GetMaxSum_16_17 {
    public static void main(String[] args) {
        int[] array = {2, 3, -8, -1, 2, 4, -2, 3};
        int maxSum = getMaxSum(array);
        System.out.println(maxSum);
    }

    public static int getMaxSum(int[] array) {
        int maxSum = 0;
        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            sum += array[i];
            if (maxSum < sum) {
                maxSum = sum;
            } else if (sum < 0) {
                sum = 0;
            }
        }
        return maxSum;
    }
}
