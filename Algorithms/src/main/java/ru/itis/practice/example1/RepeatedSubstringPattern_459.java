package ru.itis.practice.example1;

public class RepeatedSubstringPattern_459 {
    public static void main(String[] args) {
        String str = "abcabc";
        System.out.println(repeatedSubstring(str));
    }

    public static boolean repeatedSubstring(String str) {
        int length = str.length();
        for (int i = length / 2; i >= 1; i--) {
            if (length % i == 0) {
                int numRepeats = length / i;
                String substring = str.substring(0, i);
                StringBuilder sb = new StringBuilder();
                for (int j = 0; j < numRepeats; j++) {
                    sb.append(substring);
                }
                if (sb.toString().equals(str)) {
                    return true;
                }
            }
        }
        return false;
    }
}
