package ru.itis.practice.example1;

import java.util.HashMap;
import java.util.Map;

public class SingleNumber_v2_136 {
    public static void main(String[] args) {
//        int[] nums = new int[]{2, 2, 1};
        int[] nums = new int[]{4, 1, 2, 1, 2};
        System.out.println(singleNumberv2(nums));
    }

    public static int singleNumberv2(int[] nums) {
        Map<Integer, Integer> num_frequencies = new HashMap<>();

        for (int num : nums) {
            if (!num_frequencies.containsKey(num)) {
                num_frequencies.put(num, 1);
            } else {
                num_frequencies.put(num, num_frequencies.get(num + 1));
            }

            for (Map.Entry<Integer, Integer> entry : num_frequencies.entrySet()) {
                int key = entry.getKey();
                int value = entry.getValue();

                if (value == 1) {
                    return key;
                }
            }
        }
        return -1;
    }
}
