package ru.itis.practice.example1;

public class RemoveAllAdjacentDuplicatesInString_1047 {
    public static void main(String[] args) {
        System.out.println(deleteDuplicates("abbaca"));
    }

    private static String deleteDuplicates(final String str) {
        StringBuilder sb = new StringBuilder();
        for (final char charElement : str.toCharArray()) {
            final int strLength = sb.length();
            if (strLength > 0 && sb.charAt(strLength - 1) == charElement) {
                sb.deleteCharAt(strLength - 1);
            } else {
                sb.append(charElement);
            }
        }
        return sb.toString();
    }
}
