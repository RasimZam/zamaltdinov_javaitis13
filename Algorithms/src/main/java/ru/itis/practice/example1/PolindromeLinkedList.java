package ru.itis.practice.example1;

// 234. Palindrome Linked List

public class PolindromeLinkedList {

    static class ListNode {
        int val;
        ListNode next;
        ListNode() {

        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public static void main(String[] args) {
        ListNode listNode = new ListNode(1, new ListNode(2));
        int i = 0;
    }

    private static boolean isPolindrome(ListNode head) {
        if (head == null) {
            return true;
        }

        ListNode secondHalf = getMiddle(head);
        ListNode secondHead = reverse(secondHalf);

        ListNode left = head;
        ListNode right = secondHead;
        while (right != null) {
            if (left.val != right.val) {
                reverse(secondHead);
                return false;
            }

            left = left.next;
            right = right.next;
        }
        reverse(secondHead);
        return true;
    }

    private static ListNode reverse(ListNode middle) {
        ListNode previous = null;
        ListNode current = middle;
        while (current != null) {
            ListNode temp = current.next;
            current.next = previous;
            previous = current;
            current = temp;
        }
        return previous;
    }
    private static ListNode getMiddle(ListNode head) {
        ListNode slow = head;
        ListNode fast = head.next;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow.next;
    }

}
