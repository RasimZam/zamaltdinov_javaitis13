package ru.itis.practice.example1;

import java.util.Arrays;

// 283. Move Zeroes
// Сдвиг нулей
public class MoveZeroes_283 {
    public static void main(String[] args) {
        int[] nums = new int[]{0, 1, 0, 3, 12};
        int size = nums.length;
        if (size < 2) {
            return;
        }

        int leftPointer = 0;
        int rightPointer = 1;

        while (rightPointer < size) {
            if (nums[leftPointer] != 0) {
                leftPointer++;
                rightPointer++;
            } else if (nums[rightPointer] == 0){
                rightPointer++;
            } else {
                int temp = nums[rightPointer];
                nums[rightPointer] = nums[leftPointer];
                nums[leftPointer] = temp;
            }
        }
        System.out.println(Arrays.toString(nums));
    }
}
