package ru.itis.practice.example1;

// 263. Ugly number

public class UglyNumber_263 {
    public static void main(String[] args) {
        System.out.println(isUgly(14));
    }

    public static boolean isUgly(int targetNumber) {
        if (targetNumber == 0) {
            return false;
        }

        for (final int prime : new int[]{2, 3, 5}) {
            while (targetNumber % prime == 0) {
                targetNumber /= prime;
            }
        }
        return targetNumber == 1;
    }
}
