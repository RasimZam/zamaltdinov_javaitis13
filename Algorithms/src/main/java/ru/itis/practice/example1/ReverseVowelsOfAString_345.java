package ru.itis.practice.example1;

public class ReverseVowelsOfAString_345 {
    public static void main(String[] args) {
        String str = "hello";
        System.out.println(reverseVowels(str));
    }

    public static String reverseVowels(String s) {
        int i = 0, j = s.length() - 1;
        char[] c = s.toCharArray();
        while (i < j) {
//            if i index is a vowel
            while (i < j && !isVowel(c[i])) {
                i++;
            }
            while (i < j && !isVowel(c[j])) {
                j--;
            }
            if (i >= j) {
                break;
            }
            swap(c, i, j);
            i++;
            j--;
        }
        return new String(c);
    }

    public static void swap(char[] chars, int i, int j) {
        char temp = chars[i];
        chars[i] = chars[j];
        chars[j] = temp;
    }

    public static boolean isVowel(char c) {
        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' ||
                c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U';
    }
}
