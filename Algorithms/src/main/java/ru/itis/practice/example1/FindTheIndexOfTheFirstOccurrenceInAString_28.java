package ru.itis.practice.example1;

public class FindTheIndexOfTheFirstOccurrenceInAString_28 {
    public static void main(String[] args) {
//        String haystack = "sadbutsad";
//        String needle = "sad";
//        System.out.println(strStr(haystack, needle));
//        System.out.println("------------------------------------");
        String haystack1 = "leetcode";
        String needle1 = "leeto";
        System.out.println(strStr(haystack1, needle1));
    }

    private static int strStr(String haystack, String needle) {
        if (haystack == null || needle == null || needle.length() > haystack.length()) {
            return -1;
        }
        if (needle.isEmpty()) {
            return 0;
        }
        for (int i = 0; i < haystack.length() - needle.length() + 1; i++) {
            if (haystack.charAt(i) == needle.charAt(0)) {
                if (haystack.substring(i, needle.length() + i).equals(needle)) {
                    return i;
                }
            }
        }
        return -1;
    }
}
