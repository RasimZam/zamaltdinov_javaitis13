package ru.itis.practice.example1;

public class ReverseWordsInAStringIII_557 {
    public static void main(String[] args) {
        String str = "Let's take LeetCode contest";
        System.out.println(reverseWords(str));
    }

    public static String reverseWords(String s) {
        if (s == null || s.length() == 0) {
            return s;
        }
        
        StringBuilder result = new StringBuilder();
        String[] splittedWords = s.split(" ");

        for (String word: splittedWords) {
            StringBuilder wordSb = new StringBuilder();
            wordSb.append(word);
            wordSb.reverse();
            result.append(wordSb.toString() + " ");
        }
        result.setLength(result.length() - 1);
        return result.toString();

    }
}
