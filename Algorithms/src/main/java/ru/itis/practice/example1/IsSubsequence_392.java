package ru.itis.practice.example1;

/*
392. isSubsequence
Является ли строка последовательностью
Подпоследовательностью является строка, полученная из исходной строки
путем удаления символов без изменения порядка следования символов.*/

public class IsSubsequence_392 {
    public static void main(String[] args) {
        boolean tmp = isSubsequence("abc", "ahbhdc");
        System.out.println(tmp);
        boolean tmp1 = isSubsequence("axc", "ahbhdc");
        System.out.println(tmp1);
    }

    public static boolean isSubsequence(String s, String t) {
        if(s.isEmpty()) {
            return true;
        }

        int sPointer = 0;
        int tPointer = 0;

        while (tPointer < t.length()) {
            if (t.charAt(tPointer) == s.charAt(sPointer)) {
                sPointer++;
                if (sPointer == s.length()) {
                    return true;
                }
            }
            tPointer++;
        }
        return false;
    }
}
