package ru.itis.practice.example1;

import java.util.Arrays;

public class ReshapeTheMatrix_566 {
    public static void main(String[] args) {
        int[][] srcArray = {{1, 2}, {3, 4}};
        int r = 1;
        int c = 4;
        int[][] array = matrixReshape(srcArray, r, c);
        Arrays.stream(array)
                .map(Arrays::toString)
                .forEach(System.out::println);
    }

    public static int[][] matrixReshape(int[][] mat, int r, int c) {
        int rows = mat.length;
        int columns = mat[0].length;

        if ((rows * columns) != (r * c)) {
            return mat;
        }
        int [][] outputArray = new int[r][c];
        int row_num = 0;
        int column_num = 0;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                outputArray[row_num][column_num] = mat[i][j];
                column_num++;
                if (column_num == c) {
                    column_num = 0;
                    row_num++;
                }
            }
        }
        return outputArray;
    }
}
