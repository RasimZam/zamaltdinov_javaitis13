package ru.itis.practice.example1;

public class FindUnsortedSequence_16_16 {
    public static void main(String[] args) {
        int[] array = {1, 2, 4, 7, 10, 11, 8, 12, 5, 6, 16, 18, 19};
        findUnsortedSequence(array);
    }

    public static void findUnsortedSequence(int[] array) {
        // нахождение левой подпоследовательности
        int endLeft = findEndOfLeftSubsequence(array);
        if (endLeft >= array.length - 1) {
            return; // уже отсортировано
        }
        // нахождение правой подпоследовательности
        int startRight = findEndOfRightSubsequence(array);
        // определение минимума и максимума
        int maxIndex = endLeft; // максимум левой стороны
        int minIndex = startRight; // минимум правой стороны
        for (int i = endLeft + 1; i < startRight; i++) {
            if (array[i] < array[minIndex]) {
                minIndex = i;
            }
            if (array[i] > array[maxIndex]) {
                maxIndex = i;
            }
        }
        // двигаться влево, пока не дойдем до array[minIndex]
        int leftIndex = shrinkLeft(array, minIndex, endLeft);

        // двигаться в право, пока не дойдем до array[maxIndex]
        int rightIndex = shrinkRight(array, maxIndex, startRight);

        System.out.println(leftIndex + " " + rightIndex);
    }

    private static int shrinkLeft(int[] array, int minIndex, int start) {
        int comp = array[minIndex];
        for (int i = start - 1; i >= 0; i--) {
            if (array[i] <= comp) return i + 1;
        }
        return 0;
    }

    private static int shrinkRight(int[] array, int maxIndex, int start) {
        int comp = array[maxIndex];
        for (int i = start; i < array.length; i++) {
            if (array[i] >= comp) return i - 1;
        }
        return array.length - 1;
    }

    private static int findEndOfRightSubsequence(int[] array) {
        for (int i = array.length - 2; i >= 0; i--) {
            if (array[i] > array[i + 1]) {
                return i + 1;
            }
        }
        return 0;
    }

    private static int findEndOfLeftSubsequence(int[] array) {
        for (int i = 1; i < array.length; i++) {
            if (array[i] < array[i - 1]) {
                return i - 1;
            }
        }
        return array.length - 1;
    }
}
