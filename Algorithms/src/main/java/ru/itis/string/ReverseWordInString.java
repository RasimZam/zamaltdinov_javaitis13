package ru.itis.string;

import java.util.ArrayList;
import java.util.Collections;

public class ReverseWordInString {
    public static void main(String[] args) {
        String word = "AlgoExpert is the best!";
        String result = reverseWord(word);
        System.out.println(result);

    }

    public static String reverseWord(String string) {
        ArrayList<String> words = new ArrayList<>();
        int startOfWord = 0;
        for (int i = 0; i < string.length(); i++) {
            char character = string.charAt(i);
            if (character == ' ') {
                words.add(string.substring(startOfWord, i));
                startOfWord = i;
            } else if (string.charAt(startOfWord) == ' ') {
                words.add(" ");
                startOfWord = i;
            }
        }
        words.add(string.substring(startOfWord));
        Collections.reverse(words);
        return String.join("", words);
    }
}
