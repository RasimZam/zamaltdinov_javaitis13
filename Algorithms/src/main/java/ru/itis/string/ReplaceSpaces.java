package ru.itis.string;

public class ReplaceSpaces {
    public static void main(String[] args) {
        String str = "Mr John Smith    ";
        replaceSpaces(str.toCharArray(), 13);
    }

    public static void replaceSpaces(char[] str, int length) {
        int spaceCount = 0;
        for (int i = length - 1; i >= 0; i--) {
            if (str[i] == ' ') {
                spaceCount++;
            }
        }
        int shift = spaceCount * 2;
        int newLength = length + shift;
        for (int i = newLength - 1; shift > 0; i--) {
            char c = str[i - shift];
            if (c != ' ') {
                str[i] = c;
                System.out.println(str);
            } else {
                str[i] = '0';
                str[--i] = '2';
                str[--i] = '%';
                shift -= 2;
                System.out.println(str);
            }
        }
    }
}
