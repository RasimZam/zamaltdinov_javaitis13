package ru.itis.string;

// 28. Find the Index of the First Occurrence in a String

public class FindTheIndexOfTheFirstOccurrenceInAString_28 {
    public static void main(String[] args) {
//        String haystack = "sadbutsad";
//        String needle = "sad";
        String haystack = "leetcode";
        String needle = "leeto";
        int result = strStr(haystack, needle);
        System.out.println(result);

    }

    public static int strStr(String haystack, String needle) {
        if (needle.isEmpty()) {
            return 0;
        }
        int haystackLength = haystack.length();
        int needleLength = needle.length();
        int haystackPointer = 0;
        int needlePointer = 0;

        while (haystackPointer < haystackLength) {
            if (haystack.charAt(haystackPointer) == needle.charAt(needlePointer)) {
                if(needleLength == 1) {
                    return haystackPointer;
                }
                haystackPointer++;
                needlePointer++;
            } else {
                haystackPointer -= needlePointer - 1;
                needlePointer = 0;
            }

            if (needlePointer == needleLength) {
                return haystackPointer - needlePointer;
            }
        }
        return  -1;
    }
}
