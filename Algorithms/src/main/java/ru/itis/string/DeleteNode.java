package ru.itis.string;

import ru.itis.model.ListNode;

public class DeleteNode {
    boolean deleteNode(ListNode node) {
        if (node == null || node.next == null) {
            return false;
        }
        ListNode next = node.next;
        node.val = next.val;
        node.next = next.next;
        return true;
    }
}
