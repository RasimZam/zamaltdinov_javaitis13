package ru.itis.string;

public class IsPermutationOfPalindrome {
    public static void main(String[] args) {
        String str = "Tact Coa";
        boolean result = isPermutationOfPalindrome(str);
        System.out.println(result);
    }

    public static boolean isPermutationOfPalindrome(String phrase) {
        int[] table = buildCharFrequencyTable(phrase);
        return checkMaxOneOdd(table);
    }

    private static boolean checkMaxOneOdd(int[] table) {
        boolean foundOdd = false;
        for (int count : table) {
            if (count % 2 == 1) {
                if (foundOdd) {
                    return false;
                }
                foundOdd = true;
            }
        }
        return true;
    }

    //    Подсчет количества вхождений каждого символа
    private static int[] buildCharFrequencyTable(String phrase) {
        int[] table = new int[Character.getNumericValue('z') - Character.getNumericValue('a') + 1];
        for (char value : phrase.toCharArray()) {
            int symbol = getCharNumber(value);
            if (symbol != -1) {
                table[symbol]++;
            }
        }
        return table;
    }

//    Каждый символ связывается с числом: а -> 0, Ь -> 1, с -> 2, и т.д.
//    (без учета регистра). Небуквенным символам соответствует -1.
    private static int getCharNumber(Character c) {
        int a = Character.getNumericValue('a');
        int z = Character.getNumericValue('z');
        int A = Character.getNumericValue('A');
        int Z = Character.getNumericValue('Z');
        int val = Character.getNumericValue(c);
        if (a <= val && val <= z) {
            return val - a;
        } else if (A <= val && val <= Z) {
            return val - A;
        }
        return -1;
    }
}
