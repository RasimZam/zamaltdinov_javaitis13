package ru.itis.string;

// 647. Palindromic Substrings

public class PalindromicSubstrings_647 {
    public static void main(String[] args) {
        String str = "abc";
        int result = countSubstrings(str);
        System.out.println(result);
    }

    public static int countSubstrings(String s) {
        int count = 0;
        for(int i = 0; i < s.length(); i++) {
            count += countPolindromes(s, i, i);
            count += countPolindromes(s, i, i + 1);
        }
        return count;
    }

    private static int countPolindromes(String s, int left, int right) {
        int count = 0;
        while(left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
         count++;
         left--;
         right++;
        }
        return count;
    }
}
