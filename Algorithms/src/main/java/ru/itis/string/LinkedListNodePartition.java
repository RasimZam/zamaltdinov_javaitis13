package ru.itis.string;

import ru.itis.model.ListNode;

public class LinkedListNodePartition {
    ListNode partition(ListNode node, int x) {
        ListNode beforeStart = null;
        ListNode beforeEnd = null;
        ListNode afterStart = null;
        ListNode afterEnd = null;

        // 3 - 5 - 8 - 5 - 10 - 2 - 1
        while (node != null) {
            // 5
            // 8
            // 5
            // 10
            // 2
            // 1
            // null
            ListNode next = node.next;
            // 3 - null
            // 5 - null
            // 8 - null
            // 5 - null
            // 10 - null
            // 2 - null
            // 1 - null
            node.next = null;
            if (node.val < x) {
                //Узел вставляется в конец списка before
                if (beforeStart == null) {
                    // beforeStart = 3
                    beforeStart = node;
                    beforeEnd = beforeStart;
                } else {
                    // 3 - 2
                    beforeEnd.next = node;
                    beforeEnd = node;
                }
            } else {
                // Узел вставляется в конец списка after
                if (afterStart == null) {
                    // afterStart = 5
                    afterStart = node;
                    afterEnd = afterStart;
                } else {
                    // 5 - 8 - 5 - 10
                    afterEnd.next = node;
                    afterEnd = node;
                }
            }
            // 5
            // 8
            // 5
            // 10
            // 2
            // 1
            node = next;
        }
        if (beforeStart == null) {
            return afterStart;
        }
        // Слияние списков before и start
        beforeEnd.next = afterStart;
        return beforeStart;
    }

    ListNode partition1(ListNode node, int x) {
        ListNode head = null;
        ListNode tail = null;

        // 3 - 5 - 8 - 5 - 10 - 2 - 1
        while (node != null) {
            ListNode next = node.next;
            if (node.val < x) {
                // вставить узел в начало
                node.next = head;
                head = node;
            } else {
                // вставить узел в конец
                tail.next = node;
                tail = node;
            }
            node = next;
        }
        tail.next = null;
        return head;
    }
}
