package ru.itis.intervals;

// 435. Non-overlapping Intervals

import java.util.Arrays;
import java.util.Comparator;

public class NonOverlappingIntervals_435 {
    public static void main(String[] args) {
        int[][] intervals = {{1, 2}, {2, 3}, {3, 4}, {1, 3}};
        int result = eraseOverLapIntervals(intervals);
        System.out.println(result);
    }

    public static int eraseOverLapIntervals(int[][] intervals) {
        if (intervals.length == 0) {
            return 0;
        }

        Arrays.sort(intervals, Comparator.comparingInt(value -> value[1]));
        int count = 1;
        int previousInterval = 0;
        for (int i = 1; i < intervals.length; i++) {
            if (intervals[i][0] >= intervals[previousInterval][1]) {
                previousInterval = i;
                count++;
            }
        }
        return intervals.length - count;
    }
}
