package ru.itis.streamapi;

import java.util.List;
import java.util.function.BinaryOperator;

public class Main7 {
    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7, 8);
        BinaryOperator<Integer> bop = (a, b) -> a + b;

        Integer sum = list.stream()
                .filter(a -> a % 2 == 0)
                .reduce(bop)
                .get();

        System.out.println(sum);
    }
}
