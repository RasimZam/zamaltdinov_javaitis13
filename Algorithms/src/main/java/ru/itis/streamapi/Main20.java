package ru.itis.streamapi;

import java.util.Date;
import java.util.Spliterator;
import java.util.function.Consumer;

public class Main20 {
    public static void main(String[] args) {
        Date date = new Date();

        MessageRecord record1 = new MessageRecord(1, date, "Hello");
        MessageRecord record2 = new MessageRecord(2, date, "world");
        MessageRecord record3 = new MessageRecord(3, date, "Java the best");
        MessageRecord record4 = new MessageRecord(4, date, "Cat");
        MessageRecord record5 = new MessageRecord(4, date, "Hello Jonn");
        MessageRecord record6 = new MessageRecord(3, date, "Bae Katty");
        MessageRecord record7 = new MessageRecord(5, date, "I choose java");
        MessageRecord record8 = new MessageRecord(1, date, "Good luck");

        Messages messages = new Messages();

        messages.addMessageRecord(record1);
        messages.addMessageRecord(record2);
        messages.addMessageRecord(record3);
        messages.addMessageRecord(record4);
        messages.addMessageRecord(record5);
        messages.addMessageRecord(record6);
        messages.addMessageRecord(record7);
        messages.addMessageRecord(record8);

        Consumer<MessageRecord> action = System.out::println;
        Spliterator<MessageRecord> split = messages.getSpliterator();

        for (;split.tryAdvance(action);) {
            System.out.println(split.estimateSize());
        }
    }
}
