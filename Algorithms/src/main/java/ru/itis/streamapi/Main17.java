package ru.itis.streamapi;

import java.util.List;

public class Main17 {
    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3, 4 , 5);
        Integer result = list.parallelStream()
                .reduce(0, (a, b) -> a + b);
        System.out.println(result);
    }
}
