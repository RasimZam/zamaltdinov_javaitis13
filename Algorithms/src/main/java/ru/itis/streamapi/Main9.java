package ru.itis.streamapi;

import java.util.List;

public class Main9 {
    public static void main(String[] args) {
        List<String> list = List.of("world", "!");
        String result = list.stream().reduce("Hello", (a ,b) -> a + " " + b);
        System.out.println(result);
    }
}
