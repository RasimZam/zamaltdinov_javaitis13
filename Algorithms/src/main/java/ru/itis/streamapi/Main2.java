package ru.itis.streamapi;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Main2 {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Luska", 5, "White");
        Cat cat2 = new Cat("Umka", 7, "Black");
        Cat cat3 = new Cat("Barsik", 2, "Red");
        Cat cat4 = new Cat("Kuzia", 3, "Grey");

        Cat[] cats = new Cat[]{cat1, null, cat2, cat3, cat4};

        List<String> result = Arrays.stream(cats)
                .filter(Objects::nonNull)
                .filter(n -> n.getWeigth() < 5)
                .map(Cat::getName)
                .collect(Collectors.toList());

        System.out.println(result);
    }
}
