package ru.itis.streamapi;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main14 {
    public static void main(String[] args) {
        List<Integer> listNumber = List.of(1, 2, 3, 4, 5);
        Map<Integer, String> result = listNumber.stream()
                .collect(Collectors.toMap(Function.identity(), a -> (a % 2 == 0) ? "even" : "odd"));
        System.out.println(result);
    }
}
