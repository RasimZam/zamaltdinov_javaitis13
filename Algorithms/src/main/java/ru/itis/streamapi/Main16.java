package ru.itis.streamapi;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main16 {
    public static void main(String[] args) {
        List<Integer> listNumber = List.of(1, 2, 3, 4, 5);
        Map<String, List<Integer>> result = listNumber.stream()
                .collect(Collectors.groupingBy(a -> (a % 2 == 0) ? "even" : "odd"));
        System.out.println(result);
    }
}
