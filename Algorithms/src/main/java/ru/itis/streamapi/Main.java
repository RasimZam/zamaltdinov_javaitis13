package ru.itis.streamapi;

import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = List.of(0, 5, -2, 1, -4, 7);
        List<Integer> resultList = list.stream()
                .filter(n -> n > 0)
                .collect(Collectors.toList());
        System.out.println(resultList);
        System.out.println();

        List<Integer> resultList1 = list.stream()
                .filter(n -> n > 0)
                .filter(n -> n % 3 == 0)
                .collect(Collectors.toList());
        System.out.println(resultList1);


    }
}
