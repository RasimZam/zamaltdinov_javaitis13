package ru.itis.streamapi;

public class Cat {
    private String name;
    private int weigth;
    private String color;

    public Cat(String name, int weigth, String color) {
        this.name = name;
        this.weigth = weigth;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeigth() {
        return weigth;
    }

    public void setWeigth(int weigth) {
        this.weigth = weigth;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", age=" + weigth +
                ", color='" + color + '\'' +
                '}';
    }
}
