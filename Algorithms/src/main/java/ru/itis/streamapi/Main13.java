package ru.itis.streamapi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main13 {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        Map<String, List<Integer>> result = numbers.stream()
                .collect(Main13::createMap, Main13::addToMap, Main13::mergeMap);
        System.out.println(result);
    }

    private static void mergeMap(Map<String, List<Integer>> map1, Map<String, List<Integer>> map2) {
        map2.forEach((k, v) -> map1.get(k).addAll(v));
    }

    private static void addToMap(Map<String, List<Integer>> map, Integer element ) {
        if (element % 2 == 0) {
            map.get("even").add(element);
        } else {
            map.get("odd").add(element);
        }
    }

    private static Map<String, List<Integer>> createMap() {
        Map<String, List<Integer>> map = new HashMap<>();
        map.put("even", new ArrayList<>());
        map.put("odd", new ArrayList<>());
        return map;
    }


}
