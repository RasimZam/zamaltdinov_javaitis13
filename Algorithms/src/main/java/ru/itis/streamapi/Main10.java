package ru.itis.streamapi;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class Main10 {
    public static void main(String[] args) {
        List<Goods> list = List.of(new Goods("Apple", 50),
                new Goods("Orange", 70),
                new Goods("Pear", 65),
                new Goods("Cherry", 75));

        BiFunction<Integer, Goods, Integer> biFunc = (a, b) -> a + b.getPrice();
        BinaryOperator<Integer> biOp = (a, b) -> a + b;

        Integer totalSum = list.stream().filter(a -> a.getPrice() > 65)
                .reduce(0, biFunc, biOp);

        System.out.println(totalSum);
    }
}
