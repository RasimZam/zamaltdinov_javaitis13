package ru.itis.streamapi;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class Main18 {
    public static void main(String[] args) {
        String[] languages = new String[]{"Java", "Fortran", "Python"};
        String start = "languages = ";
        BinaryOperator<String> combiner = (a, b) -> {
            if (b.startsWith(start)) {
                return a + b.substring(start.length());
            }
            return a + b;
        };

        BiFunction<String, String, String> reduceFunction = (a, b) -> a + " " + b;
        String result = Arrays.stream(languages).parallel().reduce(start, reduceFunction, combiner);
        System.out.println(result);
    }
}
