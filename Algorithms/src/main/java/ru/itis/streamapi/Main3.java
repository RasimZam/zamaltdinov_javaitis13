package ru.itis.streamapi;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main3 {
    public static void main(String[] args) {
        Singer singer1 = new Singer("Freddie", new String[]{"We are champions", "Somebody to love"});
        Singer singer2 = new Singer("David Bowie", new String[]{"Space Oddity", "Let me Sleep Beside You",
        "Suffragette City"});
        Singer singer3 = new Singer("James Paul McCartney", new String[]{"Can't Buy Me Love", "Another Girl"});

        Singer[] rocksStars = new Singer[]{singer1, singer2, singer3};

        List<String> song = Arrays.stream(rocksStars)
                .flatMap(n -> Arrays.stream(n.getSongs()))
                .collect(Collectors.toList());
        System.out.println(song);
    }
}
