package ru.itis.streamapi;

import java.util.List;
import java.util.function.BinaryOperator;

public class Main8 {
    public static void main(String[] args) {
        List<String> worlds = List.of("Java", "Fortran", "Python", "C++");
        BinaryOperator<String> bop = (a, b) -> a.length() > b.length() ? a : b;
        String result = worlds.stream().reduce(bop).get();

        System.out.println(result);
    }
}
