package ru.itis.linkedlist;

// 2181. Merge Nodes in Between Zeros

import ru.itis.model.ListNode;

public class MergeNodesInBetweenZeros_2181 {

    // dummy -> 0 -> 3 -> 1 -> 0 -> 4 -> 5 -> 2 -> 0
    public ListNode mergeNodes(ListNode head) {
        ListNode dummy = new ListNode(Integer.MAX_VALUE), prev = dummy;
        while (head != null && head.next != null) {
            // 0
            prev.next = head;
            // 1
            head = head.next;
            while (head != null && head.val != 0) {
                // 0 + 3 + 1
                prev.next.val += head.val;
                head = head.next;
            }
            prev = prev.next;
        }
        prev.next = null;
        return dummy.next;
    }
}
