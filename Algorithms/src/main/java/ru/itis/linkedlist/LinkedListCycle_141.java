package ru.itis.linkedlist;

// 141. Linked List Cycle

import ru.itis.model.ListNode;

public class LinkedListCycle_141 {
    public static void main(String[] args) {

    }

    public boolean hasCycle(ListNode head) {
        ListNode slowPtr = head;
        ListNode fastPtr = head;

        while (slowPtr != null && fastPtr != null && fastPtr.next != null) {
            slowPtr = slowPtr.next;
            fastPtr = fastPtr.next.next;

            if (slowPtr == fastPtr) {
                return true;
            }
        }
        return false;
    }
}
