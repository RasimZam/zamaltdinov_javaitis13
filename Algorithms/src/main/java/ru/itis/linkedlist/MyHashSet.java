package ru.itis.linkedlist;

import java.util.LinkedList;
import java.util.List;

// 705. Design HashSet

public class MyHashSet {
    private int numBuckets = 1500;
    private List<Integer>[] buckets;

    private int hash(int key) {
        return key % numBuckets;
    }

    public MyHashSet() {
        buckets = new LinkedList[numBuckets];
    }

    public void add(int key) {
        int i = hash(key);
        if (buckets[i] == null) {
            buckets[i] = new LinkedList<>();
        }

        if (buckets[i].indexOf(key) == -1) {
            buckets[i].add(key);
        }
    }

    public void remove(int key) {
        int i = hash(key);
        if (buckets[i] == null) {
            return;
        }

        if (buckets[i].indexOf(key) != -1) {
            buckets[i].remove(buckets[i].indexOf(key));
        }
    }

    public boolean contains(int key) {
        int i = hash(key);
        if (buckets[i] == null || buckets[i].indexOf(key) == -1) {
            return false;
        }
        return true;
    }
}
