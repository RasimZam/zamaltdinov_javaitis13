package ru.itis.linkedlist;

// 203. Remove Linked List Elements

import ru.itis.model.ListNode;

public class RemoveLinkedListElements_203 {
    public static void main(String[] args) {

    }

    public ListNode removeElements(ListNode head, int val) {
        // Create a dummy head node
        ListNode dummy = new ListNode(-1);
        dummy.next = head;
        ListNode current = dummy;

        while (current.next != null) {

            // check if the value matches
            if (current.next.val == val) {
                current.next = current.next.next;
            } else {
                current = current.next;
            }
        }

        // return the head
        return dummy.next;
    }

    public ListNode removeElements1(ListNode head, int val) {
        if (head == null) return null;

        head.next = removeElements1(head.next, val);
        return head.val == val ? head.next : head;
    }
}
