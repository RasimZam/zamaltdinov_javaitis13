package ru.itis.linkedlist;

// 876. Middle of the Linked List
// O(n) time complexity
// O(1) memory complexity

import ru.itis.model.ListNode;

public class MiddleOfTheLinkedList_876 {
    public static void main(String[] args) {

    }

    public static ListNode middleNode(ListNode head) {
        ListNode slow = head;
        ListNode fast = head;

        while(fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }
}
