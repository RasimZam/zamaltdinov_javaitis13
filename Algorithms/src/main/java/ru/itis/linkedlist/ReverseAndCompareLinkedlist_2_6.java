package ru.itis.linkedlist;

import ru.itis.model.ListNode;

import java.util.Stack;

public class ReverseAndCompareLinkedlist_2_6 {
    boolean isPolindrome(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;
        Stack<Integer> stack = new Stack<>();

       /* Элементы первой половины связного списка помещаются в стек. Когда быстрый
       указатель (со скоростью 2х) достигает конца связного списка, медленный находится в середине
       */

        while(fast != null && fast.next != null) {
           stack.push(slow.val);
           slow = slow.next;
           fast = fast.next.next;
        }

        /*Нечетное количество элементов, средний элемент пропускается*/
        if (fast != null) {
            slow = slow.next;
        }

        while (slow != null) {
            int top = stack.pop().intValue();
            /*Если значения не совподают, это не палндром*/
            if (top != slow.val) {
                return false;
            }
            slow = slow.next;
        }
        return true;
    }

    boolean isPolindrome1(ListNode head) {
        if (head == null) {
            return true;
        }

        ListNode fast = head;
        ListNode slow = head;

        while(fast.next != null && fast.next.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }

        ListNode secondHalfHead = reverse(slow.next);
        ListNode firstHalfHead = head;

        while (firstHalfHead.next != null && secondHalfHead != null) {
            if (firstHalfHead.val != secondHalfHead.val) {
                return false;
            }
            secondHalfHead = secondHalfHead.next;
            firstHalfHead = firstHalfHead.next;
        }
        return true;
    }

    private ListNode reverse(ListNode head) {
        ListNode newNode = null;
        while (head.next != null) {
            ListNode next = head.next;
            head.next = newNode;
            newNode = head;
            head = next;
        }
        return newNode;
    }
}
