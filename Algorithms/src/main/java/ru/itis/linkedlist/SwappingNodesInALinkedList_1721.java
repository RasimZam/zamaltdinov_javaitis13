package ru.itis.linkedlist;

// 1721. Swapping Nodes in a Linked List
// O(n) time complexity
// O(1) memory complexity

import ru.itis.model.ListNode;

public class SwappingNodesInALinkedList_1721 {
    public static void main(String[] args) {

    }

    public static ListNode swapNodes(ListNode head, int k) {
        ListNode kthFromStart = head;

         while(--k > 0) {
             kthFromStart = kthFromStart.next;
         }

         ListNode kthFromEnd = head;
         ListNode current = kthFromStart;

         while(current.next != null) {
             current = current.next;
             kthFromEnd = kthFromEnd.next;
         }

         int temp = kthFromStart.val;
         kthFromStart.val = kthFromEnd.val;
         kthFromEnd.val = temp;

         return head;
    }
}
