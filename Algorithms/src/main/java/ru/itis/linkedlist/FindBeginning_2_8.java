package ru.itis.linkedlist;


// Task from book 2.8

import ru.itis.model.ListNode;

public class FindBeginning_2_8 {
    ListNode findBeginning(ListNode head) {
        ListNode slow = head;
        ListNode fast = head;

        /*Поиск точки встречи (РАЗМЕР_ПЕТЛИ - k шагов в связном списке)*/
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if (slow == fast) {
                break;
            }
        }

        /*Проверка ошибок - точки встречи нет, а значит, нет и петли*/
        if (fast == null || fast.next == null) {
            return null;
        }

        /*Slow  перемещается в начало, fast остается в точке встречи. Если
        * они будут двигаться в одном темпе, то встретятся в начале петли */
        slow = head;
        while (slow != fast) {
            slow = slow.next;
            fast = fast.next;
        }
        return fast;
    }
}
