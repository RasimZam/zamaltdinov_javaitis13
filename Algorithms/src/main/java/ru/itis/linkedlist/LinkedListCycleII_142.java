package ru.itis.linkedlist;

// 142. Linked List Cycle II
// O(n) time complexity
// O(1) memory complexity

import ru.itis.model.ListNode;

public class LinkedListCycleII_142 {
    public static void main(String[] args) {

    }

    public static ListNode detectCycle(ListNode head) {
        ListNode slow = head;
        ListNode fast = head;

        while(fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;

            if(slow == fast) {
                while(head != slow) {
                    head = head.next;
                    slow = slow.next;
                }
                return slow;
            }
        }
        return null;
    }
}
