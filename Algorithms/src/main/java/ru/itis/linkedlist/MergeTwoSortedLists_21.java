package ru.itis.linkedlist;

// 21. Merge Two Sorted Lists

import ru.itis.model.ListNode;

public class MergeTwoSortedLists_21 {
    public static void main(String[] args) {

    }

    public static ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode dummy = new ListNode(0);
        ListNode current = dummy;

        while (list1 != null && list2 != null) {
            if (list1.val <= list2.val) {
                current.next = list1;
                list1 = list1.next;
            } else {
                current.next = list2;
                list2 = list2.next;
            }
            current = current.next;
        }
        if (list1 != null) {
            current.next = list1;
            list1 = list1.next;
        }

        if (list2 != null) {
            current.next = list2;
            list2 = list2.next;
        }
        return dummy.next;
    }
}
