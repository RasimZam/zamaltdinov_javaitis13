package ru.itis.linkedlist;

// 61. Rotate List
// O(n) time complexity
// O(1) memory complexity

import ru.itis.model.ListNode;

public class RotateList_61 {
    public static void main(String[] args) {

    }

    public ListNode rotateRight(ListNode head, int k) {
        if (head == null || head.next == null) {
            return head;
        }

        ListNode pre = null;
        ListNode curr = head;
        int size = 0;

        while (curr != null) {
            pre = curr;
            curr = curr.next;
            size++;
        }

        curr = head;
        pre.next = curr;

        int rotate = k % size;
        int location = size - rotate;

        for(int i = 0; i < location; i++) {
            pre = curr;
            curr = curr.next;
        }

        pre.next = null;
        return curr;
    }
}
