package ru.itis.linkedlist;

// 83. Remove Duplicates from Sorted List

import ru.itis.model.ListNode;

public class RemoveDuplicatesFromSortedList_83 {
    public static void main(String[] args) {

    }

    public ListNode deleteDuplicates(ListNode head) {
        if (head == null) return null;
        ListNode pointer = head;

        while (pointer != null) {
            if (pointer.next == null) {
                break;
            }

            if (pointer.val == pointer.next.val) {
                pointer.next = pointer.next.next;
            } else {
                pointer = pointer.next;
            }

        }
        return head;
    }
}
