package ru.itis.linkedlist;

// 206. Reverse Linked List

import ru.itis.model.ListNode;

public class ReverseLinkedList_206 {
    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);
        ListNode listNode = reverseList(head);
    }

    public static ListNode reverseList(ListNode head) {
        if(head == null) {
            return head;
        }

        if(head.next == null) {
            return head;
        }

        ListNode preNode = null;
        ListNode currNode = head;

        while (currNode != null) {
            ListNode nextNode = currNode.next;
            currNode.next = preNode;
            preNode = currNode;
            currNode = nextNode;
        }
        head = preNode;
        return head;
    }
}
