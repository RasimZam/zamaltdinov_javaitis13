package ru.itis.linkedlist;

import ru.itis.model.ListNode;

// 2807. Insert Greatest Common Divisors in Linked List

public class InsertGreatestCommonDivisorsInLinkedList_2807 {
    public static void main(String[] args) {
        int gcd = gcd(6, 10);
        System.out.println(gcd);
    }

    // 18 -> 6 -> 10 -> 3
    public ListNode insertGreatestCommonDivisors(ListNode head) {
        for (ListNode curr = head; curr.next != null;) {
            // curr.val = 18, curr.next.val = 6, curr.next = 6
            ListNode inserted = new ListNode(gcd(curr.val, curr.next.val), curr.next);
            // 18 -> 6 ->
            curr.next = inserted;
            curr = inserted.next;
        }
        return head;
    }

    // a = 18, b = 6
    private static int gcd(int a, int b) {
        // a = 6, b = 3;
        // a = 3, b = 2
        // a = 2, b
        return b == 0 ? a : gcd(b, a % b);
    }
}
