package ru.itis.linkedlist;

import ru.itis.model.ListNode;
import ru.itis.model.Result;
import ru.itis.structureData.LinkedList;

public class LinkedListFindIntersection_2_7 {

    ListNode findIntersection(ListNode list1, ListNode list2) {
        if (list1 == null || list2 == null) {
            return null;
        }

        Result result1 = getTailAndSize(list1);
        Result result2 = getTailAndSize(list2);

        if (result1.tail != result2.tail) {
            return null;
        }

        ListNode shorter = result1.size < result2.size ? list1 : list2;
        ListNode longer = result1.size < result2.size ? list2 : list1;

        longer = getKthNode(longer, Math.abs(result1.size - result2.size));

        while (shorter != longer) {
            shorter = shorter.next;
            longer = longer.next;
        }
        return longer;
    }

    private ListNode getKthNode(ListNode head, int k) {
        ListNode current = head;
        while (k > 0 && current != null) {
            current = current.next;
            k--;
        }
        return current;
    }

    private Result getTailAndSize(ListNode node) {
        if (node == null) {
            return null;
        }

        int size = 1;
        ListNode current = node;
        while (current != null) {
            size++;
            current = current.next;
        }
        return new Result(current, size);
    }
}
