package ru.itis.linkedlist;

import ru.itis.model.ListNode;

import java.util.ArrayList;
import java.util.List;

// 1290. Convert Binary Number in a Linked List to Integer

public class ConvertBinaryNumberInALinkedListToInteger_1290 {
    public static void main(String[] args) {

    }

    public static int getDecimalValue(ListNode head) {
        List<Integer> list = new ArrayList<>();
        int res = 0;

        while (head != null) {
            list.add(head.val);
            head = head.next;
        }

        int p = 0;
        for (int i = list.size() - 1; i >= 0; i--) {
            int pow = (1 << p);
            res += (pow * list.get(i));
            p++;
        }
        return res;
    }

    public static int getDecimalValue1(ListNode head) {
        StringBuilder s = new StringBuilder();

        while (head != null) {
            s.append(Integer.toString(head.val));
            head = head.next;
        }
        return Integer.parseInt(s.toString(), 2);
    }
}
