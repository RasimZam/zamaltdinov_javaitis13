package ru.itis.linkedlist;

// 24. Swap Nodes in Pairs

import ru.itis.model.ListNode;

public class SwapNodesInPairs_24 {
    public static void main(String[] args) {

    }

    public static ListNode swapPairs(ListNode head) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode point = dummy;

        while (point.next != null && point.next.next != null) {
            ListNode swap1 = point.next;
            ListNode swap2 = point.next.next;

            swap1.next = swap2.next;
            swap2.next = swap1;

            point.next = swap2;
            point = swap1;
        }
        return dummy.next;
    }
}
