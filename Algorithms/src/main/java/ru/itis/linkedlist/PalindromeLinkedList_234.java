package ru.itis.linkedlist;

// 234. Palindrome Linked List

import ru.itis.model.ListNode;

public class PalindromeLinkedList_234 {
    public static void main(String[] args) {

    }

    public static boolean isPalindrome(ListNode head) {
        if (head == null) return false;

        ListNode slow = head;
        ListNode fast = head;

//        1 -> 2 -> 2 -> 1
        while (fast != null && fast.next != null) {

            fast = fast.next.next;
            slow = slow.next;
        }

        slow = reversed(slow);
        fast = head;

        //        1 -> 2 -> null 1 -> 2 -> null
        while (slow != null) {
            if (slow.val != fast.val) {
                return false;
            }
            slow = slow.next;
            fast = fast.next;
        }
        return true;
    }

    public static ListNode reversed(ListNode head) {
        ListNode prev = null;
        while (head != null) {
            ListNode nextNode = head.next;
            head.next = prev;
            prev = head;
            head = nextNode;
        }
        return prev;
    }
}
