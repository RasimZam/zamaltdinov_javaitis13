package ru.itis.linkedlist;

// 146. LRU Cache

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class LRUCache_146 {
    private int capacity;
    private Map<Integer, LinkedListNode> cache;
    private LinkedList<LinkedListNode> lru;

    private class LinkedListNode {
        int key;
        int value;

        LinkedListNode(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }

    public LRUCache_146(int capacity) {
        this.capacity = capacity;
        this.cache = new HashMap<>();
        this.lru = new LinkedList<>();
    }

    public int get(int key) {
        if (cache.containsKey(key)) {
            LinkedListNode node = cache.get(key);
            lru.remove(node);
            lru.addFirst(node);
            return node.value;
        }
        return -1;
    }

    public void put(int key, int value) {
        if (cache.containsKey(key)) {
            LinkedListNode node = cache.get(key);
            lru.remove(node);
            node.value = value;
            lru.addFirst(node);
        } else {
            if(cache.size() == capacity) {
                LinkedListNode node = lru.removeLast();
                cache.remove(node);
            }
            LinkedListNode newNode = new LinkedListNode(key, value);
            lru.addFirst(newNode);
            cache.put(key, newNode);
        }
    }
}
