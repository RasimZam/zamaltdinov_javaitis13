package ru.itis.linkedlist;

// 92. Reverse Linked List II

import ru.itis.model.ListNode;

import java.util.List;

public class ReverseLinkedListII_92 {
    public ListNode reverseBetween(ListNode head, int left, int right) {
        if (head == null) {
            return null;
        }

        // 1 -> 2 -> 3 -> 4 -> 5 ----> prev = 1, start = 2, then = 3

        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode prev = dummy;

        for (int i = 0; i < left - 1; i++) {
            prev = prev.next;
        }

        ListNode start = prev.next;
        ListNode then = start.next;

        for (int i = 0; i < right - left; i++) {
            // start.next = 4
            start.next = then.next;
            // then.next = 2
            then.next = prev.next;
            // prev.next = 2
            prev.next = then;
            // then = 3
            then = start.next;
        }

        // first reversing : dummy->1 - 3 - 2 - 4 - 5; prev = 1, start = 2, then = 4
        // second reversing: dummy->1 - 4 - 3 - 2 - 5; prev = 1, start = 2, then = 5 (finish)
        return dummy.next;
    }

    public ListNode reverseBetween1(ListNode head, int left, int right) {
        if (head == null) {
            return null;
        }
        // 1 -> 2 -> 3 -> 4 -> 5 ----> prev = 1, start = 2, then = 3
        ListNode cur = head, prev = null;

        while (left > 1) {
            prev = cur;
            cur = cur.next;
            left--;
            right--;
        }

        ListNode start = prev, tail = cur;
        ListNode temp = null;

        while (right > 0) {
            temp = cur.next;
            cur.next = prev;
            prev = cur;
            cur = temp;
            right--;
        }

        if (start != null) {
            start.next = prev;
        } else {
            head = prev;
        }
        tail.next = cur;
        return head;
    }

    public ListNode reverseBetween2(ListNode head, int left, int right) {
        if (head == null || left == right) {
            return head;
        }

        ListNode dummy = new ListNode(0);
        dummy.next = head;

        ListNode leftPre = dummy;
        ListNode currNode = head;

        for(int i = 0; i < left - 1; i++) {
            leftPre = leftPre.next;
            currNode = currNode.next;
        }

        ListNode subListNode = currNode;
        ListNode preNode = null;

        for (int i = 0; i < right - left + 1; i++) {
            ListNode nextNode = currNode.next;
            currNode.next  = preNode;
            preNode = currNode;
            currNode = nextNode;
        }

        leftPre.next = preNode;
        subListNode.next = currNode;

        return dummy.next;
    }
}
