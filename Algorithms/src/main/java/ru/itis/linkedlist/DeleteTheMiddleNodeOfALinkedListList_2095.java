package ru.itis.linkedlist;

// 2095. Delete the Middle Node of a Linked List
// O(n) time complexity
// O(1) memory complexity

import ru.itis.model.ListNode;

public class DeleteTheMiddleNodeOfALinkedListList_2095 {
    public static void main(String[] args) {

    }

    public static ListNode deleteMiddle(ListNode head) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode slow = dummy;
        ListNode fast = head;

        while(fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }

        slow.next = slow.next.next;
        return dummy.next;
    }
}
