package ru.itis.linkedlist;

// 706. Design HashSet

public class MyHashMap {
    private int[] v = new int[1000001];

    public MyHashMap() {
        for (int i =0; i < 10000001; i++) {
            v[i] = -1;
        }

    }

    public void put(int key, int value) {
        v[key] = value;
    }

    public int get(int key) {
        return v[key];
    }

    public void remove(int key) {
        v[key] = -1;
    }
}
