package ru.itis.search;

// Бинарный поиск
public class BinarySearch {
    public static void main(String[] args) {
        int[] sequince = new int[] {-2, 0, 3, 5, 7, 9, 11, 15, 18};
        System.out.println("index = " + binarySearch(sequince, 5));
    }

    public static int binarySearch(int[] array, int searchElement) {
        int left = 0;
        int right = array.length - 1;
        while (left <= right) {
            int middleIndex = left + (right - left) / 2;
            int element = array[middleIndex];
            if (searchElement == element) {
                return middleIndex;
            }
            if (element < searchElement) {
                left = middleIndex + 1;
            } else {
                right = middleIndex - 1;
            }
        }
        return -1;
    }
}
