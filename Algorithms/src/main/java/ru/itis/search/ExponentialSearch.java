package ru.itis.search;

// Экспоненциальный поиск

public class ExponentialSearch {
    public static void main(String[] args) {
        int[] arr = new int[]{-2, 0, 3, 5, 7, 9, 11, 15, 18};
        int requiredElement = 5;
        System.out.println(exponentialSearch(arr,requiredElement));
    }

    public static int binarySearch(int[] array, int requiredElement, int left, int right) {
        while (left <= right) {
            int middle = left + (right - left) / 2;
            int element = array[middle];
            if (requiredElement == element) {
                return middle;
            }
            if (element < requiredElement) {
                left = middle + 1;
            } else {
                right = middle - 1;
            }
        }
        return -1;
    }

    public static int exponentialSearch(int[] array, int requiredElement) {
        long border = 1;
        while (border < array.length && array[(int) border] < requiredElement) {
            border *= 2;
        }
        int left = (int) (border / 2);
        int right;
        if (border > array.length - 1) {
            right = array.length - 1;
        } else {
            right = (int) border;
        }
        return binarySearch(array, requiredElement, left, right);
    }
}
