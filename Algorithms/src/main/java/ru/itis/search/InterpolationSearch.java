package ru.itis.search;

// Интерполяционный поиск
public class InterpolationSearch {
    public static void main(String[] args) {
        int[] sequince = new int[] {-2, 0, 3, 5, 7, 9, 11, 15, 18};
        System.out.println("index = " + interpolationSearch(sequince, 5));
    }
    public static int interpolationSearch(int[] sequince, int element) {
        int left = 0;
        int right = sequince.length - 1;
        while (sequince[left] < element && element < sequince[right]) {
            if (sequince[left] == sequince[right]) {
                break;
            }
            int index = (element - sequince[left]) * (left - right) / (sequince[left] - sequince[right]) + left;
            if (sequince[index] > element) {
                right = index - 1;
            } else if (sequince[index] < element) {
                left = index + 1;
            } else {
                return index;
            }
        }
        if (sequince[left] == element) {
            return left;
        }
        if (sequince[right] == element) {
            return right;
        }
        return -1;
    }
}
