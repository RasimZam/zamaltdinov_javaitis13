package ru.itis.search;

// Поиск Фибоначчи. Массив должен быть отсортирован изначально
public class Main {
    public static void main(String[] args) {
        int[] sequince = new int[]{-2, 0, 3, 5, 7, 9, 11, 15, 18, 21};
        FibonacciSearch fibonacciSearch = new FibonacciSearch();
        int index = fibonacciSearch.search(sequince, 7);
        System.out.println("index = " + index);
    }
}
