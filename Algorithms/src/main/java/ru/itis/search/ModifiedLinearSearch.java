package ru.itis.search;

import java.util.ArrayList;
import java.util.List;

// Модифицированный линейный поиск
public class ModifiedLinearSearch {
    public static void main(String[] args) {
        int[] array = new int[] {-2, 0, 3, 5, 7, 9, 11, 15, 18, 21};
        List<Integer> sequince = new ArrayList<>();
        for (int value: array) {
            sequince.add(value);
        }
        int  targetValue = 5;
        System.out.println("index -> " + modifiedLinearSearch(sequince, targetValue));
    }

    public static int modifiedLinearSearch(List<Integer> sequince, int targetValue) {
        sequince.add(targetValue);
        int lastIndex = sequince.size() - 1;
        int i = 0;
        while (sequince.get(i) != targetValue) {
            i++;
        }
        sequince.remove(lastIndex);
        if (i != lastIndex) {
            return i;
        }
        return -1;
    }
}
