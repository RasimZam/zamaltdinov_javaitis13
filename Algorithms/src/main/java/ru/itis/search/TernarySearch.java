package ru.itis.search;

import java.util.Arrays;

// Тренарный поиск

public class TernarySearch {
    public static void main(String[] args) {
        int[] arr = new int[]{-2, 0, 3, 5, 7, 9, 11, 15, 18};
        System.out.println(Arrays.toString(arr));
        System.out.println(ternarySearch(arr, 5));
    }

    public static int ternarySearch(int[] array, int element) {
        int right = array.length - 1;
        int left = 0;
        while (left <= right) {
            int h = (right - left) / 3;
            int m1 = left + h;
            int m2 = right - h;

            if (array[m1] == element) {
                return m1;
            }
            if (array[m2] == element) {
                return m2;
            }
            if (array[m1] < element && element < array[m2]) {
                left = m1 + 1;
                right = m2 -1;
            } else if (element < array[m1]) {
                right = m1 - 1;
            } else {
                left = m2 + 1;
            }
        }
        return -1;
    }
}
