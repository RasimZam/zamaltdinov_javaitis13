package ru.itis.search;

// Евклида алгоритм
public class EuclideanAlgorithm {
    public static void main(String[] args) {
        System.out.println(gcd(30 , 18));
    }

    public static int gcd(int a, int b) {
        a = Math.abs(a);
        b = Math.abs(b);
        if (b > a) {
            int temp = a;
            a = b;
            b = temp;
        }
        int r = a % b;
        while (r != 0) {
            a = b;
            b = r;
            r = a % b;
        }
        return b;
    }
}
