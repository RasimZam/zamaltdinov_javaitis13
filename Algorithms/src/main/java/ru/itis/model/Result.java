package ru.itis.model;

public class Result {
    public ListNode tail;
    public int size;

    public Result(ListNode tail, int size) {
        this.tail = tail;
        this.size = size;
    }
}
