package ru.itis.heap;

// 347. Top K Frequent Elements
// Time complexity O(n)
// Memory complexity O(n)

import java.util.*;

public class TopKFrequentElements_347 {
    public static void main(String[] args) {
        int[] array = {1, 1, 1, 2, 2, 3};
        int k = 2;
        int[] result = topKfrequent(array, k);
        System.out.println(Arrays.toString(result));

    }

    public static int[] topKfrequent(int[] nums, int k) {
        List<Integer>[] bucket = new List[nums.length + 1];
        Map<Integer, Integer> frequencyMap = new HashMap<>();

        for (int n : nums) {
            frequencyMap.put(n, frequencyMap.getOrDefault(n, 0) + 1);
        }

        for (int key : frequencyMap.keySet()) {
            int frequent = frequencyMap.get(key);
            if (bucket[frequent] == null) {
                bucket[frequent] = new ArrayList<>();
            }
            bucket[frequent].add(key);
        }

        int[] res = new int[k];
        int counter = 0;

        for (int pos = bucket.length - 1; pos >= 0 && counter < k; pos--) {
            if (bucket[pos] != null) {
                for (Integer integer : bucket[pos]) {
                    res[counter++] = integer;
                }
            }
        }
        return res;
    }
}
