package ru.itis.heap;

// 373.Find K Pairs With Smallest Sums

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

public class FindKPairsWithSmallestSums_373 {
    public static void main(String[] args) {
        int[] nums1 = {1,7,11};
        int[] nums2 = {2,4,6};
        int k = 3;
        List<List<Integer>> result = kSmallestPairs(nums1, nums2, k);
        System.out.println(result);

    }

    public static List<List<Integer>> kSmallestPairs(int[] nums1, int[] nums2, int k) {
        PriorityQueue<int[]> queue = new PriorityQueue<>((a, b) -> a[0] - b[0]);

        for(int i = 0; i < Math.min(nums1.length, k); i++) {
            queue.offer(new int[]{nums1[i] + nums2[0], i, 0});
        }

        List<List<Integer>> result = new ArrayList<>();

        while(!queue.isEmpty() && k > 0) {
            int[] currentPair = queue.poll();

            result.add(Arrays.asList(nums1[currentPair[1]], nums2[currentPair[2]]));
            --k;

            if (currentPair[2] + 1 < nums2.length) {
                queue.offer(new int[] {nums1[currentPair[1]] + nums2[currentPair[2] + 1], currentPair[1], currentPair[2] + 1});
            }
        }

        return result;
    }
}
