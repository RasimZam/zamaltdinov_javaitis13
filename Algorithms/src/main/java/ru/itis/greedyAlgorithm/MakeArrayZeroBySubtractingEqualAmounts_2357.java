package ru.itis.greedyAlgorithm;

// 2357. Make Array Zero by Subtracting Equal Amounts

import java.util.HashSet;
import java.util.Set;

public class MakeArrayZeroBySubtractingEqualAmounts_2357 {
    public int minimumOperations(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int number : nums) {
            if (number > 0) {
                set.add(number);
            }
        }
        return set.size();
    }
}
