package ru.itis.greedyAlgorithm;

// 2259. Remove Digit From Number to Maximize Result

public class RemoveDigit_2259 {
    public static void main(String[] args) {
//        String number = "15435";
        String number = "1231";
//        char digit = '5';
        char digit = '1';
        String result = removeDigit(number, digit);
        System.out.println(result);
    }

    public static String removeDigit(String number, char digit) {
        int toRemove = -1;
        for (int i = 0; i < number.length(); i++) {
            if (number.charAt(i) == digit) {
                toRemove = i;
                if (i + 1 < number.length() && number.charAt(i + 1) > digit) {
                    break;
                }
            }
        }
        return number.substring(0, toRemove) + number.substring(toRemove + 1, number.length());
    }
}
