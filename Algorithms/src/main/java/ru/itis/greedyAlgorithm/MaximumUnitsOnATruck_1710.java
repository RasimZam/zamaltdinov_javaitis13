package ru.itis.greedyAlgorithm;

// 1710. Maximum Units on a Truck

import java.util.Arrays;

public class MaximumUnitsOnATruck_1710 {
    public static void main(String[] args) {
        int[][] boxTypes = {{1, 3}, {2, 2}, {3, 1}};
        int result = maximumUnits1(boxTypes, 4);
        System.out.println(result);

    }

    public static int maximumUnits(int[][] boxTypes, int truckSize) {
        Arrays.sort(boxTypes, (a, b) -> b[1] - a[1]);
        int result = 0;
        for (int i = 0; i < boxTypes.length && truckSize > 0; i++) {
            int currentBoxType = boxTypes[i][0];
            int unitsCount = boxTypes[i][1];
            truckSize = truckSize - currentBoxType;
            result += currentBoxType * unitsCount;
            if (truckSize < 0) {
                result += truckSize * unitsCount;
                break;
            }
        }
        return result;
    }

    public static int maximumUnits1(int[][] boxTypes, int truckSize) {
        Arrays.sort(boxTypes, (a, b) -> -Integer.compare(a[1], b[1]));
        int result = 0;
        for (int[] box : boxTypes) {
            if (truckSize < box[0]) {
                return result + truckSize * box[1];
            }
            result += box[0] * box[1];
            truckSize -= box[0];
        }
        return result;
    }
}
