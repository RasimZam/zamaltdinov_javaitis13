package ru.itis.greedyAlgorithm;

// 2656. Maximum Sum With Exactly K Elements

public class MaximumSumWithExactlyKElements_2656 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5};
        int result = maximizeSum(array, 3);
        System.out.println(result);
    }


    public static int maximizeSum(int[] nums, int k) {
        int max = 0;
        for (int num : nums) {
            max = Math.max(max, num);
        }
        int ans = 0;
        for (int i = 0; i < k; i++) {
            ans += max + i;
        }
        return ans;
    }
}
