package ru.itis.greedyAlgorithm;

import java.util.Arrays;
import java.util.Comparator;

public class FractionalKnapsack {
    public static void main(String[] args) {
        Item item1 = new Item(4, 20);
        Item item2 = new Item(3, 18);
        Item item3 = new Item(2, 14);

        Item[] items = {item1, item2, item3};

        Arrays.sort(items, Comparator.comparingDouble(Item::valuePerUnitOfWeight).reversed());

        System.out.println(Arrays.toString(items));

        int baggage = 7;

        int weightSoFar = 0;
        double valueSoFar = 0;
        int currentItem = 0;

        while (currentItem < items.length && weightSoFar != baggage) {
            if (weightSoFar + items[currentItem].getWeight() < baggage) {
                // берем объект целеком
                valueSoFar += items[currentItem].getValue();
                weightSoFar += items[currentItem].getWeight();
            } else {
                // берем только часть объекта
                valueSoFar += ((baggage - weightSoFar) / (double) items[currentItem].getWeight()) *
                        items[currentItem].getValue();

                weightSoFar = baggage; // полный рюкзак
            }
            currentItem++;
        }
        System.out.println(valueSoFar);
    }
}
