package ru.itis.greedyAlgorithm;

import java.util.Arrays;

public class CountWays_8_1 {
    public static void main(String[] args) {
        int number = 8;
        int result = countWays1(number);
        System.out.println(result);
    }

    private static int countWays(int n) {
        if (n < 0) {
            return 0;
        } else if (n == 0) {
            return 1;
        } else {
            return countWays(n - 1) + countWays(n - 2) + countWays(n - 3);
        }
    }

    private static int countWays1(int n) {
        int[] memo = new int[n + 1];
        Arrays.fill(memo, -1);
        return countWays1(n, memo);
    }

    private static int countWays1(int n, int[] memo) {
        if (n < 0) {
            return 0;
        } else if (n == 0) {
            return 1;
        } else if (memo[n] > -1) {
            return memo[n];
        } else {
            memo[n] = countWays1(n - 1, memo) + countWays1(n - 2, memo) +
                    countWays1(n - 3, memo);
            return memo[n];
        }
    }
}
