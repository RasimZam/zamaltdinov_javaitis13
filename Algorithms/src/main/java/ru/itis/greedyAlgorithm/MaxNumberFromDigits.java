package ru.itis.greedyAlgorithm;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

public class MaxNumberFromDigits {
    public static void main(String[] args) {
        int[] digits = {3, 1, 7, 9, 9, 5};
        String result = maxNumberFromDigits1(digits);
        System.out.println(result);
    }

    public static String maxNumberFromDigits(int[] digits) {
        Arrays.sort(digits);
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = digits.length - 1; i >= 0; i--) {
            stringBuilder.append(digits[i]);
        }
        return stringBuilder.toString();
    }

    public static String maxNumberFromDigits1(int[] digits) {
        return Arrays.stream(digits)
                .boxed()
                .sorted(Collections.reverseOrder())
                .map(String::valueOf)
                .collect(Collectors.joining());
    }
}
