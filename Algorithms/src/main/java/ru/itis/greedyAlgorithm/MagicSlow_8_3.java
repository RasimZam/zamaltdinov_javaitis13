package ru.itis.greedyAlgorithm;

// определить волшебный индекс

public class MagicSlow_8_3 {
    public static void main(String[] args) {
        int[] array = {-10, -5, 2, 2, 2, 3, 4, 7, 9, 12, 13};
        int result = magicFast1(array);
        System.out.println(result);
    }

    public static int magicFast(int[] array) {
        return magicFast(array, 0, array.length - 1);
    }

    private static int magicFast(int[] array, int start, int end) {
        if (end < start) {
            return -1;
        }
        int mid = (start + end) / 2;
        if (array[mid] == mid) {
            return mid;
        } else if (array[mid] > mid) {
            return magicFast(array, start, mid - 1);
        } else {
            return magicFast(array, mid + 1, end);
        }
    }

    public static int magicFast1(int[] array) {
        return magicFast1(array, 0, array.length - 1);
    }

    private static int magicFast1(int[] array, int start, int end) {
        if (end < start) {
            return -1;
        }
        int midIndex = (start + end) / 2;
        int midValue = array[midIndex];
        if (midValue == midIndex) {
            return midIndex;
        }

        /* Поиск в левой части */
        int leftIndex = Math.min(midIndex - 1, midValue);
        int left = magicFast1(array, start, leftIndex);
        if (left > 0) {
            return left;
        }

        /* Поиск в правой части */
        int rightIndex = Math.max(midIndex + 1, midValue);
        int right = magicFast1(array, rightIndex, end);
        return right;
    }
}
