package ru.itis.greedyAlgorithm;

// 1217. Minimum Cost to Move Chips to The Same Position

public class MinimumCostToMoveChipsToTheSamePosition_1217 {
    public int minCostToMoveChips(int[] position) {
        int even = 0;
        for (int i : position) {
            if (i % 2 == 0) {
                even++;
            }
        }
        return Math.min(even, position.length - even);
    }

    public int minCostToMoveChips1(int[] position) {
        int even = 0, odd = 0;
        for (int i : position) {
            if (i % 2 == 0) {
                even++;
            } else {
                odd++;
            }
        }
        return Math.min(even, odd);
    }
}
