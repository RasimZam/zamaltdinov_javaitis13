package ru.itis.greedyAlgorithm;

// 409. Longest Palindrome

import java.util.HashMap;
import java.util.Map;

public class LongestPalindrome_409 {
    public static void main(String[] args) {
        String str = "abccccdd";
        int result = longestPalindrome1(str);
        System.out.println(result);
    }

    public static int longestPalindrome(String s) {
        Map<Character, Integer> counts = new HashMap<>();
        for (char symbol: s.toCharArray()) {
            counts.put(symbol, counts.getOrDefault(symbol, 0) + 1);
        }
        int result = 0;
        boolean oddFound = false;
        for (int value: counts.values()) {
            if (value % 2 == 0) {
                result += value - 1;
            } else {
                oddFound = true;
                result += value - 1;
            }
        }
        if (oddFound) {
            result++;
        }
        return result;
    }

    public static int longestPalindrome1(String s) {
        // abccccdd
        // 1142
        int[] charCounts = new int[128];
        for (char c: s.toCharArray()) {
            charCounts[c]++;
        }
        int result = 0;
        for (Integer charCount: charCounts) {

            result += charCount / 2 * 2;
            if (result % 2 == 0 && charCount % 2 == 1) {
                result++;
            }
        }
        return result;
    }
}
