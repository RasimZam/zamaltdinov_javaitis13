package ru.itis.greedyAlgorithm;

// 1046. Last Stone Weight

import java.util.Collections;
import java.util.PriorityQueue;

public class LastStoneWeight_1046 {

    public static void main(String[] args) {
        int[] array = {2, 7, 4, 1, 8, 1};
        int result = lastStoneWeight(array);
        System.out.println(result);
    }

    public static int lastStoneWeight(int[] stones) {
        PriorityQueue<Integer> pq = new PriorityQueue<>(Collections.reverseOrder());
        for (int st : stones) {
            pq.add(st);
        }
        while(pq.size() > 1) {
            int stone1 = pq.poll();
            int stone2 = pq.poll();
            if (stone1 > stone2) {
                pq.add(stone1 - stone2);
            }
        }
        return pq.isEmpty() ? 0 : pq.peek();
    }
}
