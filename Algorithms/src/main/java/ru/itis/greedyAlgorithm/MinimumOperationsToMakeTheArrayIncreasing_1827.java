package ru.itis.greedyAlgorithm;

// 1827. Minimum Operations to Make the Array Increasing

public class MinimumOperationsToMakeTheArrayIncreasing_1827 {
    public static void main(String[] args) {
//        int[] array = {1, 1, 1};
        int[] array = {1, 5, 2, 4, 1};
        int result = minOperations1(array);
        System.out.println(result);
    }

    public static int minOperations(int[] nums) {
        if (nums.length <= 1) {
            return 0;
        }
        int count = 0;
        int num = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if (num == nums[i]) {
                count++;
                num++;
            } else if (num > nums[i]) {
                num++;
                count += num - nums[i];
            } else {
                num = nums[i];
            }
        }
        return count;
    }

    public static int minOperations1(int[] nums) {
        int result = 0;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] <= nums[i - 1]) {
                int temp = nums[i - 1] - nums[i] + 1;
                result += temp;
                nums[i] += temp;
            }
        }
        return result;
    }
}
