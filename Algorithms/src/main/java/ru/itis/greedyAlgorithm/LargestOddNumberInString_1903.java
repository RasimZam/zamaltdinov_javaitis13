package ru.itis.greedyAlgorithm;

// 1903. Largest Odd Number in String

public class LargestOddNumberInString_1903 {
    public static void main(String[] args) {
        String number = "52";
        String number1 = "35427";
        String result = largestOddNumber(number1);
        System.out.println(result);
    }

    public static String largestOddNumber(String num) {
        for (int i = num.length() - 1; i >= 0; i--) {
            if (Integer.parseInt("" + num.charAt(i)) % 2 != 0) {
                return num.substring(0, i + 1);
            }
        }
        return "";
    }
}
