package ru.itis.functionalInterface.consumer;

import java.util.Map;
import java.util.function.BiConsumer;

public class Main3 {
    public static void main(String[] args) {
        Map<Integer, String> map = Map.of(1, "one", 2, "two", 5, "five");
        BiConsumer<Integer, String> biConsumer = (a, b) -> System.out.println((b + " ").repeat(a));
        map.forEach(biConsumer);
    }
}
