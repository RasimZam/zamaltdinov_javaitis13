package ru.itis.functionalInterface.consumer;

import java.util.function.BiConsumer;

public class Main2 {
    public static void main(String[] args) {
        BiConsumer<String, Integer> biConsumer = (a, b) -> System.out.println(a.repeat(b));
        biConsumer.accept("Hello ", 3);
    }
}
