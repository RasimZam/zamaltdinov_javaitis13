package ru.itis.functionalInterface.consumer;

import java.util.function.Consumer;

public class Main1 {
    public static void main(String[] args) {
        Consumer<String> consumer = (a) -> System.out.println("{ " + a + " }");
        Consumer<String> consumer1 = Main1::simplePrinter;
        consumer.accept("Hello world");
        consumer1.accept("Hello world");
    }

    public static <T> void simplePrinter(T operand) {
        System.out.println("{ " + operand + " }");
    }
}
