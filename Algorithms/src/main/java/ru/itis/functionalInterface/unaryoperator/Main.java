package ru.itis.functionalInterface.unaryoperator;

import java.util.function.UnaryOperator;

public class Main {
    public static void main(String[] args) {
        UnaryOperator<String> unOp1 = new UOp();
        System.out.println(unOp1.apply("Hello World"));
    }
}
