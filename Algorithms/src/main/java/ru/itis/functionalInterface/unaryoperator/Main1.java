package ru.itis.functionalInterface.unaryoperator;

import java.util.function.UnaryOperator;

public class Main1 {
    public static void main(String[] args) {
        UnaryOperator<Integer> uoOp1 = (a) -> -a;
        UnaryOperator<Integer> uoOp2 = Main1::negative;
        UnaryOperator<Integer> unOp = UnaryOperator.identity();
        System.out.println(unOp.apply(5));

        System.out.println(uoOp1.apply(3));
        System.out.println(uoOp2.apply(3));

    }

    private static Integer negative(Integer number) {
        return -number;
    }
}
