package ru.itis.functionalInterface.unaryoperator;

import java.util.function.UnaryOperator;

public class UOp implements UnaryOperator<String> {
    @Override
    public String apply(String s) {
        return s.toLowerCase();
    }
}
