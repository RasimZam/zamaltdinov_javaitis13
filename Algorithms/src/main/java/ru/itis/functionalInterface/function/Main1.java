package ru.itis.functionalInterface.function;

import java.util.function.Function;

public class Main1 {
    public static void main(String[] args) {
        String text = "Java the best!";
        Function<String, Integer> function = Main1::countWithSpace;
        System.out.println(applyToStr(text, function));
    }

    public static  Integer applyToStr(String text, Function<String, Integer> function) {
        return function.apply(text);
    }

    public static Integer countWithSpace(String text) {
        int n = 0;
        char[] chars = text.toCharArray();
        for (char aChar : chars) {
            if (aChar == ' ') {
                n++;
            }
        }
        return n;
    }
}
