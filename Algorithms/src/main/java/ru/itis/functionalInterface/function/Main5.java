package ru.itis.functionalInterface.function;

import java.util.function.BiFunction;

public class Main5 {
    public static void main(String[] args) {
        BiFunction<String, Character, Integer> countLetter = Main5::count;
        System.out.println(countLetter.apply("Hello", 'l'));
    }

    public static Integer count(String letter, Character chr) {
        int result = 0;
        char[] ls = letter.toCharArray();
        for (char l : ls) {
            if (l == chr) {
                result++;
            }
        }
        return result;
    }
}
