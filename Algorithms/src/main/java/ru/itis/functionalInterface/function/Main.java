package ru.itis.functionalInterface.function;

import java.util.function.Function;

public class Main {
    public static void main(String[] args) {
        Function<String, Integer> function = a -> a.length();
        System.out.println(function.apply("Hello"));
        Function<String, Integer> function1 = String::length;
        System.out.println(function1.apply("Hello"));
    }
}
