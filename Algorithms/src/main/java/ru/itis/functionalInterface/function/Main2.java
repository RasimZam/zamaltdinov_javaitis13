package ru.itis.functionalInterface.function;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class Main2 {
    public static void main(String[] args) {
        List<String> list = List.of("Java", "Python", "Fortran", "C");
        Function<String, Integer> function = String::length;
        List<Integer> resList = applyAndCreate(function, list);
        System.out.println(resList);
    }

    private static <R, T> List<R> applyAndCreate(Function<T, R> function, List<T> list) {
        List<R> result = new ArrayList<>();
        for (T t: list) {
            result.add(function.apply(t));
        }
        return result;
    }

}
