package ru.itis.functionalInterface.function;

import java.util.function.Function;

public class Main4 {
    public static void main(String[] args) {
        Function<Integer, Integer> function = Function.identity();
        System.out.println(function.apply(100));
    }
}
