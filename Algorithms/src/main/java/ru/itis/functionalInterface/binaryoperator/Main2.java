package ru.itis.functionalInterface.binaryoperator;

import java.util.List;
import java.util.function.BinaryOperator;

public class Main2 {
    public static void main(String[] args) {
        List<Integer> list1 = List.of(1, 2, 3);
        BinaryOperator<Integer> binaryOp1 = (a, b) -> a + b;
        Integer s1 = reduce(list1, binaryOp1, 0);
        System.out.println(s1);

        List<String> list2 = List.of("Hello", "Java", "world");
        BinaryOperator<String> binaryOp2 = (a, b) -> a + " " + b;
        String s2 = reduce(list2, binaryOp2, "");
        System.out.println(s2);
    }

    private static <T> T reduce(List<T> list, BinaryOperator<T> binaryOp, T startValue) {
        T result = startValue;
        for (T element: list) {
            result = binaryOp.apply(result, element);
        }
        return result;
    }
}
