package ru.itis.functionalInterface.binaryoperator;

import java.util.function.BinaryOperator;

public class StringConcat implements BinaryOperator<String> {
    private int minLength;

    public StringConcat(int minLength) {
        this.minLength = minLength;
    }

    @Override
    public String apply(String t1, String t2) {
        String a = t1.length() >= minLength ? t1 : "";
        String b = t2.length() >= minLength ? t2 : "";
        return a + b;
    }
}
