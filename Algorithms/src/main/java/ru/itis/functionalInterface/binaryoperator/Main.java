package ru.itis.functionalInterface.binaryoperator;

import java.util.function.BinaryOperator;

public class Main {
    public static void main(String[] args) {
        BinaryOperator<String> binaryOperator = new StringConcat(5);
        String text = binaryOperator.apply("Hello", "cat");
        System.out.println(text);
    }
}
