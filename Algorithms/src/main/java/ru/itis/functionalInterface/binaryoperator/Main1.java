package ru.itis.functionalInterface.binaryoperator;

import java.util.function.BinaryOperator;

public class Main1 {
    public static void main(String[] args) {
        BinaryOperator<Integer> binOp1 = (a, b) -> a + b;
        BinaryOperator<Integer> binOp2 = Main1:: sum;

        System.out.println(binOp1.apply(1, 3));
        System.out.println(binOp2.apply(1, 3));

    }

    private static Integer sum(Integer a, Integer b) {
        return a + b;
    }
}
