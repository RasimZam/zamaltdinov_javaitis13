package ru.itis.functionalInterface.comparator;

import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Umka", 12);
        Cat cat2 = new Cat("Luska", 5);
        Cat cat3 = new Cat("Barsik", 8);
        Cat cat4 = new Cat("Timka", 5);
        Cat cat5 = new Cat("Kuzia", 2);

        CatAgeComparator comparator = new CatAgeComparator();
        System.out.println(comparator.compare(cat1, cat2) == -comparator.compare(cat2, cat1));
        System.out.println(comparator.compare(cat1, cat2) > 0 && comparator.compare(cat2, cat5) > 0 && comparator.compare(cat1, cat5) > 0);
        System.out.println(comparator.compare(cat2, cat4) == 0 && (comparator.compare(cat2, cat1) < 0 && comparator.compare(cat4, cat1) < 0));
    }
}

