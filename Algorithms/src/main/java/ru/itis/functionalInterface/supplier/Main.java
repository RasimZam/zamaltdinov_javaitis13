package ru.itis.functionalInterface.supplier;

import java.util.function.Supplier;

public class Main {
    public static void main(String[] args) {
        Supplier<Integer> counter = new IntegerSequince(0, 10, 1);
        Integer number;
        for (; (number = counter.get()) != null;) {
            System.out.println(number);
        }
    }
}
