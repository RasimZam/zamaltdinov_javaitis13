package ru.itis.functionalInterface.supplier;

import java.util.function.Supplier;

public class IntegerSequince implements Supplier<Integer> {
    private Integer start;
    private Integer end;
    private Integer step;

    public IntegerSequince(Integer start, Integer end, Integer step) {
        this.start = start;
        this.end = end;
        this.step = step;
    }

    @Override
    public Integer get() {
        if (start > end) {
            return null;
        }
        start += step;
        return (start - step);
    }
}
