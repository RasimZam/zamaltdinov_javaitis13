package ru.itis.functionalInterface.supplier;

import java.util.function.Supplier;

public class Main1 {
    public static void main(String[] args) {
        Supplier<Integer> sup1 = () -> (int) (Math.random() * 10);
        Supplier<Integer> sup2 = Main1::getRandomNumber;

        System.out.println(sup1.get());
        System.out.println(sup2.get());
    }

    private static Integer getRandomNumber() {
        return (int) (Math.random() * 10);
    }
}
