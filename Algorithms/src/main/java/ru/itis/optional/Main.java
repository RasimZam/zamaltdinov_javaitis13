package ru.itis.optional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<String> isbns = Arrays.asList("book", "book1", "book3", null, "book4");
        List<String> list = isbns.stream()
                .map(Main::fetchBookByIsbn)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
        System.out.println(list);
        System.out.println("----------------------------");

        List<String> list1 = isbns.stream()
                .map(Main::fetchBookByIsbn)
                .flatMap(Optional::stream)
                .collect(Collectors.toList());
        System.out.println(list1);
        System.out.println("----------------------------");
    }

    private static Optional<String> fetchBookByIsbn (String isbn) {
        return Optional.ofNullable(isbn);
    }
}
