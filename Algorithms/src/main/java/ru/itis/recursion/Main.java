package ru.itis.recursion;

import java.math.BigInteger;

public class Main {
    public static void main(String[] args) {
        int number = 5;
        int sum = sumOfArithmeticProgress(number);
        System.out.println(sum);
        System.out.println("________________");

        int number1 = 3;
        System.out.println(factorial(number1));
    }

    private static BigInteger factorial(int number1) {
        if (number1 <= 1) {
            return new BigInteger("1");
        }
        return BigInteger.valueOf(number1).multiply(factorial(number1 - 1));
    }

    private static int sumOfArithmeticProgress(int number) {
        if (number <= 0) {
            return 0;
        } else {
            return number + sumOfArithmeticProgress(number - 1);
        }
    }
}
