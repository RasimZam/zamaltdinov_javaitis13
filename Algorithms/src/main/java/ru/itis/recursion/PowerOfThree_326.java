package ru.itis.recursion;

// 326. Power of three

public class PowerOfThree_326 {

    public boolean isPowerOfThree(int n) {
        if (n == 1) return true;
        if (n == 0) return false;
        if (n % 3 != 0) {
            return false;
        } else {
            return isPowerOfThree(n / 3);
        }
    }

    public boolean isPowerOfThree1(int n) {
        if (n < 1) {
            return false;
        }

        while (n % 3 == 0) {
            n /= 3;
        }

        return n == 1;
    }
}
