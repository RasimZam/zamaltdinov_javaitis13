package ru.itis.recursion;

// 342. Power of Four

public class PowerOfFour_342 {
    public static void main(String[] args) {
        System.out.println(isPowerOfFour(16));
    }

    public static boolean isPowerOfFour(int n) {
        if (n > 1) {
            while (n % 4 == 0) {
                n /= 4;
            }
        }
        return n == 1;
    }

    public static boolean isPowerOfFour1(int n) {
        return Math.log(n) / Math.log(4) % 1 == 0;
    }
}
