package ru.itis.recursion;

public class Factorial {
    public static void main(String[] args) {
        System.out.println(fac(4));
    }

    //fac(4)
    //4 * fac(3)
    //3 * fac(2)
    //2 * fac(1)
    //fac(1) == return 1
    public static int fac(int n) {
        if (n == 1) {
            return 1;
        }
        return n * fac(n - 1);
    }
}
