package ru.itis.recursion;

public class Triangle {
    public static void main(String[] args) {
        int result = triangle(5);
        System.out.println(result);
    }

    public static int triangle(int number) {
        System.out.println("Entering number = " + number);
        if (number == 1) {
            System.out.println("Returning 1");
            return 1;
        } else {
            int temp = number + triangle(number - 1);
            System.out.println("Returning " + temp);
            return temp;
        }
    }
}
