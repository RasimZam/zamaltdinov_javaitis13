package ru.itis.recursion;

// 206. Reverse Linked List // recursive solution

import ru.itis.model.ListNode;

import java.util.HashMap;
import java.util.Map;

public class ReverseLinkedList_206 {
    public static void main(String[] args) {

    }

    public ListNode reverseList(ListNode head) {
        return reverseListInt(head, null);
    }

    private ListNode reverseListInt(ListNode head, ListNode newHead) {
        if (head == null) {
            return newHead;
        }
        ListNode next = head.next;
        head.next = newHead;
        return reverseListInt(next, head);
    }
}
