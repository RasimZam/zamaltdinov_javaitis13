package ru.itis.recursion;

// 231. Power of Two

public class PowerOfTwo_231 {
    public static void main(String[] args) {
        System.out.println(isPowerOfTwo(16));
    }

    public static boolean isPowerOfTwo(int n) {
        if (n > 1) {
            while (n % 2 == 0) {
                n /= 2;
            }
        }
        return n == 1;
    }

    public static boolean isPowerOfTwo1(int n) {
        if (n == 1) return true;
        if (n == 0) return false;
        if (n % 2 != 0) {
            return false;
        } else {
            return isPowerOfTwo(n / 2);
        }
    }
}
