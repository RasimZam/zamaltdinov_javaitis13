package ru.itis.recursion;

// 509. Fibonacci Number

import java.util.HashMap;
import java.util.Map;

public class FibonacciNumber_509 {
    public static void main(String[] args) {
        int result = fib3(4);
        System.out.println(result);
    }

    public static int fib(int n) {
        if (n == 0) {
            return n;
        } else if (n == 1) {
            return 1;
        }
        return fib(n - 1) + fib(n - 2);
    }

    public static int fib1(int N) {
        // easy base cases/edge cases
        // this is not really necessary as our helper function already handles these use cases
        if (N <= 1) {
            return N;
        }

        // initialize our memoization map and put our base cases
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, 0);
        map.put(1, 1);

        // call helper function
        return fib(N, map);
    }

    private static int fib(int N, Map<Integer, Integer> map) {
        // have we already computed this sub-problem?
        // if not, then compute it and store it in our map
        if (!map.containsKey(N)) {
            int currentFib = fib(N - 1, map) + fib(N - 2, map);
            map.put(N, currentFib);
        }

        // since we have stored it already, simply return it
        return map.get(N);
    }

    public static int fib3(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;

        int a = 0;
        int b = 1;
        // sum = 1
        int sum = a + b;

        // 3
        while (n > 1) {
            // sum = 1
            // sum = 1 + 1 = 2
            sum = a + b;
            // a = 1
            // a = 1
            a = b;
            // b = 1
            // b = 2
            b = sum;
            // n = 2
            // n = 1
            n--;
        }
        return sum;
    }

    public static int fib4(int n) {
        // easy base cases/ edge cases
        if (n <= 1) return n;

        // initialize our memoization map
        // size N + 1 so that we can accommodate from 0 to N
        int[] map = new int[n + 1];

        // put our base cases
        map[0] = 0;
        map[1] = 1;

        // iterate through remaining values (2...N)
        for (int i = 2; i <= n; i++) {
            map[i] = map[i - 1] + map[i + 2];
        }
        return map[n];
    }
}
