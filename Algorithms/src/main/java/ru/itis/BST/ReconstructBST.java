package ru.itis.BST;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReconstructBST {
    public static int index = 0;

    public static void main(String[] args) {
        int[] array = {10, 4, 2, 1, 5, 17, 19, 18};
        List<Integer> integers = List.of(10, 4, 2, 1, 5, 17, 19, 18);
//        BSTree tree = new BSTree(array[0]);
//        for (int i = 1; i < array.length; i++) {
//            tree = insert(tree, array[i]);
//        }

        BSTree tree = reconstractBstHelper(integers, Integer.MIN_VALUE, Integer.MAX_VALUE);
        int i = 0;
    }

    public static BSTree insert(BSTree root, int value) {
        if (root == null) {
            return new BSTree(value);
        }
        if (value < root.key) {
            root.left = insert(root.left, value);
        } else {
            root.right = insert(root.right, value);
        }
        return root;
    }

    public static BSTree reconstractBstHelper(List<Integer> preOrder, int min, int max) {
        if(index >= preOrder.size()) {
            return null;
        }

        int value = preOrder.get(index);
        BSTree bst = null;
        if(value > min && value < max) {
            bst = new BSTree(value);
            index++;
            bst.left = reconstractBstHelper(preOrder, min, value);
            bst.right = reconstractBstHelper(preOrder, value, max);
        }
        return bst;
    }




    static class BSTree {
        int key;
        BSTree left, right;

        public BSTree(int key) {
            this.key = key;
            left = right = null;
        }
    }
}
