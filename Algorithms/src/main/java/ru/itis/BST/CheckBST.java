package ru.itis.BST;

import ru.itis.practice.example1.TreeNode;

public class CheckBST {

    public static boolean checkBST(TreeNode n) {
        return checkBST(n, null, null);
    }

    public static boolean checkBST(TreeNode n, Integer min, Integer max) {
        if (n == null) {
            return true;
        }
        if ((min != null && n.val <= min) || (max != null && n.val > max)) {
            return false;
        }
        if (!checkBST(n.left, min, n.val) || !checkBST(n.right, n.val, max)) {
            return false;
        }
        return true;
    }
}
