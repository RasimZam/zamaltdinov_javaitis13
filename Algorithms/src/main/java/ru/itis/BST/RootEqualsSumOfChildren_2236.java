package ru.itis.BST;

// 2236. Root Equals Sum of Children

import ru.itis.practice.example1.TreeNode;

public class RootEqualsSumOfChildren_2236 {

    public boolean checkTree(TreeNode root) {
        return root.left.val + root.right.val == root.val;
    }
}
