package ru.itis.BST;

// 98. Validate Binary Search Tree

import ru.itis.practice.example1.TreeNode;

public class ValidateBinarySearchTree_98 {
    public boolean isValidBST(TreeNode root) {
        return isValid(root, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    private boolean isValid(TreeNode root, long minValue, long maxValue) {
        if (root == null) {
            return true;
        }
        if (root.val <= minValue || root.val >= maxValue) {
            return false;
        }
        boolean left = isValid(root.left, minValue, root.val);
        if (left) {
            return isValid(root.right, root.val, maxValue);
        }
        return false;
    }
}
