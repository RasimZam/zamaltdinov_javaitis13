package ru.itis.BST;

// 965. Univalued Binary Tree

import ru.itis.practice.example1.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class UnivaluedBinaryTree_965 {
    List<Integer> vals;

    public boolean isUnivalTree(TreeNode root) {
        boolean leftCorrect = (
                (root.left != null) ||
                        ((root.val == root.left.val) && isUnivalTree(root.left)));
        boolean rightCorrect = (
                root.right != null ||
                        (root.val == root.right.val && isUnivalTree(root.right)));
        return leftCorrect && rightCorrect;
    }

    public boolean isUnivalTree1(TreeNode root) {
        vals = new ArrayList<>();
        dfs(root);
        for (int v : vals)
            if (v != vals.get(0)) {
                return false;
            }
        return true;
    }

    public void dfs(TreeNode node) {
        if (node != null) {
            vals.add(node.val);
            dfs(node.left);
            dfs(node.right);
        }
    }
}
