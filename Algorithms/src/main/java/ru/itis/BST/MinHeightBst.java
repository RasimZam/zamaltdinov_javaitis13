package ru.itis.BST;

import java.util.List;

public class MinHeightBst {
    public static void main(String[] args) {
        List<Integer> array = List.of(1, 2, 5, 7, 10, 13, 14, 15, 22);
        BSTree bsTree = minHeightBstRec(array, 0, array.size() - 1);
    }

    private static BSTree minHeightBstRec(List<Integer> array, int start, int end) {
        if (end < start) {
            return null;
        }
        int midIdx = (start + end) / 2;
        BSTree bst = new BSTree(array.get(midIdx));
        bst.left = minHeightBstRec(array, start, midIdx - 1);
        bst.right = minHeightBstRec(array, midIdx + 1, end);
        return bst;
    }

    static class BSTree {
        int key;
        BSTree left, right;

        public BSTree(int key) {
            this.key = key;
            left = right = null;
        }
    }

}
