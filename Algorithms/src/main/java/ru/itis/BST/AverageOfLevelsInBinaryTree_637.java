package ru.itis.BST;

/* 637. Average of Levels in Binary Tree */

import ru.itis.practice.example1.TreeNode;

import java.io.ObjectStreamConstants;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class AverageOfLevelsInBinaryTree_637 {
    public List<Double> averageOfLevels(TreeNode root) {
        Queue<TreeNode> levelQueue = new LinkedList<>();
        levelQueue.add(root);
        levelQueue.add(null);
        List<Double> avgList = new ArrayList<>();

        while (levelQueue.peek() != null) {
            double sum = 0;
            int nodes = 0;

            while (levelQueue.peek() != null) {
                TreeNode node = levelQueue.poll();
                sum += node.val;
                nodes++;

                if (node.left != null) {
                    levelQueue.add(node.left);
                }

                if (node.right != null) {
                    levelQueue.add(node.right);
                }
                levelQueue.add(levelQueue.poll());
                avgList.add(sum / nodes);
            }
        }
        return avgList;
    }

    public List<Double> averageOfLevels1(TreeNode root) {
        List<Double> result = new ArrayList<>();
        if (root == null) {
            return result;
        }

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);

        double levelSum = 0;
        while (!queue.isEmpty()) {
            levelSum = 0;
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode currentNode = queue.poll();
                levelSum += currentNode.val;

                if (currentNode.left != null) {
                    queue.offer(currentNode.left);
                }
                if (currentNode.right != null) {
                    queue.offer(currentNode.right);
                }
            }
            double levelAverage = levelSum / size;
            result.add(levelAverage);
        }
        return result;
    }
}
