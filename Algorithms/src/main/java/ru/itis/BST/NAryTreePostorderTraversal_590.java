package ru.itis.BST;

// 590. N-ary Tree Postorder Traversal

import ru.itis.model.Node;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class NAryTreePostorderTraversal_590 {
    public List<Integer> postorder(Node root) {
        LinkedList<Integer> ans = new LinkedList<>();
        Stack<Node> stack = new Stack<>();
        if (root == null) {
            return ans;
        }
        stack.add(root);
        while (!stack.isEmpty()) {
            Node current = stack.pop();
            ans.addFirst(current.val);

            for (Node node : current.children) {
                if (node != null) {
                    stack.add(node);
                }
            }
        }
        return ans;
    }
}
