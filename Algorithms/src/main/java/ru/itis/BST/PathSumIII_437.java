package ru.itis.BST;

// 437. Path Sum III

public class PathSumIII_437 {
    private static int counter = 0;

    private TreeNode root;

    private class TreeNode {
        private TreeNode left;
        private TreeNode right;
        private int val;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    public TreeNode createBinaryTree() {
        TreeNode first = new TreeNode(10);
        TreeNode second = new TreeNode(5);
        TreeNode third = new TreeNode(3);
        TreeNode fourth = new TreeNode(3);
        TreeNode fifth = new TreeNode(-2);
        TreeNode sixth = new TreeNode(2);
        TreeNode seventh = new TreeNode(1);
        TreeNode eighth = new TreeNode(-3);
        TreeNode ninth = new TreeNode(11);

        root = first;
        first.left = second;
        first.right = eighth;
        second.left = third;
        second.right = sixth;
        third.left = fourth;
        third.right = fifth;
        sixth.right = seventh;
        eighth.right = ninth;
        return root;
    }

    public static void main(String[] args) {
        PathSumIII_437 pathSumIII437 = new PathSumIII_437();
        TreeNode binaryTree = pathSumIII437.createBinaryTree();
        int result = pathSum(binaryTree, 8);
        System.out.println(result);
    }

    public static int pathSum(TreeNode root, int targetSum) {
        if (root == null) return 0;

        pathSumHelper(root, targetSum, 0);
        pathSum(root.left, targetSum);
        pathSum(root.right, targetSum);

        return counter;
    }

    public static void pathSumHelper(TreeNode root, int sum, int currentSum) {
        if (root == null) {
            return;
        }
        currentSum += root.val;
        if (currentSum == sum) {
            counter++;
        }
        pathSumHelper(root.left, sum, currentSum);
        pathSumHelper(root.right, sum, currentSum);
    }
}
