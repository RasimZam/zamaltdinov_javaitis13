package ru.itis.BST;

// 938. Range Sum of BST

import java.util.Stack;

public class RangeSumOfBST_938 {

    private TreeNode root;

    private class TreeNode {
        private TreeNode left;
        private TreeNode right;
        private int val;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    public TreeNode createBinaryTree() {
        TreeNode first = new TreeNode(10);
        TreeNode second = new TreeNode(5);
        TreeNode third = new TreeNode(3);
        TreeNode fourth = new TreeNode(7);
        TreeNode fifth = new TreeNode(15);
        TreeNode sixth = new TreeNode(18);

        root = first;
        first.left = second;
        first.right = fifth;
        second.left = third;
        second.right = fourth;
        fifth.right = sixth;
        return root;
    }

    public static void main(String[] args) {
        RangeSumOfBST_938 rangeSumOfBST938 = new RangeSumOfBST_938();
        TreeNode binaryTree = rangeSumOfBST938.createBinaryTree();
        rangeSumBST(binaryTree, 7, 15);
    }

    public static int rangeSumBST(TreeNode root, int low, int high) {
        if (root == null) {
            return 0;
        }
        int sum = 0;

        if (low <= root.val && root.val <= high) {
            sum += root.val;
        }
        if (low < root.val) {
            sum += rangeSumBST(root.left, low, high);
        }

        if (high > root.val) {
            sum += rangeSumBST(root.right, low, high);
        }
        return sum;
    }

    public int rangeSumBST1(TreeNode root, int low, int high) {
        if (root == null) {
            return 0;
        }
        int sum = 0;
        Stack<TreeNode> stk = new Stack<>();
        stk.push(root);
        while (!stk.isEmpty()) {
            TreeNode current = stk.pop();
            if (current == null) {
                continue;
            }

            if (low <= current.val && current.val <= high) {
                sum += current.val;
            }
            if (low < current.val) {
                stk.push(current.left);
            }

            if (high > current.val) {
                stk.push(current.right);
            }
        }
        return sum;
    }
}
