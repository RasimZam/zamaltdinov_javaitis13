package ru.itis.BST;

public class IsBalanced_4_4 {
    private TreeNode root;

    private class TreeNode {
        private TreeNode left;
        private TreeNode right;
        private int data;

        public TreeNode(int data) {
            this.data = data;
        }
    }

    public TreeNode createBinaryTree() {
        TreeNode first = new TreeNode(3);
        TreeNode second = new TreeNode(9);
        TreeNode third = new TreeNode(20);
        TreeNode fourth = new TreeNode(15);
        TreeNode fifth = new TreeNode(7);

        root = first;
        first.left = second;
        first.right = third;
        third.left = fourth;
        third.right = fifth;
        return root;
    }

    public static void main(String[] args) {
        IsBalanced_4_4 binaryTree = new IsBalanced_4_4();
        TreeNode root = binaryTree.createBinaryTree();
//        boolean balanced = isBalanced(root);
        boolean balanced = isBalanced1(root);
        System.out.println(balanced);
    }

    public static boolean isBalanced(TreeNode root) {
        return checkHeight(root) != Integer.MIN_VALUE;
    }

    public static int checkHeight(TreeNode root) {
        if (root == null) {
            return -1;
        }

        int leftHeight = checkHeight(root.left);
        if (leftHeight == Integer.MIN_VALUE) {
            return Integer.MIN_VALUE;
        }

        int rightHeight = checkHeight(root.right);
        if (rightHeight == Integer.MIN_VALUE) {
            return Integer.MIN_VALUE;
        }
        int heightDiff = leftHeight - rightHeight;
        if (Math.abs(heightDiff) > 1) {
            return Integer.MIN_VALUE;
        } else {
            return Math.max(leftHeight, rightHeight) + 1;
        }
    }

    public static boolean isBalanced1(TreeNode root) {
        if (root == null) {
            return true;
        }
        if (isHeight(root) == -1) {
            return false;
        }
        return true;
    }

    private static int isHeight(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int leftHeight = isHeight(root.left);
        int rightHeight = isHeight(root.right);
        if (leftHeight == -1 || rightHeight == -1) {
            return -1;
        }
        if (Math.abs(leftHeight - rightHeight) > 1) {
            return -1;
        }
        return Math.max(leftHeight, rightHeight) + 1;
    }
}
