package ru.itis.BST;

import ru.itis.practice.example1.TreeNode;

public class CreateMinimalBST_4_2 {
    public static void main(String[] args) {
        int[] array = {1, 2, 5, 7, 8, 9, 10};
        TreeNode minimalBST = createMinimalBST(array);

    }

    public static TreeNode createMinimalBST(int[] array) {
        return createMinimalBST(array, 0, array.length - 1);
    }

    private static TreeNode createMinimalBST(int[] array, int start, int end) {
        if (end < start) {
            return null;
        }

        int mid = (start + end) / 2;
        TreeNode root = new TreeNode(array [mid]);

        root.left = createMinimalBST(array, start, mid - 1);
        root.right = createMinimalBST(array, mid + 1, end);
        return root;
    }
}
