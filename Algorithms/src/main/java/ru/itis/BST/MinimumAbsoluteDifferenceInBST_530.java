package ru.itis.BST;

// 530. Minimum Absolute Difference in BST

import ru.itis.practice.example1.TreeNode;

public class MinimumAbsoluteDifferenceInBST_530 {
    private int min = Integer.MAX_VALUE;
    private Integer prev = null;

    public int getMinimumDifference(TreeNode root) {
        if (root == null) return min;

        getMinimumDifference(root.left);

        if (prev != null) {
            min = Math.min(min, root.val - prev);
        }
        prev = root.val;

        getMinimumDifference(root.right);

        return min;
    }
}
