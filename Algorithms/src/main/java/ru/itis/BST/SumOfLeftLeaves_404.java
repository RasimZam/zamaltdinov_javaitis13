package ru.itis.BST;

// 404. Sum of Left Leaves

public class SumOfLeftLeaves_404 {
    private TreeNode root;

    private class TreeNode {
        private TreeNode left;
        private TreeNode right;
        private int data;

        public TreeNode(int data) {
            this.data = data;
        }
    }
    public void createTreeNode() {
        TreeNode first = new TreeNode(3);
        TreeNode second = new TreeNode(9);
        TreeNode third = new TreeNode(20);
        TreeNode fourth = new TreeNode(15);
        TreeNode fifth = new TreeNode(7);

        root = first;
        root.left = second;
        root.right = third;
        third.right = fifth;
        third.left = fourth;
    }
    public static int sumOfLeftLeaves(TreeNode root) {
        if (root == null) {
            return 0;
        } else if (root.left != null && root.left.left == null && root.left.right == null) {
            return root.left.data + sumOfLeftLeaves(root.right);
        } else {
            return sumOfLeftLeaves(root.left) + sumOfLeftLeaves(root.right);
        }
    }

    public static void main(String[] args) {
        SumOfLeftLeaves_404 treeNode = new SumOfLeftLeaves_404();
        treeNode.createTreeNode();
        int result = sumOfLeftLeaves(treeNode.root);
        System.out.println(result);
    }
}
