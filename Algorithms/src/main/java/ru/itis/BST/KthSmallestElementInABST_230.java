package ru.itis.BST;

import ru.itis.practice.example1.TreeNode;

// O(n) time complexity
// O(n) memory complexity

public class KthSmallestElementInABST_230 {
    public static void main(String[] args) {

    }

    public static int kthSmallest(TreeNode root, int k) {
        int[] result = new int[2];
        inOrder(root, result, k);
        return result[1];
    }

    private static void inOrder(TreeNode root, int[] result, int k) {
        if(root == null) {
            return;
        }

        inOrder(root.left, result, k);
        if(++result[0] == k) {
            result[1] = root.val;
            return;
        }
        inOrder(root.right, result, k);
    }
}
