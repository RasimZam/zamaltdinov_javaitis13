package ru.itis.BST;

public class CountPathWithSum_4_12 {
    private TreeNode root;

    private class TreeNode {
        private TreeNode left;
        private TreeNode right;
        private int data;

        public TreeNode(int data) {
            this.data = data;
        }
    }

    public TreeNode createBinaryTree() {
        TreeNode first = new TreeNode(10);
        TreeNode second = new TreeNode(5);
        TreeNode third = new TreeNode(3);
        TreeNode fourth = new TreeNode(3);
        TreeNode fifth = new TreeNode(-2);
        TreeNode sixth = new TreeNode(2);
        TreeNode seventh = new TreeNode(1);
        TreeNode eight = new TreeNode(-3);
        TreeNode ninth = new TreeNode(11);

        root = first;
        first.left = second;
        first.right = eight;
        second.left = third;
        second.right = sixth;
        sixth.right = seventh;
        third.left = fourth;
        third.right = fifth;
        eight.right = ninth;
        return root;
    }

    public static void main(String[] args) {
        CountPathWithSum_4_12 tree = new CountPathWithSum_4_12();
        TreeNode binaryTree = tree.createBinaryTree();
        countPathWithSum(binaryTree, 8);

    }

    public static int countPathWithSum(TreeNode root, int targetSum) {
        if (root == null) {
            return 0;
        }

        /* подсчет путей с заданной суммой, начиная с корня*/
        int pathsFromRoot = countPathWithSumNode(root, targetSum, 0);

        int pathsOnLeft = countPathWithSum(root.left, targetSum);
        int pathsOnRight = countPathWithSum(root.right, targetSum);

        return pathsFromRoot + pathsOnLeft + pathsOnRight;
    }

    /* возвращает количество путей с заданной суммой от данного узла */
    private static int countPathWithSumNode(TreeNode node, int targetSum, int currentSum) {
        if (node == null) {
            return 0;
        }

        currentSum += node.data;
        int totalPaths = 0;

        if (currentSum == targetSum) { // найден путь от корня
            totalPaths++;
        }

        totalPaths += countPathWithSumNode(node.left, targetSum, currentSum);
        totalPaths += countPathWithSumNode(node.right, targetSum, currentSum);

        return totalPaths;
    }
}
