package ru.itis.BST;

import ru.itis.practice.example1.TreeNode;

public class CountPathsWithSum {

    public int countPathsWithSum(TreeNode root, int targetSum) {
        if (root == null) {
            return 0;
        }

        /* Подсчет путей с заданной суммой, начиная с корня*/
        int pathsFromRoot = countPathsWithSumFromNode(root, targetSum, 0);

        /* Проверка узла слева и справа */
        int pathsOnLeft = countPathsWithSum(root.left, targetSum);
        int pathsOnRight = countPathsWithSum(root.right, targetSum);

        return pathsFromRoot + pathsOnLeft + pathsOnRight;
    }

    /* Возвращает количество путей с заданной суммой данного узла */
    private int countPathsWithSumFromNode(TreeNode node, int targetSum, int currentSum) {
        if (node == null) {
            return 0;
        }

        currentSum += node.val;
        int totalPaths = 0;
        if (currentSum == targetSum) { // Найден путь от корня
            totalPaths++;
        }

        totalPaths += countPathsWithSumFromNode(node.left, targetSum, currentSum);
        totalPaths += countPathsWithSumFromNode(node.right, targetSum, currentSum);
        return totalPaths;
    }
}
