package ru.itis.BST;

// 783. Minimum Distance Between BST Nodes

import ru.itis.practice.example1.TreeNode;

public class MinimumDistanceBetweenBSTNodes_783 {

    private int ans = Integer.MAX_VALUE;
    private Integer pred = null;

    public int minDiffInBST(TreeNode root) {
        inorder(root);
        return ans;
    }

    private void inorder(TreeNode root) {
        if (root == null) {
            return;
        }

        inorder(root.left);
        if (pred != null) {
            ans = Math.min(ans, root.val - pred);
        }
        pred = root.val;
        inorder(root.right);
    }
}
