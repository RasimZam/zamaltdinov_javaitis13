package ru.itis.BST;

// 112. Path Sum

import ru.itis.practice.example1.TreeNode;

import java.util.Stack;

public class PathSum_112 {
    public static void main(String[] args) {

    }

    public static boolean hasPathSum(TreeNode root, int targetSum) {
        if (root == null) return false;

        Stack<TreeNode> path = new Stack<>();
        Stack<Integer> pathSum = new Stack<>();
        path.push(root);
        pathSum.push(root.val);

        while (!path.isEmpty()) {
            TreeNode node = path.pop();
            int tempVal = pathSum.pop();

            if (node.left == null && node.right == null && tempVal == targetSum) {
                return true;
            }

            if(node.right != null) {
                path.push(node.right);
                pathSum.push(tempVal + node.right.val);
            }

            if(node.left != null) {
                path.push(node.left);
                pathSum.push(tempVal + node.left.val);
            }
        }
        return false;
    }

    public static boolean hasPathSum1(TreeNode root, int targetSum) {
        if(root == null) return false;

        targetSum -= root.val;
        if(root.left == null && root.right == null) {
            return targetSum == 0;
        }
        return hasPathSum(root.left, targetSum) || hasPathSum(root.right, targetSum);
    }
}
