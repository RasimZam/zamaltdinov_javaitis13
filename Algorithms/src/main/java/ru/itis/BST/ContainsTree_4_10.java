package ru.itis.BST;

import ru.itis.practice.example1.TreeNode;

public class ContainsTree_4_10 {
    public boolean containsTree(TreeNode t1, TreeNode t2) {
        if (t2 == null) {
            return true;
        }
        return subTree(t1, t2);
    }

    private boolean subTree(TreeNode r1, TreeNode r2) {
        if (r1 == null) {
            return false; // большое дерево пустое, а поддерево так и не найдено
        } else if (r1.val == r2.val && matchTree(r1, r2)) {
            return true;
        }
        return subTree(r1.left, r2) || subTree(r1.right, r2);
    }

    private boolean matchTree(TreeNode r1, TreeNode r2) {
        if (r1 == null && r2 == null) {
            return true; // в поддереве не осталось узлов
        } else if (r1 == null || r2 == null) {
            return false; // ровно одно дерево пусто, поэтому совпадений нет
        } else if (r1.val != r2.val) {
            return false; // данные не совпадают
        } else {
            return matchTree(r1.left, r2.left) && matchTree(r1.right, r2.right);
        }
    }
}
