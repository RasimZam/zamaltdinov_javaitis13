package ru.itis.BST;

public class ValidateBst {

    private TreeNode root;

    private static class TreeNode {
        private TreeNode left;
        private TreeNode right;
        private int data;

        public TreeNode(int data) {
            this.data = data;
        }
    }

    public void createBinaryTree() {
        TreeNode first = new TreeNode(10);
        TreeNode second = new TreeNode(5);
        TreeNode third = new TreeNode(2);
        TreeNode fourth = new TreeNode(1);
        TreeNode fifth = new TreeNode(5);
        TreeNode sixth = new TreeNode(15);
        TreeNode seventh = new TreeNode(13);
        TreeNode eighth = new TreeNode(14);
        TreeNode ninth = new TreeNode(22);

        root = first;
        first.left = second;
        first.right = sixth;
        second.left = third;
        second.right = fifth;
        third.left = fourth;
        sixth.right = ninth;
        sixth.left = seventh;
        seventh.right = eighth;
    }

    public static void main(String[] args) {
        ValidateBst bsTree = new ValidateBst();
        bsTree.createBinaryTree();

        boolean result = validateBst(bsTree.root, Integer.MIN_VALUE, Integer.MAX_VALUE);
        System.out.println(result);

    }

    public static boolean validateBst(TreeNode tree, int min, int max) {
        if (tree == null) {
            return true;
        }
        if (tree.data < min || tree.data >= max) {
            return false;
        }
        if (tree.left != null && !validateBst(tree.left, min, tree.data)) {
            return false;
        }
        if (tree.right != null && !validateBst(tree.right, tree.data, max)) {
            return false;
        }
        return true;
    }
}
