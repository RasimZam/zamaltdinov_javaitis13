package ru.itis.BST;

import ru.itis.practice.example1.MinimumDepthOfBinaryTree_111;
import ru.itis.practice.example1.TreeNode;

import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class MaximumDepthOfBinaryTree_104 {
    private TreeNode root;

    private class TreeNode {
        private TreeNode left;
        private TreeNode right;
        private int data;

        public TreeNode(int data) {
            this.data = data;
        }
    }

    public TreeNode createBinaryTree() {
        TreeNode first = new TreeNode(3);
        TreeNode second = new TreeNode(9);
        TreeNode third = new TreeNode(20);
        TreeNode fourth = new TreeNode(15);
        TreeNode fifth = new TreeNode(7);

        root = first;
        first.left = second;
        first.right = third;
        third.left = fourth;
        third.right = fifth;
        return root;
    }
    public static void main(String[] args) {
        MaximumDepthOfBinaryTree_104 maximumDepthOfBinaryTree104 = new MaximumDepthOfBinaryTree_104();
        TreeNode treeNode = maximumDepthOfBinaryTree104.createBinaryTree();
        int result = maxDepth(treeNode);
        System.out.println(result);
    }

    public static int maxDepth(TreeNode root) {
        if(root == null) {
            return 0;
        }
        int leftMaxDepth = maxDepth(root.left);
        int rightMaxDepth = maxDepth(root.right);
        return leftMaxDepth > rightMaxDepth ? leftMaxDepth + 1 : rightMaxDepth + 1;
    }

    public int maxDepth1(TreeNode root) {
        if(root == null) {
            return 0;
        }
        return 1 + Math.max(maxDepth1(root.left), maxDepth1(root.right));
    }

    public int maxDepth2(TreeNode root) {
        Queue<TreeNode> elementQueue = new LinkedList<>();
        elementQueue.add(root);
        int numberOfLevels = -1;

        while (true) {
            // Maintain a count of nodes at each level
            int nodeCountAtLevel = elementQueue.size();
            if (nodeCountAtLevel == 0) {
                // if we find no nodes at level, it means tree ended, so return
                return numberOfLevels + 1;
            }
            // Deque all the nodes present a particular level and add it to queue
            while (nodeCountAtLevel > 0) {
                TreeNode element = elementQueue.poll();
                if (element.left != null) {
                    elementQueue.add(element.left);
                }
                if (element.right != null) {
                    elementQueue.add(element.right);
                }
                nodeCountAtLevel--;
            }
            numberOfLevels++;
        }
    }

    public int maxDepth3(TreeNode root) {
        if (root == null) {
            return 0;
        }
        LinkedList<TreeNode> stack = new LinkedList<>();
        LinkedList<Integer> depths = new LinkedList<>();
        stack.add(root);
        depths.add(1);
        int depth = 0;

        while (!stack.isEmpty()) {
            TreeNode curr = stack.pollLast();
            int currDepth = depths.pollLast();

            if (curr != null) {

                depth = Math.max(depth, currDepth);
                stack.add(curr.right);
                depths.add(currDepth + 1);
                stack.add(curr.left);
                depths.add(currDepth + 1);
            }
        }
        return depth;
    }
}
