package ru.itis.BST;

// 606. Construct String from Binary Tree

import ru.itis.practice.example1.TreeNode;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class ConstructStringFromBinaryTree_606 {
    public String tree2str(TreeNode root) {
        StringBuilder res = new StringBuilder();
        dfs(root, res);
        return res.toString();
    }

    public static void dfs(TreeNode root, StringBuilder res) {
        if (root == null) {
            return;
        }
        res.append(String.valueOf(root.val));
        if (root.left == null && root.right == null) {
            return;
        }
        res.append('(');
        dfs(root.left, res);
        res.append(')');
        if (root.right != null) {
            res.append('(');
            dfs(root.right, res);
            res.append(')');
        }
    }

    public String tree2str1(TreeNode root) {
        if (root == null) {
            return "";
        }

        // 1,2,3,4
        Stack<TreeNode> stack = new Stack<>();
        // 1
        stack.push(root);
        Set<TreeNode> visited = new HashSet<>();
        StringBuilder s = new StringBuilder();
        while (!stack.isEmpty()) {
            // 1
            // 2
            // 4

            root = stack.peek();
            if (visited.contains(root)) {
                stack.pop();
                s.append(")");
            } else {
                // 1
                // 2
                // 4
                visited.add(root);
                // (1
                // (1(2
                // (1(2(4
                s.append("(" + root.val);
                if (root.left == null && root.right == null) {
                    s.append("()");
                    // (1(2(4()
                }
                if (root.right != null) {
                    // 3
                    stack.push(root.right);
                }
                if (root.left != null) {
                    // 2
                    // 4
                    stack.push(root.left);
                    //3, 2, 4
                }
            }
        }
        return s.substring(1, s.length() - 1);
    }
}
