package ru.itis.BST;

import java.util.Random;

public class TreeNode_4_11 {
    private int data;
    public TreeNode_4_11 left;
    public TreeNode_4_11 right;
    private int size = 0;

    public TreeNode_4_11(int data) {
        this.data = data;
        this.size = 1;
    }

    public TreeNode_4_11 getRandomNode() {
        int leftSize = left == null ? 0 : left.size;
        Random random = new Random();
        int index = random.nextInt(size);
        if (index < leftSize) {
            return left.getRandomNode();
        } else if (index == leftSize) {
            return this;
        } else {
            return right.getRandomNode();
        }
    }

    public void insertInOrder(int d) {
        if (d <= data) {
            if (left == null) {
                left = new TreeNode_4_11(d);
            } else {
                left.insertInOrder(d);
            }
        } else {
            if (right == null) {
                right = new TreeNode_4_11(d);
            } else {
                right.insertInOrder(d);
            }
        }
        size++;
    }

    public int size() {
        return size;
    }

    public int date() {
        return data;
    }

    public TreeNode_4_11 find(int d) {
        if (d == data) {
            return this;
        } else if (d <= data) {
            return left != null ? left.find(d) : null;
        } else if (d > data) {
            return right != null ? right.find(d) : null;
        }
        return null;
    }
}
