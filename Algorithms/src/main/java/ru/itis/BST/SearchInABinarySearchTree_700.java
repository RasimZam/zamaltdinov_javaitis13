package ru.itis.BST;

// 700. Search in a Binary Search Tree

import ru.itis.practice.example1.TreeNode;

public class SearchInABinarySearchTree_700 {
    public TreeNode searchBST(TreeNode root, int val) {
        if (root == null) {
            return null;
        }
        while (root != null && root.val != val) {
            root = val < root.val ? root.left : root.right;
        }
        return root;
    }

    public TreeNode searchBST1(TreeNode root, int val) {
        if (root == null) {
            return null;
        }
        if (root.val == val) {
            return root;
        }
        return val < root.val ? searchBST1(root.left, val) : searchBST1(root.right, val);
    }
}
