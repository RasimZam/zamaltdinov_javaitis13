package ru.itis.BST;

// 101. Symmetric Tree


import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.TreeSet;

public class SymmetricTree_101 {
    private TreeNode root;

    private class TreeNode {
        private TreeNode left;
        private TreeNode right;
        private int data;

        public TreeNode(int data) {
            this.data = data;
        }
    }

    private TreeNode createTreeNode() {
        TreeNode first = new TreeNode(1);
        TreeNode second = new TreeNode(2);
        TreeNode third = new TreeNode(3);
        TreeNode fourth = new TreeNode(4);
        TreeNode fifth = new TreeNode(2);
        TreeNode sixth = new TreeNode(4);
        TreeNode seventh = new TreeNode(3);

        root = first;
        first.left = second;
        first.right = fifth;
        second.left = third;
        second.right = fourth;
        fifth.left = sixth;
        fifth.right = seventh;
        return root;
    }

    public static void main(String[] args) {
        SymmetricTree_101 symmetricTree101 = new SymmetricTree_101();
        TreeNode treeNode = symmetricTree101.createTreeNode();
        isSymmetric(treeNode);
    }

    private static boolean isSymmetric(TreeNode root) {
        return root == null || isSymmetricHelp(root.left, root.right);
    }

    private static boolean isSymmetricHelp(TreeNode left, TreeNode right) {
        if (left == null || right == null) {
            return left == right;
        }
        if (left.data != right.data) {
            return false;
        }
        return isSymmetricHelp(left.left, right.right) && isSymmetricHelp(left.right, right.left);
    }

    private static boolean isSymmetricStack(TreeNode root) {
        if (root == null) {
            return true;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root.right);
        stack.push(root.left);

        while (!stack.isEmpty()) {
            TreeNode n1 = stack.pop();
            TreeNode n2 = stack.pop();
            if (n1 == null && n2 == null) {
                continue;
            }
            if (n1 == null || n2 == null
                    || n1.data != n2.data) {
                return false;
            }
            stack.push(n1.left);
            stack.push(n2.right);
            stack.push(n1.right);
            stack.push(n2.left);
        }
        return true;
    }

    private static boolean isSymmetricQueue(TreeNode root) {
        if (root == null) {
            return true;
        }
        Queue<TreeNode> leftTree = new LinkedList<>();
        Queue<TreeNode> rightTree = new LinkedList<>();
        leftTree.add(root.left);
        rightTree.add(root.right);

        while (!leftTree.isEmpty() && !rightTree.isEmpty()) {
            TreeNode leftNode = leftTree.poll();
            TreeNode rightNode = rightTree.poll();

            if(leftNode == null && rightNode == null) continue;
            if(leftNode == null || rightNode == null) return false;
            if(leftNode.data != rightNode.data) return false;

            leftTree.add(leftNode.left);
            leftTree.add(leftNode.right);
            rightTree.add(rightNode.right);
            rightTree.add(rightNode.left);
        }
        return true;
    }

}
