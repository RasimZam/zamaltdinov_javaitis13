package ru.itis.BST;

// 257. Binary Tree Paths

import ru.itis.practice.example1.InvertBinaryTree_226;
import ru.itis.practice.example1.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreePaths_257 {
    private TreeNode root;

    private static class TreeNode {
        private TreeNode left;
        private TreeNode right;
        private int data;

        public TreeNode(int data) {
            this.data = data;
        }
    }

    public TreeNode createBinaryTree() {
        TreeNode first = new TreeNode(1);
        TreeNode second = new TreeNode(2);
        TreeNode third = new TreeNode(1);
        TreeNode fourth = new TreeNode(5);
        TreeNode fifth = new TreeNode(3);
        root = first;
        first.left = second;
        first.right = fifth;
        second.right = fourth;
        return root;
    }

    public static void main(String[] args) {
        BinaryTreePaths_257 binaryTreePaths257 = new BinaryTreePaths_257();
        TreeNode binaryTree = binaryTreePaths257.createBinaryTree();
        List<String> paths = new ArrayList<>();
        dfs1(binaryTree, new StringBuilder(), paths);
    }

    public List<String> binaryTreePaths(TreeNode root) {
        List<String> result = new ArrayList<>();
        if (root == null) {
            return result;
        }

        String currentPath = Integer.toString(root.data);
        if (root.left == null && root.right == null) {
            result.add(currentPath);
        }
        if (root.left != null) {
            dfs(root.left, currentPath, result);
        }

        if (root.right != null) {
            dfs(root.right, currentPath, result);
        }
        return result;
    }

    public static void dfs(TreeNode node, String currentPath, List<String> result) {
        currentPath += "->" + node.data;
        if (node.left == null && node.right == null) {
            result.add(currentPath);
            return;
        }

        if (node.left != null) {
            dfs(node.left, currentPath, result);
        }

        if (node.right != null) {
            dfs(node.right, currentPath, result);
        }
    }

    public static void dfs1(TreeNode node, StringBuilder currentPath, List<String> result) {
        int len = currentPath.length();
        currentPath.append(node.data);
        if (node.left == null && node.right == null) {
            result.add(currentPath.toString());
        } else {
            currentPath.append("->");
            if (node.left != null) {
                dfs1(node.left, currentPath, result);
            }

            if (node.right != null) {
                dfs1(node.right, currentPath, result);
            }
        }
        currentPath.setLength(len);
    }
}
