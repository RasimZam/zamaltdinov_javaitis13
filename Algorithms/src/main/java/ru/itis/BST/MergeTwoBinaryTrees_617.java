package ru.itis.BST;

// 617. Merge Two Binary Trees

public class MergeTwoBinaryTrees_617 {
    private TreeNode root;

    private class TreeNode {
        private TreeNode left;
        private TreeNode right;
        private int data;

        public TreeNode(int data) {
            this.data = data;
        }
    }

    public TreeNode createBinaryTree1() {
        TreeNode first = new TreeNode(1);
        TreeNode second = new TreeNode(3);
        TreeNode third = new TreeNode(7);
        TreeNode fourth = new TreeNode(4);
        TreeNode fifth = new TreeNode(2);

        root = first;
        first.left = second;
        first.right = fifth;
        second.left = third;
        second.right = fourth;
        return root;
    }

    public TreeNode createBinaryTree2() {
        TreeNode first = new TreeNode(1);
        TreeNode second = new TreeNode(5);
        TreeNode third = new TreeNode(2);
        TreeNode fourth = new TreeNode(9);
        TreeNode fifth = new TreeNode(7);
        TreeNode sixth = new TreeNode(6);

        root = first;
        first.left = second;
        first.right = fourth;
        second.left = third;
        fourth.left = fifth;
        fourth.right = sixth;
        return root;
    }

    public static void main(String[] args) {
        MergeTwoBinaryTrees_617 mergeTwoBinary = new MergeTwoBinaryTrees_617();
        TreeNode binaryTree1 = mergeTwoBinary.createBinaryTree1();
        TreeNode binaryTree2 = mergeTwoBinary.createBinaryTree2();
        TreeNode result = mergeTrees(binaryTree1, binaryTree2);

    }

    public static TreeNode mergeTrees(TreeNode root1, TreeNode root2) {
        if (root1 == null) {
            return root2;
        }
        if (root2 == null) {
            return root1;
        }

        root1.data += root2.data;
        root1.left = mergeTrees(root1.left, root2.left);
        root1.right = mergeTrees(root1.right, root2.right);
        return root1;
    }
}
