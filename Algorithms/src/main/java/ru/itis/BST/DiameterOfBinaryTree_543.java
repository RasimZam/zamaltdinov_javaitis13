package ru.itis.BST;

// 543. Diameter of Binary Tree

import ru.itis.practice.example1.TreeNode;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class DiameterOfBinaryTree_543 {
    public static int longestPath = 0;
    private TreeNode root;

    private class TreeNode {
        private TreeNode left;
        private TreeNode right;
        private int data;

        public TreeNode(int data) {
            this.data = data;
        }
    }

    public TreeNode createBinaryTree() {
        TreeNode first = new TreeNode(1);
        TreeNode second = new TreeNode(2);
        TreeNode third = new TreeNode(4);
        TreeNode fourth = new TreeNode(5);
        TreeNode fifth = new TreeNode(3);

        root = first;
        first.left = second;
        first.right = fifth;
        second.left = third;
        second.right = fourth;
        return root;
    }

    public static void main(String[] args) {
        DiameterOfBinaryTree_543 diameter = new DiameterOfBinaryTree_543();
        TreeNode bst = diameter.createBinaryTree();
        int result = diameterOfBinaryTree1(bst);
        System.out.println(result);
    }

    public int diameterOfBinaryTree(TreeNode root) {
        Map<TreeNode, Integer> map = new HashMap<>();
        Stack<TreeNode> stack = new Stack<>();
        int diameter = 0;
        if (root != null) {
            stack.push(root);
        }
        while(!stack.isEmpty()) {
            TreeNode node = stack.peek();
            // Fill up stack perform post-order traversal
            if (node.left != null && !map.containsKey(node.left)) {
                stack.push(node.left);
            } else if (node.right != null && !map.containsKey(node.right)) {
                stack.push(node.right);
            } else {
                //Process the root, once left and right sub-tree have been processed
                stack.pop();
                int leftDepth = map.getOrDefault(node.left, 0);
                int rightDepth = map.getOrDefault(node.right, 0);
                //Put the max depth at a node in the map
                map.put(node, 1 + Math.max(leftDepth, rightDepth));
                //Update the max diameter found so far
                diameter = Math.max(diameter, leftDepth + rightDepth);
            }
        }
        return diameter;
    }

    public static int diameterOfBinaryTree1(TreeNode node) {
        if(node == null) {
            return 0;
        }

        int leftHeight = diameterOfBinaryTree1(node.left);
        int rightHeight = diameterOfBinaryTree1(node.right);
        longestPath = Math.max(longestPath, leftHeight + rightHeight);
        return Math.max(leftHeight, rightHeight) + 1;
    }
}
