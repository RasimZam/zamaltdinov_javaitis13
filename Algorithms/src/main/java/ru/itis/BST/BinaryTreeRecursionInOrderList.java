package ru.itis.BST;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreeRecursionInOrderList {
    private TreeNode root;

    private class TreeNode {
        private TreeNode left;
        private TreeNode right;
        private int data;

        public TreeNode(int data) {
            this.data = data;
        }
    }

    public void createBinaryTree() {
        TreeNode first = new TreeNode(9);
        TreeNode second = new TreeNode(2);
        TreeNode third = new TreeNode(3);
        TreeNode fourth = new TreeNode(4);

        root = first;
        first.left = second;
        first.right = third;
        second.left = fourth;
    }

    public List<Integer> inOrder(TreeNode root, List<Integer> array) {
        if (root.left != null) {
            inOrder(root.left, array);
        }
        array.add(root.data);
        if (root.right != null) {
            inOrder(root.right, array);
        }
        return array;
    }

    public static void main(String[] args) {
        BinaryTreeRecursionInOrderList binaryTree = new BinaryTreeRecursionInOrderList();
        binaryTree.createBinaryTree();
        List<Integer> result = binaryTree.inOrder(binaryTree.root, new ArrayList<>());
    }
}
