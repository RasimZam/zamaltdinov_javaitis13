package ru.itis.BST;

// 589. N-ary Tree Preorder Traversal

import ru.itis.model.Node;

import java.util.*;

public class NAryTreePreorderTraversal_589 {
    public List<Integer> preorder(Node root) {
        LinkedList<Node> stack = new LinkedList<>();
        LinkedList<Integer> ans = new LinkedList<>();
        if (root == null) {
            return ans;
        }

        stack.add(root);

        while (!stack.isEmpty()) {
            Node node = stack.pollLast();
            ans.add(node.val);
            Collections.reverse(node.children);
            for (Node child: node.children) {
                stack.add(child);
            }
        }
        return ans;
    }

    public List<Integer> preorder1(Node root) {
        List<Integer> ans = new ArrayList<>();
        if (root == null) {
            return ans;
        }
        Stack<Node> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            Node node = stack.pop();
            ans.add(node.val);

            for (int i = node.children.size() - 1; i >= 0; i--) {
                Node child = node.children.get(i);
                stack.push(child);
            }
        }
        return ans;
    }
}
