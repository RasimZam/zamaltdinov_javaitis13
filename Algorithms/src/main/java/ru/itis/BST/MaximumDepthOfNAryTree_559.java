package ru.itis.BST;

// 559. Maximum Depth of N-ary Tree

import ru.itis.model.Node;

public class MaximumDepthOfNAryTree_559 {
    public int maxDepth(Node root) {
        if (root == null) {
            return 0;
        }

        int height = 1;
        for (Node node: root.children) {
            height = Math.max(height, maxDepth(node));
        }
        return height + 1;
    }
}
