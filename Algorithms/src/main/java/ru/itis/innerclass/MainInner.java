package ru.itis.innerclass;

public class MainInner {
    public static void main(String[] args) {
        Letter letter = new Letter("Ivan");
        Letter.Address address = letter.new Address(1);
        System.out.println(address);
    }
}
