package ru.itis.enumuration;

public enum UserRole {
    ADMINISTRATOR,
    EDITOR,
    READER
}
