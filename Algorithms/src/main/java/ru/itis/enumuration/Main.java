package ru.itis.enumuration;

public class Main {
    public static void main(String[] args) {
        UserRole roleEditor = UserRole.EDITOR;
        UserRole roleReader = UserRole.READER;
        System.out.println(roleEditor == roleReader);

        System.out.println(roleEditor.getClass().getSuperclass());

    }
}
