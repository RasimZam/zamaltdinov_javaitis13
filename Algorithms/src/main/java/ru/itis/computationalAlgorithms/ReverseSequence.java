package ru.itis.computationalAlgorithms;

import java.util.Arrays;

public class ReverseSequence {
    public static void main(String[] args) {
        int[] array = new int[]{0, 5, -2, 3, 7};
        System.out.println(Arrays.toString(array));
        reverseArray(array);
        System.out.println(Arrays.toString(array));
    }

    private static void reverseArray(int[] array) {
        int startIndex = 0;
        int endIndex = array.length - 1;
        int middle = (startIndex + endIndex) / 2;
        for (int i = startIndex; i < middle; i++) {
            int temp = array[i];
            array[i] = array[endIndex + startIndex - i];
            array[endIndex + startIndex - i] = temp;
        }
    }
}
