package ru.itis.stackAndQueues;

// 1614. Maximum Nesting Depth of the Parentheses

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Stack;

public class MaximumNestingDepthOfTheParentheses_1614 {
    public static void main(String[] args) {
        String str = "(1+(2*3)+((8)/4))+1";
        int result = maxDepth1(str);
        System.out.println(result);
    }

    public static int maxDepth(String s) {
        Stack<String> stack = new Stack<>();
        int maxNumParentheses = 0;
        for (char element : s.toCharArray()) {
            if (element == '(') {
                stack.push("(");
                maxNumParentheses = Math.max(maxNumParentheses, stack.size());
            } else if (element == ')') {
                stack.pop();
            }
        }
        return maxNumParentheses;
    }

    public static int maxDepth1(String s) {
        int count = 0;
        int result = 0;
        char[] characters = s.toCharArray();
//        Deque<Character> stack = new ArrayDeque<>();
        for (char element : characters) {
            if (element == '(') {
//                stack.push(element);
                count++;
                result = Math.max(result, count);
            } else if (element == ')') {
//                stack.pop();
                count--;
            }
        }
        return result;
    }
}
