package ru.itis.stackAndQueues;

// 1544. Make The String Great

import java.util.Stack;

public class MakeTheStringGreat_1544 {
    public static void main(String[] args) {
        String s = "leEeetcode";
        String result = makeGood(s);
        System.out.println(result);
    }

    public static String makeGood(String s) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char element = s.charAt(i);
            if (stack.isEmpty()) {
                stack.add(element);
            } else {
                if (Character.toLowerCase(stack.peek()) == Character.toLowerCase(element)) {
                    if (Character.isLowerCase(stack.peek()) && Character.isUpperCase(element)) {
                        stack.pop();
                    } else if (Character.isLowerCase(element) && Character.isUpperCase(stack.peek())) {
                        stack.pop();
                    } else {
                        stack.add(element);
                    }
                } else {
                    stack.add(element);
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        while (!stack.isEmpty()) {
            sb.append(stack.pop());
        }
        return sb.reverse().toString();
    }
}
