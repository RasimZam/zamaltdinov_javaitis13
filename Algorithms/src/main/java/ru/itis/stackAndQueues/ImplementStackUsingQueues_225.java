package ru.itis.stackAndQueues;

// 225. Implement Stack using Queues

import java.util.LinkedList;
import java.util.Queue;

public class ImplementStackUsingQueues_225 {

    Queue<Integer> queue;

    public ImplementStackUsingQueues_225() {
        this.queue = new LinkedList<>();
    }

    public void push(int x) {
        queue.add(x);
        for (int i = 0; i < queue.size() - 1; i++) {
            queue.add(queue.poll());
        }
    }

    public int pop() {
        return queue.poll();
    }

    public int top() {
        return queue.peek();
    }

    public boolean empty() {
        return queue.isEmpty();
    }
}
