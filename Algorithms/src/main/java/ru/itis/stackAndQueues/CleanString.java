package ru.itis.stackAndQueues;

import java.util.ArrayDeque;
import java.util.Deque;

public class CleanString {
    public static void main(String[] args) {

    }

    public static String cleanString(String s) {
        Deque<Character> stack = new ArrayDeque<>();
        for(char c : s.toCharArray()) {
            if(c != '#') {
                stack.push(c);
            } else if (!stack.isEmpty()) {
                stack.pop();
            }
        }

        StringBuilder sb = new StringBuilder();
        while(!stack.isEmpty()) {
            sb.append(stack.removeLast());
        }
        return sb.toString();
    }
}
