package ru.itis.stackAndQueues;

// 71. Simplify Path

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Stack;

public class SimplifyPath_71 {
    public static void main(String[] args) {
//        String path = "/home//foo/";
        String path = "/.../a/../b/c/../d/./";
        String result = simplifyPath1(path);
        System.out.println(result);
    }

    public static String simplifyPath(String path) {
        Stack<String> stack = new Stack<>();
        String[] vals = path.split("/");

        for(String val : vals) {
            if(val.equals("..") && !stack.isEmpty()) {
                stack.pop();
            } else if(!val.equals(".") && !val.equals("..") && !val.equals("")) {
                stack.push(val);
            }
        }
        return "/" + String.join("/", stack);
    }

    public static String simplifyPath1(String path) {
        Deque<String> stack = new ArrayDeque<>();
        String[] directories = path.split("/");
        for(String dir : directories) {
            if (dir.isEmpty() || dir.equals(".")) {
                continue;
            }

            if (dir.equals("..")) {
                if (!stack.isEmpty()) {
                    stack.pop();
                }
            } else {
                stack.push(dir);
            }
        }
        return convertToString(stack);
    }

    public static String convertToString(Deque<String> stack) {
        StringBuilder result = new StringBuilder();
        while (!stack.isEmpty()) {
            result.append("/").append(stack.removeLast());
        }

        if(result.length() == 0) {
            result.append("/");
        }

        return result.toString();
    }
}
