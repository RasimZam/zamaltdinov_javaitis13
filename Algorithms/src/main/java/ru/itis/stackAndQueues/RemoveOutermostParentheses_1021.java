package ru.itis.stackAndQueues;

// 1021. Remove Outermost Parentheses

public class RemoveOutermostParentheses_1021 {
    public static void main(String[] args) {
        String str = "(()())(())";
        System.out.println(removeOuterParentheses(str));
    }

    public static String removeOuterParentheses(String s) {
        StringBuilder result = new StringBuilder();
        int opened = 0;

        for (char element: s.toCharArray()) {
            // (
            if (element == '(' && opened++ > 0) {
                result.append(element);
            }

            if (element == ')' && opened-- > 1) {
                result.append(element);
            }
        }
        return result.toString();
    }
}
