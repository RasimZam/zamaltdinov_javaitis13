package ru.itis.stackAndQueues;

// 20. Valid Parentheses

import java.util.Stack;

public class ValidParentheses_20 {
    public static void main(String[] args) {
        String s = "()[]{}";
        boolean valid = isValid(s);
    }

    public static boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();

        for(char c : s.toCharArray()) {
            if(c == '(') {
                stack.push(')');
            } else if (c == '{') {
                stack.push('}');
            } else if (c == '[') {
                stack.push(']');
            } else if (stack.isEmpty() || stack.pop() != c) {
                return false;
            }
        }
        return stack.isEmpty();
    }


    public static boolean isValid1(String s) {
        Stack<Character> stack = new Stack<>();

        for (char element : s.toCharArray()) {
            if (element == '(' || element == '[' || element == '{') {
                stack.push(element);
            } else {
                if (stack.isEmpty()) {
                    return false;
                }

                char top = stack.peek();
                if ((element == ')' && top == '(')
                        || (element == ']' && top == '[')
                        || (element == '}' && top == '{')) {
                    stack.pop();
                } else {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}
