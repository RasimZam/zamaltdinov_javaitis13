package ru.itis.stackAndQueues;

// 144. Binary Tree Preorder Traversal

import ru.itis.practice.example1.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class BinaryTreePreorderTraversal_144 {
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        LinkedList<TreeNode> list = new LinkedList<>();
        if (root == null) {
            return ans;
        }
        list.add(root);

        while(!list.isEmpty()) {
            TreeNode current = list.pollLast();
            ans.add(current.val);

            if (current.right != null) {
                list.add(current.right);
            }
            if (current.left != null) {
                list.add(current.left);
            }
        }
        return ans;
    }

    public List<Integer> preorderTraversal1(TreeNode root) {
        List<Integer> pre = new LinkedList<>();
        Stack<TreeNode> toVisit = new Stack<>();
        if (root == null) {
            return pre;
        }
        toVisit.push(root);
        while (!toVisit.isEmpty()) {
            TreeNode visiting = toVisit.pop();
            pre.add(visiting.val);
            if (visiting.right != null) {
                toVisit.push(visiting.right);
            }
            if (visiting.left != null) {
                toVisit.push(visiting.left);
            }
        }
        return pre;
    }
}
