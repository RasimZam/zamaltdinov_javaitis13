package ru.itis.stackAndQueues;

// 897. Increasing Order Search Tree

import ru.itis.practice.example1.TreeNode;

public class IncreasingOrderSearchTree_897 {

    TreeNode current = null;

    public TreeNode increasingBST(TreeNode root) {
        TreeNode first = new TreeNode(-1);
        current = first;
        dfs(root);
        return first.right;
    }

    public void dfs(TreeNode node) {
        if (node == null) {
            return;
        }
        dfs(node.left);
        current.right = new TreeNode(node.val);
        current = current.right;
        dfs(node.right);
    }
}
