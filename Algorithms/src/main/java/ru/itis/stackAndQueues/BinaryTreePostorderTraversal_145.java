package ru.itis.stackAndQueues;

// 145. Binary Tree Postorder Traversal

import ru.itis.practice.example1.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class BinaryTreePostorderTraversal_145 {
    public List<Integer> postorderTraversal(TreeNode root) {
        return dfs(root, new ArrayList<>());
    }

    private List<Integer> dfs(TreeNode root, List<Integer> list) {
        if (root == null) {
            return list;
        }
        list = dfs(root.left, list);
        list = dfs(root.right, list);
        list.add(root.val);
        return list;
    }

    public List<Integer> postorderTraversal1(TreeNode root) {
        List<Integer> res = new LinkedList<>();
        if (root == null) {
            return res;
        }
        // 1, null, 2, 3
        Stack<TreeNode> stack = new Stack<>();
        // 1
        stack.add(root);
        while(!stack.isEmpty()) {
            // 1
            // 2
            TreeNode top = stack.pop();
            // 2
            res.add(0, top.val);
            // null
            if (top.left != null) {
                stack.add(top.left);
            }
            if (top.right != null) {
                // 2
                stack.add(top.right);
            }
        }
        return res;
    }

    public List<Integer> postorderTraversal2(TreeNode root) {
        List<Integer> res = new LinkedList<>();
        if (root == null) {
            return res;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.add(root);
        while(!stack.isEmpty()) {
            TreeNode top = stack.peek();
            if (top.left == null && top.right == null) {
                res.add(top.val);
                stack.pop();
            } else {
                if (top.right != null) {
                    stack.add(top.right);
                    top.right = null;
                }
                if (top.left != null) {
                    stack.add(top.left);
                    top.left = null;
                }
            }
        }
        return res;
    }
}
