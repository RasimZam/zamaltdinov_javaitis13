package ru.itis.stackAndQueues;

// 232. Implement Queue using Stacks

import java.util.ArrayDeque;
import java.util.Deque;

public class ImplementQueueUsingStacks_232 {
    private Deque<Integer> inStk = new ArrayDeque<>();
    private Deque<Integer> outStk = new ArrayDeque<>();

    public ImplementQueueUsingStacks_232() {
    }

    public void push(int x) {
        inStk.push(x);
    }

    public int pop() {
        peek();
        return outStk.pop();
    }

    public int peek() {
        if (outStk.isEmpty()) {
            while (!inStk.isEmpty()) {
                outStk.push(inStk.pop());
            }
        }
        return outStk.peek();
    }

    public boolean empty() {
        return inStk.isEmpty() && outStk.isEmpty();
    }
}
