package ru.itis.workonstirng;

// LSD сортировка строк

import java.util.Arrays;

public class LSDSort {
    public static void main(String[] args) {
        String[] array = {"cat", "car", "dot", "dog"};
        String[] result = radixSort(array);
        System.out.println(Arrays.toString(result));

    }

    public static String[] countSorting(String[] arrays, int position) {
        int n = 256;
        int[] support = new int[n];
        for (String text : arrays) {
            support[text.codePointAt(position)]++;
        }
        int size = arrays.length;
        for (int i = support.length - 1; i >= 0 ; i--) {
            size -= support[i];
            support[i] = size;
        }
        String[] result = new String[arrays.length];
        for (String text : arrays) {
            int resultIndex = text.codePointAt(position);
            result[support[resultIndex]] = text;
            support[resultIndex]++;
        }
        return result;
    }

    public static String[] radixSort(String[] arrays) {
        int maxLenght = arrays[0].length();
        for (int i = maxLenght - 1; i >= 0 ; i--) {
            arrays = countSorting(arrays, i);
        }
        return arrays;
    }
}
