package ru.itis.workonstirng;

//1816. Truncate Sentence

public class TruncateSentence_1816 {
    public static void main(String[] args) {
        String str = "Hello how are you Contestant";
        int k = 4;
        String result = truncateSentence(str, k);
        System.out.println(result);
    }

    public static String truncateSentence(String s, int k) {
        StringBuilder sb = new StringBuilder();
        String[] arrayString = s.split(" ");
        int count = 0;

        while (count < k) {
            sb.append(arrayString[count]).append(" ");
            count++;
        }
        return sb.toString().trim();
    }
}
