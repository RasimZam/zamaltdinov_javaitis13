package ru.itis.workonstirng;

// 1684. Count the Number of Consistent Strings

import java.util.HashSet;
import java.util.Set;

public class CountTheNumberOfConsistentStrings_1684 {
    public static void main(String[] args) {
        String allowed = "ab";
        String[] words = {"ad", "bd", "aaab", "baa", "badab"};

        int result = countConsistentStrings(allowed, words);
        System.out.println(result);
    }

    public static int countConsistentStrings(String allowed, String[] words) {
        Set<Character> set = new HashSet<>();
        for (char value: allowed.toCharArray()) {
            set.add(value);
        }

        int count = 0;
        for (String word: words) {
            count++;
            for (char value: word.toCharArray()) {
                if (!set.contains(value)) {
                    count--;
                    break;
                }
            }
        }

        return count;
    }
}
