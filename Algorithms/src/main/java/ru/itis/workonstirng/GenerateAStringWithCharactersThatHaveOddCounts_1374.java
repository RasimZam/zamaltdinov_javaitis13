package ru.itis.workonstirng;

// 1374. Generate a String With Characters That Have Odd Counts

public class GenerateAStringWithCharactersThatHaveOddCounts_1374 {
    public static void main(String[] args) {
        String result = generateTheString(4);
        System.out.println(result);
    }

    public static String generateTheString(int n) {
        StringBuilder res = new StringBuilder();

        if (n % 2 == 0) {
            res.append('a');
            n--;
        }

        while (n-- > 0) {
            res.append('b');
        }
        return res.toString();
    }
}
