package ru.itis.workonstirng;

// 709. To Lower Case

public class ToLowerCase_709 {
    public static void main(String[] args) {
        String str = "Hello";
        String str1 = "here";
        System.out.println(toLowerCase(str1));
    }

    public static String toLowerCase(String s) {
        return s.toLowerCase();
    }
}
