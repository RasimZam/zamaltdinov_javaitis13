package ru.itis.workonstirng;

//Поиск подстроки. Метод грубой силы

public class FindSubstring {
    public static void main(String[] args) {
        String text = "Little cat, little cat, i have no a flat.";
        String substring = "cat";
        int i = 0;
        while (true) {
            i = substringSearch(text, substring, i);
            if (i == -1) {
                break;
            } else {
                System.out.println(i);
                i++;
            }
        }
    }

    private static int substringSearch(String text, String substring, int i) {
        while (true) {
            if (i > text.length() - substring.length()) {
                return -1;
            }
            if (text.substring(i, i + substring.length()).equals(substring)) {
                return i;
            }
            i++;
        }
    }
}
