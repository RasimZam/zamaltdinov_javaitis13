package ru.itis.workonstirng;

// 1678. Goal Parser Interpretation

public class GoalParserInterpretation_1678 {
    public static void main(String[] args) {
        String command = "G()(al)";
        String result = interpret(command);
        System.out.println(result);

    }

    public static String interpret(String command) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < command.length()) {
            if (command.charAt(i) == 'G') {
                sb.append('G');
            } else if (command.charAt(i) == '(') {
                if (command.charAt(i + 1) == ')') {
                    sb.append('o');
                } else {
                    sb.append("al");
                    i = i + 3;
                }
            }
            i++;
        }
        return sb.toString();
    }
}
