package ru.itis.workonstirng;

// 2011. Final Value of Variable After Performing Operations

public class FinalValueOfVariableAfterPerformingOperations_2011 {
    public static void main(String[] args) {
        String[] operations = new String[]{"--X", "X++", "X++"};
        int result = finalValueAfterOperations(operations);
        System.out.println(result);
    }

    public static int finalValueAfterOperations(String[] operations) {
        int value = 0;
        for (String operation: operations) {
            value += operation.charAt(1) == '+' ? 1 : -1;
        }
        return value;
    }
}
