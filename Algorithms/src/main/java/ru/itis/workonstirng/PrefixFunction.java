package ru.itis.workonstirng;

// Префикс-функция

import java.util.Arrays;

public class PrefixFunction {
    public static void main(String[] args) {
        String text = "avada kedavra";
        System.out.println(Arrays.toString(prefixFunction(text)));
    }

    private static int[] prefixFunction(String text) {
        int n = text.length();
        int[] pi = new int[n];
        for (int i = 1; i < pi.length; i++) {
            int j = pi[i - 1];
            while (j > 0 && text.charAt(j) != text.charAt(i)) {
                j = pi[j - 1];
            }
            if (text.charAt(i) == text.charAt(j)) {
                j++;
            }
            pi[i] = j;
        }
        return pi;
    }
}
