package ru.itis.workonstirng;

// 1221. Split a String in Balanced Strings

public class SplitAStringInBalancedStrings_1221 {
    public static void main(String[] args) {
        String str = "RLRRLLRLRL";
        System.out.println(balancedStringSplit(str));
    }

    public static int balancedStringSplit(String s) {
        int result = 0;
        int sum = 0;

        for (char letter : s.toCharArray()) {
            sum += (letter == 'R' ? 1 : -1);
            if (sum == 0) {
                result++;
            }
        }
        return  result;
    }

}
