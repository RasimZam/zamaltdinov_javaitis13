package ru.itis.workonstirng;

// 1108. Defanging an IP Address

public class DefangingAnIPAddress_1108 {
    public static void main(String[] args) {
        String address = "1.1.1.1";
        String address1 = "255.100.50.0";
        String result = defangIPaddr(address1);
        System.out.println(result);
    }

    public static String defangIPaddr(String address) {
       return address.replace(".", "[.]");
    }

    public static String defangIPaddr1(String address) {
        return String.join("[.]", address.split("\\."));
    }

    public static String defangIPaddr2(String address) {
        return address.replaceAll("\\.", "[.]");
    }

    public static String defangIPaddr3(String address) {
        StringBuilder sb = new StringBuilder();
        for (char c : address.toCharArray()) {
            sb.append(c == '.' ? "[.]" : c);
        }
        return sb.toString();
    }
}
