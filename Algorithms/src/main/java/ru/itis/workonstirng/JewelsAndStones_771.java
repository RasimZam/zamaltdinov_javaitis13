package ru.itis.workonstirng;

// 771. Jewels and Stones

import java.util.Set;
import java.util.stream.Collectors;

public class JewelsAndStones_771 {
    public static void main(String[] args) {
        String jewels = "aA";
        String stones = "aAAbbbb";
        int result = numJewelsInStones1(jewels, stones);
        System.out.println(result);
    }

    public static int numJewelsInStones(String jewels, String stones) {
        int num = 0;
        for (int i = 0; i < stones.length(); i++) {
            if (jewels.indexOf(stones.charAt(i)) != -1) {
                num++;
            }
        }
        return num;
    }

    public static int numJewelsInStones1(String jewels, String stones) {
        int ans = 0;
        Set<Character> jewelsSet = jewels.chars()
                .mapToObj(c -> (char) c).collect(Collectors.toSet());

        for (final char stone : stones.toCharArray())
            if (jewelsSet.contains(stone)) {
                ++ans;
            }
        return ans;
    }
}
