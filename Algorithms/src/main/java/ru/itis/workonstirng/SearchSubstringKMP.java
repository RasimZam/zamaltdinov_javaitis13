package ru.itis.workonstirng;

// Поиск подстроки. Алгоритм Кнута-Морриса-Пратта

public class SearchSubstringKMP {
    public static void main(String[] args) {
        String text = "AAATGAACGAAAATCTGT";
        String subText = "ACGA";
        System.out.println(kmpSearch(text, subText, 0));
    }

    private static int kmpSearch(String text, String subText, int startIndex) {
        int j = 0;
        int[] pi = prefixFunction(subText);
        for (int i = startIndex; i < text.length(); i++) {
            while (j > 0 && text.charAt(i) != subText.charAt(j)) {
                j = pi[j - 1];
            }
            if (text.charAt(i) == subText.charAt(j)) {
                j++;
            }
            if (j >= subText.length()) {
                return i - j + 1;
            }
        }
        return -1;
    }

    private static int[] prefixFunction(String text) {
        int n = text.length();
        int[] pi = new int[n];
        for (int i = 1; i < pi.length; i++) {
            int j = pi[i - 1];
            while (j > 0 && text.charAt(j) != text.charAt(i)) {
                j = pi[j - 1];
            }
            if (text.charAt(i) == text.charAt(j)) {
                j++;
            }
            pi[i] = j;
        }
        return pi;
    }
}
