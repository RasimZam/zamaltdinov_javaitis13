package ru.itis.workonstirng;

// 2710. Remove Trailing Zeros From a String

public class RemoveTrailingZeros_2710 {
    public static void main(String[] args) {
        String num = "51230100";
        String result = removeTrailingZeros1(num);
        System.out.println(result);
    }

    public static String removeTrailingZeros(String num) {
        char[] elements = num.toCharArray();

        int count = 0;
        for (int i = elements.length - 1; i >= 0; i--) {
            if (elements[i] == '0') {
                count++;
            } else {
                break;
            }
        }
        return num.substring(0, num.length() - count);
    }

    public static String removeTrailingZeros1(String num) {
        return num.replaceAll("0+$", "");
    }
}
