package ru.itis.workonstirng;

// Алгоритм поиска подстраки Бойера-Мура-Хорспула

public class SearchSubstringBMX {
    public static void main(String[] args) {
        String substring = "apple";
        String text = "awersome apple";
        System.out.println(bmhSearch(text, substring));
    }

    public static int[] preprocess(String substring, int startIndex, int endIndex) {
        int[] alphabetTable = new int[endIndex - startIndex + 1];
        for (int i = 0; i < alphabetTable.length; i++) {
            alphabetTable[i] = substring.length();
        }
        char[] symbols = substring.toCharArray();
        for (int i = 0; i < symbols.length - 1; i++) {
            alphabetTable[symbols[i] - startIndex] = substring.length() - i - 1;
        }
        return alphabetTable;
    }

    public static int bmhSearch(String text, String substring) {
        if (substring.length() > text.length()) {
            return -1;
        }
        int startIndex = ' ';
        int endIndex = '~';
        int[] alphabetTable = preprocess(substring, startIndex, endIndex);
        int i = substring.length() - 1;
        int n = i;
        while (i < text.length()) {
            if (text.substring(i - n, i + 1).equals(substring)) {
                return i - n;
            }
            i = i + alphabetTable[text.charAt(i) - startIndex];
        }
        return -1;
    }
}
