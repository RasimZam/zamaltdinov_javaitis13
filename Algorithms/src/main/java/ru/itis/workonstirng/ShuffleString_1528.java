package ru.itis.workonstirng;

// 1528. Shuffle String

public class ShuffleString_1528 {
    public static void main(String[] args) {
        String str = "codeleet";
        int[] indices = {4, 5, 6, 7, 0, 2, 1, 3};
        System.out.println(restoreString(str, indices));
    }

    public static String restoreString(String s, int[] indices) {
        StringBuilder sb = new StringBuilder();
        char[] chars = new char[s.length()];

        for (int i = 0; i < s.length(); i++) {
            chars[indices[i]] = s.charAt(i);
        }
        return sb.append(chars).toString();
    }
}
