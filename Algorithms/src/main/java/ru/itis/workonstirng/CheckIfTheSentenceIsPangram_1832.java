package ru.itis.workonstirng;

// 1832. Check if the Sentence Is Pangram

import java.util.HashSet;
import java.util.Set;

public class CheckIfTheSentenceIsPangram_1832 {
    public static void main(String[] args) {
        String str = "thequickbrownfoxjumpsoverthelazydog";
        boolean result = checkIfPangram1(str);
        System.out.println(result);
    }

    public static boolean checkIfPangram(String sentence) {
        boolean[] letters = new boolean[26];

        for (char c : sentence.toCharArray()) {
            letters[c - 'a'] = true;
        }

        for(boolean existLetter : letters) {
            if(!existLetter) return false;
        }

        return true;
    }

    public static boolean checkIfPangram1(String sentence) {
        Set<Character> seen = new HashSet<>();

        for (char c : sentence.toCharArray()) {
            seen.add(c);
        }
        return seen.size() == 26;
    }
}
