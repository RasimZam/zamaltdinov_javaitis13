package ru.itis.workonstirng;

// 1773. Count Items Matching a Rule

import java.util.List;

public class CountItemsMatchingARule_1773 {
    public static void main(String[] args) {
        List<String> listItem = List.of("phone","blue","pixel","computer","silver","lenovo","phone","gold","iphone");
        List<List<String>> items = List.of(listItem);
        String ruleKey = "color";
        String ruleValue = "silver";

    }

    public static int countMatches(List<List<String>> items, String ruleKey, String ruleValue) {
        if (ruleKey.equals("type")) {
            return count(items, 0, ruleValue);
        }
        if (ruleKey.equals("color")) {
            return count(items, 1, ruleValue);
        }
        return count(items, 2, ruleValue);
    }

    private static int count(List<List<String>> items, int index, String ruleValue) {
        return (int) items.stream()
                .map(item -> item.get(index))
                .filter(s -> s.equals(ruleValue))
                .count();
    }

}
