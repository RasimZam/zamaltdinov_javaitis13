package ru.itis.workonstirng;

// 1859. Sorting the Sentence

import java.util.Arrays;

public class SortingTheSentence_1859 {
    public static void main(String[] args) {
        String str = "is2 sentence4 This1 a3";
        String result = sortSentence1(str);
        System.out.println(result);
    }

    public static String sortSentence(String s) {
        String[] str = s.split(" ");
        String[] res = new String[str.length];
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (String elem : str) {
            i = (int) (elem.charAt(elem.length() - 1) - '0');
            res[i - 1] = elem.substring(0, elem.length() - 1);
        }

        for (i = 0; i < res.length; i++) {
            sb.append(res[i]).append(" ");
        }
        return sb.toString().trim();
    }

    public static String sortSentence1(String s) {
        String[] words = s.split(" ");
        Arrays.sort(words, (a, b) -> a.charAt(a.length() - 1) - b.charAt(b.length() -1));

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < words.length; i++) {
            sb.append(words[i], 0, words[i].length() - 1).append(" ");
        }
        return sb.toString().trim();
    }
}
