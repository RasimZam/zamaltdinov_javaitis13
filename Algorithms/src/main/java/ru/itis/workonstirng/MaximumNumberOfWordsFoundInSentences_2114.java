package ru.itis.workonstirng;

// 2114. Maximum Number of Words Found in Sentences

import java.util.stream.Stream;

public class MaximumNumberOfWordsFoundInSentences_2114 {
    public static void main(String[] args) {
        String[] sentences = {"alice and bob love leetcode", "i think so too", "this is great thanks very much"};
        int result = mostWordsFound1(sentences);
        System.out.println(result);
    }

    public static int mostWordsFound(String[] sentences) {
        int maxLength = 0;

        for (String currentSentence : sentences) {
            int currenLength = currentSentence.split(" ").length;
            if (maxLength < currenLength) {
                maxLength = currenLength;
            }
        }
        return maxLength;
    }

    public static int mostWordsFound1(String[] sentences) {
        return 1 + Stream.of(sentences)
                .mapToInt(s -> (int) s.chars().filter(c -> c == ' ').count())
                .max()
                .getAsInt();
    }
}
