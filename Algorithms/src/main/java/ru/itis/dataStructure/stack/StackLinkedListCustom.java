package ru.itis.dataStructure.stack;

public class StackLinkedListCustom {
    private class Node {
        String date;
        Node next;

        public Node(String date, Node next) {
            this.date = date;
            this.next = next;
        }

        public Node() {
        }
    }

    private Node head;

    public StackLinkedListCustom() {
    }

    public void push(String value) {
        Node newNode = new Node(value, head);
        head = newNode;
    }

    public String pop() {
        if (head != null) {
            String result = head.date;
            head = head.next;
            return result;
        }
        return null;
    }

    public long size() {
        long size = 0;
        for (Node currentNode = head; currentNode != null; currentNode = currentNode.next) {
            size++;
        }
        return size;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Node currentNode = head; currentNode != null; currentNode = currentNode.next) {
            result.append(currentNode.date).append(" -> ") ;
        }
        return result.toString();
    }

}
