package ru.itis.dataStructure.stack;

public class Main1 {
    public static void main(String[] args) {
        StackLinkedListCustom stack = new StackLinkedListCustom();
        stack.push("Word");
        stack.push("Java");
        stack.push("Hello");
        System.out.println(stack);
        String temp = stack.pop();
        System.out.println(stack);
    }
}
