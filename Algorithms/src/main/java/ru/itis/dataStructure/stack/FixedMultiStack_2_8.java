package ru.itis.dataStructure.stack;

import java.util.EmptyStackException;

public class FixedMultiStack_2_8 {
    private int numberOfStacks = 3;
    private int stackCapacity;
    private int[] values;
    private int[] sizes;

    public FixedMultiStack_2_8(int stackSize) {
        this.stackCapacity = stackSize;
        this.values = new int[stackSize * numberOfStacks];
        this.sizes = new int[numberOfStacks];
    }

    /*Занесение значения в стек*/
    public void push(int stackNum, int value) {
        /*Проверка наличия места следующего элемента*/
        if (isFull(stackNum)) {
            throw new EmptyStackException();
        }
        /*Увелечение указателя стека с последующим обновлением вершины*/
        sizes[stackNum]++;
        values[indexOfTop(stackNum)] = value;
    }

    /*Извлечение элемента с вершины стека*/
    public int pop(int stackNum) {
        if (isEmpty(stackNum)) {
            throw new EmptyStackException();
        }
        int topIndex = indexOfTop(stackNum);
        int value = values[topIndex]; // Получение вершины
        values[topIndex] = 0; //Очистка
        sizes[stackNum]--; //Сокращение
        return value;
    }

    /*Return top element*/
    public int peek(int stackNum) {
        if (isEmpty(stackNum)) {
            throw new EmptyStackException();
        }
        return values[indexOfTop(stackNum)];
    }

    /*Проверка пустого стека*/
    public boolean isEmpty(int stackNum) {
        return sizes[stackNum] == 0;
    }

    /*Проверка заполненного стека*/
    public boolean isFull(int stackNum) {
        return sizes[stackNum] == stackCapacity;
    }

    /*Получение индекса вершины*/
    private int indexOfTop(int stackNum) {
        int offset = stackNum * stackCapacity;
        int size = sizes[stackNum];
        return offset + size - 1;
    }
}
