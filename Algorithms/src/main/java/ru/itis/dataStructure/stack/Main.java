package ru.itis.dataStructure.stack;

public class Main {
    public static void main(String[] args) {
        StackArrayCustom stack = new StackArrayCustom();
        stack.push("Hello");
        stack.push("Java");
        stack.push("stack");

        System.out.println(stack);

        while (stack.peek() != null) {
            System.out.println(stack.pop());
        }

        System.out.println(stack);
    }
}
