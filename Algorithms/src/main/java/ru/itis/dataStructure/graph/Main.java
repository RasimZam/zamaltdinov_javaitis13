package ru.itis.dataStructure.graph;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Graph graph = new Graph();
        graph.addNode("a");
        graph.addNode("b");
        graph.addNode("c");
        graph.addNode("d");
        graph.addNode("e");

        graph.addEdge("a", "b");
        graph.addEdge("a", "e");
        graph.addEdge("a", "d");
        graph.addEdge("a", "c");
        graph.addEdge("c", "b");

        System.out.println(graph);
//        graph.removeEdge("b", "c");
//        System.out.println(graph);
//        graph.removeNode("b");
//        System.out.println(graph);

        System.out.println(graph.isAdjacent("b", "c"));
        System.out.println(Arrays.toString(graph.getAdjacentNodesId("a")));
    }
}
