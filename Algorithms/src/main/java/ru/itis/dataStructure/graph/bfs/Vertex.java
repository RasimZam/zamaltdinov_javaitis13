package ru.itis.dataStructure.graph.bfs;

public class Vertex {
    public char label;
    public boolean wasVisited;

    public Vertex(char lab) {
        this.label = lab;
        this.wasVisited = false;
    }
}
