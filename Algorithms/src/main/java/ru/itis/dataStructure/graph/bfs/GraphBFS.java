package ru.itis.dataStructure.graph.bfs;

public class GraphBFS {
    private final int MAX_VERTS = 20;
    private Vertex vertexList[]; // массив вершин
    private int addjMat[][]; // матрица смежности
    private int nVerts; // Текущее количество вершин
    private Queue theQueue;

    public GraphBFS() {
        vertexList = new Vertex[MAX_VERTS]; // матрица смежности
        addjMat = new int[MAX_VERTS][MAX_VERTS];
        nVerts = 0;
        for (int j = 0; j < MAX_VERTS; j++) { // матрица смежности заполняется нулями
            for (int k = 0; k < MAX_VERTS; k++) {
                addjMat[j][k] = 0;
            }
        }
        this.theQueue = new Queue();
    }

    public void addVertex(char lab) { // в аргументе передается метка
        vertexList[nVerts++] = new Vertex(lab);
    }

    public void addEdge(int start, int end) {
        addjMat[start][end] = 1;
        addjMat[end][start] = 1;
    }

    public void displayVertex(int v) {
        System.out.println(vertexList[v].label);
    }

    // обход в ширину
    public void bfs() {
        vertexList[0].wasVisited = true; // Алгоритм начинается с вершины 0, пометка
        displayVertex(0); // вывод
        theQueue.insert(0); // вставка в конец очереди
        int v2;

        // пока очередь не опустеет
        while (!theQueue.isEmpty()) {
            // извлечение вершины в начале очереди пока остаются непосещенные соседи
            int v1 = theQueue.remove();
            while ((v2 = getAdjUnvisitedVertex(v1)) != -1) {
                vertexList[v2].wasVisited = true; // получение вершины пометка
                displayVertex(v2); // вывод
                theQueue.insert(v2); // вставка
            }
        }
        // очередь пуста, обход закончен
        for (int j = 0; j < nVerts; j++) { //сброс флагов
            vertexList[j].wasVisited = false;
        }
    }

    // метод возвращает непосещенную вершину, смежную по отношению к v
    private int getAdjUnvisitedVertex(int v) {
        for (int j = 0; j < nVerts; j++) {
            if (addjMat[v][j] == 1 && vertexList[j].wasVisited == false) {
                return j; // возвращает первую найденную вершину
            }
        }
        return -1; // таких вершин нет
    }
}
