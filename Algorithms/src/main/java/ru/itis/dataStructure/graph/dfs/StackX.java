package ru.itis.dataStructure.graph.dfs;

public class StackX {
    private final int SIZE = 20;
    private int[] st;
    private int top;

    public StackX() {
        this.st = new int[SIZE];
        this.top = -1;
    }

    // размещение элемента в стеке
    public void push(int j) {
        st[++top] = j;
    }

    // Извлечение элемента из стека
    public int pop() {
        return st[top--];
    }

    // Чтение с вершины стека
    public int peek() {
        return st[top];
    }

    // true, если стек пуст
    public boolean isEmpty() {
        return top == -1;
    }
}
