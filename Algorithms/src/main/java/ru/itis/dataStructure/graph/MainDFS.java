package ru.itis.dataStructure.graph;

public class MainDFS {
    public static void main(String[] args) {
        GraphDFS graph = new GraphDFS();
        graph.addNode("a");
        graph.addNode("b");
        graph.addNode("c");
        graph.addNode("d");
        graph.addNode("e");

        graph.addEdge("a", "b");
        graph.addEdge("a", "e");
        graph.addEdge("a", "d");
        graph.addEdge("a", "c");
        graph.addEdge("c", "b");

        System.out.println(graph);
        System.out.println(graph.isConnectedGraph());
        graph.removeEdge("a", "d");
        System.out.println(graph);
        System.out.println(graph.isConnectedGraph());
    }
}
