package ru.itis.dataStructure.graph;

public class GraphExample {
    private final int MAX_VERTS = 20;
    private Vertex vertexList[]; // массив вершин
    private int addjMat[][]; // матрица смежности
    private int nVerts; // Текущее количество вершин

    public GraphExample() {
        vertexList = new Vertex[MAX_VERTS]; // матрица смежности
        addjMat = new int[MAX_VERTS][MAX_VERTS];
        nVerts = 0;
        for (int j = 0; j < MAX_VERTS; j++) { // матрица смежности заполняется нулями
            for (int k = 0; k < MAX_VERTS; k++) {
                addjMat[j][k] = 0;
            }
        }
    }

    static class Vertex {
        public char label;
        public boolean wasVisited;

        public Vertex(char lab) {
            this.label = lab;
            this.wasVisited = false;
        }
    }

    public void addVertex(char lab) { // в аргументе передается метка
        vertexList[nVerts++] = new Vertex(lab);
    }

    public void addEdge(int start, int end) {
        addjMat[start][end] = 1;
        addjMat[end][start] = 1;
    }

    public void displayVertex(int v) {
        System.out.println(vertexList[v].label);
    }
}
