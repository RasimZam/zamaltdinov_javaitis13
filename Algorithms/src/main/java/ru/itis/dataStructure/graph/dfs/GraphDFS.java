package ru.itis.dataStructure.graph.dfs;

public class GraphDFS {
    private final int MAX_VERTS = 20;
    private Vertex vertexList[]; // массив вершин
    private int addjMat[][]; // матрица смежности
    private int nVerts; // Текущее количество вершин
    private StackX theStack;

    public GraphDFS() {
        vertexList = new Vertex[MAX_VERTS]; // матрица смежности
        addjMat = new int[MAX_VERTS][MAX_VERTS];
        nVerts = 0;
        for (int j = 0; j < MAX_VERTS; j++) { // матрица смежности заполняется нулями
            for (int k = 0; k < MAX_VERTS; k++) {
                addjMat[j][k] = 0;
            }
        }
        this.theStack = new StackX();
    }

    public void addVertex(char lab) { // в аргументе передается метка
        vertexList[nVerts++] = new Vertex(lab);
    }

    public void addEdge(int start, int end) {
        addjMat[start][end] = 1;
        addjMat[end][start] = 1;
    }

    public void displayVertex(int v) {
        System.out.println(vertexList[v].label);
    }

    // обход в глубину
    public void dfs() {
        vertexList[0].wasVisited = true; // Алгоритм начинается с вершины 0, пометка
        displayVertex(0); // вывод
        theStack.push(0); // занесение в стек

        // пока стек не опустеет
        while (!theStack.isEmpty()) {
            // получение непосещенной вершины, смежной к текущей
            int v = getAdjUnvisitedVertex(theStack.peek());
            if (v == -1) {
                theStack.pop(); // Если такой вершины нет, элемент извлекается из стека. Если вершина найдена
            } else {
                vertexList[v].wasVisited = true; // Пометка
                displayVertex(v); // вывод
                theStack.push(v); // занесение в стек
            }
        }
        // стек пуст, работа закончена
        for (int j = 0; j < nVerts; j++) { //сброс флагов
            vertexList[j].wasVisited = false;
        }
    }

    // метод возвращает непосещенную вершину, смежную по отношению к v
    private int getAdjUnvisitedVertex(int v) {
        for (int j = 0; j < nVerts; j++) {
            if (addjMat[v][j] == 1 && vertexList[j].wasVisited == false) {
                return j; // возвращает первую найденную вершину
            }
        }
        return -1; // таких вершин нет
    }
}
