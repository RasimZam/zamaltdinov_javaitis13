package ru.itis.dataStructure.doublyLinkedList;

public class DoublyLinkedListCustom {
    private class Node {
        Object data;
        Node next;
        Node prev;

        public Node(Object data, Node next, Node prev) {
            this.data = data;
            this.next = next;
            this.prev = prev;
        }

        public Node() {
        }

        @Override
        public String toString() {
            return "Node [data=" + data + ", next=" + next + ", prev=" + prev + "]";
        }
    }

    private final Node head;
    private final Node tail;
    private long length = 0;

    public DoublyLinkedListCustom() {
        head = new Node();
        tail = new Node();
        head.next = tail;
        tail.prev = head;
    }

    public void addFirst(Object value) {
        Node newNode = new Node(value, head.next, head);
        head.next.prev = newNode;
        head.next = newNode;
        length++;
    }

    public void addLast(Object value) {
        Node newNode = new Node(value, tail, tail.prev);
        tail.prev.next = newNode;
        tail.prev = newNode;
        length++;
    }

    public void removeFirst() {
        if (isEmpty()) {
            return;
        }
        Node removeNode = head.next;
        head.next = removeNode.next;
        removeNode.next = null;
        removeNode.prev = null;
        length--;
    }

    public void removeLast() {
        if (isEmpty()) {
            return;
        }
        Node removeNode = tail.prev;
        tail.prev = removeNode.prev;
        removeNode.prev.next = tail;
        removeNode.next = null;
        removeNode.prev = null;
        length--;
    }

    public boolean isEmpty() {
        return head.next == tail;
    }

    public Node getNodeByIndex(long index) {
        Node resultNode = null;
        if (index < length / 2) {
            long nodeIndex = 0;
            resultNode = head.next;
            while (nodeIndex != index) {
                resultNode = resultNode.next;
                nodeIndex++;
            }
        } else {
            long nodeIndex = length - 1;
            resultNode = tail.prev;
            while (nodeIndex != index) {
                resultNode = resultNode.prev;
                nodeIndex--;
            }
        }
        return resultNode;
    }

    public Object getByIndex(long index) {
        if (index < 0 || index >= length) {
            throw new IndexOutOfBoundsException();
        }

        Node resultNode = getNodeByIndex(index);
        return resultNode.data;
    }

    public void removeByIndex(long index) {
        if (index < 0 || index >= length) {
            throw new IndexOutOfBoundsException();
        }

        Node resultNode = getNodeByIndex(index);
        resultNode.prev.next = resultNode.next;
        resultNode.next.prev = resultNode.prev;
        resultNode.next = null;
        resultNode.prev = null;
        length--;
    }

    public void insertByIndex(long index, Object value) {
        if (index < 0 || index >= length) {
            throw new IndexOutOfBoundsException();
        }

        Node resultNode = getNodeByIndex(index);
        Node newNode = new Node(value, resultNode, resultNode.prev);
        resultNode.prev.next = newNode;
        resultNode.prev = newNode;
        length++;
    }

    public void setByIndex(long index, Object value) {
        if (index < 0 || index >= length) {
            throw new IndexOutOfBoundsException();
        }
        Node resultNode = getNodeByIndex(index);
        resultNode.data = value;
    }

    public long getLength() {
        long length = 0;
        Node currentNode = head.next;
        while (currentNode != tail) {
            length++;
            currentNode = currentNode.next;
        }
        return length;
    }

    public void clear() {
        while (length > 0) {
            removeFirst();
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        Node currentNode = head.next;
        while (currentNode != tail) {
            stringBuilder.append(currentNode.data).append(" ");
            currentNode = currentNode.next;
        }
        return stringBuilder.append("]").toString();
    }
}
