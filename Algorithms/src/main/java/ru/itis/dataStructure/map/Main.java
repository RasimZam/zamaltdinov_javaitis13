package ru.itis.dataStructure.map;

public class Main {
    public static void main(String[] args) {
        HashMapCustom<String ,String> map = new HashMapCustom<String, String>();
        map.put("A", "B");
        map.put("E", "F");
        map.put("H", "P");
        map.put("P", "2");
        map.put("1", "G");
        map.put("2", "6");
        map.put("3", "2");
        map.put("4", "4");
        map.put("1", "H");

        System.out.println(map);
    }
}
