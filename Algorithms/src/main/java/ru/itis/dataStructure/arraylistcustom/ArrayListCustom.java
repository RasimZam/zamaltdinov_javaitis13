package ru.itis.dataStructure.arraylistcustom;

import java.util.Collection;

public class ArrayListCustom {
    private int size = 0;
    private int capacity = 10;
    private Object[] array;

    public ArrayListCustom() {
        array = new Object[capacity];
    }

    public ArrayListCustom(int newCapacity) {
        this.capacity = newCapacity;
        array = new Object[capacity];
    }

    public boolean add(Object element) {
        if (size < array.length) {
            array[size] = element;
            size++;
            return true;
        } else {
            capacity = capacity * 3 / 2 + 1;
            Object[] newArray = new Object[capacity];
            System.arraycopy(array, 0, newArray, 0, newArray.length);
            newArray[size] = element;
            array = newArray;
            size++;
            return true;

        }
    }

    public void addElement(int index, Object element) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException("Index " + index + ", size: " + size);
        } else if (index == this.size) {
            add(element);
        } else {
            Object[] newArray = new Object[array.length];
            System.arraycopy(array, 0, newArray, 0, index);
            newArray[index] = element;
            System.arraycopy(array, index, newArray, index + 1, this.size - index);
            array = newArray;
            size++;
        }
    }

    public boolean addAll(Collection collection) {
        Object[] newArray = collection.toArray();
        for (int i = 0; i < array.length; i++) {
            add(newArray[i]);
        }
        return true;
    }

    public boolean remove(Object element) {
        if (element != null) {
            for (int i = 0; i < size; i++) {
                if (array[i].equals(element)) {
                    int numMoved = size - i - 1;
                    if (numMoved > 0) {
                        System.arraycopy(array, i + 1, array, i, numMoved);
                        array[--size] = null;
                        return true;
                    }

                }
            }
        }
        return false;
    }

    public Object get(int index) {
        if (index >= this.size) {
            throw new IndexOutOfBoundsException();
        }
        return array[index];
    }

    public Object set(int index, Object element) {
        Object oldElement = array[index];
        array[index] = element;
        return oldElement;
    }

    public boolean contains(Object element) {
        for (int i = 0; i < size; i++) {
            if (array[i].equals(element)) {
                return true;
            }
        }
        return false;
    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void clear() {
        array = new Object[capacity];
        size = 0;
    }
}
