package ru.itis.dataStructure.arraylistcustom;

public class Main {
    public static void main(String[] args) {
        ArrayListCustom list = new ArrayListCustom();
        list.add("123");
        list.add(4587);
        list.add("Ivan");
        list.add("Anton");
        list.add("Ivanov");
        list.addElement(4, "sfsdfsdfasdfsaf");
        System.out.println("-------------------------------------");
        for (int i = 0; i < list.getSize(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println("-------------------------------------");
        list.set(0, "new value");
        for (int i = 0; i < list.getSize(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println("-------------------------------------");

        list.remove("Anton");

        System.out.println("contains: " + list.contains("new value"));
        System.out.println("isEmpty: " + list.isEmpty());
        System.out.println("size before cleaning: " + list.getSize());
        list.clear();
        System.out.println("size after cleaning: " + list.getSize());

    }
}
