package ru.itis.dataStructure.linkedlistcustom;

public class LinkedListCustom1 {
    Node head;
    Node tail;

    class Node {
        int val;
        Node next;

        public Node(int val) {
            this.val = val;
        }
    }

    public void addFirst(int value) {
        if (isEmpty()) {
            head = new Node(value);
            tail = head;
            return;
        }
        Node currentNode = new Node(value);
        currentNode.next = head;
        head = currentNode;
    }

    public void addLast(int value) {
        if (isEmpty()) {
            head = new Node(value);
            tail = head;
            return;
        }
        Node currentNode = new Node(value);
        tail.next = currentNode;
        tail = currentNode;
    }

    public void add(int pos, int value) {
        if (pos == 1) {
            addFirst(value);
            return;
        }

        Node current = head;
        int count = 1;
        while (count < pos - 1) {
            count++;
            current = current.next;

        }

        Node newNode = new Node(value);
        newNode.next = current.next;
        current.next = newNode;
    }

    public int deleteFirst() {
        if (isEmpty()) {
            throw new RuntimeException("List is empty");
        }
        Node current = head;
        head = head.next;
        current.next = null;
        if (isEmpty()) {
            tail = null;
        }
        return current.val;
    }

    public int deleteLast() {
        if (isEmpty()) {
            throw new RuntimeException("List is empty");
        }
        if (head == tail) {
            int value = head.val;
            head = null;
            tail = null;
            return  value;
        }
        Node current = head;
        Node prev = null;
        while (current.next != null) {
            prev = current;
            current = current.next;
        }
        prev.next = null;
        tail = prev;
        return current.val;
    }

    public int delete(int pos) {
        if (isEmpty()) {
            throw new RuntimeException("List is empty");
        }
        if (pos == 1) {
            deleteFirst();
        }

        Node current = head;
        Node prev = null;
        int count = 1;
        while (count < pos) {
            count++;
            prev = current;
            current = current.next;
        }
        prev.next = current.next;
        current.next = null;
        return current.val;
    }

    public boolean search(int value) {
        Node current = head;
        while (current != null) {
            if (value == current.val) {
                return true;
            }
            current = current.next;
        }
        return false;
    }


    private boolean isEmpty() {
        return head == null;
    }

    public void print() {
        Node current = head;
        while (current != null) {
            System.out.print(current.val + "->");
            current = current.next;
        }
        System.out.println();
    }
}
