package ru.itis.dataStructure.linkedlistcustom;

public class Main1 {
    public static void main(String[] args) {
        LinkedListCustom1 ll = new LinkedListCustom1();
        ll.addFirst(20);
        System.out.println("After adding 20 at first : ");
        ll.print();

        ll.addFirst(10);
        System.out.println("After adding 10 at first : ");
        ll.print();

        ll.addFirst(30);
        System.out.println("After adding 30 at first : ");
        ll.print();

        ll.addFirst(40);
        System.out.println("After adding 40 at first : ");
        ll.print();

        ll.addFirst(5);
        System.out.println("After adding 5 at first : ");
        ll.print();

        ll.addFirst(25);
        System.out.println("After adding 25 at first : ");
        ll.print();

        ll.deleteFirst();
        System.out.println("After deleting first Node : ");
        ll.print();

        ll.deleteLast();
        System.out.println("After deleting last Node : ");
        ll.print();

        ll.delete(3);
        System.out.println("After deleting Node at pos 3 : ");
        ll.print();
    }
}
