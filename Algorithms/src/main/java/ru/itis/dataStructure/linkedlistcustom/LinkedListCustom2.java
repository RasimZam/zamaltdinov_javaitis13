package ru.itis.dataStructure.linkedlistcustom;

public class LinkedListCustom2 {
    private class Node {
        int date;
        Node next;

        public Node(int date, Node next) {
            this.date = date;
            this.next = next;
        }

        public Node() {

        }
    }

    private Node head;

    public LinkedListCustom2() {
        head = new Node();
    }

    public void addFirst(int value) {
        Node newNode = new Node(value, head.next);
        head.next = newNode;
    }

    public void addLast(int value) {
        Node currentNode = head;
        while (currentNode != null) {
            currentNode = currentNode.next;
        }
        currentNode.next = new Node(value, null);
    }

    public void deleteFirst() {
        head.next = (head.next == null) ? null : head.next.next;
    }

    public void deleteLast() {
        if (head.next == null) {
            return;
        }
        Node currentNode = head.next;
        Node previousNode = head;
        while (currentNode.next != null) {
            previousNode = currentNode.next;
        }
        previousNode.next = null;
    }

    public void insertByIndex(int value, long index) {
        long nodeNumber = 0;
        for (Node currentNode = head; currentNode != null; currentNode = currentNode.next) {
            if (nodeNumber == index) {
                currentNode.next = new Node(value, currentNode.next);
                return;
            }
            nodeNumber++;
        }
        throw new IndexOutOfBoundsException();
    }

    public void deleteByIndex(int value, long index) {
        long nodeNumber = 0;
        Node currentNode = head.next;
        Node previousNode = head;
        while (currentNode != null) {
            currentNode = currentNode.next;
            if (nodeNumber == index) {
                previousNode.next = currentNode.next;
                currentNode.next = null;
                return;
            }
            previousNode = currentNode;
            nodeNumber++;
        }
        throw new IndexOutOfBoundsException();
    }

    public int getByIndex(long index) {
        long nodeNumber = 0;
        Node currentNode = head.next;
        while (currentNode != null) {
            currentNode = currentNode.next;
            if (nodeNumber == index) {
                return currentNode.date;
            }
            nodeNumber++;
        }
        throw new IndexOutOfBoundsException();
    }

    public void setByIndex(int value, long index) {
        long nodeNumber = 0;
        Node currentNode = head.next;
        while (currentNode != null) {
            currentNode = currentNode.next;
            if (nodeNumber == index) {
                currentNode.date = value;
                return;
            }
            nodeNumber++;
        }
        throw new IndexOutOfBoundsException();
    }

    public long getLength() {
        long size = 0;
        for (Node currentNode = head.next; currentNode != null; currentNode = currentNode.next) {
            size++;
        }
        return size;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Node currentNode = head.next; currentNode != null; currentNode = currentNode.next) {
            result.append(currentNode.date).append(" -> ");
        }
        return result.toString();
    }
}
