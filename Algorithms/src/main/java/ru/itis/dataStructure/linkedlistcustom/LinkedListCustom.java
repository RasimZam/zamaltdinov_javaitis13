package ru.itis.dataStructure.linkedlistcustom;

public class LinkedListCustom {
    Node head;

    static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
    }

}
