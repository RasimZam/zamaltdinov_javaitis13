package ru.itis.dataStructure.linkedlistcustom;

public class Main {
    public static void main(String[] args) {
        LinkedListCustom linkedList = new LinkedListCustom();
        linkedList.head = new LinkedListCustom.Node(1);
        LinkedListCustom.Node second = new LinkedListCustom.Node(2);
        LinkedListCustom.Node third = new LinkedListCustom.Node(3);

        linkedList.head.next = second;
        second.next = third;

        System.out.println("Linkedlist: ");

        while (linkedList.head != null) {
            System.out.println(linkedList.head.value + " ");
            linkedList.head = linkedList.head.next;
        }

    }
}
