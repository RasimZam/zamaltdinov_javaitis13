package ru.itis.dataStructure.heap;

// 2500. Delete Greatest Value in Each Row

import java.util.Arrays;

public class DeleteGreatestValueInEachRow_2500 {
    public static void main(String[] args) {
        int[][] array = {{1, 2, 4}, {3, 3, 1}};
        int result = deleteGreatestValue(array);
        System.out.println(result);
    }


    public static int deleteGreatestValue(int[][] grid) {
        for (int[] ints : grid) {
            Arrays.sort(ints);
        }

        int result = 0;
        for (int i = 0; i < grid[0].length; i++) {
            int max = 0;
            for (int j = 0; j < grid.length; j++) {
                max = Math.max(max, grid[j][i]);
            }
            result += max;
        }
        return result;
    }
}
