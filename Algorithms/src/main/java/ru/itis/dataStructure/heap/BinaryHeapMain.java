package ru.itis.dataStructure.heap;

public class BinaryHeapMain {
    public static void main(String[] args) {
        BinaryHeap heap = new BinaryHeap();
        heap.add(6, "Orange");
        heap.add(7, "Apple");
        heap.add(3, "Plum");
        heap.add(4, "Lemon");
        heap.add(5, "Pear");
        heap.add(9, "Cherry");
        heap.add(12, "Banana");
        System.out.println(heap);
        System.out.println();
//        System.out.println(heap.extract());
//        System.out.println(heap.insertAndExtract(2, "Hello"));
//        heap.delete(6);
//        heap.changeKey(6, 2);
//        System.out.println();
//        System.out.println(heap);
    }
}
