package ru.itis.dataStructure.hashtable;

public class Main {
    public static void main(String[] args) {
        HashTableCustom hashTable = new HashTableCustom();
        hashTable.addPair("one", 1);
        hashTable.addPair("five", 5);
        hashTable.addPair("four", 4);
        hashTable.addPair("two", 2);
        hashTable.addPair("three", 3);
        hashTable.addPair("nine", 9);

        System.out.println(hashTable.get("nine"));

        System.out.println(hashTable);

        hashTable.remove("four");
        System.out.println(hashTable);
    }
}
