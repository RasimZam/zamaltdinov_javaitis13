package ru.itis.dataStructure.bst;

public class BSTree {
    class Node {
        int key;
        Node left, right;

        public Node(int key) {
            this.key = key;
            left = right = null;
        }
    }

    Node root;

    public BSTree() {
        root = null;
    }

    // insert a node in BST
    void insert(int key)  {
        root = insertRecursive(root, key);
    }

    //recursive insert function
    Node insertRecursive(Node root, int key) {
        //tree is empty
        if (root == null) {
            root = new Node(key);
            return root;
        }
        //traverse the tree
        if (key < root.key)     //insert in the left subtree
            root.left = insertRecursive(root.left, key);
        else if (key > root.key)    //insert in the right subtree
            root.right = insertRecursive(root.right, key);
        // return pointer
        return root;
    }

    // method for inorder traversal of BST
    void inorder() {
        inorderRecursive(root);
    }

    // recursively traverse the BST
    void inorderRecursive(Node root) {
        if (root != null) {
            inorderRecursive(root.left);
            System.out.print(root.key + " ");
            inorderRecursive(root.right);
        }
    }

    boolean search(int key)  {
        root = searchRecursive(root, key);
        if (root!= null)
            return true;
        else
            return false;
    }

    //recursive insert function
    Node searchRecursive(Node root, int key)  {
        // Base Cases: root is null or key is present at root
        if (root==null || root.key==key)
            return root;
        // val is greater than root's key
        if (root.key > key)
            return searchRecursive(root.left, key);
        // val is less than root's key
        return searchRecursive(root.right, key);
    }

    public void delete(int key) {
        deleteRecursive(root, key);
    }

    private Node deleteRecursive(Node root, int key) {
        if (root == null) {
            return root;
        }

        if (key < root.key) {
            root.left = deleteRecursive(root.left, key);
        } else if (key > root.key) {
            root.right = deleteRecursive(root.right, key);
        } else {
            // node contains only one child
            if (root.left == null) {
                return root.right;
            } else if (root.right == null) {
                return root.left;
            }

            // node has two children;
            //get inorder successor (min value in the right subtree)
            root.key = minValue(root.right);

            root.right = deleteRecursive(root.right, root.key);
        }
        return root;
    }

    private int minValue(Node node) {
        //initially minval = root
        int minval = node.key;
        //find minval
        while (node.left != null) {
            minval = node.left.key;
            node = node.left;
        }
        return minval;
    }
}
