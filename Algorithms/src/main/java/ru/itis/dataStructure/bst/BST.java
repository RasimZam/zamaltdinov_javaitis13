package ru.itis.dataStructure.bst;

public class BST {
    class Node {
        int data;
        Node left;
        Node right;
    }

    public Node insert(Node node, int value) {
        if (node == null) {
            return createNewNode(value);
        }

        if (value < node.data) {
            node.left = insert(node.left, value);
        } else if (value > node.data) {
            node.right = insert(node.right, value);
        }
        return node;
    }

    private Node createNewNode(int value) {
        Node newNode = new Node();
        newNode.data = value;
        newNode.left = null;
        newNode.right = null;
        return newNode;
    }

    public Node delete(Node node, int value) {
        if (node == null) {
            return null;
        }
        if (value < node.data) {
            node.left = delete(node.left, value);
        } else if (value > node.data) {
            node.right = delete(node.right, value);
        } else {
            if (node.left == null || node.right == null) {
                Node temp = null;
                temp = node.left == null ? node.right : node.left;

                if (temp == null) {
                    return null;
                } else {
                    return temp;
                }
            } else {
                Node successor = getSuccessor(node);
                node.data = successor.data;
                node.right = delete(node.right, successor.data);
                return node;
            }
        }
        return node;
    }

    public void inorder(Node node) {
        if (node == null) {
            return;
        }
        inorder(node.left);
        System.out.print(node.data + " ");
        inorder(node.right);
    }

    public void preorder(Node node) {
        if (node == null) {
            return;
        }

        System.out.println(node.data);
        preorder(node.left);
        preorder(node.right);
    }

    public void postorder(Node node) {
        if (node == null) {
            return;
        }
        postorder(node.left);
        postorder(node.right);
        System.out.println(node.data);
    }

    public boolean ifNodePresent(Node node, int value) {
        if (node == null) {
            return false;
        }

        boolean isPresent = false;

        while (node != null) {
            if (value < node.data) {
                node = node.left;
            } else if (value > node.data) {
                node = node.right;
            } else {
                isPresent = true;
                break;
            }
        }
        return isPresent;
    }

    public Node getParentNode(Node node, int value) {
        if (node == null) {
            return null;
        }

        Node getParent = null;
        while (node != null) {
            if (value < node.data) {
                getParent = node;
                node = node.left;
            } else if (value > node.data) {
                getParent = node;
                node = node.right;
            } else {
                break;
            }
        }
        return node != null ? getParent : null;
    }

    public Node getSiblingNode(Node node, int value) {
        if (node == null || node.data == value) {
            return null;
        }
        
        Node parentNode = null;
        while (node != null) {
            if (value < node.data) {
                parentNode = node;
                node = node.left;
            } else if (value > node.data) {
                parentNode = node;
                node = node.right;
            } else {
                break;
            }
        }
        if (parentNode.left != null && value == parentNode.left.data) {
            return parentNode.right;
        }

        if (parentNode.right != null && value == parentNode.right.data) {
            return parentNode.left;
        }
        return null;
    }

    private Node getSuccessor(Node node) {
        if (node == null) {
            return null;
        }

        Node temp = node.right;

        while(temp.left != null) {
            temp = temp.left;
        }
        return temp;
    }
}
