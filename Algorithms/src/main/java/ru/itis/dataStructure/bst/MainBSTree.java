package ru.itis.dataStructure.bst;

public class MainBSTree {
    public static void main(String[] args) {
        BSTree bsTree = new BSTree();
        bsTree.insert(45);
        bsTree.insert(10);
        bsTree.insert(7);
        bsTree.insert(12);
        bsTree.insert(90);
        bsTree.insert(50);

        System.out.println("The BST Created with input data(Left-root-right):");
        bsTree.inorder();

        //delete leaf node
        System.out.println("\nThe BST after Delete 12(leaf node):");
        bsTree.delete(7);
        bsTree.inorder();

        //delete the node with one child
//        System.out.println("\nThe BST after Delete 90 (node with 1 child):");
//        bsTree.delete(90);
//        bsTree.inorder();

//        //delete node with two children
//        System.out.println("\nThe BST after Delete 45 (Node with two children):");
//        bsTree.delete(45);
//        bsTree.inorder();
    }
}
