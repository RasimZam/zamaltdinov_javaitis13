package ru.itis.dataStructure.bst;

public class BSTCustom {

    class Node {
        int key;
        Node left;
        Node right;

        public Node(int value) {
            this.key = value;
            this.left = this.right = null;
        }
    }

    private Node root;

    public BSTCustom() {
        this.root = null;
    }

    public int getMinValue(Node node) {
        Node current = node;
        while (current.left != null) {
            current = current.left;
        }
        return current.key;
    }
    public int getMaxValue(Node node) {
        Node current = node;
        while (current.right != null) {
            current = current.right;
        }
        return current.key;
    }

    public void insert(int key) {
        root = insert(root, key);
    }

    public Node search(int key) {
        return insert(root, key);
    }

    public Node delete(int key) {
        return delete(root, key);
    }

    public int findCeil(int input) {
        return findCeil(root, input);
    }

    public int findFloor(int input) {
        return findFloor(root, input);
    }

    private Node insert(Node node, int key) {
        if (root == null) {
           return new Node(key);
        }

        if (node.key > key) {
            node.left = insert(node.left, key);
        } else {
            node.right = insert(node.right, key);
        }
        return node;
    }

    private Node search(Node node, int key) {
        if (node == null) {
            return null; // if the key not in tree
        }
        if (node.key == key) {
            return node; // found key
        } else if (node.key > key) {
            return search(node.left, key);
        } else {
            return search(node.right, key);
        }
    }

    private Node delete(Node node, int key) {
        if (node == null) {
            return null; // the key not in tree
        }
        if (node.key > key) {
            node.left = delete(node.left, key);
        } else if (node.key < key) {
            node.right = delete(node.right, key);
        } else {// we found target node
            // case 1: node is a leaf
            if (node.left == null && node.right == null) {
                return null;
            }
            // case 2: node has 1 subtree
            if (node.left == null) {
                return node.right;
            } else if (node.right == null) {
                return node.left;
            }

            // case 3: node has both subtrees
            int replaceKey = getMinValue(node.right);
            node.key = replaceKey;
            node.right = delete(node.right, key);
        }
        return node;
    }

    private int findCeil(Node node, int input) {
        if (node == null) {
            return -1;
        }
        if (node.key == input) {
            return node.key;
        }
        if (node.key < input) {
            return findCeil(node.right, input);
        }
        int ceil = findCeil(node.left, input);
        if (ceil >= input) {
            return ceil;
        } else {
            return node.key;
        }
    }

    private int findFloor(Node node, int input) {
        if (node == null) {
            return -1;
        }
        if (node.key == input) {
            return node.key;
        }
        if (node.key > input) {
            return findFloor(node.left, input);
        }
        int floor = findFloor(node.right, input);
        if (floor >= node.key) {
            return floor;
        } else {
            return node.key;
        }
    }

    // traversal methods

    public void printPostOrder() {
        printPostOrder(root);
    }

    public void printPreOrder() {
        printPreOrder(root);
    }

    public void printInOrder() {
        printInOrder(root);
    }

    private void printPostOrder(Node node) {
        if (node == null) {
            return;
        }
        printPostOrder(node.left);
        printPostOrder(node.right);
        System.out.print(node.key + " ");
    }

    private void printPreOrder(Node node) {
        if (node == null) {
            return;
        }
        System.out.print(node.key + " ");
        printPreOrder(node.left);
        printPreOrder(node.right);
    }

    private void printInOrder(Node node) {
        if (node == null) {
            return;
        }
        printInOrder(node.left);
        System.out.println(node.key + " ");
        printInOrder(node.right);
    }
}
