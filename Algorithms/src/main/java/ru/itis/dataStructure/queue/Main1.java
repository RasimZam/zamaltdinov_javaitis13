package ru.itis.dataStructure.queue;

public class Main1 {
    public static void main(String[] args) {
        QueueLinkedListCustom queue = new QueueLinkedListCustom();
        queue.enqueue("Hello");
        queue.enqueue("world");
        System.out.println(queue);
        System.out.println(queue.getLength());

        System.out.println(queue.dequeue());
        System.out.println(queue);
        System.out.println(queue.getLength());

        queue.clear();
        System.out.println(queue);
    }
}
