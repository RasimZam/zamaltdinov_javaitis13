package ru.itis.dataStructure.queue;

public class QueueLinkedListCustom {
    private class Node {
        Object data;
        Node next;
        Node prev;

        public Node(Object data, Node next, Node prev) {
            this.data = data;
            this.next = next;
            this.prev = prev;
        }

        public Node() {
        }

        @Override
        public String toString() {
            return "Node [data=" + data + ", next=" + next + ", prev=" + prev + "]";
        }
    }

    private final Node head;
    private final Node tail;
    private long length = 0;

    public QueueLinkedListCustom() {
        this.head = new Node();
        this.tail = new Node();
        head.next = tail;
        tail.prev = head;
    }

    public void enqueue(Object value) {
        Node addNode = new Node(value, head.next, head);
        head.next.prev = addNode;
        head.next = addNode;
        length++;
    }

    public Object dequeue() {
        if (isEmpty()) {
            return null;
        }
        Node removeNode = tail.prev;
        tail.prev = removeNode.prev;
        removeNode.prev.next = tail;
        Object value = removeNode.data;
        removeNode.next = null;
        removeNode.prev = null;
        length--;
        return value;
    }

    public Object peek() {
        if (isEmpty()) {
            return null;
        }
        Node removeNode = tail.prev;
        return removeNode.data;
    }

    public boolean isEmpty() {
        return head.next == tail;
    }

    public long getLength() {
        long length = 0;
        Node currentNode = head.next;
        while (currentNode != tail) {
            length++;
            currentNode = currentNode.next;
        }
        return length;
    }

    public void clear() {
        while (length > 0) {
            dequeue();
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        Node currentNode = head.next;
        while (currentNode != tail) {
            stringBuilder.append(currentNode.data).append(" ");
            currentNode = currentNode.next;
        }
        return stringBuilder.append("]").toString();
    }
}
