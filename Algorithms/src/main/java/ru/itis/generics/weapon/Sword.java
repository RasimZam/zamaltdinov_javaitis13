package ru.itis.generics.weapon;

public class Sword implements MeleeWeapon {
    @Override
    public int getDamage() {
        return 15;
    }
}
