package ru.itis.generics.weapon;

public interface Weapon {
    int getDamage();
}
