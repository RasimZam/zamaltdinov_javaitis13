package ru.itis.generics.weapon;

public interface Mortal {
    boolean isAlive();
}
