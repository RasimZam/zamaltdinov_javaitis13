package ru.itis.hashing;

// 242. Valid Anagram

public class ValidAnagram_242 {
    public static void main(String[] args) {

    }

    public static boolean isAnagram(String s, String t) {
        if(s.length() != t.length()) {
            return false;
        }

        int[] countOfChars = new int[26];

        for (int i = 0; i < s.length(); i++) {
            countOfChars[s.charAt(i) - 'a']++;
            countOfChars[t.charAt(i) - 'a']--;
        }

        for (int value : countOfChars) {
            if(value != 0) {
                return false;
            }
        }
        return true;
    }
}
