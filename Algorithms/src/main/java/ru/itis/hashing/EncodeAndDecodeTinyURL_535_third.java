package ru.itis.hashing;

// 535. Encode and Decode TinyURL

import java.util.*;

public class EncodeAndDecodeTinyURL_535_third {
    private final List<String> array = new ArrayList<>();
    private int ptr = 0;
    // Encodes a URL to a shortened URL.
    public String encode(String longUrl) {
        array.add(longUrl);
        return String.valueOf(ptr++);
    }

    // Decodes a shortened URL to its original URL.
    public String decode(String shortUrl) {
        return array.get(Integer.parseInt(shortUrl));
    }
}
