package ru.itis.hashing;

// 535. Encode and Decode TinyURL

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class EncodeAndDecodeTinyURL_535_first {
    Map<String, String> urlMap = new HashMap<>();
    // Encodes a URL to a shortened URL.
    public String encode(String longUrl) {
        String tinyURL = "https://dummyURL/" + UUID.randomUUID();
        urlMap.put(tinyURL, longUrl);
        return tinyURL;
    }

    // Decodes a shortened URL to its original URL.
    public String decode(String shortUrl) {
        if (shortUrl != null) {
            return urlMap.get(shortUrl);
        }
        return "";
    }


}
