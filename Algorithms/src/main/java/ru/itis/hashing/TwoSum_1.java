package ru.itis.hashing;

// 1. Two Sum

import java.util.HashMap;
import java.util.Map;

public class TwoSum_1 {

    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int requiredNumber = target - nums[i];

            if (map.containsKey(requiredNumber)) {
                return new int[] {map.get(requiredNumber), i};
            }
            map.put(nums[i], i);
        }
        throw new IllegalArgumentException("No solution for defined input data");
    }
}
