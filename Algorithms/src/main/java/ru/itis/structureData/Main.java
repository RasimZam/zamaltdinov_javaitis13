package ru.itis.structureData;

public class Main {
    public static void main(String[] args) {
        ArrayBasedList list = new ArrayBasedList();
        list.add(1);
        list.add(3);
        list.add(5);
        list.add(6);
        System.out.println(list);
        list.addByIndex(-2, 0);
        System.out.println(list);
    }
}
