package ru.itis.structureData.queue;

public class Queue {
    private final int queueLength = 3;
    private final int[] items = new int[queueLength];
    private int front = -1;
    private int back = -1;
    
    private boolean isFull() {
        if (back == queueLength - 1) {
            return true;
        } else {
            return false;
        }
    }
    
    private boolean isEmpty() {
        if (front == -1 && back == -1) {
            return true;
        } else {
            return false;
        }
    }
    
    public void enQueue(int itemValue) {
        if (isFull()) {
            System.out.println("Queue is full");
        } else if (front == -1 && back == -1) {
            front = back = 0;
            items[back] = itemValue;
        } else {
            back++;
            items[back] = itemValue;
        }
    }

    public void deQueue() {
        if (isEmpty()) {
            System.out.println("Queue is empty");
        } else if (front == back) {
            front = back = -1;
        } else {
            front++;
        }
    }

    public void display() {
        if (isEmpty()) {
            System.out.println("Queue is empty");
        } else {
            for (int i = front; i <= back ; i++) {
                System.out.println(items[i]);
            }
        }
    }

    public void peak() {
        System.out.println("Front value is " + items[front]);
    }
}
