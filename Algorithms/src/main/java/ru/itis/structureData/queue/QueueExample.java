package ru.itis.structureData.queue;

import java.util.LinkedList;
import java.util.Queue;

public class QueueExample {
    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>();
        queue.offer("Казань");
        queue.offer("Москва");
        queue.offer("Екатеринбург");
        queue.offer("Самара");
        System.out.println(queue.element());
        System.out.println("-----------------");

        String town;
        while ((town = queue.poll()) != null) {
            System.out.println(town);
        }
    }
}
