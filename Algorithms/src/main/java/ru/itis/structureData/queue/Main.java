package ru.itis.structureData.queue;

public class Main {
    public static void main(String[] args) {
        Queue queue = new Queue();
        queue.enQueue(3);
        queue.enQueue(2);
        queue.enQueue(1);
        queue.display();
        queue.deQueue();
        queue.peak();
    }
}
