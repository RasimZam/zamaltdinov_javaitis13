package ru.itis.structureData.queue1;

public class Main {
    public static void main(String[] args) {
        Queue queue = new Queue(5);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        System.out.println("The front element is " + queue.peek());
        queue.deQueue();
        System.out.println("The front element is " + queue.peek());

        System.out.println("The queue size is " + queue.size());

        queue.deQueue();
        queue.deQueue();

        if (queue.isEmpty()) {
            System.out.println("The queue is empty");
        } else {
            System.out.println("The queue is not empty");
        }
    }
}
