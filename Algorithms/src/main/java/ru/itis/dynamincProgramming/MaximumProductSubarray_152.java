package ru.itis.dynamincProgramming;

// 152. Maximum Product Subarray
// O(n) time complexity
// O(1) memory complexity

public class MaximumProductSubarray_152 {
    public static void main(String[] args) {

    }

    public static int maxProduct(int[] nums) {
        int n = nums.length;
        int leftProduct = 1;
        int rightProduct = 1;
        int ans = nums[0];

        for(int i = 0; i < nums.length; i++) {
            leftProduct = leftProduct == 0 ? 1 : leftProduct;
            rightProduct = rightProduct == 0 ? 1 : rightProduct;

            ans = Math.max(ans, Math.max(leftProduct, rightProduct));
        }
        return ans;
    }
}
