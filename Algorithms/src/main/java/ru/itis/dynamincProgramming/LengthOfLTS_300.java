package ru.itis.dynamincProgramming;

// 300. Longest Increasing Subsequence

public class LengthOfLTS_300 {
    public static void main(String[] args) {

    }

    public static int lengthOfLIS(int[] nums) {
        int[] result = new int[nums.length];

        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j < i; j++) {
                if (nums[i] > nums[j]) {
                    if (result[i] + 1 > result[j]) {
                        result[i] = result[j] + 1;
                    }
                }
            }
        }

        int maxIndex = 0;
        for (int i = 0; i < result.length; i++) {
            if (result[i] > result[maxIndex]) {
                maxIndex = i;
            }
        }
        return result[maxIndex] + 1;
    }
}
