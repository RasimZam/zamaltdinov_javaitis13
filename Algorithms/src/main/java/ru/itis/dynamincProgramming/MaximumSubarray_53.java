package ru.itis.dynamincProgramming;

// 53. Maximum Subarray
// O(n) time complexity
// O(1) memory complexity

public class MaximumSubarray_53 {
    public static void main(String[] args) {
        int[] nums = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
        int result = maxSubarray(nums);
        System.out.println(result);
    }

    public static int maxSubarray(int[] nums) {
        int maxSoFar = nums[0];
        int currMax = nums[0];

        for (int i = 1; i < nums.length; i++) {
            currMax = Math.max(nums[i], nums[i] + currMax);
            maxSoFar = Math.max(currMax, maxSoFar);
        }
        return maxSoFar;
    }
}
