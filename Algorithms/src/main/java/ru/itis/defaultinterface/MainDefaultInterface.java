package ru.itis.defaultinterface;

public class MainDefaultInterface {
    public static void main(String[] args) {
        SampleInterface classA = new ClassA("Hello A");
        SampleInterface classB = new ClassB("Hello B");

        System.out.println(classA.getMessage());
        System.out.println(classB.getMessage());

        System.out.println("---------------------");

        C classC = new C("Hello");
        A interfaceA = classC;
        B interfaceB = classC;

        System.out.println(classC.getMessage());
        System.out.println(interfaceA.getMessage());
        System.out.println(interfaceB.getMessage());
    }
}
