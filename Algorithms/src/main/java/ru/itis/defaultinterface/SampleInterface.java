package ru.itis.defaultinterface;

public interface SampleInterface {

    default String getMessage() {
        return "Default message";
    }
}
