package ru.itis.defaultinterface;

public interface B extends A {
    @Override
    default String getMessage() {
        return "Hello Java";
    }
}
