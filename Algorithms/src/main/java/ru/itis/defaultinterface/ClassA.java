package ru.itis.defaultinterface;

public class ClassA implements SampleInterface {
    private String message;

    public ClassA(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ClassA [ message= " + message + "]";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
