package ru.itis.defaultinterface;

public interface A {
    default String getMessage() {
        return "Hello world";
    }
}
