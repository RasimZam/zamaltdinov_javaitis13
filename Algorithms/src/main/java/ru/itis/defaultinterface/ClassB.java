package ru.itis.defaultinterface;

public class ClassB implements SampleInterface {
    private String message;

    public ClassB(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ClassB [ message= " + message + "]";
    }
}
