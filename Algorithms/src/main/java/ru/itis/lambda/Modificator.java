package ru.itis.lambda;

@FunctionalInterface
public interface Modificator<T> {
    T modification(T element);
}
