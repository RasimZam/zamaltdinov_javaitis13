package ru.itis.lambda;

public class SimpleClass {
    private int[] arr;

    private Summator sm = () -> {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    };

    public SimpleClass(int[] arr) {
        this.arr = arr;
    }

    public Summator getSummatorInnstance() {
        return this.sm;
    }
}
