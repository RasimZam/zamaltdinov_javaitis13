package ru.itis.lambda;

public class Main {
    public static void main(String[] args) {
        StringModificator sm = text -> text.toUpperCase();
        System.out.println(sm.getString("hello"));
        System.out.println("========================");

        StringModificator sm1 = text -> {
            StringBuilder result = new StringBuilder();
            for (char letter: text.toCharArray()) {
                if (Character.isLetter(letter) || letter == ' ') {
                    result.append(letter);
                }
            }
            return result.toString();
        };
        System.out.println(sm1.getString("hel1o 12 hello"));
    }
}
