package ru.itis.lambda;

@FunctionalInterface
public interface Summator {
    int getSum();
}
