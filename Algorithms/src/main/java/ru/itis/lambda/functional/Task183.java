package ru.itis.lambda.functional;

import java.util.Arrays;
import java.util.List;

public class Task183 {
    public static void main(String[] args) {
        List<String> melons = Arrays.asList(
                "Gac", "Cantaloupe", "Hemi", "Gac", "Gac",
                "Hemi", "Cantaloupe", "Horned", "Hemi", "Hemi"
        );

        String anyApollo = melons.stream()
                .filter(m -> m.equals("Apollo"))
                .findAny()
                .orElse("empty");
        System.out.println(anyApollo);
    }
}
