package ru.itis.lambda.functional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Task179 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("anna", "bob", "crishtian", null, "carmen", "rick", "carla");
        names.stream()
                .peek(p -> System.out.println("\tstream(): " + p))
                .filter(s -> s.startsWith("c"))
                .peek(p -> System.out.println("\tfilter(): " + p))
                .map(String::toUpperCase)
                .peek(p -> System.out.println("\tmap(): " + p))
                .sorted()
                .peek(p -> System.out.println("\tsorted(): " + p))
                .collect(Collectors.toList());

    }
}
