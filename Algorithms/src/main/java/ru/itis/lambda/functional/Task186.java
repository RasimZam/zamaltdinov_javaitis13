package ru.itis.lambda.functional;

import ru.itis.lambda.functional.model.Melon;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task186 {
    public static void main(String[] args) {
        List<Melon> melons = Arrays.asList(new Melon("Crenshaw", 2000),
                new Melon("Hemi", 1600),
                new Melon("Gac", 2000), new Melon("Apollo", 2000),
                new Melon("Horned", 1700), new Melon("Cantalope", 2600));

        String str = "Lorem Ipsum is simply" +
                "Ipsum Lorem not simply Ipsum";

        List<Integer> resultToList = melons.stream()
                .map(Melon::getWeight)
                .filter(x -> x >= 1000)
                .collect(Collectors.toList());

        List<Integer> resultToList1 = melons.stream()
                .map(Melon::getWeight)
                .filter(x -> x >= 1000)
                .collect(Collectors.toCollection(ArrayList::new));

        Set<Integer> resultToSet = melons.stream()
                .map(Melon::getWeight)
                .filter(x -> x >= 1000)
                .collect(Collectors.toSet());

        Set<Integer> resultToSet1 = melons.stream()
                .map(Melon::getWeight)
                .filter(x -> x >= 1000)
                .collect(Collectors.toCollection(HashSet::new));

        Set<Integer> resultToTreeset = melons.stream()
                .map(Melon::getWeight)
                .filter(x -> x >= 1000)
                .collect(Collectors.toCollection(TreeSet::new));

        Map<String, Integer> resultToMap = melons.stream()
                .distinct()
                .collect(Collectors.toMap(Melon::getType, Melon::getWeight));

        Map<Integer, Integer> resultToMap1 = melons.stream()
                .distinct()
                .map(x -> Map.entry(
                        new Random().nextInt(Integer.MAX_VALUE), x.getWeight()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Map<String, Integer> resultToMap2 = melons.stream()
                .sorted(Comparator.comparingInt(Melon::getWeight))
                .collect(Collectors.toMap(Melon::getType, Melon::getWeight,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        Map<String, Integer> mapOfWords = Stream.of(str)
                .map(w -> w.split("\\s+"))
                .flatMap(Arrays::stream)
                .collect(Collectors.toMap(String::toLowerCase, w -> 1, Integer::sum));
        System.out.println(mapOfWords);
    }
}
