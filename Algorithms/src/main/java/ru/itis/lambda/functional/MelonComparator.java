package ru.itis.lambda.functional;

import ru.itis.lambda.functional.model.MelonNew;

import java.util.Comparator;

public class MelonComparator implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        return Integer.compare(((MelonNew) o1).getWeight(),
                ((MelonNew) o2).getWeight());
    }
}
