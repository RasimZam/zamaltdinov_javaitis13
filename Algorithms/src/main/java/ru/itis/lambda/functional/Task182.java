package ru.itis.lambda.functional;

import ru.itis.lambda.functional.model.Melon;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Task182 {
    public static void main(String[] args) {
        Melon[][] melonArray = {
                {new Melon("Gac", 2000), new Melon("Hemi", 1600)},
                {new Melon("Gac", 2000), new Melon("Apollo", 2000)},
                {new Melon("Horned", 1700), new Melon("Hemi", 1600)}
        };

        List<Melon> melons = Arrays.stream(melonArray)
                .flatMap(Arrays::stream)
                .distinct()
                .collect(Collectors.toList());
        System.out.println(melons);

        List<List<String>> melonLists = Arrays.asList(
                Arrays.asList("Gac", "Cantaloupe"),
                Arrays.asList("Hemi", "Gac", "Apollo"),
                Arrays.asList("Gac", "Hemi", "Cantloupe"),
                List.of("Apollo"),
                Arrays.asList("Horned", "Hemi"),
                List.of("Hemi")
        );

        List<String> distinctNames = melonLists.stream()
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());
        System.out.println(distinctNames);
    }
}
