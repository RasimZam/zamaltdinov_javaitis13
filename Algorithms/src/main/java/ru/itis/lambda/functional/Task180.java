package ru.itis.lambda.functional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Task180 {
    public static void main(String[] args) {
        List<Integer> ints = Arrays.asList(1, 2, -4, 0, 2, 0, -1, 14, 0, -1);
        List<Integer> result = ints.stream()
                .filter(Task180::evenBetween0And10)
                .collect(Collectors.toList());
        result.forEach(System.out::println);
    }

    private static boolean evenBetween0And10(int value) {
        return value > 0 && value < 10 && value % 2 == 0;
    }
}
