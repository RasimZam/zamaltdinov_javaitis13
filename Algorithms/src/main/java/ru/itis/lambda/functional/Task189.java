package ru.itis.lambda.functional;

import ru.itis.lambda.functional.model.MelonNew;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toSet;

public class Task189 {
    public static void main(String[] args) {
        List<MelonNew> melons = Arrays.asList(
                new MelonNew("Grenshaw", 1200),
                new MelonNew("Gac", 3000), new MelonNew("Hemi", 2600),
                new MelonNew("Hemi", 1600), new MelonNew("Gac", 1200),
                new MelonNew("Apollo", 2600), new MelonNew("Horned", 1700),
                new MelonNew("Gac", 3000), new MelonNew("Hemi", 2600)
        );

        Map<String, List<MelonNew>> byTypeInList = melons.stream()
                .collect(groupingBy(MelonNew::getType));
        System.out.println(byTypeInList);
        System.out.println("---------------------------");

        Map<Integer, List<MelonNew>> byWeightInList = melons.stream()
                .collect(groupingBy(MelonNew::getWeight));
        System.out.println(byWeightInList);
        System.out.println("---------------------------");

        Map<Integer, Set<MelonNew>> byWeightInSet = melons.stream()
                .collect(groupingBy(MelonNew::getWeight, toSet()));
        System.out.println(byWeightInSet);
        System.out.println("---------------------------");

        Map<Integer, Set<MelonNew>> byWeightInSetOrdered = melons.stream()
                .collect(groupingBy(MelonNew::getWeight, TreeMap::new, toSet()));
        System.out.println(byWeightInSetOrdered);
        System.out.println("---------------------------");

        MelonComparator mc = new MelonComparator();
        List<MelonNew> sorted = melons.stream()
                .sorted((MelonNew m1, MelonNew m2) -> mc.compare(m1, m2))
                .collect(Collectors.toList());
        System.out.println(sorted);
        System.out.println("---------------------------");
    }
}
