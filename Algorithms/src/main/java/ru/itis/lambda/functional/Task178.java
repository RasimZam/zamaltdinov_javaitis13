package ru.itis.lambda.functional;

import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Task178 {
    public static final Function<String, String> firstAndLastChar
            = (String s) -> String.valueOf(s.charAt(0)) + String.valueOf(s.charAt(s.length() - 1));

    public List<String> rndStringFromStrings(List<String> strs) {
        return strs.stream()
                .map(Task178::extractorCharacter)
                .collect(Collectors.toList());
    }

    public static String extractorCharacter(String str) {
        Random rnd = new Random();
        int nr = rnd.nextInt(str.length());
        return String.valueOf(str.charAt(nr));
    }
}
