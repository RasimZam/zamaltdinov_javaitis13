package ru.itis.lambda.functional.model;

import java.util.Objects;

public class MelonNew {
    enum Sugar {
        LOW, MEDIUM, HIGH, UNKNOWN
    }

    private final String type;
    private final int weight;
    private Sugar sugar;

    public MelonNew(String type, int weight) {
        this.type = type;
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MelonNew melonNew = (MelonNew) o;
        return weight == melonNew.weight && Objects.equals(type, melonNew.type) && sugar == melonNew.sugar;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, weight, sugar);
    }

    @Override
    public String toString() {
        return "{" +
                "type='" + type + '\'' +
                ", weight=" + weight +
                '}';
    }

    public String getType() {
        return type;
    }

    public int getWeight() {
        return weight;
    }

    public Sugar getSugar() {
        return sugar;
    }
}
