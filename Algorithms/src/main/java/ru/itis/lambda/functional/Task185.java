package ru.itis.lambda.functional;

import ru.itis.lambda.functional.model.Melon;

import java.util.Arrays;
import java.util.List;

public class Task185 {
    public static void main(String[] args) {
        List<Melon> melons = Arrays.asList(new Melon("Gac", 2000), new Melon("Hemi", 1600),
                new Melon("Gac", 2000), new Melon("Apollo", 2000),
                new Melon("Horned", 1700), new Melon("Hemi", 1600));

        int max = melons.stream()
                .mapToInt(Melon::getWeight)
                .max()
                .orElse(-1);
        System.out.println(max);

        int min = melons.stream()
                .mapToInt(Melon::getWeight)
                .min()
                .orElse(-1);
        System.out.println(min);
    }
}
