package ru.itis.lambda.functional;

import ru.itis.lambda.functional.model.Melon;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Task187 {
    public static void main(String[] args) {
        List<Melon> melons = Arrays.asList(new Melon("Crenshaw", 2000),
                new Melon("Hemi", 1600),
                new Melon("Gac", 2000), new Melon("Apollo", 2000),
                new Melon("Horned", 1700), new Melon("Cantalope", 2600));

        String melonNames = melons.stream()
                .map(Melon::getType)
                .distinct()
                .sorted()
                .collect(Collectors.joining(", ","Дыни в наличии: "," Благодарю!"));
        System.out.println(melonNames);
    }
}
