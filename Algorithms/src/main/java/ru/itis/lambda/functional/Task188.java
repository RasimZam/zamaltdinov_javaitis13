package ru.itis.lambda.functional;

import ru.itis.lambda.functional.model.Melon;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task188 {
    public static void main(String[] args) {
        List<Melon> melons = Arrays.asList(new Melon("Crenshaw", 2000),
                new Melon("Hemi", 1600),
                new Melon("Gac", 2000), new Melon("Apollo", 2000),
                new Melon("Horned", 1700), new Melon("Cantalope", 2600));

        String str = "Lorem Ipsum is simply dummy text ....";

        int sumWeightsGrams = melons.stream()
                .collect(Collectors.summingInt(Melon::getWeight));

        long numberOfWords = Stream.of(str)
                .map(w -> w.split("\\s+"))
                .flatMap(Arrays::stream)
                .filter(w -> w.trim().length() != 0)
                .count();

        System.out.println(numberOfWords);
    }
}
