package ru.itis.lambda;

@FunctionalInterface
public interface StringModificator {
    String getString(String text);
}
