package ru.itis.lambda;

public class SimpleClassMain {
    public static void main(String[] args) {
        SimpleClass a = new SimpleClass(new int[]{1, 2, 3});
        Summator sm = a.getSummatorInnstance();
        System.out.println(sm.getSum());
        System.out.println("================");

        MathUtil.setArr(new int[] {3, 4, 5});
        System.out.println(MathUtil.getSummator().getSum());
        System.out.println("================");

        Summator sm1 = getIntegerSummator(new int[]{1, 2, 3, 4});
        System.out.println(sm1.getSum());
        System.out.println("================");

        Modificator<String> mod = (text) -> text.toUpperCase();
        System.out.println(mod.modification("hello"));
    }

    public static Summator getIntegerSummator(int[] array) {
        Summator sm = () -> {
            int sum = 0;
            for (int element : array) {
                sum += element;
            }
            return sum;
        };
        return sm;
    }
}
