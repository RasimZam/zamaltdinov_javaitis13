package ru.itis.slidingwindows;

// 209. Minimum Size Subarray Sum

public class MinimumSizeSubArraySum_209 {

    public static void main(String[] args) {
        int[] nums = {2, 3, 1, 2, 4, 3};
        int target = 7;
        int result = minSubArrayLen(target, nums);
        System.out.println(result);
    }

    public static int minSubArrayLen(int target, int[] nums) {
        int left = 0, right = 0;
        int min = Integer.MAX_VALUE, sum = 0;
        int length = nums.length;

        while (right < length) {
            sum += nums[right];

            while (target <= sum) {
                min = Math.min(min, right - left + 1);
                sum -= nums[left];
                left++;
            }
            right++;
        }
        return min == Integer.MAX_VALUE ? 0 : min;
    }
}
