package ru.itis.slidingwindows;

// 1004. Max Consecutive Ones II

public class MaxConsecutiveOnesII_487 {
    public static void main(String[] args) {
//        int[] nums = {1, 0, 1, 1, 0};
        int[] nums = {1, 0, 0, 0, 1, 1, 1};
        int result = findMaxConsecutiveOnes(nums);
        System.out.println(result);
    }

    public static int findMaxConsecutiveOnes(int[] nums) {
        // base case
        int length = nums.length;
        if (length < 2) {
            return length;
        }

        // define pointers
        int leftPointer = 0, rightPointer = 0;

        // define maxLen, counter
         int maxLen = 0, counter = 0;

        // find max consecutive ones
        while (rightPointer < length) {
            if (nums[rightPointer] == 0) {
                counter++;
            }

            // contract window if we don't meet the condition
            while (counter > 1) {
                if (nums[leftPointer] == 0) {
                    counter--;
                }
                leftPointer++;
            }
            // update the maxLen
            maxLen = Math.max(maxLen, rightPointer - leftPointer + 1);
            // move the rightPointer one to the right
            rightPointer++;
        }
        // return maxLen
        return maxLen;
    }
}
