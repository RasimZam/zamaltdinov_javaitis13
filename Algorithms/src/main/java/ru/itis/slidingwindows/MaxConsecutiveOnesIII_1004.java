package ru.itis.slidingwindows;

// 1004. Max Consecutive Ones III

public class MaxConsecutiveOnesIII_1004 {
    public static void main(String[] args) {
        int[] nums = {1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0};
        int result = longestOnes(nums, 2);
        System.out.println(result);
    }

    public static int longestOnes(int[] nums, int k) {
        // base case
        int length = nums.length;
        if (length < 2 && 0 < k) {
            return length;
        }

        // define pointers
        int leftPointer = 0, rightPointer = 0;

        // define maxLen, counter
        int maxLen = 0, counter = 0;

        // longest ones
        while (rightPointer < length) {
            // expand the window
            //increase the counter if cur == 0
            if (nums[rightPointer] == 0) {
                counter++;
            }
            // contract our window if condition doesn't meet
            while (k < counter) {
                if (nums[leftPointer] == 0) {
                    counter--;
                }
                leftPointer++;
            }
            //update maxLen
            maxLen = Math.max(maxLen, rightPointer - leftPointer + 1);
            // move the rightPointer one to the right
            rightPointer++;
        }
        // return maxLen
        return maxLen;
    }
}
