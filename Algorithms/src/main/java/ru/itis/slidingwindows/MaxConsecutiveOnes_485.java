package ru.itis.slidingwindows;

// 485. Max Consecutive Ones

public class MaxConsecutiveOnes_485 {
    public static void main(String[] args) {
        int[] nums = {1, 1, 0, 1, 1, 1};
        int result = findMaxConsecutiveOnes(nums);
        System.out.println(result);
    }

    public static int findMaxConsecutiveOnes(int[] nums) {
        // define vars
        int maxLen = 0, counter = 0;
        //iterate all elements in the nums
        for (int num : nums) {
            if (num == 0) {
                counter = 0;
            } else {
                counter++;
                maxLen = Math.max(maxLen, counter);
            }
        }
        // return maxLen
        return maxLen;
    }
}
