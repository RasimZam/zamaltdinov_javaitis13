package ru.itis.slidingwindows;

// 3. Longest Substring Without Repeating Characters

import java.util.HashMap;
import java.util.Map;

public class LongestSubstringWithoutRepeatingCharacters_3 {
    public static void main(String[] args) {
        String s = "abcabcbb";
        int result = lengthOfLongestSubstring(s);
        System.out.println(result);
    }

    public static int lengthOfLongestSubstring(String s) {
        // abcabcbb
        // base case
        char[] arr = s.toCharArray();
        int length = arr.length;
        if (length < 2) {
            return length;
        }
        // define pointers
        int left = 0, right = 0;
        // define table
        Map<Character, Integer> map = new HashMap<>();
        // define max len
        int maxLen = 0;
        // find longest substring
        while (right < length) {
            // add current element
            map.put(arr[right], map.getOrDefault(arr[right], 0) + 1);
            // check if we meet the condition
            while (map.size() != right - left + 1) {
                map.put(arr[left], map.get(arr[left]) - 1);
                if (map.get(arr[left]) == 0) {
                    map.remove(arr[left]);
                }
                left++;
            }
            // update the maxLen
            maxLen = Math.max(maxLen, right - left + 1);
            // move the right one to the right
            right++;
        }
        // return max len
        return maxLen;
    }

    public static int lengthOfLongestSubstring1(String s) {
        // a b c a b c b b
        //           lr
        int left = 0;
        int minLength = 0;
        Map<Character, Integer> slidingWindow = new HashMap<>();
        for (int right = 0; right < s.length(); right++) {
            char currentChar = s.charAt(right);
            if(slidingWindow.containsKey(currentChar)) {
                left = Math.max(slidingWindow.get(currentChar) + 1, left);
            }
            slidingWindow.put(currentChar, right);
            minLength = Math.max(minLength, right - left + 1);
        }
        return minLength;
    }
}
