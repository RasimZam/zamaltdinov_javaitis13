package ru.itis.slidingwindows;

// 567. Permutation in String

import java.util.Arrays;

public class PermutationInString_567 {
    public static void main(String[] args) {
        String s1 = "ab", s2 = "eidbaooo";
        System.out.println(checkInclusion(s1, s2));
    }

    public static boolean checkInclusion(String s1, String s2) {
        // define table
        int[] arr = new int[128];
        //define pointers
        int left = 0, right = 0;
        //fill up table
        char[] s1_arr = s1.toCharArray();
        char[] s2_arr = s2.toCharArray();

        for (char cur : s1_arr) {
            arr[cur]++;
        }
        // define minLen
        int minLen = Integer.MAX_VALUE;
        //define counter
        int counter = 0;
        //check if there is a permutation
        while (right < s2_arr.length) {
            //expand window
            char cur = s2_arr[right];
            if (--arr[cur] >= 0) {
                counter++;
            }
            //contract window
            while (counter == s1.length()) {
                int curLen = right - left + 1;
                minLen = Math.min(curLen, minLen);
                char leftChar = s2_arr[left];
                if (++arr[leftChar] > 0) {
                    counter--;
                }
                left++;
            }
            right++;
        }
        //return if minLen == size of s1
        return minLen == s1.length();
    }

    public static boolean checkInclusion1(String s1, String s2) {
      if (s1.length() > s2.length() || s2.isEmpty()) return false;
      if (s1.isEmpty()) return true;

      int x = s1.length(), y = s2.length();
      int[] array1 = new int[26];
      int[] array2 = new int[26];

      for(int i = 0; i < x; i++) {
          array1[s1.charAt(i) - 'a']++;
          array2[s2.charAt(i) - 'a']++;
      }

      for(int i = x; i < y; i++) {
          if(Arrays.equals(array1, array2)) {
              return true;
          }
          array2[s2.charAt(i - x) - 'a']--;
          array2[s2.charAt(i) - 'a']++;
      }

      return Arrays.equals(array1, array2);
    }
}

