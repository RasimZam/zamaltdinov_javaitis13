package ru.itis.slidingwindows;

// 424. Longest Repeating Character Replacement

import java.util.HashMap;
import java.util.Map;

public class LongestRepeatingCharacterReplacement_424 {
    public static void main(String[] args) {
        String str = "ABAB";
        String str1 = "AABABBA";
        int result = characterReplacement(str1, 1);
        System.out.println(result);
    }

    public static int characterReplacement(String s, int k) {
        char[] arr = s.toCharArray();
        int left = 0, right = 0, length = s.length();
        //define table
        Map<Character, Integer> map = new HashMap<>();
        //define maxLen and mostReq
        int maxLen = 0, mostFreq = 0;
        //find longest repeating character replacement
        while (right < length) {
            //expand the window
            map.put(arr[right], map.getOrDefault(arr[right], 0) + 1);
            mostFreq = Math.max(mostFreq, map.get(arr[right]));

            //shrink the window if we need to replace more than k char
            if ((right - left + 1) - mostFreq > k) {
                map.put(arr[left], map.get(arr[left]) - 1);
                left++;
            }
            maxLen = Math.max(maxLen, right - left + 1);
            right++;
        }
        return maxLen;
    }

    public static int characterReplacement1(String s, int k) {
        // Make an array of size 26...
        int[] arr = new int[26];
        // Initialize largestCount, maxlen & beg pointer...
        int largestCount = 0, beg = 0, maxlen = 0;
        // Traverse all characters through the loop...
        for (int end = 0; end < s.length(); end++) {
            arr[s.charAt(end) - 'A']++;
            // Get the largest count of a single, unique character in the current window...
            largestCount = Math.max(largestCount, arr[s.charAt(end) - 'A']);
            // We are allowed to have at most k replacements in the window...
            // So, if max character frequency + distance between beg and end is greater than k...
            // this means we have considered changing more than k charactres. So time to shrink window...
            // Then there are more characters in the window than we can replace, and we need to shrink the window...
            if (end - beg + 1 - largestCount > k) {     // The main equation is: end - beg + 1 - largestCount...
                arr[s.charAt(beg) - 'A']--;
                beg++;
            }
            // Get the maximum length of repeating character...
            maxlen = Math.max(maxlen, end - beg + 1);     // end - beg + 1 = size of the current window...
        }
        return maxlen;      // Return the maximum length of repeating character...
    }
}
