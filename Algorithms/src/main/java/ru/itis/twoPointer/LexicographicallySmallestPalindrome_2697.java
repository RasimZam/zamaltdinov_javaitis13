package ru.itis.twoPointer;

import java.util.Arrays;

// 2697. Lexicographically Smallest Palindrome
public class LexicographicallySmallestPalindrome_2697 {
    public static void main(String[] args) {
        String str = "egcfe";
        String str1 = "abcd";
        String str2 = "seven";
        String result = makeSmallestPalindrome1(str);
        System.out.println(result);
    }

    public static String makeSmallestPalindrome(String s) {
        int lengthWord = s.length();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < lengthWord; i++) {
            sb.append(Character.toString(Math.min(s.charAt(i), s.charAt(lengthWord - i - 1))));
        }
        return sb.toString();
    }

    public static String makeSmallestPalindrome1(String s) {
        char[] chars = s.toCharArray();
        int i = 0, j = s.length() - 1;
        while(i < j) {
            if (chars[i] < chars[j]) {
                chars[j--] = chars[i++];
            } else {
                chars[i++] = chars[j--];
            }
        }
        return new String(chars);
    }
}
