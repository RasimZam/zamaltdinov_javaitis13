package ru.itis.twoPointer;

import java.util.Arrays;

// Повернуть массив К шагов
public class Rotate {
    public static void main(String[] args) {
        int[] array = new int[]{1, 2, 3, 4, 5, 6, 7};
        rotate(array, 3);
        System.out.println(Arrays.toString(array));
    }

    public static void rotate(int[] array, int step) {
        reverse(array, 0, array.length - 1);
        reverse(array, 0, step - 1);
        reverse(array, step, array.length - 1);

    }

    private static void reverse(int[] input, int start, int end) {
        while (start < end) {
            int temp = input[start];
            input[start] = input[end];
            input[end] = temp;
            start++;
            end--;
        }
    }
}
