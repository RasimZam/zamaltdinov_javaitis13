package ru.itis.twoPointer;

import java.util.Arrays;

// Two Sum II - Input Array Is Sorted
public class TwoSumII_167 {
    public static void main(String[] args) {
        int[] array = new int[]{2, 7, 11, 15};
        int target = 9;
        int[] result1 = twoSum1(array, target);
        int[] result2 = twoSum2(array, target);

        System.out.println((Arrays.toString(result1)));
        System.out.println((Arrays.toString(result2)));
    }

    // first example
    public static int[] twoSum1(int[] numbers, int target) {
        int leftPointer = 0;
        int rightPointer = numbers.length - 1;

        while(leftPointer < rightPointer) {
            int sum = numbers[leftPointer] + numbers[rightPointer];

            if (sum == target) {
                return new int[]{leftPointer + 1, rightPointer + 1};
            } else if (sum < target) {
                leftPointer++;
            } else {
                rightPointer--;
            }
        }
        return new int[]{};
    }

    // second example
    public static int[] twoSum2(int[] numbers, int target) {
        int leftPointer = 0;
        int rightPointer = numbers.length - 1;

        while(leftPointer < rightPointer) {
            if (numbers[leftPointer] + numbers[rightPointer] == target) {
                return new int[]{leftPointer + 1, rightPointer + 1};
            } else if (numbers[leftPointer] + numbers[rightPointer] > target) {
                rightPointer--;
            } else {
                leftPointer++;
            }
        }
        return new int[]{-1, -1};
    }
}
