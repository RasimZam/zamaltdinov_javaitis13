package ru.itis.twoPointer;

// 27. Remove Element

public class RemoveElement_27 {
    public static void main(String[] args) {
        int[] array = new int[]{3, 2, 2, 3};
        System.out.println(removeElement(array, 3));
    }

    public static int removeElement(int[] array, int target) {
        int left = 0;
        for (int right = 0; right < array.length; right++) {
            if (array[right] != target) {
                array[left] = array[right];
                left++;
            }
        }
        return left;
    }
}
