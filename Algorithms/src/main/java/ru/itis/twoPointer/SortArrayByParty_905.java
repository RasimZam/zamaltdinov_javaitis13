package ru.itis.twoPointer;

import java.util.Arrays;

// 905. Sort Array By Parity
public class SortArrayByParty_905 {
    public static void main(String[] args) {
        int[] nums = {3, 1, 2, 4};
        int[] nums1 = {0};
        sortArrayByParity2(nums);
        System.out.println(Arrays.toString(nums));

    }

    public static int[] sortArrayByParity(int[] nums) {
        for (int i = 0, j = 0; j < nums.length; j++) {
            if (nums[j] % 2 == 0) {
                int temp = nums[i];
                nums[i++] = nums[j];
                nums[j] = temp;
            }
        }
        return nums;
    }

    public static int[] sortArrayByParity1(int[] nums) {
        int[] arr = new int[nums.length];
        int j=0;
        int k = nums.length-1;
        for (int num : nums) {
            if (num % 2 == 0) {
                arr[j] = num;
                j++;
            } else {
                arr[k] = num;
                k--;
            }
        }
        return arr;
    }

    public static int[] sortArrayByParity2(int[] nums) {
        int left = 0;
        int right = nums.length - 1;

        while (left < right) {
            if (nums[left] % 2 == 1 && nums[right] % 2 == 0) {
                int temp = nums[left];
                nums[left] = nums[right];
                nums[right] = temp;
            }

            if (nums[left] % 2 == 0) {
                ++left;
            }

            if (nums[right] % 2 == 1) {
                --right;
            }
        }
        return nums;
    }
}
