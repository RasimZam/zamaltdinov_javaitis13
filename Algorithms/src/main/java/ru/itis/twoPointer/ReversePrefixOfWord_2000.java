package ru.itis.twoPointer;

// 2000. Reverse Prefix of Word
public class ReversePrefixOfWord_2000 {
    public static void main(String[] args) {
        String str = "abcdefd";
        String str1 = "xyxzxe";
        String str2 = "abcd";
        String result = reversePrefix1(str2, 'z');
        System.out.println(result);
    }

    public static String reversePrefix(String word, char ch) {
        char[] c = word.toCharArray();
        int locate = 0;
        for (int i = 0; i < word.length(); i++) {
            if (ch == c[i]) {
                locate = i;
                break;
            }
        }
        char[] res = new char[word.length()];
        for (int i = 0; i <= locate; i++) {
            res[i] = c[locate - i];
        }
        for (int i = locate + 1; i < word.length(); i++) {
            res[i] = c[i];
        }
        return String.valueOf(res);
    }

    public static String reversePrefix1(String word, char ch) {
        int i = word.indexOf(ch) + 1;
        return new StringBuilder(word.substring(0, i))
                .reverse()
                .append(word.substring(i))
                .toString();
    }
}
