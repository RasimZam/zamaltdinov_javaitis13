package ru.itis.twoPointer;

// 1. Two Sum
public class TwoSum {
    public static void main(String[] args) {
        int[] array = new int[]{1, 1, 2, 3, 4, 6, 8, 9};
        System.out.println(twoSum(array, 5));

    }

    public static boolean twoSum(int[] array, int target) {
        int leftPointer = 0;
        int rightPointer = array.length - 1;

        while(leftPointer < rightPointer) {
            int sum = array[leftPointer] + array[rightPointer];

            if (sum == target) {
                return true;
            } else if (sum < target) {
                leftPointer++;
            } else {
                rightPointer--;
            }
        }
        return false;
    }
}
