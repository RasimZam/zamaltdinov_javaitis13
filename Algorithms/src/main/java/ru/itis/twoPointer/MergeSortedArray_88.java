package ru.itis.twoPointer;

import java.util.Arrays;

public class MergeSortedArray_88 {
    public static void main(String[] args) {
        int[] nums1 = new int[]{1, 2, 3, 0, 0, 0};
        int[] nums2 = new int[]{2, 5, 6};
        int m = 3;
        int n = 3;
        merge(nums1, m, nums2, n);
        System.out.println(Arrays.toString(nums1));
    }

    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int p = m + n - 1;
        int pointer1 = m - 1;
        int pointer2 = n - 1;

        while (pointer1 >= 0 && pointer2 >= 0) {
            if (nums1[pointer1] > nums2[pointer2]) {
                nums1[p] = nums1[pointer1];
                pointer1--;
                p--;
            } else {
                nums1[p] = nums2[pointer2];
                pointer2--;
                p--;
            }
        }

        while (pointer2 >= 0) {
            nums1[p] = nums2[pointer2];
            pointer2--;
            p--;
        }
    }
}
