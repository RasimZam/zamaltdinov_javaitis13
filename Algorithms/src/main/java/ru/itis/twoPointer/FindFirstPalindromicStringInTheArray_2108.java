package ru.itis.twoPointer;

// 2108. Find First Palindromic String in the Array
public class FindFirstPalindromicStringInTheArray_2108 {
    public static void main(String[] args) {
        String[] words = {"abc","car","ada","racecar","cool"};
        String[] words1 = {"notapalindrome","racecar"};
        String[] words2 = {"def","ghi"};
        String str = firstPalindrome1(words1);
        System.out.println(str);
    }

    public static String firstPalindrome(String[] words) {
        for (String word : words) {
            if (reverseString(word).equals(word)) {
                return word;
            }
        }
        return "";
    }

    public static String reverseString(String str) {
        char[] ch = str.toCharArray();
        StringBuilder rev = new StringBuilder();
        for (int i = ch.length - 1; i >= 0; i--) {
            rev.append(ch[i]);
        }
        return rev.toString();
    }

    public static String firstPalindrome1(String[] words) {
        for (String word : words) {
            StringBuilder sb = new StringBuilder();
            sb.append(word);
            if (sb.reverse().toString().equals(word)) {
                return word;
            }
        }
        return "";
    }

}
