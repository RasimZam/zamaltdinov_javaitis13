package ru.itis.twoPointer;

import java.util.Arrays;

// 942. DI String Match
public class DIStringMatch_942 {
    public static void main(String[] args) {
        String str = "IDID";
        int[] match = diStringMatch(str);
        System.out.println(Arrays.toString(match));

    }

    public static int[] diStringMatch(String s) {
        final int length = s.length();
        int[] result = new int[length + 1];
        int min = 0;
        int max = length;

        for (int i = 0; i < length; ++i) {
            result[i] = s.charAt(i) == 'I' ? min++ : max--;
            result[length] = min;
        }
        return result;
    }
}
