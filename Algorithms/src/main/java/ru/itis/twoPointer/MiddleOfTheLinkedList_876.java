package ru.itis.twoPointer;

import java.util.List;

// 876. Middle of the Linked List
public class MiddleOfTheLinkedList_876 {
    public static class ListNode {
        int val;
        ListNode next;

        public ListNode() {

        }

        public ListNode(int val) {
            this.val = val;
        }

        public ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

    }
    public static void main(String[] args) {

    }

    public ListNode middleNode(ListNode head) {
        ListNode softPointer = head;
        ListNode fastPointer = head;

        while (fastPointer != null && fastPointer.next != null) {
            softPointer = softPointer.next;
            fastPointer = fastPointer.next.next;
        }
        return softPointer;
    }
}
