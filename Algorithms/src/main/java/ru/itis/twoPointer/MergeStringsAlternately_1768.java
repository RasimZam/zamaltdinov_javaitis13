package ru.itis.twoPointer;

// 1768. Merge Strings Alternately
public class MergeStringsAlternately_1768 {
    public static void main(String[] args) {
//        String word1 = "abc";
//        String word2 = "pqr";
//        String word1 = "ab";
//        String word2 = "pqrs";
        String word1 = "abcd";
        String word2 = "pq";
        String str = mergeAlternately(word1, word2);
        System.out.println(str);

    }

    public static String mergeAlternately(String word1, String word2) {
        StringBuilder result = new StringBuilder();
        int i = 0;

        while (i < word1.length() || i < word2.length()) {
            if (i < word1.length()) {
                result.append(word1.charAt(i));
            }

            if (i < word2.length()) {
                result.append(word2.charAt(i));
            }
            i++;
        }
        return result.toString();
    }
}
