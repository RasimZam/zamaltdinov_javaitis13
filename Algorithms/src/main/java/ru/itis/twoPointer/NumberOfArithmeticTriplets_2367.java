package ru.itis.twoPointer;

import java.util.HashSet;

// 2367. Number of Arithmetic Triplets
public class NumberOfArithmeticTriplets_2367 {
    public static void main(String[] args) {
        int[] nums = new int[]{0, 1, 4, 6, 7, 10};
        int triplets = arithmeticTriplets1(nums, 3);
        System.out.println(triplets);
    }

    public static int arithmeticTriplets(int[] nums, int diff) {
        int res = 0;
        HashSet<Integer> set = new HashSet<>();
        for (int num : nums) {
            if (set.contains(num - diff) && set.contains(num - 2 * diff)) {
                res++;
            }
            set.add(num);
        }
        return res;
    }

    public static int arithmeticTriplets1(int[] nums, int diff) {
        int count = 0;
        for (int i = 0; i < nums.length - 2; i++) {
            for (int j = i + 1; j < nums.length -1; j++) {
                for (int k = j + 1; k < nums.length; k++) {
                    if (nums[j] - nums[i] == diff && nums[k] - nums[j] == diff) {
                        count++;
                    }
                }
            }
        }
        return count;
    }
}

