package ru.itis.array;

// 977. Squares of a Sorted Array

import java.util.Arrays;

public class SquaresOfASortedArray_977 {
    public static void main(String[] args) {
        int[] nums = {-4, -1, 0, 3, 10};
        int[] result = sortedSquares1(nums);
        System.out.println(Arrays.toString(result));
    }

    public static int[] sortedSquares(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            nums[i] *= nums[i];
        }
        Arrays.sort(nums);
        return nums;
    }

    public static int[] sortedSquares1(int[] nums) {
        int leftPointer = 0;
        int rightPointer = nums.length - 1;
        int index = nums.length - 1;
        int[] result = new int[nums.length];

        while (leftPointer <= rightPointer) {
            if (Math.abs(nums[leftPointer]) > Math.abs(nums[rightPointer])) {
                result[index] = nums[leftPointer] * nums[leftPointer];
                leftPointer++;
            } else {
                result[index] = nums[rightPointer] * nums[rightPointer];
                rightPointer--;
            }
            index--;
        }
        return result;
    }
}
