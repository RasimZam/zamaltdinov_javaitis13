package ru.itis.array;

import java.util.Arrays;

public class ArrayOfProducts {
    public static void main(String[] args) {
        int[] products = {5, 1, 4, 2};
        int[] result = arrayOfProducts(products);
        System.out.println(Arrays.toString(result));
    }

    public static int[] arrayOfProducts(int[] arr) {
        int[] products = new int[arr.length];
        int leftRunningProduct = 1;
        for (int i = 0; i < arr.length; i++) {
            products[i] = leftRunningProduct;
            leftRunningProduct *= arr[i];
        }

        int rightRunningProduct = 1;
        for (int i = arr.length - 1; i >= 0; i--) {
            products[i] *= rightRunningProduct;
            rightRunningProduct *= arr[i];
        }
        return products;
    }
}
