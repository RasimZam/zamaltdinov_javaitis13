package ru.itis.array;

public class FirstDuplicateValue {
    public static void main(String[] args) {
        int[] array = {2, 1, 5, 2, 3, 3, 4};
        int result = firstDuplicate(array);
        System.out.println(result);
    }

    public static int firstDuplicate(int[] array) {
        for (int value : array) {
            int absValue = Math.abs(value);
            if (array[absValue - 1] < 0) {
                return absValue;
            } else {
                array[absValue - 1] *= -1;
            }
        }
        return -1;
    }

    public static int firstDuplicate1(int[] array) {
        int minimumSecondIndex = array.length;
        for (int i = 0; i < array.length; i++) {
            int value = array[i];
            for (int j = i + 1; j < array.length; j++) {
                int valueToCompare = array[j];
                if (value == valueToCompare) {
                    minimumSecondIndex = Math.min(minimumSecondIndex, j);
                }
            }
        }
        if (minimumSecondIndex == array.length) {
            return -1;
        }
        return array[minimumSecondIndex];
    }
}
