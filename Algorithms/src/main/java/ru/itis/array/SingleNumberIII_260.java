package ru.itis.array;

// 260. Single Number III

import java.util.HashSet;

public class SingleNumberIII_260 {
    public static void main(String[] args) {

    }


    // O(n^2) time complexity
    // O(1) memory complexity
    public static int[] singleNumber(int[] nums) {
        int n = nums.length;
        int[] result = new int[2];
        int index = 0;

        for (int i = 0; i < n; i++) {
            boolean found = false;
            for (int j = 0; j < n; j++) {
                if (i != j && nums[i] == nums[j]) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                result[index++] = nums[i];
                if (index == 2) {
                    break;
                }
            }
        }

        return result;
    }

    // O(n) time complexity
    // O(n) memory complexity
    public static int[] singleNumber1(int[] nums) {
        HashSet<Integer> set = new HashSet<>();

        for (int num : nums) {
            if (set.contains(num)) {
                set.remove(num);
            } else {
                set.add(num);
            }
        }
        int[] result = new int[2];
        int i = 0;
        for (int num : set) {
            result[i++] = num;
        }
        return result;
    }

    // O(n) time complexity
    // O(1) memory complexity
    public static int[] singleNumber2(int[] nums) {
        int xor = 0;
        for (int num : nums) {
            xor ^= num;
        }
        int[] res = new int[2];
        int lowesSetBit = xor & (xor * (-1));

        for (int elem : nums) {
            if ((lowesSetBit & elem) == 0) {
                res[0] ^= elem;
            } else {
                res[1] ^= elem;
            }
        }
        return res;
    }
}
