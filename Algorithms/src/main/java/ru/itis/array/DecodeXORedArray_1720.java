package ru.itis.array;

// 1720. Decode XORed Array

import java.util.Arrays;

public class DecodeXORedArray_1720 {
    public static void main(String[] args) {
        int[] encoded = {1, 2, 3};
        int[] result = decode(encoded, 1);
        System.out.println(Arrays.toString(result));
    }

    public static int[] decode(int[] encoded, int first) {
        int n = encoded.length, res[] = new int[n + 1];
        res[0] = first;
        for (int i = 0; i < n; ++i) {
            // res[0] = 1
            // res[1] = 1 ^ 1 = 0
            // i = 1; res[2] = 0 ^ 2 = 2
            res[i + 1] = res[i] ^ encoded[i];
        }
        return res;
    }
}
