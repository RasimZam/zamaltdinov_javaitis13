package ru.itis.array;

// 1920. Build Array from Permutation

import java.util.Arrays;

public class BuildArrayFromPermutation_1920 {
    public static void main(String[] args) {
        int[] array = {0, 2, 1, 5, 3, 4};
        int[] result = buildArray1(array);
        System.out.println(Arrays.toString(result));
    }

    public static int[] buildArray(int[] nums) {
        int[] ans = new int[nums.length];

        for (int i = 0; i < nums.length; i++) {
            ans[i] = nums[nums[i]];
        }
        return ans;
    }

    public static int[] buildArray1(int[] nums) {
        int n = nums.length;

        for (int i = 0; i < n; ++i) {
            nums[i] += n * (nums[nums[i]] % n);
        }

        for (int i = 0; i < n; ++i) {
            nums[i] /= n;
        }
        return nums;
    }
}
