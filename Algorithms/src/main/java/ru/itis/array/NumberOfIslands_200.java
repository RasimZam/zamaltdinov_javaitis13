package ru.itis.array;

// 200. Number of Islands
// O(n * m) time complexity
// O(n * m) memory complexity

public class NumberOfIslands_200 {
    public static void main(String[] args) {

    }

    public int numIslands1(char[][] grid) {
        int count = 0;
        boolean[][] visited = new boolean[grid.length][grid[0].length];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if(grid[i][j] == '1' && !visited[i][j]) {
                    callDFS1(grid, visited, i, j);
                    count++;
                }
            }
        }
        return count;
    }

    private void callDFS1(char[][] grid, boolean[][] visited, int row, int col) {
        if (row < 0 || row >= grid.length || col < 0 || col >= grid[row].length || grid[row][col] == '0'
        || visited[row][col]) {
            return;
        }
        visited[row][col] = true;
        callDFS1(grid, visited, row + 1, col);
        callDFS1(grid, visited, row - 1, col);
        callDFS1(grid, visited, row, col - 1);
        callDFS1(grid, visited, row, col + 1);
    }

    public int numIslands(char[][] grid) {
        int count = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if(grid[i][j] == '1') {
                    count++;
                    callDFS(grid, i, j);
                }
            }
        }
        return count;
    }

    private void callDFS(char[][] grid, int row, int col) {
        if (row < 0 || row >= grid.length || col < 0 || col >= grid[row].length || grid[row][col] == '0') {
            return;
        }

        grid[row][col] = '0';
        callDFS(grid, row + 1, col);
        callDFS(grid, row - 1, col);
        callDFS(grid, row, col - 1);
        callDFS(grid, row, col + 1);
    }
}
