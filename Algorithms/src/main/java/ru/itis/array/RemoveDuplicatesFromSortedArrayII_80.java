package ru.itis.array;

// 80. Remove Duplicates from Sorted Array II

public class RemoveDuplicatesFromSortedArrayII_80 {
    public static void main(String[] args) {
        int[] nums = {1, 1, 1, 2, 2, 3};
        int result = removeDuplicates(nums);
        System.out.println(result);
    }

    public static int removeDuplicates(int[] nums) {
        int l = 0;
        for (int r = 0; r < nums.length; r++) {
            if (l < 2 || nums[l - 2] < nums[r]) {
                nums[l++] = nums[r];
            }
        }
        return l;
    }
}
