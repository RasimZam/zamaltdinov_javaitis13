package ru.itis.array;

public class MajorityElement {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 2, 2, 1, 2};
        int result = majorityElement1(arr);
        System.out.println(result);
    }

    private static int majorityElement(int[] arr) {
        int count = 0;
        int answer = arr[0];
        for(int value : arr) {
            if(count == 0) {
                answer = value;
            }
            if(value == answer) {
                count++;
            } else {
                count--;
            }
        }
        return answer;
    }

    private static int majorityElement1(int[] arr) {
        int majority = arr[0];
        int votes = 1;
        for(int i = 1; i < arr.length; i++) {
            if(votes == 0) {
                votes++;
                majority = arr[i];
            } else if(majority == arr[i]) {
                votes++;
            } else {
                votes--;
            }
        }
        return majority;
    }
}
