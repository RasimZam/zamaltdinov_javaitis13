package ru.itis.array;

// 75. Sort Colors

public class SortColors_75 {
    public static void main(String[] args) {
        int[] nums = {2, 0, 2, 1, 1, 0};
        sortColors(nums);
    }

    public static void sortColors1(int[] nums) {
        int start = 0;
        int mid = 0;
        int end = nums.length - 1;
        while(mid <= end) {
            switch (nums[mid]) {
                case 0:
                    swap(nums, start, mid);
                    mid++;
                    start++;
                    break;
                case 1:
                    mid++;
                    break;
                case 2:
                    swap(nums, mid, end);
                    end++;
                    break;
            }
        }
    }

    public static void swap(int[] nums, int pos1, int pos2) {
        int temp = nums[pos1];
        nums[pos1] = nums[pos2];
        nums[pos2] = temp;
    }

    public static void sortColors(int[] nums) {
        int redCount = 0;
        int whiteCount = 0;
        int blueCount = 0;

        for (int num : nums) {
            if (num == 0) {
                redCount++;
            } else if (num == 1) {
                whiteCount++;
            } else if (num == 2) {
                blueCount++;
            }
        }
        int pointer = 0;
        if(redCount != 0) {
            while(redCount > 0) {
                nums[pointer] = 0;
                redCount--;
                pointer++;
            }
        }

        if(whiteCount != 0) {
            while(whiteCount > 0) {
                nums[pointer] = 1;
                whiteCount--;
                pointer++;
            }
        }

        if(blueCount != 0) {
            while(blueCount > 0) {
                nums[pointer] = 2;
                blueCount--;
                pointer++;
            }
        }
    }
}
