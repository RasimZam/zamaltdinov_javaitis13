package ru.itis.array;

import java.util.Arrays;

public class MissingNumbers {
    public static void main(String[] args) {
        int[] nums = {1, 4, 3};
        int[] result = missingNumbers(nums);
        System.out.println(Arrays.toString(result));

    }

    public static int[] missingNumbers(int[] nums) {
        int total = 0;
        for (int i = 1; i < nums.length + 3; i++) {
            total += i;
        }

        for (int num : nums) {
            total -= num;
        }

        int averageMissingValue = total / 2;
        int foundFirstHalf = 0;
        int foundSecondHalf = 0;
        for (int num : nums) {
            if (num <= averageMissingValue) {
                foundFirstHalf += num;
            } else {
                foundSecondHalf += num;
            }
        }

        int expectedFirstHalf = 0;
        for (int i = 1; i <= averageMissingValue; i++) {
            expectedFirstHalf += i;
        }

        int expectedSecondHalf = 0;
        for (int i = averageMissingValue + 1; i < nums.length + 3 ; i++) {
            expectedSecondHalf += i;
        }

        return new int[]{expectedFirstHalf - foundFirstHalf, expectedSecondHalf - foundSecondHalf};
    }
}
