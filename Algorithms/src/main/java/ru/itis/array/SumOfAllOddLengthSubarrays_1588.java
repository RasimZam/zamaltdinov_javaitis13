package ru.itis.array;

// 1588. Sum of All Odd Length Subarrays

public class SumOfAllOddLengthSubarrays_1588 {
    public static void main(String[] args) {
        int[] arr = {1, 4, 2, 5, 3};
        int result = sumOddLengthSubarrays(arr);
        System.out.println(result);
    }

    public static int sumOddLengthSubarrays(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; ++i) {
            // left = 0 + 1 = 1
            // left = 1 + 1 = 2
            int left = i + 1;
            // right = 5 - 0 = 5
            // right = 5 - 1 = 4
            int right = arr.length - i;
            // sub_arrays = 1 * 5 = 5
            // sub_arrays = 2 * 4 = 8
            int num_subarrays = left * right;
            // odd_subarrays = 1
            // odd_subarrays = 1
            int odd_subarrays = num_subarrays % 2 == 0 ? num_subarrays / 2 : num_subarrays / 2 + 1;
            // sum = 1 * 0 = 0
            // sum = 1 * 1 = 1
            sum += odd_subarrays * arr[i];
        }
        return sum;
    }
}
