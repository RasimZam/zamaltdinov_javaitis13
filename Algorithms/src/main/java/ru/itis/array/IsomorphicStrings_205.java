package ru.itis.array;

// 205. Isomorphic Strings

import java.util.HashMap;

public class IsomorphicStrings_205 {
    public static void main(String[] args) {
//        String s = "egg";
//        String t = "add";
        String s = "foo";
        String t = "bar";
        boolean isomorphic = isIsomorphic(s, t);
        System.out.println(isomorphic);

    }

    public static boolean isIsomorphic(String s, String t) {
        if(s.length() != t.length()) {
            return false;
        }
        HashMap<Character, Character> charMapping = new HashMap<>();
        for(int i = 0; i < s.length(); i++) {
            char original = s.charAt(i);
            char replacement = t.charAt(i);

            if(!charMapping.containsKey(original)) {
                if(!charMapping.containsValue(replacement)) {
                    charMapping.put(original, replacement);
                } else {
                    return false;
                }
            } else {
                char mappedCharacter = charMapping.get(original);
                if(mappedCharacter != replacement) {
                    return false;
                }
            }
        }
        return true;
    }

}
