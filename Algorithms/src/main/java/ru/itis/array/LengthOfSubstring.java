package ru.itis.array;

import java.util.HashMap;
import java.util.HashSet;

public class LengthOfSubstring {
    public static void main(String[] args) {
        String s = "pwwkew";
        int result = lengthOfSubstring(s);
        System.out.println(result);
    }

    public static int lengthOfSubstring(String s) {
        int maxLength = 0;
        int left = 0;
        HashMap<Character, Integer> slidingWindow = new HashMap<>();
        for (int right = 0; right < s.length(); right++) {
            char c = s.charAt(right);
            if (slidingWindow.containsKey(c)) {
                left = Math.max(slidingWindow.get(c) + 1, left);
            }
            slidingWindow.put(c, right);
            maxLength = Math.max(maxLength, right - left + 1);
        }
        return maxLength;
    }

    public static int lengthOfSubstring1(String s) {
        int maxLength = 0;
        int left = 0;
        HashSet<Character> slidingWindow = new HashSet<>();
        for (int right = 0; right < s.length(); right++) {
            char c = s.charAt(right);
            while (slidingWindow.contains(c)) {
                slidingWindow.remove(s.charAt(left));
                left++;
            }
            slidingWindow.add(c);
            maxLength = Math.max(maxLength, right - left + 1);
        }
        return maxLength;
    }
}
