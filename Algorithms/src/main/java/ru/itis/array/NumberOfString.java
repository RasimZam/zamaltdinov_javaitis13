package ru.itis.array;

import java.util.HashMap;
import java.util.Map;

public class NumberOfString {
    public static void main(String[] args) {
        String s = "abcabc";
        int result = numberOfStrings1(s);
        System.out.println(result);
    }

    public static int numberOfStrings(String s) {
        Map<Character, Integer> map = new HashMap<>();
        int count = 0;
        int start = 0;
        int end = 0;
        int n = s.length();
        while (end < n) {
            char c = s.charAt(end);
            map.put(c, map.getOrDefault(c, 0) + 1);
            while (map.getOrDefault('a', 0) > 0 && map.getOrDefault('b', 0) > 0
                    && map.getOrDefault('c', 0) > 0) {
                count += n - end;
                c = s.charAt(start);
                map.put(c, map.get(c) - 1);
                start++;
            }
            end++;
        }
        return count;
    }

    public static int numberOfStrings1(String s) {
        int[] latestPosition = new int[]{-1, -1, -1};
        int answer = 0;

        for (int i = 0; i < s.length(); ++i) {
            char currentChar = s.charAt(i);
            latestPosition[currentChar - 'a'] = i;

            int minPosition = Math.min(latestPosition[0], Math.min(latestPosition[1], latestPosition[2]));
            answer += minPosition + 1;
        }
        return answer;
    }
}
