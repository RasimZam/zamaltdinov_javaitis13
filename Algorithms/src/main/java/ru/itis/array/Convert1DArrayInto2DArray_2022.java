package ru.itis.array;

// 2022. Convert 1D Array Into 2D Array

public class Convert1DArrayInto2DArray_2022 {
    public static void main(String[] args) {

    }

    public static int[][] construct2DArray(int[] original, int m, int n) {
        if(m * n != original.length) {
            return new int[0][0];
        }

        int[][] result = new int[m][n];
        int idx = 0;

        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                result[i][j] = original[idx++];
            }
        }

        return result;
    }
}
