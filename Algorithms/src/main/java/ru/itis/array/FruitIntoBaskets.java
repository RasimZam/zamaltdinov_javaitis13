package ru.itis.array;

import com.fasterxml.jackson.databind.node.BigIntegerNode;

import java.util.HashMap;

public class FruitIntoBaskets {
    public static void main(String[] args) {
        int[] fruits = {1, 2, 1, 1, 3, 4, 2, 2, 2, 2, 4};
        int result = fruitBaskets(fruits);
        System.out.println(result);
    }

    public static int fruitBaskets(int[] fruits) {
        int left = 0;
        int right = 0;
        int maxFruit = 0;
        HashMap<Integer, Integer> basket = new HashMap<>();

        for (right = 0; right < fruits.length; right++) {
            int currentCount = basket.getOrDefault(fruits[right], 0);
            basket.put(fruits[right], currentCount + 1);
            while (basket.size() > 2) {
                int fruitCount = basket.get(fruits[left]);
                if (fruitCount == 1) {
                    basket.remove(fruits[left]);
                } else {
                    basket.put(fruits[left], fruitCount - 1);
                }
                left++;
            }
            maxFruit = Math.max(maxFruit, right - left + 1);
        }
        return maxFruit;
    }
}
