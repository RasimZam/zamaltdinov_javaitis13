package ru.itis.array;

// 1512. Number of Good Pairs

import java.util.HashMap;
import java.util.Map;

public class NumberOfGoodPairs_1512 {
    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 1, 1, 3};
        int result = numIdenticalPairs(nums);
        System.out.println(result);
    }

    public static int numIdenticalPairs(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        int res = 0;

        for (int num : nums) {
            if (map.containsKey(num)) {
                res += map.get(num);
            }

            map.put(num, map.getOrDefault(num, 0) + 1);
        }

        return res;
    }
}
