package ru.itis.array;

// 2535. Difference Between Element Sum and Digit Sum of an Array

import java.util.Arrays;

public class DifferenceBetweenElementSumAndDigitSumOfAnArray_2535 {
    public static void main(String[] args) {
        int[] nums = {1, 15, 6, 3};
        int result = differenceOfSum(nums);
        System.out.println(result);
    }

    public static int differenceOfSum(int[] nums) {
        int esum = 0, dsum = 0;

        for (int i = 0; i < nums.length; i++) {
            esum += nums[i];
            while (nums[i] != 0) {
                dsum += nums[i] % 10;
                nums[i] /= 10;
            }
        }
        return Math.abs(dsum - esum);
    }
}
