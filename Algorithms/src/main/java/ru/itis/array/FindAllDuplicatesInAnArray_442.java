package ru.itis.array;

// 442. Find All Duplicates in an Array

import java.util.ArrayList;
import java.util.List;

public class FindAllDuplicatesInAnArray_442 {
    public static void main(String[] args) {

    }

    // O(n) time complexity
    // O(1) memory complexity
    public List<Integer> findDuplicates(int[] nums) {
        List<Integer> result = new ArrayList<>();

        for(int i = 0; i < nums.length; i++) {
            int index = Math.abs(nums[i]) - 1;

            if(nums[index] < 0) {
                result.add(index + 1);
            }

            nums[index] = nums[index] * -1;
        }
        return result;
    }
}
