package ru.itis.array;

import java.util.Arrays;

public class RunningSumOf1DArray_1480 {
    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4};
        int[] resultSumArray = runningSum(nums);
        System.out.println(Arrays.toString(resultSumArray));
    }

    public static int[] runningSum(int[] nums) {
        int[] result = new int[nums.length];
        int temp = 0;
        for (int i = 0, j = 0; i < nums.length; i++, j++) {
            temp += nums[j];
            result[i] = temp;
        }
        return result;
    }

    public static int[] runningSum1(int[] nums) {
        for (int i = 1; i < nums.length; i++) {
            nums[i] = nums[i - 1] + nums[i];
        }
        return nums;
    }
}
