package ru.itis.array;

// 1470. Shuffle the Array

import java.util.Arrays;

public class ShuffleTheArray_1470 {
    public static void main(String[] args) {
        int[] nums = {2, 5, 1, 3, 4, 7};
        int[] result = shuffle(nums, 3);
        System.out.println(Arrays.toString(result));
    }

    public static int[] shuffle(int[] nums, int n) {
        int[] result = new int[nums.length];
        int leftPointer = 0;
        int rightPointer = n;

        for (int i = 1; i < nums.length; i += 2) {
            result[i - 1] = nums[leftPointer++];
            result[i] = nums[rightPointer++];
        }
        return result;
    }
}
