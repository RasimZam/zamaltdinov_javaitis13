package ru.itis.array;

// 724. Find Pivot Index

public class FindPivotIndex_724 {
    public static void main(String[] args) {

    }

    // O(n) time complexity
    // O(1) memory complexity
    public static int pivotIndex(int[] nums) {
        int rightSum = 0;
        for(int num : nums) {
            rightSum += num;
        }

        int leftSum = 0;
        for(int i = 0; i < nums.length; i++) {
            rightSum -= nums[i];

            if(leftSum == rightSum) {
                return i;
            }
            leftSum += nums[i];
        }
        return -1;
    }
}
