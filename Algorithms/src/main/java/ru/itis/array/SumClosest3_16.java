package ru.itis.array;

// 16. 3Sum Closest

public class SumClosest3_16 {
    public static void main(String[] args) {

    }

    public int threeSumClosest(int[] nums, int target) {
        int resultSum = 0;
        int minDifference = Integer.MAX_VALUE;

        for (int i = 0; i < nums.length - 2; i++) {
            int left = i + 1;
            int right = nums.length - 1;

            while (left < right) {
                int sum = nums[i] + nums[left] + nums[right];

                if (sum == target) {
                    return target;
                } else if (sum < target) {
                    left++;
                } else {
                    right--;
                }

                int diffToTarget = Math.abs(sum - target);
                if(diffToTarget < minDifference) {
                    minDifference = diffToTarget;
                    resultSum = sum;
                }
            }
        }
        return resultSum;
    }
}
