package ru.itis.array;

// 45. Jump Game II

// time O(n)
// space O(1)

public class JumpGameII_45 {
    public static void main(String[] args) {
        int[] nums = {2, 3, 1, 1, 4};
        int result = jump(nums);
        System.out.println(result);
    }

    public static int jump(int[] nums) {
        int totalJumps = 0;

        int destination = nums.length - 1;
        int coverage = 0, lastJumpIdx = 0;
        if (nums.length == 1) {
            return 0;
        }

        for (int i = 0; i < nums.length; i++) {
            coverage = Math.max(coverage, i + nums[i]);

            if (i == lastJumpIdx) {
                lastJumpIdx = coverage;
                totalJumps++;

                if (coverage >= destination) {
                    return totalJumps;
                }
            }
        }
        return totalJumps;
    }
}
