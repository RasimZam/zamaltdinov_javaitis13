package ru.itis.array;

public class Array {
    public static void main(String[] args) {
        int[] array1 = {1, 2, 4, 5};
        int[] array2 = {3, 3, 4};
        int[] array3 = {2, 3, 4, 5, 6};
        int i = 0;
        int j = 0;
        int k = 0;

        while (i < array1.length && j < array2.length && k < array3.length) {
            if (array1[i] == array2[j] && array2[j] == array3[k]) {
                System.out.println("Common number: " + array1[i]);
                break;
            } else if (array1[i] < array2[j]) {
                i++;
            } else if (array2[j] < array3[k]) {
                j++;
            } else {
                k++;
            }
        }
    }
}
