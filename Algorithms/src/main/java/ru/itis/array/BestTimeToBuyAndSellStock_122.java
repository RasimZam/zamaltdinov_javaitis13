package ru.itis.array;

// 122. Best Time to Buy and Sell Stock II

//O(n) time
//O(1) space

public class BestTimeToBuyAndSellStock_122 {
    public static void main(String[] args) {

    }

    public static int maxProfit(int[] prices) {
        int maxProfit = 0;
        for (int i = 0; i < prices.length - 1; i++) {
            if(prices[i + 1] - prices[i] > 0) {
                maxProfit += prices[i + 1] - prices[i];
            }
        }
        return maxProfit;
    }

}
