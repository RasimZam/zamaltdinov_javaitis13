package ru.itis.array;

// 2574. Left and Right Sum Differences

import java.util.Arrays;

public class LeftAndRightDifferences_2574 {
    public static void main(String[] args) {
        int[] nums = {10, 4, 8, 3};
        int[] result = leftRightDifference(nums);
        System.out.println(Arrays.toString(result));
    }

    public static int[] leftRightDifference(int[] nums) {
        int leftSum = 0, rightSum = 0, n = nums.length;

        for (int num : nums) {
            rightSum += num;
        }

        for (int i = 0; i < n; i++) {
            int val = nums[i];
            rightSum -= val;
            nums[i] = Math.abs(leftSum - rightSum);
            leftSum += val;
        }
        return nums;
    }
}
