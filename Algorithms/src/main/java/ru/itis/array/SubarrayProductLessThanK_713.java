package ru.itis.array;

// 713. Subarray Product Less Than K

public class SubarrayProductLessThanK_713 {
    public static void main(String[] args) {
        int[] nums = {10, 5, 2, 6};
        int k = 100;
        int result = numSubarrayProductLessThanK(nums, k);
        System.out.println(result);
    }

    public static int numSubarrayProductLessThanK(int[] nums, int k) {
        if (k <= 1) return 0;
        int product = 1;
        int left = 0;
        int ans = 0;

        for (int right = 0; right < nums.length; right++) {
            product *= nums[right];
            while (product >= k) {
                product /= nums[left];
            }
            ans += right - left + 1;
        }
        return ans;
    }
}
