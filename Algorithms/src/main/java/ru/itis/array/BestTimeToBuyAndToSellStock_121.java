package ru.itis.array;

// 121. Best Time to Buy and Sell Stock

public class BestTimeToBuyAndToSellStock_121 {
    public static void main(String[] args) {
        int[] prices = {7, 1, 5, 3, 6, 4};
        int maxProfit = maxProfit2(prices);
        System.out.println(maxProfit);
    }

    public static int maxProfit(int[] prices) {
        int maxValue = Integer.MAX_VALUE;
        int maxProfit = 0;
        int profitToday = 0;

        for(int i = 0; i < prices.length; i++) {
            if(prices[i] < maxValue) {
                maxValue = prices[i];
            }
            profitToday = prices[i] - maxValue;
            if(maxProfit < profitToday) {
                maxProfit = profitToday;
            }
        }
        return maxProfit;
    }

    public static int maxProfit1(int[] prices) {
        int minValue = Integer.MAX_VALUE;
        int maxProfit = 0;

        for(int i = 0; i < prices.length; i++) {
            if(prices[i] < minValue) {
                minValue = prices[i];
            } else if(prices[i] - minValue > maxProfit) {
                maxProfit = prices[i] - minValue;

            }
        }
        return maxProfit;
    }

    public static int maxProfit2(int[] prices) {
        int buyPrice = prices[0];
        int maxProfit = 0;

        for(int i = 1; i < prices.length; i++) {
            if(prices[i] < buyPrice) {
                buyPrice = prices[i];
            } else {
                int currentProfit = prices[i] - buyPrice;
                maxProfit = Math.max(currentProfit, maxProfit);
            }
        }
        return maxProfit;
    }
}
