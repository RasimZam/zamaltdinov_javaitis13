package ru.itis.sort;

// Сортировка подсчетом

import java.util.Arrays;

public class CountingSort {
    public static void main(String[] args) {
        int[] array = new int[]{5, 0, -2, 7, 3, -2};
        System.out.println(Arrays.toString(array));
        countingSort(array);
        System.out.println(Arrays.toString(array));
    }

    public static void countingSort(int[] array) {
        int[] minMax = findMinMax(array);
        int minValue = minMax[0];
        int maxValue = minMax[1];
        int[] support = new int[maxValue - minValue + 1];
        for (int element: array) {
            support[element - minValue]++;
        }
        int index = 0;
        for (int i = 0; i < support.length; i++) {
            for (int j = 0; j < support[i]; j++) {
                array[index] = i + minValue;
                index++;
            }
        }
    }

    public static int[] findMinMax(int[] array) {
        int min = array[0];
        int max = array[0];
        for (int element: array) {
            if (min > element) {
                min = element;
            }
            if (max < element) {
                max = element;
            }
        }
        return new int[]{min, max};
    }
}
