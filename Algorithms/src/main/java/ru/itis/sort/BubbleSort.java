package ru.itis.sort;

import java.util.Arrays;

// сортировка пузырьком
public class BubbleSort {
    public static void main(String[] args) {
        int[] array = new int[]{5, 0, 2, 7, 3};

//        int sortedIndex = array.length;
//        boolean isSorted = false;
//
//        while (!isSorted) {
//            isSorted = true;
//            for (int i = 0; i < sortedIndex - 1; i++) {
//                if (array[i] > array[i + 1]) {
//                    isSorted = false;
//                    int temp = array[i];
//                    array[i] = array[i + 1];
//                    array[i + 1] = temp;
//                }
//            }
//        }
//
//        System.out.println(Arrays.toString(array));

        int[] result = bubbleSort(array);
        System.out.println(Arrays.toString(result));
    }

    public static int[] bubbleSort(int[] array) {
        if (array.length == 0) {
            return new int[]{};
        }
        boolean isSorted = false;
        int counter = 0;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1 - counter; i++) {
                if (array[i] > array[i + 1]) {
                    swap(i, i + 1, array);
                    isSorted = false;
                }
            }
            counter++;
        }
        return array;
    }

    private static void swap(int i, int j, int[] array) {
        int temp = array[j];
        array[j] = array[i];
        array[i] = temp;
    }
}
