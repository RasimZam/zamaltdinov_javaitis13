package ru.itis.sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RadixSortWithDynamicLists {
    public static void main(String[] args) {
        int[] array = {41, 573, 3, 836, 93, 508, 271};
        sort(array);
        System.out.println(Arrays.toString(array));
    }

    public static void sort(int[] elements) {
        int max = getMaximum(elements);
        int numberOfDigits = getNumberOfDigits(max);

        for (int digitIndex = 0; digitIndex < numberOfDigits; digitIndex++) {
            sortByDigit(elements, digitIndex);
        }
    }

    private static void sortByDigit(int[] elements, int digitIndex) {
        Bucket[] buckets = partition(elements, digitIndex);
        collect(buckets, elements);
    }

    private static Bucket[] partition(int[] elements, int digitIndex) {
        Bucket[] buckets = createBuckets();
        distributeToBuckets(elements, digitIndex, buckets);
        return buckets;
    }

    private static void distributeToBuckets(int[] elements, int digitIndex, Bucket[] buckets) {
        int divisor = calculateDivisor(digitIndex);
        for (int element : elements) {
            int digit = element / divisor % 10;
            buckets[digit].add(element);
        }
    }

    private static int calculateDivisor(int digitIndex) {
        int divisor = 1;
        for (int i = 0; i < digitIndex; i++) {
            divisor *= 10;
        }
        return divisor;
    }

    private static void collect(Bucket[] buckets, int[] elements) {
        int targetIndex = 0;
        for (Bucket bucket : buckets) {
            for (int element : bucket.getElements()) {
                elements[targetIndex] = element;
                targetIndex++;
            }
        }
    }

    private static Bucket[] createBuckets() {
        Bucket[] buckets = new Bucket[10];
        for (int i = 0; i < 10; i++) {
            buckets[i] = new Bucket();
        }
        return buckets;
    }

    private static int getNumberOfDigits(int number) {
        int numberOfDigits = 1;
        while (number >= 10) {
            number /= 10;
            numberOfDigits++;
        }
        return numberOfDigits;
    }

    private static int getMaximum(int[] elements) {
        int max = 0;
        for (int element : elements) {
            if (element > max) {
                max = element;
            }
        }
        return max;
    }

    private static class Bucket {
        private final List<Integer> elements = new ArrayList<>();

        private void add(int element) {
            elements.add(element);
        }

        private List<Integer> getElements() {
            return elements;
        }
    }
}
