package ru.itis.sort;

import java.util.Arrays;

// Быстрая сортировка. Разбиение Ломуто

public class QuickSortLomuto {
    public static void main(String[] args) {
        int[] array = new int[]{5, 0, -2, 7, 3, -2};
        quickSort(array);
        System.out.println(Arrays.toString(array));
    }

    public static void quickSort(int[] array) {
        quickSort(array, 0, array.length - 1);
    }

    public static void quickSort(int[] array, int lo, int hi) {
        if (lo >= hi) {
            return;
        }
        int h = breakPartition(array, lo, hi);
        quickSort(array, lo, h - 1);
        quickSort(array, h + 1, hi);
    }

    private static int breakPartition(int[] array, int lo, int hi) {
        int j = lo;
        int supportElement = array[lo];
        for (int i = lo + 1; i <= hi; i++) {
            if (array[i] < supportElement) {
                j++;
                swap(array, i, j);
            }
        }
        swap(array, lo, j);
        return j;
    }

    private static void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
