package ru.itis.binarysearch;

import java.util.List;

public class Search_10_4 {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(-1, 0, 3, 4, 5, 7, 8, 10);
        int result = search(numbers, 4);
        System.out.println(result);
    }

    public static int search(List<Integer> numbers, int targetNumber) {
        int index = 1;
        while (numbers.get(index) != -1 && numbers.get(index) < targetNumber) {
            index *= 2;
        }
        return binarySearch(numbers, targetNumber, 0, index);
    }

    private static int binarySearch(List<Integer> numbers, int targetNumber, int low, int high) {
        int mid;

        while (low <= high) {
            mid = (low + high) / 2;
            int middleElement = numbers.get(mid);
            if (middleElement > targetNumber || middleElement == -1) {
                high = mid - 1;
            } else if (middleElement < targetNumber) {
                low = middleElement + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }
}
