package ru.itis.binarysearch;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

// 349. Intersection of Two Arrays
public class IntersectionOfTwoArrays_349 {
    public static void main(String[] args) {
        int[] nums1 = {1, 2, 2, 1};
        int[] nums2 = {2, 2};
        int[] nums11 = {4, 9, 5};
        int[] nums21 = {9, 4, 9, 8, 4};
        int[] intersection = intersection1(nums1, nums2);
        System.out.println(Arrays.toString(intersection));
    }

    public static int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> container = new HashSet<>();
        Set<Integer> output = new HashSet<>();

        for (int num : nums1) {
            container.add(num);
        }

        for (int num : nums2) {
            if (container.contains(num)) {
                output.add(num);
            }
        }

        int[] outputArray = new int[output.size()];
        int index = 0;
        for (int x : output) {
            outputArray[index] = x;
            index++;
        }
        return outputArray;
    }

    public static int[] intersection1(int[] nums1, int[] nums2) {
        int[] temp = new int[1001];
        int count = 0;
        for (int i = 0; i < nums1.length; i++) {
            temp[nums1[i]] = 1;
        }

        for (int i = 0; i < nums2.length; i++) {
            if (temp[nums2[i]] == 1) {
                temp[nums2[i]] = 2;
                count++;
            }
        }
        int[] ans = new int[count];
        for(int i = 0, j = 0; j < count; i++) {
            if(temp[i] == 2) {
                ans[j] = i;
                j++;
            }
        }

        return ans;

    }
}
