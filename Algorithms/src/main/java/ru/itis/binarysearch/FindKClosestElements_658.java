package ru.itis.binarysearch;

// 658. Find K Closest Elements

import java.util.ArrayList;
import java.util.List;

public class FindKClosestElements_658 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int k = 4, x = 3;
        List<Integer> result = findClosestElements(arr, k, x);
        System.out.println(result);
    }

    public static List<Integer> findClosestElements(int[] arr, int k, int x) {
        int left = 0;
        int right = arr.length - k;

        while (left < right) {
            int mid = left + (right - left) / 2;
            if (x - arr[mid] <= arr[mid + k] - x) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }

        List<Integer> result = new ArrayList<>();
        for (int i = left; i < left + k; ++i) {
            result.add(arr[i]);
        }
        return result;
    }
}
