package ru.itis.binarysearch;

public class Search_10_5 {
    public static void main(String[] args) {
        String[] str = {"at", "", "", "", "ball", "", "", "car", "", "", "dad", "", ""};
        int result = search(str, "ball");
        System.out.println(result);
    }

    public static int search(String[] strings, String str) {
        if (strings == null || str == null || str == "") {
            return -1;
        }
        return search(strings, str, 0, strings.length);
    }

    public static int search(String[] strings, String str, int first, int last) {
        if (first > last) {
            return -1;
        }

        int mid = (first + last) / 2;

        if (strings[mid].isEmpty()) {
            int left = mid - 1;
            int right = mid + 1;

            while (true) {
                if (left < first && right > last) {
                    return -1;
                } else if (right <= last && !strings[right].isEmpty()) {
                    mid = right;
                    break;
                } else if (left >= first && !strings[left].isEmpty()) {
                    mid = left;
                    break;
                }
                right++;
                left--;
            }
        }

        /* Проверить строку и проверить рекурсию при необходимости */
        if (str.equals(strings[mid])) { // строка найдена
            return mid;
        } else if (strings[mid].compareTo(str) < 0) { // искать справа
            return search(strings, str, mid + 1, last);
        } else { // искать слева
            return search(strings, str, first, mid - 1);
        }
    }
}
