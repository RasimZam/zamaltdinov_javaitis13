package ru.itis.binarysearch;

import java.util.HashSet;
import java.util.Set;

// 268. Missing Number
public class MissingNumber_268 {
    public static void main(String[] args) {
        int[] nums1 = {3, 0, 1};
        int[] nums2 = {0, 1};
        int[] nums3 = {9, 6, 4, 2, 3, 5, 7, 0, 1};
        int result = missingNumber2(nums3);
        System.out.println(result);
    }

    public static int missingNumber(int[] nums) {
        int n = nums.length + 1;
        int total = (n * (n - 1)) / 2;

        for (int num : nums) {
            total -= num;
        }
        return total;
    }

    public static int missingNumber1(int[] nums) {
        int n = nums.length;
        Set<Integer> numsSet = new HashSet<>();

        for (int num : nums) {
            numsSet.add(num);
        }

        for (int i = 0; i <= n; i++) {
            if (!numsSet.contains(i)) {
                return i;
            }
        }
        return -1;
    }

    public static int missingNumber2(int[] nums) {
        int sum = 0;

        for (int num = 1; num <= nums.length; num++) {
            sum += num;
        }

        for (int num: nums) {
            sum -= num;
        }
        return sum;
    }

    public static int missingNumber3(int[] nums) {
        int allXOR = 0;

        // XOR all numbers in range [0, n]
        for (int i = 0; i <= nums.length; i++) {
            allXOR = allXOR ^ i;
        }

        // XOR all numbers in the given array
        for (int num: nums) {
            allXOR = allXOR ^ num;
        }
        return allXOR;
    }
}
