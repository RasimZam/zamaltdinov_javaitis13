package ru.itis.binarysearch;

// 34. Find First and Last Position of Element in Sorted Array

import java.util.Arrays;

public class FindFirstAndLastPositionInSortedArray_34 {
    public static void main(String[] args) {
        int[] nums = {5, 7, 7, 8, 8, 10};
        int target = 8;
        int[] search = search(nums, target);
        System.out.println(Arrays.toString(search));
    }

    public static int[] search(int[] nums, int target) {
        int[] result = {-1, -1};
        int idx = 0;
        int l = 0;
        int r = nums.length - 1;
        while (r - l > 1) {
            int mid = l + (r - l) / 2;
            if (isGoodElement(nums[mid], target)) {
                l = mid;
            } else {
                r = mid;
            }
            if (nums[l] == target && idx <= 1) {
                result[idx] = l;
                idx++;
            }
        }
        return result;
    }

    private static boolean isGoodElement(int value, int target) {
        return value <= target;
    }
}
