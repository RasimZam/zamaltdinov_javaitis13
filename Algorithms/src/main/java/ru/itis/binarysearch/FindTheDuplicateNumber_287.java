package ru.itis.binarysearch;

// 287. Find the Duplicate Number

public class FindTheDuplicateNumber_287 {
    public static void main(String[] args) {
        int[] nums = {1, 3, 4, 2, 2};
        int result = findDuplicate1(nums);
        System.out.println(result);
    }

    // O(n) time complexity
    // O(1) memory complexity
    public static int findDuplicate(int[] nums) {
        int slow = 0, fast = 0;
        do {
            slow = nums[slow];
            fast = nums[nums[fast]];
        } while (slow != fast);

        slow = 0;
        while (slow != fast) {
            slow = nums[slow];
            fast = nums[fast];
        }
        return slow;
    }

    // O(n) time complexity
    // O(1) memory complexity
    public static int findDuplicate1(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            int ind = Math.abs(nums[i]);
            if (nums[ind] < 0) {
                return ind;
            }
            nums[ind] = -nums[ind];
        }
        return -1;
    }
}
