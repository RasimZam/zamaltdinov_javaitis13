package ru.itis.binarysearch;

import java.util.LinkedList;
import java.util.List;

public class FindKClosetElements_658 {
    public static void main(String[] args) {
        int[] arr = {1, 3, 4, 7, 8, 9};
        int k = 3;
//        int x = 3;
        int x = 5;

        List<Integer> result = findKClosetElements(arr, k, x);
        System.out.println(result.toString());

    }

    public static List<Integer> findKClosetElements(int[] arr, int k, int x) {
        int l = 0;
        int r = arr.length - k;
        while (l < r) {
            int mid = l + (r - l) / 2;
            if (x - arr[mid] > arr[mid + k] - x) {
                l = mid + 1;
            } else {
                r = mid;
            }
        }

        List<Integer> res = new LinkedList<>();
        for (int i = l; i < l + k; i++) {
            res.add(arr[i]);
        }
        return res;
    }
}
