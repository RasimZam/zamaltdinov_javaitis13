package ru.itis.binarysearch;

// 113. Path Sum II

import ru.itis.practice.example1.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class PathSumII_113 {
    public static void main(String[] args) {

    }

    public static List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        List<List<Integer>> paths = new ArrayList<>();
        findPaths(root, targetSum, new ArrayList<Integer>(), paths);
        return paths;
    }

    private static void findPaths(TreeNode root, int sum, ArrayList<Integer> current, List<List<Integer>> paths) {
        if(root == null) {
            return;
        }

        current.add(root.val);
        if(root.val == sum && root.left == null && root.right == null) {
            paths.add(current);
            return;
        }

        findPaths(root.left, sum - root.val, new ArrayList<>(current), paths);
        findPaths(root.right, sum - root.val, new ArrayList<>(current), paths);
    }
}
