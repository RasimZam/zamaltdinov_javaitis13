package ru.itis.binarysearch;

// 69. Sqrt(x)

public class Sqrt_69 {
    public static void main(String[] args) {
        int x = 8;
        int result = mySqrt(x);
        System.out.println(result);
    }

    public static int mySqrt(int x) {
        if (x == 0 || x == 1) {
            return x;
        }

        int start = 1, end = x, mid = -1;

        while(start <= end) {
            mid = start + (end - start) / 2;

            if(mid * mid == x) {
                return mid;
            } else if ((long)mid * mid > x) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        return Math.round(end);
    }
}
