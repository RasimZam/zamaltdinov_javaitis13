package ru.itis.binarysearch;

// 33. Search in Rotated Sorted Array

public class SearchInRotatedSortedArray_33 {
    public static void main(String[] args) {
        int[] nums = {4, 5, 6, 7, 0, 1, 2};
        int target = 0;
        int offset = findOffset(nums);
        int result = search1(nums, offset, target);
        System.out.println(result);
    }

    public static int search(int[] nums, int target) {
        if (nums.length == 0) {
            return -1;
        }
        int left = 0;
        int right = nums.length - 1;
        while (left <= right) {
            int middle = left + (right - left) / 2;

            if (nums[middle] == target) {
                return middle;
            }

            if (nums[left] <= nums[middle]) {
                if (target >= nums[left] && target < nums[middle]) {
                    right = middle - 1;
                } else {
                    left = middle + 1;
                }
            } else {
                if (target > nums[middle] && target <= nums[right]) {
                    left = middle + 1;
                } else {
                    right = middle - 1;
                }
            }
        }
        return -1;
    }

    public static int search1(int[] nums, int offset, int target) {
        int l = 0;
        int r = nums.length - 1;
        int n = nums.length - 1;
        while (r - l > 1) {
            int mid = l + (r - l) / 2;
            if (isGoodElement((nums[mid] + offset) % n , target)) {
                l = mid;
            } else {
                r = mid;
            }
        }
        int result = (l + offset) % n;
        return nums[result] == target ? result : -1;
    }

    private static boolean isGoodElement(int value, int target) {
        return value <= target;
    }

    public static int findOffset(int[] nums) {
        int l = 0;
        int r = nums.length - 1;
        int idxLastValue = nums.length - 1;
        while (r - l > 1) {
            int mid = l + (r - l) / 2;
            if (checkValue(nums[mid], nums[idxLastValue])) {
                l = mid;
            } else {
                r = mid;
            }
        }
        return r;
    }

    public static boolean checkValue(int value, int lastValueArray) {
        return value >= lastValueArray;
    }
}
