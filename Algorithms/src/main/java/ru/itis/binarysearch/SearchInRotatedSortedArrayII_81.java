package ru.itis.binarysearch;

// 81. Search in Rotated Sorted Array II

public class SearchInRotatedSortedArrayII_81 {
    public static void main(String[] args) {
        
    }
    
    public boolean search(int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1;
        
        while(left < right) {
            int mid = left + (right - left) / 2;
            
            if(nums[mid] > nums[right]) {
                if (nums[left] <= target && target <= nums[mid]) {
                    right = mid;
                } else {
                    left = mid + 1;
                }
            } else if (nums[mid] < nums[right]) {
                if(nums[mid] < target && target <= nums[right]) {
                    left = mid + 1;
                } else {
                    right = mid;
                }
            } else {
                right--;
            }
        }
        return nums[left] == target;
    }
}
