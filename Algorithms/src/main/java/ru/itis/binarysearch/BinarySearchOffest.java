package ru.itis.binarysearch;

public class BinarySearchOffest {
    public static void main(String[] args) {
        int[] nums = {4, 5, 6, 7, 0, 1, 2};
        int target = 0;
        int offset = findOffset(nums);
        System.out.println(offset);
        int search = search(nums, offset, target);
    }

    public static int search(int[] nums, int offset, int target) {
        int l = 0;
        int r = nums.length - 1;
        int n = nums.length - 1;
        while (r - l > 1) {
            int mid = l + (r - l) / 2;
            if (isGoodElement((nums[mid] + offset) % n , target)) {
                l = mid;
            } else {
                r = mid;
            }
        }
        int result = (l + offset) % n;
        return nums[result] == target ? result : -1;
    }

    private static boolean isGoodElement(int value, int target) {
        return value <= target;
    }

    public static int findOffset(int[] nums) {
        int l = 0;
        int r = nums.length - 1;
        int idxLastValue = nums.length - 1;
        while (r - l > 1) {
            int mid = l + (r - l) / 2;
            if (checkValue(nums[mid], nums[idxLastValue])) {
                l = mid;
            } else {
                r = mid;
            }
        }
        return r;
    }

    public static boolean checkValue(int value, int lastValueArray) {
        return value >= lastValueArray;
    }
}
