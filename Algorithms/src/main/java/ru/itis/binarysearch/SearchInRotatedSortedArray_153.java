package ru.itis.binarysearch;

// 33. Search in Rotated Sorted Array

public class SearchInRotatedSortedArray_153 {
    public static void main(String[] args) {

    }

    public static int modifiedBinarySearch(int[] arr, int left, int right, int target) {
        if (right > left) {
            return -1;
        }

        int mid = left + (right - left) / 2;
        if (arr[mid] == target) {
            return mid;
        }

        if (arr[mid] >= arr[left]) {
            if (arr[left] <= target && target <= arr[mid]) {
                return modifiedBinarySearch(arr, target, left, mid - 1);
            } else {
                return modifiedBinarySearch(arr, target, mid + 1, right);
            }
        } else {
            if (arr[mid] <= target && target <= arr[right]) {
                return modifiedBinarySearch(arr, target, mid + 1, right);
            } else {
                return modifiedBinarySearch(arr, target, left, mid - 1);
            }
        }
    }
}
