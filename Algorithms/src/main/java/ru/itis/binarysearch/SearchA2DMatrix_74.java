package ru.itis.binarysearch;

// 74. Search a 2D Matrix

public class SearchA2DMatrix_74 {
    public static void main(String[] args) {
        int[][] nums = {{1, 3, 5, 7}, {10, 11, 16, 20}, {23, 30, 34, 60}};
        int target = 23;
        boolean result = isExistTargetElement1(nums, target);
//        boolean result = searchIn2DMatrix(nums, target);
        System.out.println(result);
    }

    public static boolean searchIn2DMatrix(int[][] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            boolean isExistTarget = isExistTargetElement(nums[i], target);
            if (isExistTarget) {
                return true;
            }
        }
        return false;
    }

    private static boolean isExistTargetElement(int[] nums, int target) {
        int l = 0;
        int r = nums.length - 1;
        while (r - l > 1) {
            int mid = l + (r - l) / 2;
            if (isGoodElement(nums[mid], target)) {
                l = mid;
            } else {
                r = mid;
            }
        }
        return nums[l] == target;
    }

    private static boolean isGoodElement(int value, int target) {
        return value <= target;
    }

    private static boolean isExistTargetElement1(int[][] matrix, int target) {
        int rows = matrix.length;
        int columns = matrix[0].length;
        int l = 0;
        int r = rows * columns - 1;
        while (l <= r ) {
            int mid = l + (r - l) / 2;
            int current = matrix[mid / columns][mid % columns];
            if (current == target) {
                return true;
            }
            if (current < target) {
                l = mid + 1;
            } else {
                r = mid - 1;
            }
        }
        return false;
    }
}
