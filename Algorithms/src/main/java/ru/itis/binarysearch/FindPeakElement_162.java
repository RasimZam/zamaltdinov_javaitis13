package ru.itis.binarysearch;

// 162. Find Peak Element

public class FindPeakElement_162 {
    public static void main(String[] args) {

    }

    public int findPeakElement(int[] nums) {
        int low = 0;
        int high = nums.length - 1;

        while (low < high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] < nums[mid + 1]) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }
        return low;
    }
}
