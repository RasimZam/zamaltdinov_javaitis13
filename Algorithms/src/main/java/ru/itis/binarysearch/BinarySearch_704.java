package ru.itis.binarysearch;

public class BinarySearch_704 {
    public static void main(String[] args) {
        int[] nums = {-1, 0, 3, 5, 9, 12};
        int target = 9;
        int result = search1(nums, target);
        System.out.println(result);
    }

    public static int search(int[] nums, int target) {
        if (nums.length == 0) {
            return -1;
        }

        int left = 0;
        int right = nums.length - 1;

        while (left <= right) {
            int middle = left + (right - left) / 2;

            if (nums[middle] == target) {
                return middle;
            }

            if (nums[middle] < target) {
                left = middle + 1;
            } else {
                right = middle - 1;
            }
        }
        return -1;
    }

    public static int search1(int[] nums, int target) {
        if (nums.length == 0) {
            return -1;
        }
        int left = 0;
        int right = nums.length - 1;

        while (right - left > 1) {
            int middle = left + (right - left) / 2;

            if (isGoodElement(nums[middle], target)) {
                left = middle;
            } else {
                right = middle;
            }
        }
        return nums[left] == target ? left : -1;
    }

    private static boolean isGoodElement(int value, int target) {
        return value <= target;
    }
}
