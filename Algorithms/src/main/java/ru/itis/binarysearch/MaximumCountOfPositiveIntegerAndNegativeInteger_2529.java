package ru.itis.binarysearch;

import java.util.Arrays;

// 2529. Maximum Count of Positive Integer and Negative Integer
public class MaximumCountOfPositiveIntegerAndNegativeInteger_2529 {
    public static void main(String[] args) {
        int[] nums = {-2, -1, -1, 1, 2, 3};
        System.out.println(maximumCount(nums));
    }

    public static int maximumCount(int[] nums) {
        return (int) Math.max(Arrays.stream(nums).filter(value -> value > 0).count(),
                Arrays.stream(nums).filter(value -> value < 0).count());
    }
}
