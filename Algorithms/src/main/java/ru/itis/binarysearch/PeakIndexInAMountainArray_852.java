package ru.itis.binarysearch;

// 852. Peak Index in a Mountain Array

public class PeakIndexInAMountainArray_852 {
    public static void main(String[] args) {

    }

    public int peakIndexInAMountainArray(int[] arr) {
        int left = 0;
        int right = arr.length - 1;

        while(left < right) {
            int mid = left + (right - left) / 2;
            if(arr[mid] < arr[mid + 1]) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }

        return left;
    }
}
