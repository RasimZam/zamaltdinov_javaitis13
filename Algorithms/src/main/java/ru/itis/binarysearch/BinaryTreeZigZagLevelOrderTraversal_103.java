package ru.itis.binarysearch;

// 103. Binary Tree Zigzag Level Order Traversal

import ru.itis.practice.example1.TreeNode;

import java.util.*;

public class BinaryTreeZigZagLevelOrderTraversal_103 {
    public static void main(String[] args) {

    }

    public static List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> zigzag = new ArrayList<>();
        if(root == null) return zigzag;

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        boolean flag = false;

        while(!queue.isEmpty()) {
            int size = queue.size();

            List<Integer> level = new ArrayList<>();
            Stack<Integer> reverseStack = new Stack<>();

            for(int i = 0; i < size; i++) {
                TreeNode node = queue.poll();

                if(flag) {
                    reverseStack.add(node.val);
                } else {
                    level.add(node.val);
                }

                if(node.left != null) {
                    queue.add(node.left);
                }
                if(node.right != null) {
                    queue.add(node.right);
                }
            }

            flag = !flag;

            while(!reverseStack.isEmpty()) {
                level.add(reverseStack.pop());
            }

            zigzag.add(level);
        }

        return zigzag;
    }
}
