package ru.itis.binarysearch;

import java.util.Arrays;

// 2389. Longest Subsequence With Limited Sum
public class LongestSubsequenceWithLimitedSum_2389 {
    public static void main(String[] args) {
        int[] nums = {4, 5, 2, 1};
        int[] queries = {3, 10, 21};
        int[] answerQueries = answerQueries(nums, queries);
        int[] answerQueries1 = answerQueries1(nums, queries);
        System.out.println(Arrays.toString(answerQueries1));

    }

    public static int[] answerQueries(int[] nums, int[] queries) {
        int[] result = new int[queries.length];

        Arrays.sort(nums);

        for (int i = 0; i < queries.length; i++) {
            result[i] = numOfElementsLessThan(nums, queries[i]);
        }
        return result;
    }

    private static int numOfElementsLessThan(int[] nums, int query) {
        int sum = 0;
        for (int i = 0; i < nums.length; ++i) {
            sum += nums[i];
            if (sum > query) {
                return i;
            }
        }
        return nums.length;
    }

    public static int[] answerQueries1(int[] nums, int[] queries) {
        Arrays.sort(nums);
        int n = nums.length;
        int m = queries.length;
        int[] res = new int[m];

        for (int i = 1; i < n; ++i) {
            nums[i] += nums[i - 1];
        }

        for (int i = 0; i < m; ++i) {
            int j = Arrays.binarySearch(nums, queries[i]);
            res[i] = Math.abs(j + 1);
        }
        return res;
    }
}
