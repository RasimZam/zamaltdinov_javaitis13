package ru.itis.binarysearch;

// необходимо найти элемент

public class Search_10_3 {

    public int search(int[] array, int left, int right, int targetNumber) {
        int mid = (left + right) / 2;
        if (array[mid] == targetNumber) {
            return mid;
        }

        if (right < left) {
            return -1;
        }

        /*
        * Либо левая, либо правая половина должна быть нормально упорядочена.
        * Найти нормально упорядоченную сторону, а затем использовать ее для
        * опеределения стороны, в которой следует искать targetNumber
        * */

        if (array[left] < array[mid]) { // левая часть нормально упорядочена
            if (targetNumber >= array[left] && targetNumber < array[mid]) {
                return search(array, left, mid - 1, targetNumber); // искать слева
            } else {
                return search(array, mid + 1, right, targetNumber); // искать справа
            }
        } else if (array[mid] < array[left]) { // правая половина нормально упорядочена
            if (targetNumber > array[mid] && targetNumber <= array[right]) {
                return search(array, mid + 1, right, targetNumber); // искать справа
            }  else {
                return search(array, left, mid - 1, targetNumber); // искать слева
            }
        } else if (array[left] == array[mid]) { // левая половина состоит из повторов
            if (array[mid] != array[right]) { // если правая половина отличается, искать в ней
                return search(array, mid + 1, right, targetNumber); // искать справа
            } else { // иначе придется искать придется в обеих половинах
                int result = search(array, left, mid - 1, targetNumber); // искать слева
                if (result == -1) {
                    return search(array, mid + 1, right, targetNumber); // искать справа
                } else {
                    return result;
                }
            }

        }
        return -1;
    }
}
