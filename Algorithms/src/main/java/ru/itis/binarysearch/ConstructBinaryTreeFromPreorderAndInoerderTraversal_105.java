package ru.itis.binarysearch;

import ru.itis.practice.example1.TreeNode;

import java.util.HashMap;
import java.util.Map;

// 105. Construct Binary Tree from Preorder and Inorder Traversal

public class ConstructBinaryTreeFromPreorderAndInoerderTraversal_105 {
    public static void main(String[] args) {
        int[] preorder = {3, 9, 20, 15, 7};
        int[] inorder = {9, 3, 15, 20, 7};
        TreeNode treeNode = buildTree(preorder, inorder);
    }

    public static TreeNode buildTree(int[] preOrder, int[] inOrder) {
        Map<Integer, Integer> inOrderIndexMap = new HashMap<>();
        for (int i = 0; i < inOrder.length; i++) {
            inOrderIndexMap.put(inOrder[i], i);
        }
        return splitTree(preOrder, inOrderIndexMap, 0, 0, inOrder.length - 1);
    }

    private static TreeNode splitTree(int[] preOrder,
                                      Map<Integer, Integer> inOrderIndexMap,
                                      int rootIndex,
                                      int left,
                                      int right) {
        TreeNode root = new TreeNode(preOrder[rootIndex]);
        int mid = inOrderIndexMap.get(preOrder[rootIndex]);
        if (mid > left) {
            root.left = splitTree(preOrder, inOrderIndexMap, rootIndex + 1, left, mid - 1);
        }
        if (mid < right) {
            root.right = splitTree(preOrder, inOrderIndexMap, rootIndex + mid - left + 1, mid + 1, right);
        }
        return root;
    }
}
