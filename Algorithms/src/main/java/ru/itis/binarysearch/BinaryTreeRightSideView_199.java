package ru.itis.binarysearch;

// 199. Binary Tree Right Side View

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinaryTreeRightSideView_199 {
    private TreeNode root;

    private class TreeNode {
        private TreeNode left;
        private TreeNode right;
        private int val;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    public TreeNode createBinaryTree() {
        TreeNode first = new TreeNode(1);
        TreeNode second = new TreeNode(2);
        TreeNode third = new TreeNode(5);
        TreeNode fourth = new TreeNode(3);
        TreeNode fifth = new TreeNode(4);

        root = first;
        first.left = second;
        first.right = fourth;
        second.right = third;
        fourth.right = fifth;
        return root;
    }


    public static void main(String[] args) {
        BinaryTreeRightSideView_199 binaryTreeRightSideView199 = new BinaryTreeRightSideView_199();
        TreeNode binaryTree = binaryTreeRightSideView199.createBinaryTree();
        List<Integer> result = rightSideView(binaryTree);
    }

    public static List<Integer> rightSideView(TreeNode root) {
        List<Integer> visibleValues = new ArrayList<>();
        if(root == null) return visibleValues;

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while(!queue.isEmpty()) {
            int size = queue.size();
            for(int i = 0; i < size; i++) {
                TreeNode current = queue.remove();
                if(i == size - 1) {
                    visibleValues.add(current.val);
                }
                if(current.left != null) {
                    queue.add(current.left);
                }
                if(current.right != null) {
                    queue.add(current.right);
                }
            }
        }
        return visibleValues;
    }
}
