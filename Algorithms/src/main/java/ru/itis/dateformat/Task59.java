package ru.itis.dateformat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Task59 {
    public static void main(String[] args) {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String stringLD = LocalDate.now().format(dateFormatter);
        System.out.println(stringLD);
        System.out.println("------------------------------------");

        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String stringLDT = LocalDateTime.now().format(timeFormatter);
        System.out.println(stringLDT);
        System.out.println("------------------------------------");

        DateTimeFormatter formatterZoneDateTime = DateTimeFormatter.ofPattern("E MMM yyyy HH:mm:ss.SSSZ");
        String stringZDT = ZonedDateTime.now().format(formatterZoneDateTime);
        System.out.println(stringZDT);
        System.out.println("------------------------------------");
    }
}
