package ru.itis.dummyObject;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.mock;

public class DummyObjectDemo {

    public Customer createDummyCustomer() {
        State state = new State("CA");
        City city = new City("Los Angeles", state);
        Address address = new Address("Mulholland Drive", city);
        Customer customer = new Customer("Jack", "Nicholson", address);
        return customer;
    }

    @Test
    public void addCustomerTest() {
        Customer dummy = createDummyCustomer();
        AddressBook addressBook = new AddressBook();
        addressBook.addCustomer(dummy);
        assertEquals(1, addressBook.getNumberOfCustomers());
    }

    @Test
    public void addNullCustomerTest() {
        Customer dummy = null;
        AddressBook addressBook = new AddressBook();
        assertThrows(NullPointerException.class, () -> addressBook.addCustomer(dummy));
    }

    @Test
    public void addCustomerWithDummyTest() {
        Customer dummy = mock(Customer.class);
        AddressBook addressBook = new AddressBook();
        addressBook.addCustomer(dummy);
        assertEquals(1, addressBook.getNumberOfCustomers());
    }
}
