package ru.itis.mockito;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.itis.mockito.Trade;
import ru.itis.mockito.repository.TradeRepository;
import ru.itis.mockito.service.AuditService;
import ru.itis.mockito.service.TradingService;
import ru.itis.mockito.service.TradingServiceImpl;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class AuditServiceTest {

    @Mock // actually, this is a stub
    TradeRepository tradeRepository;

    @Mock // and this one is indeed mock
    AuditService auditService;

    @Test
    public void testAuditLogEntryMadeForNewTrade() throws Exception {
        Trade trade = new Trade("Ref 1", "Description 1");

        when(tradeRepository.createTrade(trade)).thenReturn(anyLong());

        TradingService tradingService = new TradingServiceImpl(tradeRepository, auditService);
        tradingService.createTrade(trade);

        verify(auditService).logNewTrade(trade);
    }
}
