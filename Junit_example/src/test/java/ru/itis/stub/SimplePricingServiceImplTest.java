package ru.itis.stub;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SimplePricingServiceImplTest {

    @Test
    public void testGetHighestPricedTrade() throws Exception {
        Price price1 = new Price(10);
        Price price2 = new Price(15);
        Price price3 = new Price(20);

        Collection<Trade> trades = new ArrayList<>();
        trades.add(new Trade(price1));
        trades.add(new Trade(price2));
        trades.add(new Trade(price3));

        PricingRepository pricingRepository = mock(PricingRepository.class);
        when(pricingRepository.getPriceForTrade(any(Trade.class)))
                .thenReturn(price1, price2, price3);

        PricingService service = new PricingServiceImpl(pricingRepository);
        Price highestPrice = service.getHighestPricedTrade(trades);

        assertEquals(price3.getAmount(), highestPrice.getAmount());
    }
}
