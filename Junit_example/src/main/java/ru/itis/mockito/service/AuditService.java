package ru.itis.mockito.service;

import ru.itis.mockito.Trade;

public interface AuditService {

    void logNewTrade(Trade Trade);
}
