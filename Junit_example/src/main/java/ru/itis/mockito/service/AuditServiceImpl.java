package ru.itis.mockito.service;

import ru.itis.mockito.Trade;

public class AuditServiceImpl implements AuditService {

    @Override
    public void logNewTrade(Trade Trade) {
        throw new RuntimeException("Implement logNewTrade() in Auditservice");
    }
}
