package ru.itis.mockito.service;

import ru.itis.mockito.Trade;

public interface TradingService {

    Long createTrade(Trade trade);
}
