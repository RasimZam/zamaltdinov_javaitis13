package ru.itis.mockito.service;

import ru.itis.mockito.Trade;
import ru.itis.mockito.repository.TradeRepository;

public class TradingServiceImpl implements TradingService {

    private final TradeRepository tradeRepository;
    private final AuditService auditService;

    public TradingServiceImpl(TradeRepository tradeRepository, AuditService auditService) {
        this.tradeRepository = tradeRepository;
        this.auditService = auditService;
    }

    @Override
    public Long createTrade(Trade trade) {
        Long id = tradeRepository.createTrade(trade);
        auditService.logNewTrade(trade);
        return id;
    }
}
