package ru.itis.mockito.exception;

public class CreateTradeException extends RuntimeException {

    public CreateTradeException() {
        super("Cannot create such trade!");
    }
}
