package ru.itis.mockito;

public class Trade {

    private String reference;
    private String description;

    public Trade(String reference, String description) {
        this.reference = reference;
        this.description = description;
    }
}
