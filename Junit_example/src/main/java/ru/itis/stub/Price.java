package ru.itis.stub;

public class Price {

    private double amount;

    public Price(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }
}
