package ru.itis.stub;

public class Trade {

    private Price price;

    public Trade(Price price) {
        this.price = price;
    }

    public Price getPrice() {
        return price;
    }
}
