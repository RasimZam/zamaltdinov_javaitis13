package ru.itis.stub;

public class PricingRepository {

    public Price getPriceForTrade(Trade trade) {
        return trade.getPrice();
    }
}
