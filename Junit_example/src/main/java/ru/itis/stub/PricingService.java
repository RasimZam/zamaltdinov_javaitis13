package ru.itis.stub;

import java.util.Collection;

public interface PricingService {

    Price getHighestPricedTrade(Collection<Trade> trades);
}
