package ru.itis.stub;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

public class PricingServiceImpl implements PricingService {

    private final PricingRepository pricingRepository;

    public PricingServiceImpl(PricingRepository pricingRepository) {
        this.pricingRepository = pricingRepository;
    }

    @Override
    public Price getHighestPricedTrade(Collection<Trade> trades) {
        Trade[] tradesArr = trades.toArray(new Trade[0]);
        Comparator<Trade> comp =
                Comparator.comparingDouble(t -> t.getPrice().getAmount());
        Arrays.sort(tradesArr, comp);
        return tradesArr[tradesArr.length - 1].getPrice();
    }
}
