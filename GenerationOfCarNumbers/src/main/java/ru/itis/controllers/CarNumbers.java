package ru.itis.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.services.CarNumbersService;

@RestController
public class CarNumbers {

    @Autowired
    private CarNumbersService carNumbersService;

    @RequestMapping(value = "/number/random", method = RequestMethod.GET,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public String randomNumber() {
        return carNumbersService.getRandomNumber();

    }

    @RequestMapping(value = "/number/next", method = RequestMethod.GET,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public String nextNumber() {
        return carNumbersService.getNextNumber();
    }
}
