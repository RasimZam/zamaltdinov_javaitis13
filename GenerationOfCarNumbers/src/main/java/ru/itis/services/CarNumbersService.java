package ru.itis.services;

public interface CarNumbersService {

    String getRandomNumber();

    String getNextNumber();
}
