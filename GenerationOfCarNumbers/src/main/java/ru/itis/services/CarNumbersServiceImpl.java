package ru.itis.services;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class CarNumbersServiceImpl implements CarNumbersService {

    private String letter[] = {"А", "Е", "Т", "О", "Р", "Н", "У", "К", "Х", "С", "В", "М"};
    private static final String extraNumber = " 116 RUS";
    private static List<String> numbersCar = new ArrayList<>();

    @Override
    public String getRandomNumber() {
        return generateRandomNumber();
    }

    @Override
    public String getNextNumber() {
        return generateNextNumber();
    }

    public String generateRandomNumber() {
        Random random = new Random();
        int firstChar = random.nextInt(letter.length);
        int secondChar = random.nextInt(letter.length);
        int thirdChar = random.nextInt(letter.length);
        int firstNumber = random.nextInt(10);
        int secondNumber = random.nextInt(10);
        int thirdNumber = random.nextInt(10);
        String str = letter[random.nextInt(letter.length)];

        String numberOfCar = letter[firstChar] + firstNumber +
                                secondNumber + thirdNumber +
                                    letter[secondChar] + letter[thirdChar] + extraNumber;
        boolean check = isCheck(numberOfCar);

        if (!check) {
            numbersCar.add(numberOfCar);
        } else {
            throw new IllegalArgumentException("Such number exists");
        }
        return numberOfCar;
    }

    public String generateNextNumber() {
        boolean check = false;
        String lastNumberFromStore = numbersCar.get(numbersCar.size() - 1);
        String str = numbersCar.get(numbersCar.size() - 1).substring(1,4);
        int parseNumber = Integer.parseInt(str);

        parseNumber++;

        String firstSubStr = lastNumberFromStore.substring(0,1);
        String secondSubStr = lastNumberFromStore.substring(4,14);

        if (parseNumber < 100) {
            String numberOfCar = firstSubStr + "0" + parseNumber + secondSubStr;
            check = isCheck(numberOfCar);
            if (!check) {
                numbersCar.add(numberOfCar);
            } else {
                throw new IllegalArgumentException("Such number exists");
            }
            return numberOfCar;
        }
            String numberOfCar = firstSubStr + parseNumber + secondSubStr;
            check = isCheck(numberOfCar);
            if (!check) { numbersCar.add(numberOfCar);
            } else {
                throw new IllegalArgumentException("Such number exists");
            }
            return numberOfCar;
    }

    public boolean isCheck(String numberOfCar) {
        for (String number: numbersCar) {
            if (numberOfCar.equals(number)) {
                return true;
            }
        }
        return false;
    }
}
