package ru.itis.util;

import lombok.NonNull;
import lombok.experimental.UtilityClass;
import ru.itis.exception.BadRequestException;

@UtilityClass
public class StringChecker {

    public void checkOnEmpty(@NonNull String value, @NonNull String fieldName) {

        if (value.trim().isEmpty()) {
            throw new BadRequestException(String.format("Field with name \"%s\" can't be empty", fieldName));
        }
    }
}
