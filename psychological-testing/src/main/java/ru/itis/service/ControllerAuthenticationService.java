package ru.itis.service;

import org.springframework.stereotype.Component;
import ru.itis.exception.UnauthorizedException;
import ru.itis.model.TokenEntity;
import ru.itis.repository.TokenRepository;

import javax.transaction.Transactional;
import java.time.Instant;

@Component
@Transactional
public class ControllerAuthenticationService {

    private final TokenRepository tokenRepository;

    public ControllerAuthenticationService(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    public void authenticate(String tokenStr) {
        TokenEntity token = tokenRepository
                .findById(tokenStr)
                .orElseThrow(() -> new UnauthorizedException("You must be logged in to perform this action."));

        if (token.getExpiredAt().isBefore(Instant.now())) {
            throw new UnauthorizedException("Token time is expired. Log in again in system");
        }
    }
}
