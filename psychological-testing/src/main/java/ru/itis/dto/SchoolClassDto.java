package ru.itis.dto;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SchoolClassDto {

    @NonNull
    private Long id;

    @NonNull
    private String name;

    @NonNull
    private SchoolDto school;
}
