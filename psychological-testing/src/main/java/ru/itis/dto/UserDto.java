package ru.itis.dto;

import lombok.*;
import org.hibernate.annotations.NaturalId;
import ru.itis.domains.UserRole;

import java.time.Instant;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    @NonNull
    private Long id;

    @NonNull
    private String firstName;

    @NonNull
    private String middleName;

    @NonNull
    private String lastName;

    @NonNull
    private String login;

    @NonNull
    private String password;

    @NonNull
    private Instant birthday;

    @NonNull
    private UserRole role;

    @NonNull
    private Long schoolClassId;
}
