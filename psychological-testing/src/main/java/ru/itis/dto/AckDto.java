package ru.itis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AckDto {

    private Boolean response;

    public static AckDto makeDefault(Boolean response) {
        return builder()
                .response(response)
                .build();
    }
}
