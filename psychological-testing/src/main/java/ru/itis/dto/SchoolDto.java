package ru.itis.dto;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SchoolDto {

    @NonNull
    private Long id;

    @NonNull
    private String name;
}
