package ru.itis.dto;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TestDto {

    @NonNull
    private Long id;

    @NonNull
    private String name;

    @NonNull
    private Boolean isStarted;
}
