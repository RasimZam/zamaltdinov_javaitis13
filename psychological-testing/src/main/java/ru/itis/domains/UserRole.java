package ru.itis.domains;

public enum UserRole {
    STUDENT,
    PARENT,
    TEACHER
}
