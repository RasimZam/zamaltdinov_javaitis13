package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.model.QuestionEntity;

public interface QuestionRepository extends JpaRepository<QuestionEntity, Long> {
}
