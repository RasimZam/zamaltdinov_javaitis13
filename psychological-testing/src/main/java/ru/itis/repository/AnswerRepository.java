package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.model.AnswerEntity;

public interface AnswerRepository extends JpaRepository<AnswerEntity, Long> {
}
