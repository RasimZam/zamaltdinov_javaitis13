package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.model.SchoolClassEntity;

public interface SchoolClassRepository extends JpaRepository<SchoolClassEntity, Long> {

    void deleteByIdAndAndSchoolId(Long schoolClassId, Long schoolId);
}
