package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.model.TokenEntity;

import java.time.Instant;
import java.util.Optional;

public interface TokenRepository extends JpaRepository<TokenEntity, String> {

    void deleteAllByExpiredAtBefore(Instant currentDate);

    Optional<TokenEntity> findByPsychologistId(Integer psychologistId);
}
