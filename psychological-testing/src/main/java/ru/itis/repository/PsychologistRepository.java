package ru.itis.repository;

import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.model.PsychologistEntity;

import java.util.Optional;

public interface PsychologistRepository extends JpaRepository<PsychologistEntity, Long> {

    Optional<PsychologistEntity> findTopByLoginAndPassword(@NonNull String login, @NonNull String password);
}
