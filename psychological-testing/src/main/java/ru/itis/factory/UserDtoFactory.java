package ru.itis.factory;

import org.springframework.stereotype.Component;
import ru.itis.dto.UserDto;
import ru.itis.model.UserEntity;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserDtoFactory {

    public UserDto createUserDto(UserEntity entity) {
        return UserDto.builder()
                .id(entity.getId())
                .firstName(entity.getFirstName())
                .middleName(entity.getMiddleName())
                .lastName(entity.getLastName())
                .login(entity.getLogin())
                .password(entity.getPassword())
                .birthday(entity.getBirthday())
                .role(entity.getRole())
                .schoolClassId(entity.getSchoolClass().getId())
                .build();
    }

    public List<UserDto> createUserDtoList(List<UserEntity> entities) {
        return entities.stream().map(this::createUserDto).collect(Collectors.toList());
    }
}
