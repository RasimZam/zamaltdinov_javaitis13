package ru.itis.factory;

import org.springframework.stereotype.Component;
import ru.itis.dto.SchoolClassDto;
import ru.itis.model.SchoolClassEntity;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SchoolClassDtoFactory {

    private final SchoolDtoFactory schoolDtoFactory;

    public SchoolClassDtoFactory(SchoolDtoFactory schoolDtoFactory) {
        this.schoolDtoFactory = schoolDtoFactory;
    }

    public SchoolClassDto createSchoolClassDto(SchoolClassEntity entity) {
        return SchoolClassDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .school(schoolDtoFactory.createSchoolDto(entity.getSchool()))
                .build();
    }

    public List<SchoolClassDto> createSchoolClassDtoList(List<SchoolClassEntity> entities) {
        return entities.stream().map(this::createSchoolClassDto).collect(Collectors.toList());
    }
}
