package ru.itis.factory;

import org.springframework.stereotype.Component;
import ru.itis.dto.TestDto;
import ru.itis.dto.UserDto;
import ru.itis.model.TestEntity;
import ru.itis.model.UserEntity;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TestDtoFactory {

    public TestDto createTestDto(TestEntity entity) {
        return TestDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .isStarted(entity.getIsStarted())
                .build();
    }

    public List<TestDto> createTestDtoList(List<TestEntity> entities) {
        return entities.stream().map(this::createTestDto).collect(Collectors.toList());
    }
}
