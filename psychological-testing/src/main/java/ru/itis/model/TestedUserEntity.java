//package ru.itis.model;
//
//import lombok.*;
//import lombok.experimental.FieldDefaults;
//
//import javax.persistence.*;
//import java.time.Instant;
//import java.util.ArrayList;
//import java.util.List;
//
//@Getter
//@Setter
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor
//@Entity
//@Table(name = "tested_user")
//public class TestedUserEntity {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE)
//    private Long id;
//
//    @Column(length = 64)
//    @NonNull
//    private String firstName;
//
//    @Column(length = 64)
//    @NonNull
//    private String lastName;
//
//    @Column(length = 64)
//    private String middleName;
//
//    @NonNull
//    private Instant birthday;
//
//    @Column(length = 64)
//    @NonNull
//    @Enumerated(EnumType.STRING)
//    private TestedUserStatus status;
//
//    @Column(length = 128)
//    @NonNull
//    private String login;
//
//    @Builder.Default
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
//    @JoinColumn(name = "tested_user_id", referencedColumnName = "id")
//    private List<TestAnswerEntity> testedUserAnswers = new ArrayList<>();
//
//    @Column(length = 10)
//    @NonNull
//    private String password;
//
//    @NonNull
//    @ManyToOne
//    @JoinColumn(name = "school_class_id", referencedColumnName = "id")
//    private SchoolClassEntity schoolClass;
//
//    public static TestedUserEntity makeDefault(
//            String firstName,
//            String middleName,
//            String lastName,
//            String login,
//            String password,
//            Instant birthday,
//            TestedUserStatus status,
//            SchoolClassEntity schoolClass) {
//        return builder()
//                .firstName(firstName)
//                .middleName(middleName)
//                .lastName(lastName)
//                .login(login)
//                .password(password)
//                .birthday(birthday)
//                .status(status)
//                .schoolClass(schoolClass)
//                .build();
//    }
//}
