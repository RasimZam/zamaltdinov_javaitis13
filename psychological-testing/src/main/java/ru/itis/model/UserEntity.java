package ru.itis.model;

import lombok.*;
import ru.itis.domains.UserRole;

import javax.persistence.*;
import java.time.Instant;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NonNull
    private String firstName;

    @NonNull
    private String lastName;

    @NonNull
    private String middleName;

    @NonNull
    private Instant birthday;

    @NonNull
    @Enumerated(EnumType.STRING)
    private UserRole role;

    @NonNull
    @Column(length = 10485760)
    private String login;

    @NonNull
    @Column(length = 10485760)
    private String password;

    @NonNull
    @ManyToOne
    private SchoolClassEntity schoolClass;

    public static UserEntity makeDefault(String firstName, String middleName,
                                         String lastName,
                                         String login,
                                         String password,
                                         Instant birthday,
                                         UserRole role,
                                         SchoolClassEntity schoolClass) {
        return builder()
                .firstName(firstName)
                .middleName(middleName)
                .lastName(lastName)
                .login(login)
                .password(password)
                .birthday(birthday)
                .role(role)
                .schoolClass(schoolClass)
                .build();
    }
}
