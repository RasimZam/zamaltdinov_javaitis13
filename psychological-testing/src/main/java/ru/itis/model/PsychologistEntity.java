package ru.itis.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "psychologist")
public class PsychologistEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

//    @Column(length = 300)
    @NonNull
    private String fio;

//    @Column(length = 64)
    @NonNull
    private String login;

//    @Column(length = 64)
    @NonNull
    private String password;

//    @ManyToOne
//    @JoinColumn(name = "school_class_id", referencedColumnName = "id")
//    private SchoolClassEntity schoolClass;
//
//    @Builder.Default
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
//    @JoinColumn(name = "psychologist_id", referencedColumnName = "id")
//    private List<TestEntity> tests = new ArrayList<>();
}
