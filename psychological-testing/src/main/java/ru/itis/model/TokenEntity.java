package ru.itis.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "token")
public class TokenEntity {

    private static final int EXPIRED_SECONDS = 60 * 60; // One hour expired

    @Id
    @Builder.Default
    private String token = UUID.randomUUID().toString();

    @Builder.Default
    private Instant expiredAt = Instant.now().plusSeconds(EXPIRED_SECONDS);

    @Builder.Default
    private Instant createdAt = Instant.now();

//    @ManyToOne
//    @JoinColumn(name = "psychologist_id", referencedColumnName = "id")
//    private PsychologistEntity psychologist;

//    public void updateExpiredAt() {
//        this.expiredAt = Instant.now().plusSeconds(EXPIRED_SECONDS);
//    }
}
