package ru.itis.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "question")
public class QuestionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NonNull
    private Integer questionOrder;

    @Column(length = 256)
    private String text;

    @NonNull
    @ManyToOne
//    @JoinColumn(name = "test_id", referencedColumnName = "id")
    private TestEntity test;

//    @Column(name = "test_id", updatable = false, insertable = false)
//    private Long testId;
//
//    @Builder.Default
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
//    @JoinColumn(name = "question_id", referencedColumnName = "id")
//    private List<AnswerEntity> answers = new ArrayList<>();
//
//    public static QuestionEntity makeDefault() {
//        return builder().build();
//    }
}
