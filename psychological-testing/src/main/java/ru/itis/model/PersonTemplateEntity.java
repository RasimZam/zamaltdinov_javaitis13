//package ru.itis.model;
//
//import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
//import lombok.*;
//import lombok.experimental.FieldDefaults;
//import org.hibernate.annotations.Type;
//import org.hibernate.annotations.TypeDef;
//
//import javax.persistence.*;
//
//@Getter
//@Setter
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor
//@Entity
//@Table(name = "person_template")
////https://ligerlearn.com/creating-java-jpa-entities-with-a-json-field/
//@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
//public class PersonTemplateEntity {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE)
//    private Long id;
//
//    @Column(length = 1000)
//    private String text;
//
//    @Type(type = "jsonb")
//    @Column(columnDefinition = "jsonb", length = 20000)
//    @JoinColumn(name = "test_id", referencedColumnName = "id")
//    private List<UserShouldAnswer> answers;
//
//    @ManyToOne
//    @JoinColumn(name = "test_id", referencedColumnName = "id")
//    private TestEntity test;
//}
