//package ru.itis.model;
//
//import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
//import lombok.*;
//import lombok.experimental.FieldDefaults;
//import org.hibernate.annotations.Type;
//import org.hibernate.annotations.TypeDef;
//
//import javax.persistence.*;
//import java.time.Instant;
//
//@Getter
//@Setter
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor
//@FieldDefaults(level = AccessLevel.PRIVATE)
//@Entity
//@Table(name = "test_answer")
////https://ligerlearn.com/creating-java-jpa-entities-with-a-json-field/
//@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
//public class TestAnswerEntity {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE)
//    private Long id;
//
//    @NonNull
//    @ManyToOne
//    private TestedUserEntity testedUser;
//
//    @NonNull
//    @ManyToOne
//    @JoinColumn(name = "test_id", referencedColumnName = "id")
//    private TestEntity test;
//
//    @Type(type = "jsonb")
//    @Column(columnDefinition = "jsonb", length = 20000)
//    private List<TestedUserAnswer> answers;
//
//    private Instant startAt;
//
//    @Builder.Default
//    private Instant endAt = Instant.now();
//}
