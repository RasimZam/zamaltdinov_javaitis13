package ru.itis.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "answer")
public class AnswerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NonNull
    private Integer answerOrder;

    @NonNull
    private String name;

    @NonNull
    @ManyToOne
    private QuestionEntity question;

//    @Column(length = 50)
//    private String text;

//    @ManyToOne
//    @JoinColumn(name = "question_id", referencedColumnName = "id")
//    private Long questionId;

//    public static AnswerEntity makeDefault() {
//        return builder().build();
//    }
}
