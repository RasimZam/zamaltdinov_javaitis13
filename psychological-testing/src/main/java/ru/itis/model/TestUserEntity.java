package ru.itis.model;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "test_users")
public class TestUserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NonNull
    @ManyToOne
    private UserEntity user;

    @NonNull
    @ManyToOne
    private TestEntity test;

    @NonNull
    private String answers;

    @Builder.Default
    @NonNull
    private Instant createAt = Instant.now();
}
