//package ru.itis.model;
//
//import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
//import lombok.*;
//import org.hibernate.annotations.Type;
//import org.hibernate.annotations.TypeDef;
//
//import javax.persistence.*;
//import java.util.List;
//
//@Getter
//@Setter
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor
//@Entity
//@Table(name = "sample_test")
////https://ligerlearn.com/creating-java-jpa-entities-with-a-json-field/
//@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
//public class SampleTestEntity {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE)
//    private Long id;
//
//    @Builder.Default
//    private Integer testId = 2;
//
//    @Builder.Default
//    private Integer personalId = 3;
//
//    @Type(type = "jsonb")
//    @Column(columnDefinition = "jsonb", length = 20000)
//    private List<TestedUserAnswer> testedUserAnswers;
//}
