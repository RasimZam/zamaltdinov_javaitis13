package ru.itis.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "test")
public class TestEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

//    @Column(length = 256)
    private String name;

    @Builder.Default
    private Boolean status = false;

    @NonNull
    private Boolean isStarted;

    @ManyToOne
//    @JoinColumn(name = "psychologist_id", referencedColumnName = "id")
    private PsychologistEntity psychologist;

//    @Builder.Default
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
//    @JoinColumn(name = "test_id", referencedColumnName = "id")
//    private List<PersonTemplateEntity> personTemplates = new ArrayList<>();
//
//    @Builder.Default
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
//    @JoinColumn(name = "test_id", referencedColumnName = "id")
//    private List<TestAnswerEntity> testAnswers = new ArrayList<>();
//
//    @Builder.Default
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
//    @JoinColumn(name = "test_id", referencedColumnName = "id")
//    private List<QuestionEntity> questions = new ArrayList<>();

    public static TestEntity makeDefault() {
        return builder().build();
    }
}
