package ru.itis.controller;

import com.ibm.icu.text.Transliterator;
import lombok.experimental.ExtensionMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.itis.domains.UserRole;
import ru.itis.dto.AckDto;
import ru.itis.dto.UserDto;
import ru.itis.exception.NotFoundException;
import ru.itis.factory.UserDtoFactory;
import ru.itis.model.SchoolClassEntity;
import ru.itis.model.UserEntity;
import ru.itis.repository.SchoolClassRepository;
import ru.itis.repository.UserRepository;
import ru.itis.util.StringChecker;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@ExtensionMethod(StringChecker.class)
@Controller
@Transactional
public class UserController {

    public static final String GET_USERS = "/api/schools/classes/users";
    public static final String CREATE_USER = "/api/schools/classes/{classId}/users";
    public static final String DELETE_USER = "/api/schools/classes/users/{userId}";
    public static final String GET_USER_ID_BY_LOGIN_AND_PASSWORD = "/api/schools/classes/users";

    private static final String CYRILLIC_TO_LATIN = "Russian-Latin/BGN";
    private static  final Transliterator toLatinTrans = Transliterator.getInstance(CYRILLIC_TO_LATIN);
    private static final int PASSWORD_LENGTH = 10;

    private final UserRepository userRepository;
    private final SchoolClassRepository schoolClassRepository;
    private final UserDtoFactory userDtoFactory;

    public UserController(UserRepository userRepository, SchoolClassRepository schoolClassRepository,
                          UserDtoFactory userDtoFactory) {
        this.userRepository = userRepository;
        this.schoolClassRepository = schoolClassRepository;
        this.userDtoFactory = userDtoFactory;
    }

    @GetMapping(GET_USERS)
    public ResponseEntity<List<UserDto>> getUsers(@RequestParam String filter) {
        boolean isFiltered = !filter.trim().isEmpty();
        List<UserEntity> users = userRepository.findAllByFilter(isFiltered, filter);
        return ResponseEntity.status(HttpStatus.OK).body(userDtoFactory.createUserDtoList(users));
    }

    @PostMapping(CREATE_USER)
    public ResponseEntity<UserDto> createUser(
            @RequestParam Instant birthday,
            @RequestParam String firstName,
            @RequestParam (defaultValue = "") String middleName,
            @RequestParam String lastName,
            @RequestParam UserRole userRole,
            @PathVariable Long classId) {

        firstName = firstName.trim();
        lastName = lastName.trim();
        firstName.checkOnEmpty("firstName");
        firstName.checkOnEmpty("lastName");
        middleName = middleName.trim().isEmpty() ? null : middleName;
        String login = makeLogin(firstName, lastName);
        String password = makePassword();
        SchoolClassEntity schoolClass = schoolClassRepository
                .findById(classId)
                .orElseThrow(() ->
                        new NotFoundException(String.format("School with this id \"%s\" not found ", classId))
                );
        UserEntity user = userRepository.saveAndFlush(
                UserEntity.makeDefault(firstName, middleName, lastName, login, password, birthday,userRole, schoolClass));

        return ResponseEntity.status(HttpStatus.OK).body(userDtoFactory.createUserDto(user));
    }

    @DeleteMapping(DELETE_USER)
    public ResponseEntity<AckDto> deleteUser(@PathVariable Long userId) {
        if (userRepository.existsById(userId)) {
            userRepository.deleteById(userId);
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(AckDto.makeDefault(true));
    }

    @GetMapping(GET_USER_ID_BY_LOGIN_AND_PASSWORD)
    public ResponseEntity<Long> deleteUser(@RequestParam String login, @RequestParam String password) {
        UserEntity user = userRepository.findTopByLoginAndPassword(login, password)
                .orElseThrow(() -> new NotFoundException("User with login and password doesn't exist"));
        return ResponseEntity.status(HttpStatus.OK).body(user.getId());
    }

    private String makeLogin(String firstName, String lastName) {
        String firstNameTransliterated = toLatinTrans.transliterate(firstName.toLowerCase());
        String lastNameTransliterated = toLatinTrans.transliterate(lastName.toLowerCase());
        return String.format("%s.%s", firstNameTransliterated.charAt(0), lastNameTransliterated);
    }

    private String makePassword() {
        return UUID.randomUUID().toString().replaceAll("-", "").substring(0,PASSWORD_LENGTH);
    }
}
