//package ru.itis.controller;
//
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.*;
//import ru.itis.dto.AckDto;
//import ru.itis.dto.PsychologistDto;
//import ru.itis.factory.PsychologistDtoFactory;
//import ru.itis.repository.PsychologistRepository;
//import ru.itis.repository.SchoolClassRepository;
//
//import javax.transaction.Transactional;
//import java.util.List;
//
//@Controller
//@Transactional
//public class PsychologistController {
//
//    public static final String GET_TEST_RESULT = "/api/psychologists/{psychologistId}/tests/{testId}/results";
//    public static final String GET_USERS_BY_CLASS = "/api/psychologists/{psychologistId}/classes/{classId}";
//    public static final String GENERATE_LINK_FOR_TEST = "/api/psychologists/{psychologistId}/schools/classes/{classId}/tests/{testId}/generate-link";
//    private static final String LINK_TEMPLATE = "/test/%s/class/%s/psychologist/%s";
//
//    private final PsychologistRepository psychologistRepository;
//    private final SchoolClassRepository schoolClassRepository;
//    private final PsychologistDtoFactory psychologistDtoFactory;
//
//    public PsychologistController(PsychologistRepository psychologistRepository, SchoolClassRepository schoolClassRepository,
//                                  PsychologistDtoFactory psychologistDtoFactory) {
//        this.psychologistRepository = psychologistRepository;
//        this.schoolClassRepository = schoolClassRepository;
//        this.psychologistDtoFactory = psychologistDtoFactory;
//    }
//
//
//    @GetMapping(GET_TEST_RESULT)
//    public ResponseEntity<List<TestUserDto>> getTestResults() {
//        return null;
//    }
//
//    @PostMapping(CREATE_PSYCHOLOGIST)
//    public ResponseEntity<PsychologistDto> createPsychologist() {
//        return null;
//    }
//
//    @DeleteMapping(DELETE_PSYCHOLOGIST)
//    public ResponseEntity<AckDto> deletePsychologist() {
//        return null;
//    }
//}
