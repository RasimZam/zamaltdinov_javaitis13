package ru.itis.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.AckDto;
import ru.itis.dto.SchoolClassDto;
import ru.itis.exception.NotFoundException;
import ru.itis.factory.SchoolClassDtoFactory;
import ru.itis.model.SchoolClassEntity;
import ru.itis.model.SchoolEntity;
import ru.itis.repository.SchoolClassRepository;
import ru.itis.repository.SchoolRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Controller
public class SchoolClassController {

    public static final String GET_SCHOOL_CLASSES = "/api/schools/{schoolId}/classes";
    public static final String CREATE_SCHOOL_CLASSES = "/api/schools/{schoolId}/classes/{className}";
    public static final String DELETE_SCHOOL_CLASS = "/api/schools/{schoolId}/classes/{classId}";

    private final SchoolClassRepository schoolClassRepository;
    private final SchoolRepository schoolRepository;
    private final SchoolClassDtoFactory schoolClassDtoFactory;

    public SchoolClassController(SchoolClassRepository schoolClassRepository,
                                 SchoolRepository schoolRepository, SchoolClassDtoFactory schoolClassDtoFactory) {
        this.schoolClassRepository = schoolClassRepository;
        this.schoolRepository = schoolRepository;
        this.schoolClassDtoFactory = schoolClassDtoFactory;
    }

    @GetMapping(GET_SCHOOL_CLASSES)
    public ResponseEntity<List<SchoolClassDto>> getSchoolClasses(
            @PathVariable Long schoolId,
            @RequestParam String prefix) {

        SchoolEntity school = getSchoolOrThrowNotFound(schoolId);
        List<SchoolClassEntity> schoolClasses =  school
                .getSchoolClasses()
                .stream()
                .filter(it -> it.getName().toUpperCase().startsWith(prefix.toLowerCase()))
                .collect(Collectors.toList());
        return ResponseEntity.status(HttpStatus.OK).body(schoolClassDtoFactory.createSchoolClassDtoList(schoolClasses));
    }

    @PostMapping(CREATE_SCHOOL_CLASSES)
    public ResponseEntity<SchoolClassDto> createSchoolClass(
            @PathVariable Long schoolId,
            @PathVariable String className ) {

        SchoolEntity school = getSchoolOrThrowNotFound(schoolId);
        SchoolClassEntity schoolClass = schoolClassRepository
                .saveAndFlush(SchoolClassEntity.makeDefault(className.toUpperCase(), school));
        return ResponseEntity.status(HttpStatus.OK).body(schoolClassDtoFactory.createSchoolClassDto(schoolClass));
    }

    @DeleteMapping(DELETE_SCHOOL_CLASS)
    public ResponseEntity<AckDto> deleteSchoolClass(@PathVariable Long schoolId,
                                                    @PathVariable Long classId) {

        schoolClassRepository.deleteByIdAndAndSchoolId(classId, schoolId);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(AckDto.makeDefault(true));
    }

    private SchoolEntity getSchoolOrThrowNotFound(Long schoolId) {
        SchoolEntity school = schoolRepository
                .findById(schoolId)
                .orElseThrow(() ->
                        new NotFoundException(String.format("School with this id \"%s\" not found ", schoolId))
                );
        return school;
    }
}
