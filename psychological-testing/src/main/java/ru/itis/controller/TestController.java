package ru.itis.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.dto.TestDto;
import ru.itis.exception.BadRequestException;
import ru.itis.exception.NotFoundException;
import ru.itis.factory.TestDtoFactory;
import ru.itis.model.PsychologistEntity;
import ru.itis.model.TestEntity;
import ru.itis.repository.PsychologistRepository;
import ru.itis.repository.TestRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Controller
@Transactional
public class TestController {

    public static final String GET_TESTS = "/api/psychologists/{psychologistId}/tests";
    public static final String CREATE_OR_UPDATE_TEST = "/api/tests";
    public static final String DELETE_TEST = "/api/tests/{testId}";

    private final TestRepository testRepository;
    private final PsychologistRepository psychologistRepository;
    private final TestDtoFactory testDtoFactory;

    public TestController(TestRepository testRepository, PsychologistRepository psychologistRepository, TestDtoFactory testDtoFactory) {
        this.testRepository = testRepository;
        this.psychologistRepository = psychologistRepository;
        this.testDtoFactory = testDtoFactory;
    }

    @GetMapping(GET_TESTS)
    public ResponseEntity<List<TestDto>> getTests(
            @PathVariable Long psychologistId,
            @RequestParam String filter) {

        boolean isFiltered = !filter.trim().isEmpty();
        List<TestEntity> tests = testRepository.findAllByFilter(isFiltered, filter, psychologistId);
        return ResponseEntity.status(HttpStatus.OK).body(testDtoFactory.createTestDtoList(tests));
    }

    @PostMapping(CREATE_OR_UPDATE_TEST)
    public ResponseEntity<TestDto> createOrUpdateTest(
            @RequestParam(required = false) Long testId,
            @RequestParam(required = false) Optional<Long> psychologistId,
            @RequestParam(required = false) Optional<String> testName
    ) {

        TestEntity test;
        if (testId != null) {
            test = testRepository.findById(testId)
                    .orElseThrow(() -> new NotFoundException(String.format("Test with this id \"%s\" not found ", testId)));
            testName.ifPresent(test::setName);
        } else {
            test = TestEntity.makeDefault();
            if (!testName.isPresent()) {
                throw new BadRequestException("Test can't be empty");
            }

            if (!psychologistId.isPresent()) {
                throw new BadRequestException("Psychologist id can't be empty");
            }
            test.setName(testName.get());
            PsychologistEntity psychologist = psychologistRepository
                    .findById(psychologistId.get())
                    .orElseThrow(() -> new NotFoundException(String.format("Psychologist with this id \"%s\" not found ", psychologistId.get())));
            test.setPsychologist(psychologist);
        }
        test = testRepository.saveAndFlush(test);
        return ResponseEntity.status(HttpStatus.OK).body(testDtoFactory.createTestDto(test));
    }
}
