package ru.itis.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.AckDto;
import ru.itis.dto.SchoolDto;
import ru.itis.exception.BadRequestException;
import ru.itis.factory.SchoolDtoFactory;
import ru.itis.model.SchoolEntity;
import ru.itis.repository.SchoolRepository;

import javax.transaction.Transactional;
import java.util.List;

@Controller
@Transactional
public class SchoolController {

    public static final String GET_SCHOOL = "/api/schools";
    public static final String CREATE_SCHOOL = "/api/schools/{schoolName}";
    public static final String DELETE_SCHOOL = "/api/schools/{schoolId}";

    private final SchoolRepository schoolRepository;
    private final SchoolDtoFactory schoolDtoFactory;

    public SchoolController(SchoolRepository schoolRepository, SchoolDtoFactory schoolDtoFactory) {
        this.schoolRepository = schoolRepository;
        this.schoolDtoFactory = schoolDtoFactory;
    }

    @GetMapping(GET_SCHOOL)
    public ResponseEntity<List<SchoolDto>> getSchools (@RequestParam String filter) {
        boolean isFiltered = !filter.trim().isEmpty();
        List<SchoolEntity> schools = schoolRepository.findAllByFilter(isFiltered, filter);
        return ResponseEntity.status(HttpStatus.OK).body(schoolDtoFactory.createSchoolDtoList(schools));
    }

    @PostMapping(CREATE_SCHOOL)
    public ResponseEntity<SchoolDto> createSchool(@PathVariable String schoolName) {
        if (schoolRepository.existsByName(schoolName)) {
            throw new BadRequestException(String.format("School is same name \"%s\" exist", schoolName));
        }
        SchoolEntity school = schoolRepository.saveAndFlush(SchoolEntity.makeDefault(schoolName));
        return ResponseEntity.status(HttpStatus.CREATED).body(schoolDtoFactory.createSchoolDto(school));
    }

    @DeleteMapping(DELETE_SCHOOL)
    public ResponseEntity<AckDto> deleteSchool(@PathVariable Long schoolId) {
        if (schoolRepository.existsById(schoolId)) {
            schoolRepository.deleteById(schoolId);
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(AckDto.makeDefault(true));
    }
}
