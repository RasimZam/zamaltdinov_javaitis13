package ru.itis.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.exception.NotFoundException;
import ru.itis.model.PsychologistEntity;
import ru.itis.model.TokenEntity;
import ru.itis.repository.PsychologistRepository;
import ru.itis.repository.TokenRepository;

import javax.transaction.Transactional;

@Controller
@Transactional
public class AuthorizationController {

    public static final String AUTHORIZE = "/api/psychologists/authorizations";

    private final PsychologistRepository psychologistRepository;
    private final TokenRepository tokenRepository;

    public AuthorizationController(PsychologistRepository psychologistRepository,
                                   TokenRepository tokenRepository) {
        this.psychologistRepository = psychologistRepository;
        this.tokenRepository = tokenRepository;
    }

    @GetMapping( AUTHORIZE)
    public ResponseEntity<String> getTestResults(@RequestParam String login, @RequestParam String password) {
        psychologistRepository
                .findTopByLoginAndPassword(login, password)
                .orElseThrow(() -> new NotFoundException("User with login and password doesn't found"));
        TokenEntity tokenEntity = tokenRepository.saveAndFlush(TokenEntity.builder().build());
        return ResponseEntity.status(HttpStatus.OK).body(tokenEntity.getToken());
    }
}
