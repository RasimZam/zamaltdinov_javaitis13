package example4;

import example4.model.Student;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
                new Student("Mike", 17, Gender.MAN),
                new Student("John", 20, Gender.MAN),
                new Student("Kate", 25, Gender.WOMAN),
                new Student("Nick", 28, Gender.MAN)
        );

        System.out.printf("%.1f",students.stream().filter((s) -> s.getGender() == Gender.MAN)
                .mapToInt(Student::getAge).average().getAsDouble());
        System.out.println();

        System.out.println(students.stream().filter((s) -> s.getAge() >= 18 && s.getGender() == Gender.MAN)
                .collect(Collectors.toList()));

    }
}
