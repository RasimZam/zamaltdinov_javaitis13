package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex2 {
    public static void main(String[] args) {
//        String s1 = "ABCD ABCE ABCFABCGABCH";
//        //передаем наш шаблон поиска
//        Pattern pattern1 = Pattern.compile("ABC");

//        String s1 = "abcd abce abc5abcg6abch";
//        Pattern pattern1 = Pattern.compile("abc[^e-g4-7]");

//        String s1 = "abcd abce abc5abcg6abch";
//        Pattern pattern1 = Pattern.compile("abc(e|5)");

//        String s1 = "abcd abce abc5abcg6abch";
//        Pattern pattern1 = Pattern.compile("abc.");

//        String s1 = "abcd abce abc5abcg6abch";
//        Pattern pattern1 = Pattern.compile("\\D");

//        String s1 = "abcd abce abc5abcg6abch";
//        Pattern pattern1 = Pattern.compile("\\w+");


//        String s1 = "abcd!@abce=====abc5abcg6abch";
//        Pattern pattern1 = Pattern.compile("\\W+");

        String s1 = "abcd!@abce=====abc5abcg6abch";
        Pattern pattern1 = Pattern.compile("\\W+");
        // устанваливаем соответствие
        Matcher matcher = pattern1.matcher(s1);

        // ищем соответствие
        while(matcher.find()) {
            // если соответствие есть, то выводим на консоль
            System.out.println("Position: " + matcher.start() + "  " + matcher.group());
        }
    }
}
