package IO;

import java.io.*;


public class CopyEx {
    public static void main(String[] args) {
        try(BufferedReader reader = new BufferedReader(new FileReader("D:\\Java\\Projects\\zamaltdinov_javaitis13\\IO\\src\\main\\resources\\test2.txt"));
            BufferedWriter writer = new BufferedWriter(new FileWriter("D:\\Java\\Projects\\zamaltdinov_javaitis13\\IO\\src\\main\\resources\\test3.txt"))) {

            String line;
            while((line = reader.readLine()) != null) {
                writer.write(line);
                writer.write("\n");
            }

        } catch (FileNotFoundException e) {

        } catch (IOException e) {

        }
    }
}
