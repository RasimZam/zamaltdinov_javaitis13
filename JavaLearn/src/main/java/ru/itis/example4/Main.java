package ru.itis.example4;

public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog("Baron");
        System.out.println("Количество лап у собаки: " + dog.getPaw());
        Cat cat = new Cat("Barsik");
        cat.sleep();
    }
}
