package ru.itis.lambda_expressions;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.function.Consumer;

public class Main1 {
    public static List<Car> createThreeCars(Supplier<Car> carSupplier) {
        List<Car> al = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            al.add(carSupplier.get());
        }
        return al;
    }

    public static void changeCar(Car car, Consumer<Car> carConsumer) {
        carConsumer.accept(car);
    }

    public static void main(String[] args) {
        List<Car> ourCars = createThreeCars(() ->
                new Car("Nissan Tiida", "Silver Metalic", 1.6));
        System.out.println("Our cars: " + ourCars);

        changeCar(ourCars.get(0),
                car -> {
                    car.setColor("red");
                    car.setEngine(2.4);
                    System.out.println("Upgrade car: " + car);
                });

    }
}
