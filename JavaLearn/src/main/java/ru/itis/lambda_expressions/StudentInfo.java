package ru.itis.lambda_expressions;

import java.util.List;
import java.util.function.Predicate;

public class StudentInfo {

    void testStudents(List<Student> al, Predicate<Student> pr) {
        for (Student s: al) {
            if (pr.test(s)) {
                System.out.println(s);
            }
        }
    }

//    void printStudentsOverGrade(List<Student> al, double grade) {
//        for (Student s: al) {
//            if (s.getAvgGrade() > grade) {
//                System.out.println(s);
//            }
//        }
//    }
//
//    void printStudentsUnderAge(List<Student> al, int age) {
//        for (Student s: al) {
//            if (s.getAge() < age) {
//                System.out.println(s);
//            }
//        }
//    }
//
//    void printStudentsMixCondition(List<Student> al, int age, double grade, char sex) {
//        for (Student s: al) {
//            if (s.getAge() > age && s.getAvgGrade() < grade && s.getSex() == sex) {
//                System.out.println(s);
//            }
//        }
//    }
}
