package ru.itis.example2;

public class Main {
    public static void main(String[] args) {
        Student student = new Student();
        student.setId(1);
        student.setName("Sergei");
        student.setSurname("Ivanov");
        student.setCourse(1);
        student.setMathAverageGrade(7.8);
        student.setEconomicsAverageGrade(9.2);
        student.setForeignLanguageAverageGrade(8.8);

        Student student1 = new Student(2, "Anton", "Petrov", 2);
        student1.setMathAverageGrade(8.4);
        student1.setEconomicsAverageGrade(7);
        student1.setForeignLanguageAverageGrade(7.9);

        Student student2 = new Student(2, "Ivan", "Sidorov", 2, 6.3, 7.5, 8.7);
    }
}
