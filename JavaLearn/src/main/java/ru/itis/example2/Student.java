package ru.itis.example2;

public class Student {

    private int id;
    private String name;
    private String surname;
    private int course;
    private double mathAverageGrade;
    private double economicsAverageGrade;
    private double foreignLanguageAverageGrade;

    public Student(int id, String name, String surname, int course, double mathAverageGrade,
                   double economicsAverageGrade, double foreignLanguageAverageGrade) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.course = course;
        this.mathAverageGrade = mathAverageGrade;
        this.economicsAverageGrade = economicsAverageGrade;
        this.foreignLanguageAverageGrade = foreignLanguageAverageGrade;
    }

    public Student(int id1, String name1, String surname1, int course1) {
        this(id1, name1, surname1, course1, 0, 0, 0);
    }

    public Student() {

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public double getMathAverageGrade() {
        return mathAverageGrade;
    }

    public void setMathAverageGrade(double mathAverageGrade) {
        this.mathAverageGrade = mathAverageGrade;
    }

    public double getEconomicsAverageGrade() {
        return economicsAverageGrade;
    }

    public void setEconomicsAverageGrade(double economicsAverageGrade) {
        this.economicsAverageGrade = economicsAverageGrade;
    }

    public double getForeignLanguageAverageGrade() {
        return foreignLanguageAverageGrade;
    }

    public void setForeignLanguageAverageGrade(double foreignLanguageAverageGrade) {
        this.foreignLanguageAverageGrade = foreignLanguageAverageGrade;
    }
}
