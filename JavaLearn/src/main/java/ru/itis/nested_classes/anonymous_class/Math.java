package ru.itis.nested_classes.anonymous_class;

public interface Math {
    int doOperation(int a, int b);
}
