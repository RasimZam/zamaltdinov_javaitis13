package ru.itis.nested_classes.inner_class;

public class Main {
    public static void main(String[] args) {
        Car car = new Car("green", 4);
        Car.Engine engine = car.new Engine(150);
        car.setEngine(engine);
        System.out.println(engine);
        System.out.println(car);

    }
}
