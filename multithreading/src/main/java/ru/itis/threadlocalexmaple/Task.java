package ru.itis.threadlocalexmaple;

public class Task implements Runnable {
    private final ThreadLocal<Integer> value = ThreadLocal.withInitial(() -> 0);

    @Override
    public void run() {
        Integer currentValue = value.get();
        value.set(currentValue + 1);
        System.out.println(Thread.currentThread().getName() + " -> " + value.get());
    }
}

