package ru.itis.example32;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Runner {
    public static void main(String[] args) {
        Lock fistGivenLock = new ReentrantLock();
        Lock secondGivenLock = new ReentrantLock();

        Thread firstGivenThread = new Thread(new Task(fistGivenLock, secondGivenLock));
        Thread secondGivenThread = new Thread(new Task(fistGivenLock, secondGivenLock));

        firstGivenThread.start();
        secondGivenThread.start();
    }
}
