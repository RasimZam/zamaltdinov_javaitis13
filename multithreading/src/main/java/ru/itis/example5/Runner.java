package ru.itis.example5;

import java.util.concurrent.ThreadFactory;

import static java.lang.Thread.setDefaultUncaughtExceptionHandler;

public class Runner {

    public static final String MESSAGE_EXCEPTION_TEMPLATE = "Exception was thrown with message '%s' in thread '%s'. \n";

    public static void main(String[] args) throws InterruptedException {
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler = (thread, exception)
                -> System.out.printf(MESSAGE_EXCEPTION_TEMPLATE, exception.getMessage(), thread.getName());

        ThreadFactory threadFactory = new DaemonThreadWithUncaughtExceptionHandlerFactory(uncaughtExceptionHandler);

        Thread fisrtThread = threadFactory.newThread(new Task());
        fisrtThread.start();

        Thread secondThread = threadFactory.newThread(new Task());
        secondThread.start();

        fisrtThread.join();
        secondThread.join();

//        Thread firstThread = new Thread(new Task());
//        firstThread.setDefaultUncaughtExceptionHandler(uncaughtExceptionHandler);
//        firstThread.start();

//        Thread secondThread = new Thread(new Task());
//        secondThread.setDefaultUncaughtExceptionHandler(uncaughtExceptionHandler);
//        secondThread.start();
    }
}
