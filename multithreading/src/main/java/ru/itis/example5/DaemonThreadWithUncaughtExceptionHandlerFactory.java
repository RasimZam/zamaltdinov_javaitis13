package ru.itis.example5;

import java.util.concurrent.ThreadFactory;

public class DaemonThreadWithUncaughtExceptionHandlerFactory implements ThreadFactory {

    private final Thread.UncaughtExceptionHandler uncaughtExceptionHandler;

    public DaemonThreadWithUncaughtExceptionHandlerFactory(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.uncaughtExceptionHandler = uncaughtExceptionHandler;
    }

    @Override
    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.setUncaughtExceptionHandler(this.uncaughtExceptionHandler);
        thread.setDaemon(true);
        return thread;
    }
}
