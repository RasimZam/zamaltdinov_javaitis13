package ru.itis.example5;

import static java.lang.Thread.currentThread;

public class Task implements Runnable {
    @Override
    public void run() {
        System.out.println(currentThread().isDaemon());
        throw new RuntimeException("Exception");
    }
}
