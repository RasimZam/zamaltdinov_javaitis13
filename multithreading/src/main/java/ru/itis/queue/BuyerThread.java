package ru.itis.queue;

import java.util.Queue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;

public class BuyerThread implements Runnable {
//    private final Queue<CashBox> cashBoxes;
    // блокирующая очередь
    private final BlockingQueue<CashBox> cashBoxes;

    public BuyerThread(BlockingQueue<CashBox> cashBoxes) {
        this.cashBoxes = cashBoxes;
    }

//    public BuyerThread(Queue<CashBox> cashBoxes) {
//        this.cashBoxes = cashBoxes;
//    }

//    @Override
//    public void run() {
//        try {
//            synchronized (cashBoxes) {
//                while (true) {
//                    if (!cashBoxes.isEmpty()) {
//                        CashBox cashBox = cashBoxes.remove();
//                        System.out.println(Thread.currentThread().getName() + " обслуживаeтся в кассе " + cashBox);
//
//                        cashBoxes.wait(5L);
//
//                        System.out.println(Thread.currentThread().getName() + " освобождаю кассу " + cashBox);
//                        cashBoxes.add(cashBox);
//                        cashBoxes.notifyAll();
//                        break;
//                    } else {
//                        System.out.println(Thread.currentThread().getName() + " ожидает свободную кассу");
//                        cashBoxes.wait();
//                    }
//                }
//            }
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public void run() {
        try {
            CashBox cashBox = cashBoxes.take();
            System.out.println(Thread.currentThread().getName() + " обслуживаeтся в кассе " + cashBox);
            Thread.sleep(5L);
            System.out.println(Thread.currentThread().getName() + " освобождаю кассу " + cashBox);
            cashBoxes.add(cashBox);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
