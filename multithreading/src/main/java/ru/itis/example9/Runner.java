package ru.itis.example9;

import java.util.Arrays;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.IntConsumer;
import java.util.stream.IntStream;

public class Runner {
    public static void main(String[] args) throws InterruptedException {
        Counter counter = new Counter();
        int incrementAmount = 10;
        Thread incrementingThread = new Thread(
                createTaskDoingOperationOnCounter(counter, i -> counter.increment(), incrementAmount));

        int decrementAmount = 10;
        Thread decrementingThread = new Thread(
                createTaskDoingOperationOnCounter(counter, i -> counter.decrement(), decrementAmount));

        startThreads(incrementingThread, decrementingThread);
        waitUntilFinish(incrementingThread, decrementingThread);

        System.out.printf("Counter's value: %d\n", counter.getValue());
    }

    private static void startThreads(Thread... threads) {
        Arrays.stream(threads).forEach(Thread::start);
    }

    private static void waitUntilFinish(Thread... threads) throws InterruptedException {
        for (Thread thread: threads) {
            thread.join();
        }
    }

    private static Runnable createTaskDoingOperationOnCounter(Counter counter, IntConsumer operation,
                                                              int times) {
        return () -> {
            counter.lock();
            try {
                IntStream.range(0, times)
                        .forEach(operation);
            } finally {
                counter.unlock();
            }
        };
    }

    private static final class Counter {
        private final Lock lock = new ReentrantLock();

        private int value;

        public void lock() {
            this.lock.lock();
            printMessageWithCurrentThreadNameArgument("Thread '%s' locked counter. \n");
        }

        public void increment() {
            this.value++;
            printMessageWithCurrentThreadNameArgument("Thread '%s' incremented counter. \n");
        }

        public void decrement() {
            this.value--;
            printMessageWithCurrentThreadNameArgument("Thread '%s' decremented counter. \n");
        }

        public void unlock() {
            printMessageWithCurrentThreadNameArgument("Thread '%s' is unlocking counter. \n");
            this.lock.unlock();
        }

        public int getValue() {
            return value;
        }

        private static void printMessageWithCurrentThreadNameArgument(String message) {
            System.out.printf(message, Thread.currentThread().getName());
        }
    }
}
