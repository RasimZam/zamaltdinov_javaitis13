package ru.itis.example1;

import java.util.stream.IntStream;

import static ru.itis.example1.Runner.printThreadNameAndNumber;


public class TaskSummingNumbers implements Runnable {
    private static final int INITIAL_VALUE_RESULT_NUMBER = 0;

    private final int fromNumber;
    private final int toNumber;
    private int resultNumber;

    public TaskSummingNumbers(final int fromNumber,final int toNumber) {
        this.fromNumber = fromNumber;
        this.toNumber = toNumber;
        this.resultNumber = INITIAL_VALUE_RESULT_NUMBER;
    }

    @Override
    public void run() {
        IntStream.rangeClosed(this.fromNumber, this.toNumber)
                .forEach(i -> this.resultNumber += i);
        printThreadNameAndNumber(this.resultNumber);
    }

    public int getResultNumber() {
        return resultNumber;
    }
}
