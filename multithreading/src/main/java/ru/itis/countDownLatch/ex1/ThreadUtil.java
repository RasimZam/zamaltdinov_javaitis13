package ru.itis.countDownLatch.ex1;

import java.util.Arrays;

public class ThreadUtil {
    public static void startThreads(Thread[] threads) {
        Arrays.stream(threads)
                .forEach(Thread::start);
    }

    private ThreadUtil() {
        throw new UnsupportedOperationException();
    }
}
