package ru.itis.countDownLatch.ex1;

import java.util.concurrent.CountDownLatch;

public abstract class ResourceTaskFactory {
    private long nextId;

    public ResourceTask create(CountDownLatch latch) {
        return this.create(this.nextId++, latch);
    }

    protected abstract ResourceTask create(long id, CountDownLatch latch);
}
