package ru.itis.countDownLatch.ex1;

import java.util.concurrent.CountDownLatch;

public class ResourceHandlerFactory extends ResourceTaskFactory {

    @Override
    protected ResourceHandler create(long id, CountDownLatch latch) {
        return new ResourceHandler(id, latch);
    }
}
