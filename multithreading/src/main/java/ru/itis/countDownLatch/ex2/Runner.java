package ru.itis.countDownLatch.ex2;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadLocalRandom;

public class Runner {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(5);
        System.out.println(Thread.currentThread().getName() + ": Let's go for hiking!!!");
        for (int i = 1; i <= 5; i++) {
            Thread thread = new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + ": Getting ready");
                try {
                    Thread.sleep(ThreadLocalRandom.current().nextInt(400, 700));
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                System.out.println(Thread.currentThread().getName() + ": Ready");
            }, "Friend-" + i);
            thread.start();
            latch.countDown();
        }
        latch.await();
        System.out.println(Thread.currentThread().getName() + ": Start hiking!!!");
    }
}
