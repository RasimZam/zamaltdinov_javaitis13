package ru.itis.example7.broker;

import ru.itis.example7.consumer.MessageConsumingTask;
import ru.itis.example7.model.Message;
import ru.itis.example7.producer.MessageProducingTask;

import java.util.ArrayDeque;
import java.util.Optional;
import java.util.Queue;

public class MessageBroker {
    private static final String MESSAGE_OF_MESSAGE_IS_PRODUCED = "Message '%s' is produced by producer '%s'. "
            + "Amount of messages before producing: %d.\n";
    private static final String MESSAGE_OF_MESSAGE_IS_CONSUMED = "Message '%s' is consumed by consumer '%s'."
            + "Amount of messages before consuming: %d.\n";

    private final Queue<Message> messagesToBeConsumed;
    private final int maxStoredMessages;

    public MessageBroker(int maxStoredMessages) {
        this.messagesToBeConsumed = new ArrayDeque<>(maxStoredMessages);
        this.maxStoredMessages = maxStoredMessages;
    }

    public synchronized void produce(Message message, MessageProducingTask producingTask) {
        try {
            while (this.messagesToBeConsumed.size() >= this.maxStoredMessages) {
                super.wait();
            }
            this.messagesToBeConsumed.add(message);
            System.out.printf(MESSAGE_OF_MESSAGE_IS_PRODUCED, message, producingTask.getName(),
                    this.messagesToBeConsumed.size() - 1);
            super.notifyAll();
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    public synchronized Optional<Message> consume(MessageConsumingTask consumingTask) {
        try {
            while (this.messagesToBeConsumed.isEmpty()) {
                super.wait();
            }
            Message consumedMessage = this.messagesToBeConsumed.poll();
            System.out.printf(MESSAGE_OF_MESSAGE_IS_CONSUMED, consumedMessage, consumingTask.getName(),
                    this.messagesToBeConsumed.size() + 1);
            super.notifyAll();
            return Optional.ofNullable(consumedMessage);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            return Optional.empty();
        }
    }

    private boolean isShouldProduce(MessageProducingTask producingTask) {
        return this.messagesToBeConsumed.size() < this.maxStoredMessages
                && this.messagesToBeConsumed.size() <= producingTask.getMaximalAmountMessagesToProduce();
    }

    private boolean isShouldConsume(MessageConsumingTask consumingTask) {
        return !this.messagesToBeConsumed.isEmpty()
                && this.messagesToBeConsumed.size() >= consumingTask.getMinimalAmountMessagesToConsume();
    }
}
