package ru.itis.example7;

import ru.itis.example7.broker.MessageBroker;
import ru.itis.example7.consumer.MessageConsumingTask;
import ru.itis.example7.producer.MessageFactory;
import ru.itis.example7.producer.MessageProducingTask;

import java.util.Arrays;

public class Runner {
    public static void main(String[] args) {
        int brokerMaxStoredMessage = 1;
        MessageBroker messageBroker = new MessageBroker(brokerMaxStoredMessage);
        MessageFactory messageFactory = new MessageFactory();

        Thread firstProducingThread = new Thread(new MessageProducingTask(messageBroker, messageFactory,
                brokerMaxStoredMessage, "PRODUCER_1"));
        Thread secondProducingThread = new Thread(new MessageProducingTask(messageBroker, messageFactory,
                10, "PRODUCER_2"));
//        Thread thirdProducingThread = new Thread(new MessageProducingTask(messageBroker, messageFactory,
//                5, "PRODUCER_3"));

        Thread firstConsumingThread = new Thread(new MessageConsumingTask(messageBroker,
                0, "CONSUMER_1"));
//        Thread secondConsumingThread = new Thread(new MessageConsumingTask(messageBroker,
//                6, "CONSUMER_2"));
//        Thread thirdConsumingThread = new Thread(new MessageConsumingTask(messageBroker,
//                11, "CONSUMER_3"));

        startThread(firstProducingThread, secondProducingThread, firstConsumingThread);

    }

    private static void startThread(Thread... threads) {
        Arrays.stream(threads).forEach(Thread::start);
    }
}
