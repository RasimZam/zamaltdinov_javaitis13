package ru.itis.example7.producer;

import ru.itis.example7.broker.MessageBroker;
import ru.itis.example7.model.Message;

import static java.util.concurrent.TimeUnit.SECONDS;


public final class MessageProducingTask implements Runnable {
    private final MessageBroker messageBroker;
    private final MessageFactory messageFactory;
    private final int maximalAmountMessagesToProduce;
    private final String name;

    public MessageProducingTask(MessageBroker messageBroker,
                                MessageFactory messageFactory,
                                int maximalAmountMessagesToProduce,
                                String name) {
        this.messageBroker = messageBroker;
        this.messageFactory = messageFactory;
        this.maximalAmountMessagesToProduce = maximalAmountMessagesToProduce;
        this.name = name;
    }

    public int getMaximalAmountMessagesToProduce() {
        return maximalAmountMessagesToProduce;
    }

    public String getName() {
        return name;
    }

    @Override
    public void run() {
         {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    Message producedMessage = this.messageFactory.create();
                    SECONDS.sleep(1);
                    this.messageBroker.produce(producedMessage, this);
                }
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
