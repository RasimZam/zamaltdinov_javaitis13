package ru.itis.example7.consumer;

import ru.itis.example7.broker.MessageBroker;
import ru.itis.example7.exception.MessageConsumingException;
import ru.itis.example7.model.Message;

import java.util.Optional;

import static java.util.concurrent.TimeUnit.SECONDS;

public class MessageConsumingTask implements Runnable {
    private final MessageBroker messageBroker;
    private final int minimalAmountMessagesToConsume;
    private final String name;

    public MessageConsumingTask(MessageBroker messageBroker,
                                int minimalAmountMessagesToConsume,
                                String name) {
        this.messageBroker = messageBroker;
        this.minimalAmountMessagesToConsume = minimalAmountMessagesToConsume;
        this.name = name;
    }

    public int getMinimalAmountMessagesToConsume() {
        return minimalAmountMessagesToConsume;
    }

    public String getName() {
        return name;
    }


    @Override
    public void run() {
//       try {
//           while (!Thread.currentThread().isInterrupted()) {
//               SECONDS.sleep(1);
//               Optional<Message> optionalConsumedMessage = this.messageBroker.consume(this);
//               optionalConsumedMessage.orElseThrow(MessageConsumingException::new);
//           }
//       } catch (InterruptedException ex) {
//           Thread.currentThread().interrupt();
//       }


        while (!Thread.currentThread().isInterrupted()) {
            Optional<Message> optionalConsumedMessage = this.messageBroker.consume(this);
            optionalConsumedMessage.orElseThrow(MessageConsumingException::new);
        }
    }
}
