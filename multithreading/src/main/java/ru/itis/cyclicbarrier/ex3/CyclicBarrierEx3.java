package ru.itis.cyclicbarrier.ex3;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierEx3 {
    public static void main(String[] args) {
        Runnable action = () -> System.out.println("На старт");
        CyclicBarrier barrier = new CyclicBarrier(3, action);

        Runnable task = () -> {
            try {
                barrier.await();
            } catch (BrokenBarrierException | InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        };

        System.out.println("Limit: " + barrier.getParties());
        for (int i = 0; i < 3; i++) {
            new Thread(task).start();
        }
    }
}
