package ru.itis.cyclicbarrier.ex1;

import java.util.List;

public abstract class CompositeTask<S extends Task> extends Task {
    private List<S> subtasks;

    public CompositeTask(long id,
                         List<S> subtasks) {
        super(id);
        this.subtasks = subtasks;
    }

    @Override
    public void perform() {
        this.subtasks.forEach(this::perform);
    }

    protected abstract void perform(S leafTask);
}
