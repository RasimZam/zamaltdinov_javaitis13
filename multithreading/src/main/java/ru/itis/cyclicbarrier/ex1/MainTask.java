package ru.itis.cyclicbarrier.ex1;

import java.util.List;

public class MainTask extends CompositeTask<Subtask> {

    public MainTask(long id, List<Subtask> subtasks) {
        super(id, subtasks);
    }

    @Override
    protected void perform(Subtask leafTask) {
        new Thread(leafTask::perform).start();
    }
}
