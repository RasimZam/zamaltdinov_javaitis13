package ru.itis.cyclicbarrier.ex1;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

public class LeafTask extends Task {
    private long secondDuration;
    private CyclicBarrier cyclicBarrier;

    public LeafTask(long id,
                    long secondDuration,
                    CyclicBarrier cyclicBarrier) {
        super(id);
        this.secondDuration = secondDuration;
        this.cyclicBarrier = cyclicBarrier;
    }

    @Override
    public void perform() {
        try{
            System.out.printf("%s is starting\n", this);
            TimeUnit.SECONDS.sleep(this.secondDuration);
            System.out.printf("%s has finished\n", this);
            this.cyclicBarrier.await();
        } catch (InterruptedException interruptedException) {
            Thread.currentThread().interrupt();
        } catch (BrokenBarrierException brokenBarrierException) {
            throw new RuntimeException(brokenBarrierException);
        }
    }
}
