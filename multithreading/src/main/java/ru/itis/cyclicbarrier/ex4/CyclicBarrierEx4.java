package ru.itis.cyclicbarrier.ex4;

import java.util.concurrent.CyclicBarrier;
import java.util.stream.IntStream;

public class CyclicBarrierEx4 {
    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(2);

        IntStream.range(0, 10).forEach(i -> {
            new Thread(new Worker(cyclicBarrier)).start();
        });
    }
}
