package ru.itis.cyclicbarrier.ex4;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

public class Worker implements Runnable {
    private CyclicBarrier barrier;

    public Worker(CyclicBarrier barrier) {
        this.barrier = barrier;
    }

    @Override
    public void run() {
        try {
            new Semaphore(2);
            System.out.println("Все за работу");
            barrier.await();
            System.out.println("Работаем");
            barrier.await();
            System.out.println("Закончили работу");
        } catch (InterruptedException | BrokenBarrierException e) {
            Thread.currentThread().interrupt();
        }

    }
}
