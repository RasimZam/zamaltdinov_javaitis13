package ru.itis.cyclicbarrier.ex2;

//Таск, который будет выполняться при достижении сторонами барьера
public class FerryBoat implements Runnable {

    @Override
    public void run() {
        try {
            Thread.sleep(500);
            System.out.println("Паром переправил автомобили!");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
