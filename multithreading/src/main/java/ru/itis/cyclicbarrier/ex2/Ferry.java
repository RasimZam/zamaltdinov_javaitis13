package ru.itis.cyclicbarrier.ex2;

import java.util.concurrent.CyclicBarrier;

public class Ferry {
    public static final CyclicBarrier BARRIER = new CyclicBarrier(3, new FerryBoat());

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 9; i++) {
            new Thread(new Car(i)).start();
            Thread.sleep(400);
        }
    }
}
