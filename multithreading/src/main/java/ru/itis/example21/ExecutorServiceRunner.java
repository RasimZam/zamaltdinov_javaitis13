package ru.itis.example21;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorServiceRunner {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Thread main started");

//        Runnable task = () -> {
//            for (int i = 0; i < 5; i++) {
//                System.out.println("[" + Thread.currentThread().getName() + "]" + "Message " + i);
//            }
//        };
//
//        ExecutorService executorService = Executors.newSingleThreadExecutor();
//        executorService.execute(task);
//        executorService.shutdownNow();

        Runnable task1 = () -> {
            for (int i = 0; i < 5; i++) {
                System.out.println("[" + Thread.currentThread().getName() + "]" + "Message " + i);
            }
        };

        Runnable task2 = () -> {
            for (int i = 0; i < 5; i++) {
                System.out.println("[" + Thread.currentThread().getName() + "]" + "Message " + i);
            }
        };

        Runnable task3 = () -> {
            for (int i = 0; i < 5; i++) {
                System.out.println("[" + Thread.currentThread().getName() + "]" + "Message " + i);
            }
        };

        Runnable task4 = () -> {
            for (int i = 0; i < 5; i++) {
                System.out.println("[" + Thread.currentThread().getName() + "]" + "Message " + i);
            }
        };

        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.execute(task1);
        executorService.execute(task2);
        executorService.execute(task3);
        executorService.execute(task4);
        executorService.shutdownNow();

        System.out.println("Thread main finished");
    }
}
