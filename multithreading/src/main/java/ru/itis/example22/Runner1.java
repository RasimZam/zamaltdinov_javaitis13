package ru.itis.example22;

import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class Runner1 {
    public static void main(String[] args) {
        BoundedBufferExample1<Integer> boundedBufferExample1 = new BoundedBufferExample1<>(5);
        Runnable producingTask = () -> Stream.iterate(0, i -> i + 1)
                .forEach(i -> {
                    try {
                        boundedBufferExample1.put(i);
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                });

        Thread producingThread = new Thread(producingTask);

        Runnable consumingTask = () -> {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    boundedBufferExample1.take();
                    TimeUnit.SECONDS.sleep(3);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        };

        Thread consumingThread = new Thread(consumingTask);
        producingThread.start();
        consumingThread.start();
    }
}
