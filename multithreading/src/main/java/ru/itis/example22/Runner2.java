package ru.itis.example22;

import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class Runner2 {
    public static void main(String[] args) {
        BoundedBufferExample2<Integer> boundedBufferExample2 = new BoundedBufferExample2<>(5);
        Runnable producingTask = () -> Stream.iterate(0, i -> i + 1)
                .forEach(i -> {
                    try {
                        boundedBufferExample2.put(i);
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                });

        Thread producingThread = new Thread(producingTask);

        Runnable consumingTask = () -> {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    boundedBufferExample2.take();
                    TimeUnit.SECONDS.sleep(1);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        };

        Thread consumingThread = new Thread(consumingTask);
        producingThread.start();
        consumingThread.start();
    }
}
