package ru.itis.example3;

import static java.lang.Thread.currentThread;
import static java.util.concurrent.TimeUnit.SECONDS;

public class Runner {
    public static void main(String[] args) throws InterruptedException {
        Thread communicatingThread = new Thread(() -> {
            try {
                while (!currentThread().isInterrupted()) {
                    doRequest();
                }
            } catch (InterruptedException e) {
                System.out.println("Thread was interrupted");
            }

        });
        communicatingThread.start();

        Thread stoppingThread = new Thread(() -> {
            if (isServerShouldBeOffed()) {
                communicatingThread.interrupt();
                stopServer();
            }
        });
        SECONDS.sleep(5);
        stoppingThread.start();
    }

    private static void doRequest() throws InterruptedException {
        System.out.println("Request was sent");
        SECONDS.sleep(1);
        System.out.println("Response was received");
    }

    private static boolean isServerShouldBeOffed() {
        return true;
    }

    private static void stopServer() {
        System.out.println("Server was stopped");
    }
}
