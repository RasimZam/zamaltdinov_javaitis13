package ru.itis.example4;

import static java.lang.Thread.currentThread;
import static java.util.concurrent.TimeUnit.SECONDS;

public class Task implements Runnable {
    @Override
    public void run() {
        while (true) {
            try {
                System.out.println("I am working");
                SECONDS.sleep(2);
            } catch (InterruptedException e) {
                currentThread().interrupt();
            }
        }
    }
}
