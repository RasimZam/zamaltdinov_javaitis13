package ru.itis.example4;

import static java.lang.System.out;

public class Runner {
    public static void main(String[] args) {
        Thread thread = new Thread(new Task());
        thread.setDaemon(true);
        thread.start();
        out.println("Main thread is finished");
    }
}
