package ru.itis.example20;

import java.util.concurrent.TimeUnit;

public class Chef implements Runnable {
    private Restaurant restaurant;
    private int count = 0;
    public Chef(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                synchronized (this) {
                    while (restaurant.meal != null) {
                        wait(); // Когда заберут блюдо
                    }
                    if (++count == 10) {
                        System.out.println("Out of food, closing");
                        restaurant.executorService.shutdownNow();
                    }
                    System.out.println("Order up!");
                    synchronized (restaurant.waitperson) {
                        restaurant.meal = new Meal(count);
                        restaurant.waitperson.notifyAll();
                    }
                    TimeUnit.MILLISECONDS.sleep(100);
                }
            }
        } catch (InterruptedException e) {
            System.out.println("Chef interrupted");
            Thread.currentThread().interrupt();
        }
    }
}
