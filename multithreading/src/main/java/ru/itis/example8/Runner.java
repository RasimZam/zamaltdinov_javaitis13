package ru.itis.example8;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

import static java.lang.Thread.currentThread;

public class Runner {
    public static void main(String[] args) {
        EvenNumberGenerator evenNumberGenerator = new EvenNumberGenerator();
        Runnable generatingTask = () -> IntStream.range(0, 100)
                .forEach(i -> System.out.println(evenNumberGenerator.generate()));

        Thread firstThread = new Thread(generatingTask);
        firstThread.start();

        Thread secondThread = new Thread(generatingTask);
        secondThread.start();

        Thread thirdThread = new Thread(generatingTask);
        thirdThread.start();
    }

    private static final class EvenNumberGenerator {
        private final Lock lock;
        private int nextEvenValue;

        public EvenNumberGenerator() {
            this.lock = new ReentrantLock();
            this.nextEvenValue = -2;
        }

        public int generate() {
            return this.lock.tryLock() ? this.onSuccessAcquireLock() : onFailAcquireLock();
        }

        private int onSuccessAcquireLock() {
            this.lock.lock();
            try {
                return this.nextEvenValue += 2;
            } finally {
                this.lock.unlock();
            }
        }

        private int onFailAcquireLock() {
            System.out.printf("Thread '%s' didn't acquire. \n", currentThread().getName());
            throw new RuntimeException();
        }
    }
}
