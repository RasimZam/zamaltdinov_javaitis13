package ru.itis.exchanger;

public class ExchangedObject {
    private long id;

    public ExchangedObject(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "[id = " + this.id + "]";
    }
}
