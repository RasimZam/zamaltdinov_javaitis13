package ru.itis.testbankaccount;

public class UnSynchBankTest {
    public static final int NACCOUNTS = 100;
    public static final double INITIAL_BALANCE = 1000;

    public static void main(String[] args) {
        Bank bank = new Bank(NACCOUNTS, INITIAL_BALANCE);
        int i = 0;
        for (i = 0; i < NACCOUNTS; i++) {
            TransferRunnable runnable = new TransferRunnable(bank, i, INITIAL_BALANCE);
            Thread thread = new Thread(runnable);
            thread.start();
        }
    }
}
