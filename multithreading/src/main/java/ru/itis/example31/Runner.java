package ru.itis.example31;

import java.util.concurrent.TimeUnit;

public class Runner {
    public static void main(String[] args) throws InterruptedException {
        PrintingTask printingTask = new PrintingTask();
        Thread thread = new Thread(printingTask);
        thread.start();
        TimeUnit.SECONDS.sleep(2);

        printingTask.setShouldPrint(false);
        System.out.println("Printing should be stoppped");
    }
}
