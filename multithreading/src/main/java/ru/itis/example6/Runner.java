package ru.itis.example6;

import java.util.Arrays;
import java.util.function.IntConsumer;
import java.util.stream.IntStream;

public class Runner {
    private static int firstCounter = 0;
    private static int secondCounter = 0;

    public static final int INCREMENT_AMOUNT_FIRST_COUNTER = 500;
    public static final int INCREMENT_AMOUNT_SECOND_COUNTER = 600;

    private static final Object LOCK_TO_INCREMENT_FIRST_COUNTER = new Object();
    private static final Object LOCK_TO_INCREMENT_SECOND_COUNTER = new Object();

    public static void main(String[] args) throws InterruptedException {
        Counter firstCounter = new Counter();
        Counter secondCounter = new Counter();


        Thread firstThread = createIncrementingCounterThread(INCREMENT_AMOUNT_FIRST_COUNTER,
                i -> firstCounter.increment());
        Thread secondThread = createIncrementingCounterThread(INCREMENT_AMOUNT_FIRST_COUNTER,
                i -> firstCounter.increment());

        Thread thirdThread = createIncrementingCounterThread(INCREMENT_AMOUNT_SECOND_COUNTER,
                i -> secondCounter.increment());
        Thread fourthThread = createIncrementingCounterThread(INCREMENT_AMOUNT_SECOND_COUNTER,
                i -> secondCounter.increment());

        startThreads(firstThread, secondThread, thirdThread, fourthThread);

        waitUntilAreCompleted(firstThread, secondThread, thirdThread, fourthThread);

        System.out.println(firstCounter.counter);
        System.out.println(secondCounter.counter);
    }

    private static Thread createIncrementingCounterThread(int incrementAmount,
                                                          IntConsumer incrementingOperation) {
        return new Thread(() -> IntStream.range(0 ,incrementAmount)
                .forEach(incrementingOperation));
    }

    private static void startThreads(Thread... threads) {
        Arrays.stream(threads).forEach(Thread::start);
    }

    private static void waitUntilAreCompleted(Thread... threads) {
        Arrays.stream(threads).forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        });
    }

    private static void incrementFirstCounter() {
        synchronized (LOCK_TO_INCREMENT_FIRST_COUNTER) {
            firstCounter++;
        }
    }

    private synchronized static void incrementSecondCounter() {
        synchronized (LOCK_TO_INCREMENT_SECOND_COUNTER) {
            secondCounter++;
        }
    }

    private static final class Counter {
        private int counter;

//        public synchronized void increment() {
//            this.counter++;
//        }

        public void increment() {
            synchronized (this) {
                this.counter++;
            }
        }
    }
}
