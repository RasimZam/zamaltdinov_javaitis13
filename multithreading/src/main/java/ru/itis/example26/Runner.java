package ru.itis.example26;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Runner {
    public static void main(String[] args) {
        ThreadPoolExecutor es = new ThreadPoolExecutor(3, 6, 1,
                TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>());

        for (int i = 0; i < 5; i++) {
            MyCallable mc = new MyCallable();
            es.submit(mc);
        }

        es.shutdown();
    }
}
