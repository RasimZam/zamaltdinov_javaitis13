package ru.itis.example25;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Runner {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
//        List<Future<Long>> tasks = new ArrayList<>();
        List<MyCallable> tasks = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
//            Future<Long> submit = executorService.submit(new MyCallable());
            MyCallable myCallable = new MyCallable();
            tasks.add(myCallable);
        }

        List<Future<Long>> futures = executorService.invokeAll(tasks);

//        for (Future<Long> f: tasks) {
//            f.get();
//        }

        System.out.println("FINISH");
        executorService.shutdown();
    }
}
