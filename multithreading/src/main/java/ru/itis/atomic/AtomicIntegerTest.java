package ru.itis.atomic;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicIntegerTest implements Runnable {
    private final AtomicInteger atomicCounter = new AtomicInteger(0);

    @Override
    public void run() {
        while (true) {
            evenIncrement();
        }
    }

    public int getValue() {
        return atomicCounter.get();
    }

    private void evenIncrement() {
        atomicCounter.addAndGet(2);
    }

    public static void main(String[] args) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                System.err.println("Aborting");
                System.exit(0);
            }
        }, 5000);

        ExecutorService threadPool = Executors.newCachedThreadPool();
        AtomicIntegerTest ait = new AtomicIntegerTest();
        threadPool.execute(ait);
        while (true) {
            int value = ait.getValue();
            if (value % 2 == 0) {
                System.out.println(value);
                System.exit(0);
            }
        }
    }
}
