package ru.itis.atomic;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicDemo {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger();
        int result = atomicInteger.incrementAndGet();
        System.out.println(result);

        int result1 = atomicInteger.addAndGet(20);
        System.out.println(result1);

    }
}
