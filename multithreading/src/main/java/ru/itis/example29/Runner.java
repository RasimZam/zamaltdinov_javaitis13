package ru.itis.example29;

import java.util.ArrayList;
import java.util.List;

public class Runner {
    public static void main(String[] args) {
        List<String> init = new ArrayList<>();
        init.add("Name: -1");
        init.add("Name: -2");

        List<Person> test = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            test.add(new Person((long)i, "Name: " + i));
        }

        test.stream()
                .map(Person::getName)
                .forEach(init::add);

        System.out.println("FINISH: " + init.size());
    }
}
