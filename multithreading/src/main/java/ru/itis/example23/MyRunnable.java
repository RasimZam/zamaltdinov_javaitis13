package ru.itis.example23;

public class MyRunnable implements Runnable {
    @Override
    public void run() {
        try {
            System.out.println("Start runnable: " + Thread.currentThread().getId());
            Thread.sleep(5000);
            System.out.println("Finish runnable: " + Thread.currentThread().getId());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
