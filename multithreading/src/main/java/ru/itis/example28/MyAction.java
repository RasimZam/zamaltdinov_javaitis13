package ru.itis.example28;

import java.util.concurrent.RecursiveAction;

public class MyAction extends RecursiveAction {
    public MyAction(int value) {
        this.value = value;
    }

    private int value;

    @Override
    protected void compute() {
        if (value <= 4) {
            System.out.println("Hello before " + Thread.currentThread().getId() + " " + value);
        try {
                Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
            System.out.println("Hello after " + Thread.currentThread().getId() + " " + value);
        } else  {
            MyAction m1 = new MyAction(value / 2);
            MyAction m2 = new MyAction(value / 2);
            invokeAll(m1, m2);
        }
    }
}
