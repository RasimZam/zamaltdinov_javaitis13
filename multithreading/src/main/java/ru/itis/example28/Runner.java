package ru.itis.example28;

import java.util.concurrent.ForkJoinPool;

public class Runner {
    public static void main(String[] args) {
        final int MAX = 16;
        ForkJoinPool pool = new ForkJoinPool();
        pool.invoke(new MyAction(MAX));
        System.out.println("FINISH");
    }
}
