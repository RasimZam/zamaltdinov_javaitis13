package ru.itis.example17;

public class PairValuesNotEqualException extends RuntimeException {

    public PairValuesNotEqualException() {
        super("Pair values not equal: ");
    }
}
