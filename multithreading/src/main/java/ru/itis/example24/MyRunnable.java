package ru.itis.example24;

public class MyRunnable implements Runnable {
    @Override
    public void run() {
        try {
            System.out.println("Started: " + Thread.currentThread().getId());
            Thread.sleep(5000);
            System.out.println("Finished: " + Thread.currentThread().getId());
        } catch (InterruptedException e) {
            e.printStackTrace(System.out);
        }
    }
}
