package ru.itis.example24;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Runner {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

//        executorService.execute(new MyRunnable());
        Future<Integer> submit = executorService.submit(new MyCallable());

        Integer id = submit.get();
        System.out.println(id);

        System.out.println("ShutDown");
        executorService.shutdown();
    }
}
