package ru.itis.semaphore.example2;

import java.util.concurrent.Semaphore;

public class CountThread implements Runnable {
    private int x = 0;
    private Semaphore semaphore;
    private String name;

    public CountThread(Semaphore semaphore, String name) {
        this.semaphore = semaphore;
        this.name = name;
    }


    @Override
    public void run() {
        try {
            System.out.println(name + " ожидает разрешение");
            semaphore.acquire();
            x = 1;
            for (int i = 1; i < 5; i++) {
                System.out.println(this.name + ": " + x);
                x++;
                Thread.sleep(2000);
            }
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(name + " освобождает разрешение");
        semaphore.release();
    }
}
