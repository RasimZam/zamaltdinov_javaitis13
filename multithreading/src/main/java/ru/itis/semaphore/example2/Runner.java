package ru.itis.semaphore.example2;

import java.util.concurrent.Semaphore;

public class Runner {
    public static void main(String[] args) throws InterruptedException {
        Semaphore semaphore = new Semaphore(1);
        for (int i = 1; i <= 3; i++) {
            new Thread(new CountThread(semaphore, "CountedThread " + i)).start();
            Thread.sleep(5000);
        }
    }
}
