package ru.itis.semaphore.example4;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.Semaphore;
import java.util.function.Supplier;
import java.util.stream.IntStream;

public abstract class AbstractPool<T> {
    private final List<PoolObject<T>> poolObjects;
    private final Semaphore semaphore;

    public AbstractPool(Supplier<T> objectSupplier, int size) {
        this.poolObjects = createPoolObjects(objectSupplier, size);
        this.semaphore = new Semaphore(size);
    }

    private static <T> List<PoolObject<T>> createPoolObjects(Supplier<T> objectSupplier, int size) {
        return IntStream.range(0, size)
                .mapToObj(i -> objectSupplier.get())
                .map(object -> new PoolObject<>(object, false))
                .toList();
    }

    public T acquire() {
        this.semaphore.acquireUninterruptibly();
        return acquireObject();
    }

    public void release(T object) {
        if (this.releaseObject(object)) {
            this.semaphore.release();
        }
    }

    protected abstract void cleanObject(T object);

    private synchronized boolean releaseObject(T object) {
        return this.poolObjects.stream()
                .filter(PoolObject::isIssued)
                .filter(poolObject -> Objects.equals(poolObject.getObject(), object))
                .findFirst()
                .map(this::cleanPoolObject)
                .isPresent();
    }

    private PoolObject<T> cleanPoolObject(PoolObject<T> poolObject) {
        poolObject.setIssued(false);
        this.cleanObject(poolObject.getObject());
        return poolObject;
    }

    private synchronized T acquireObject() {
        return this.poolObjects.stream()
                .filter(poolObject -> !poolObject.issued)
                .findFirst()
                .map(AbstractPool::markIssued)
                .map(PoolObject::getObject)
                .orElseThrow(IllegalAccessError::new);
    }

    private static <T> PoolObject<T> markIssued(PoolObject<T> poolObject) {
        poolObject.setIssued(true);
        return poolObject;
    }

    private static final class PoolObject<T> {
        private final T object;
        private boolean issued;

        private PoolObject(T object, boolean issued) {
            this.object = object;
            this.issued = issued;
        }

        public T getObject() {
            return object;
        }

        public boolean isIssued() {
            return issued;
        }

        public void setIssued(boolean issued) {
            this.issued = issued;
        }
    }
}
