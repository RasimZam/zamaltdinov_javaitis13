package ru.itis.semaphore.example4;

public class Main {
    public static void main(String[] args) {
        int poolSize = 3;
        ConnectionPool connectionPool = new ConnectionPool(poolSize);
        ConnectionPoolTask poolTask = new ConnectionPoolTask(connectionPool);
        int threadCount = 15;
        Thread[] threads = ThreadUtil.createThreads(poolTask, threadCount);
        ThreadUtil.startThreads(threads);
    }
}
