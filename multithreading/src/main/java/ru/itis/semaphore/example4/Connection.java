package ru.itis.semaphore.example4;

import java.util.Objects;

public class Connection {
    private final long id;
    private boolean autoCommit;

    public Connection(long id, boolean autoCommit) {
        this.id = id;
        this.autoCommit = autoCommit;
    }

    public boolean isAutoCommit() {
        return autoCommit;
    }

    public void setAutoCommit(boolean autoCommit) {
        this.autoCommit = autoCommit;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }

        if (otherObject == null || getClass() != otherObject.getClass()) {
            return false;
        }

        Connection other = (Connection) otherObject;
        return this.id == other.id && this.autoCommit == other.autoCommit;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.autoCommit);
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "[id=" + id + ", autoCommit=" + autoCommit + "]";
    }


}
