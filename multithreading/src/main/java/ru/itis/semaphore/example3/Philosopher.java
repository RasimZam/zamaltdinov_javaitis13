package ru.itis.semaphore.example3;

import java.util.concurrent.Semaphore;

public class Philosopher implements Runnable {
    private Semaphore semaphore;
    private int num = 0;
    private int id;

    public Philosopher(Semaphore semaphore, int id) {
        this.semaphore = semaphore;
        this.id = id;
    }

    @Override
    public void run() {
        try {
            while (num < 3) {
                //запрашиваем у семафора разрешение на выполнение
                semaphore.acquire();
                System.out.println("Философ " + id + " садится за стол");
                // Философ ест
                Thread.sleep(500);
                num++;
                System.out.println("Философ " + id + " выходит из-за стола");
                semaphore.release();

                // Философ отдыхает
                Thread.sleep(500);
            }

        } catch (InterruptedException e) {
            System.out.println ("У философа " + id + " проблемы со здоровьем");
        }
    }
}
