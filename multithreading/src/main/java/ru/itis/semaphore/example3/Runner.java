package ru.itis.semaphore.example3;

import java.util.concurrent.Semaphore;

public class Runner {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(2);
        for (int i = 0; i < 6; i++) {
            new Thread(new Philosopher(semaphore, i)).start();
        }
    }
}
