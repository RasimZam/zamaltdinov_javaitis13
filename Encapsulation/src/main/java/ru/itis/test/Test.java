package ru.itis.test;

public class Test {
    public static void main(String[] args) {
        Man man = new Man("male");
        man.setName("Rasim");
        man.setWeight(50);
        man.setAge(15);
        System.out.println(man.getName());
        System.out.println(man.getAge());
        System.out.println(man.getWeight());
        man.setWeight(-50);
        System.out.println(man.getWeight());

    }
}
