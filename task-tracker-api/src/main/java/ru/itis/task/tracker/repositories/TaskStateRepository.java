package ru.itis.task.tracker.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.task.tracker.models.TaskStateEntity;

import java.util.Optional;

public interface TaskStateRepository extends JpaRepository<TaskStateEntity, Long> {

    Optional<TaskStateEntity> findTaskStateEntityByProjectIdAndNameContainsIgnoreCase(Long projectId, String taskStateName);

}
