package ru.itis.task.tracker.api.factories;

import org.springframework.stereotype.Component;
import ru.itis.task.tracker.api.dto.ProjectDto;
import ru.itis.task.tracker.models.ProjectEntity;

@Component
public class ProjectDtoFactory {

    public ProjectDto makeProjectDto(ProjectEntity entity) {
        return ProjectDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .createdAt(entity.getCreatedAt())
                .build();
    }
}
