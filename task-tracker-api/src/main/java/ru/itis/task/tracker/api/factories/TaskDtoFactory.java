package ru.itis.task.tracker.api.factories;

import org.springframework.stereotype.Component;
import ru.itis.task.tracker.api.dto.ProjectDto;
import ru.itis.task.tracker.api.dto.TaskDto;
import ru.itis.task.tracker.models.ProjectEntity;
import ru.itis.task.tracker.models.TaskEntity;

@Component
public class TaskDtoFactory {

    public TaskDto makeTaskDto(TaskEntity entity) {
        return TaskDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .createdAt(entity.getCreatedAt())
                .description(entity.getDescription())
                .build();
    }
}
