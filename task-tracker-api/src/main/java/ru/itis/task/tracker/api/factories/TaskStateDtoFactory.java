package ru.itis.task.tracker.api.factories;

import org.springframework.stereotype.Component;
import ru.itis.task.tracker.api.dto.TaskStateDto;
import ru.itis.task.tracker.models.TaskStateEntity;

import java.util.stream.Collectors;

@Component
public class TaskStateDtoFactory {

    private final TaskDtoFactory taskDtoFactory;

    public TaskStateDtoFactory(TaskDtoFactory taskDtoFactory) {
        this.taskDtoFactory = taskDtoFactory;
    }

    public TaskStateDto makeTaskStateDto(TaskStateEntity entity) {
        return TaskStateDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .createdAt(entity.getCreatedAt())
                .leftTaskStateId(entity.getLeftTaskState().map(TaskStateEntity::getId).orElse(null))
                .rightTaskStateId(entity.getRightTaskState().map(TaskStateEntity::getId).orElse(null))
                .tasks(entity
                        .getTasks()
                        .stream()
                        .map(taskDtoFactory::makeTaskDto)
                        .collect(Collectors.toList()))
                .build();
    }
}
