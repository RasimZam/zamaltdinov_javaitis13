package ru.itis.task.tracker.api.controllers.helpers;

import org.springframework.stereotype.Component;
import ru.itis.task.tracker.exceptions.NotFoundException;
import ru.itis.task.tracker.models.ProjectEntity;
import ru.itis.task.tracker.repositories.ProjectRepository;

import javax.transaction.Transactional;

@Transactional
@Component
public class ControllerHelper {

    private final ProjectRepository projectRepository;

    public ControllerHelper(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public ProjectEntity getProjectOrThrowException(Long projectId) {
        return projectRepository
                .findById(projectId)
                .orElseThrow(() ->
                        new NotFoundException(String.format("Project with \"%s\" doesn't exist", projectId)));
    }
}
