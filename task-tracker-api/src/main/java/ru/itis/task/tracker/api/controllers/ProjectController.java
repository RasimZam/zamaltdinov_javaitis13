package ru.itis.task.tracker.api.controllers;

import org.springframework.web.bind.annotation.*;
import ru.itis.task.tracker.api.controllers.helpers.ControllerHelper;
import ru.itis.task.tracker.api.dto.AckDto;
import ru.itis.task.tracker.api.dto.ProjectDto;
import ru.itis.task.tracker.api.factories.ProjectDtoFactory;
import ru.itis.task.tracker.exceptions.BadRequestException;
import ru.itis.task.tracker.exceptions.NotFoundException;
import ru.itis.task.tracker.models.ProjectEntity;
import ru.itis.task.tracker.repositories.ProjectRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Transactional
@RestController
public class ProjectController {

    public static final String GET_PROJECT = "/api/projects";
    public static final String CREATE_PROJECT = "/api/projects";
    public static final String EDIT_PROJECT = "/api/projects/{project_id}";
    public static final String DELETE_PROJECT = "/api/projects/{project_id}";
    public static final String CREATE_OR_UPDATE_PROJECT = "/api/projects";

    private final ProjectDtoFactory projectDtoFactory;
    private final ProjectRepository projectRepository;
    private final ControllerHelper controllerHelper;

    public ProjectController(ProjectDtoFactory projectDtoFactory,
                             ProjectRepository projectRepository,
                             ControllerHelper controllerHelper) {
        this.projectDtoFactory = projectDtoFactory;
        this.projectRepository = projectRepository;
        this.controllerHelper = controllerHelper;
    }

    @GetMapping(GET_PROJECT)
    public List<ProjectDto> getProjects(@RequestParam(value = "prefix_name", required = false)
                                         Optional<String> optionalPrefixName) {
        optionalPrefixName = optionalPrefixName.filter(prefixName -> !prefixName.trim().isEmpty());
        Stream<ProjectEntity> projectStream = optionalPrefixName
                .map(projectRepository::streamAllByNameStartsWithIgnoreCase)
                .orElseGet(projectRepository::streamAllBy);
        return projectStream
                .map(projectDtoFactory::makeProjectDto)
                .collect(Collectors.toList());
    }

    @PostMapping(value = "/api/projects")
    public ProjectDto createProject(@RequestParam String projectName) {
        if (projectName.trim().isEmpty()) {
            throw new BadRequestException("Name can't be empty");
        }
        projectRepository
                .findByName(projectName)
                .ifPresent(project -> {
                    throw new BadRequestException(String.format("Project \"%s\" already exists", projectName));
                });
        ProjectEntity project = projectRepository.save(
                ProjectEntity.builder()
                        .name(projectName)
                        .build()
        );
        return projectDtoFactory.makeProjectDto(project);
    }
//
//    @PatchMapping(EDIT_PROJECT)
//    private ProjectDto editProject(@RequestParam String projectName, @PathVariable("project_id") Long projectId) {
//        if (projectName.trim().isEmpty()) {
//            throw new BadRequestException("Name can't be empty");
//        }
//        ProjectEntity project = getProjectOrThrowException(projectId);
//        projectRepository
//                .findByName(projectName)
//                .filter(anotherProject -> !Objects.equals(anotherProject.getId(), projectId))
//                .ifPresent(anotherProject -> {
//                    throw new BadRequestException(String.format("Project \"%s\" already exists", projectName));
//                });
//        project.setName(projectName);
//        project = projectRepository.saveAndFlush(project);
//        return projectDtoFactory.makeProjectDto(project);
//    }

    @PutMapping(CREATE_OR_UPDATE_PROJECT)
    public ProjectDto createOrUpdateProject(
            @RequestParam(value = "project_id", required = false) Optional<Long> optionalProjectId,
            @RequestParam(value = "project_name", required = false) Optional<String> optionalProjectName) {

        optionalProjectName = optionalProjectName.filter(projectName -> !projectName.trim().isEmpty());
        boolean isCreate = !optionalProjectId.isPresent();
        if (isCreate && !optionalProjectName.isPresent()) {
            throw new BadRequestException("Project name can't be empty");
        }
        final ProjectEntity project = optionalProjectId
                .map(controllerHelper::getProjectOrThrowException)
                .orElseGet(() -> ProjectEntity.builder().build());

        optionalProjectName
                .ifPresent(projectName -> {
                    projectRepository
                            .findByName(projectName)
                            .filter(anotherProject -> !Objects.equals(anotherProject.getId(), project.getId()))
                            .ifPresent(anotherProject -> {
                                throw new BadRequestException(String.format("Project \"%s\" already exists", projectName));
                            });
                    project.setName(projectName);
                });
        final ProjectEntity saveProject = projectRepository.saveAndFlush(project);
        return projectDtoFactory.makeProjectDto(saveProject);
    }

    @DeleteMapping(DELETE_PROJECT)
    public AckDto deleteProject(@PathVariable("project_id") Long projectId) {
        controllerHelper.getProjectOrThrowException(projectId);
        projectRepository.deleteById(projectId);
        return AckDto.makeDefault(true);
    }
}
