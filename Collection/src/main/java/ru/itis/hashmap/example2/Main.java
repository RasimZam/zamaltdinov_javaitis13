package ru.itis.hashmap.example2;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("Sim", "Sim");
        map.put("Arbus", "Arbus");
        map.put("Cat", "Cat");
        map.put("Dog", "Dog");
        map.put("Food", "Food");

        printKeys(map);
    }

    private static void printKeys(Map<String, String> map) {
        String[] text = new String[map.size()];
        int i = 0;
        for (Map.Entry<String, String> pair: map.entrySet()) {
            text[i] = pair.getKey();
            System.out.println(text[i]);
            i++;
        }
    }
}
