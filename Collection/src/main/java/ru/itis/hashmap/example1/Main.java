package ru.itis.hashmap.example1;

import ru.itis.hashmap.example1.model.Car;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        String[] cars = {"toyota", "ford", "mazda", "lada", "vw"};
        Map<String, Car> mapCars = addCarToMap(cars);

        for (Map.Entry<String, Car> pair: mapCars.entrySet()) {
            System.out.println(pair.getKey() + " - " + pair.getValue());
        }
    }

    public static Map<String, Car> addCarToMap(String[] cars) {
        Map<String,Car> mapCar = new HashMap<>();
        for (String car: cars) {
            mapCar.put(car, new Car(car));
        }
        return mapCar;
    }
}
