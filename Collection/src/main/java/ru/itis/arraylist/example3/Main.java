package ru.itis.arraylist.example3;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> items = WorkOnArrayList.getList();
        items.add(0, "blackberry");
        items.add(5, "melon");
        System.out.println(items);
    }
}
