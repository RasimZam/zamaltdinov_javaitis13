package ru.itis.arraylist.example4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class WorkOnArrayList {

    public static List getList() {
        String[] array = new String[]{"apple", "pineapple", "banana", "cherry", "grape", "orange"};
        List items = addItemList(array);
        Collections.sort(items, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.toString().compareTo(o2.toString());
            }
        });
        return items;
    }

    static List addItemList(String[] items) {
        List<String> listItems = new ArrayList<>();
        for (int i = 0; i < items.length; i++) {
            listItems.add(items[i]);
        }
        return listItems;
    }
}
