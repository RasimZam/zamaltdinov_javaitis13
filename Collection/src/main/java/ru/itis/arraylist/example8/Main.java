package ru.itis.arraylist.example8;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> listNew = new ArrayList<>();
        listNew.add("Red");
        listNew.add("Green");
        listNew.add("Black");
        listNew.add("White");
        listNew.add("Pink");
        System.out.println("listNew " + listNew);
        List<String> listOld = new ArrayList<>();
        listOld.add("Red");
        listOld.add("Green");
        listOld.add("Black");
        listOld.add("Pink");
        System.out.println("listOld " + listOld);
        List<String> resultList = new ArrayList<>();
        resultList.addAll(listNew);
        resultList.addAll(listOld);

        System.out.println(resultList);
    }
}
