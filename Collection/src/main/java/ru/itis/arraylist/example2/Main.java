package ru.itis.arraylist.example2;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> items = WorkOnArrayList.getList();

        for (String element: items) {
            System.out.println(element);
        }

    }
}
