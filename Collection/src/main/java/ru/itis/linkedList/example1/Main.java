package ru.itis.linkedList.example1;

import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> linkList = new LinkedList<>();
        linkList.add("red");
        linkList.add("green");
        linkList.add("black");
        linkList.add("yellow");
        System.out.println(linkList);
    }
}
