package ru.itis.linkedList.example4;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Main {
    public static void main(String[] args) {
        List<String> linkList = new LinkedList<>();
        linkList.add("red");
        linkList.add("green");
        linkList.add("black");
        linkList.add("yellow");
        System.out.println("Original list: " + linkList);

        ListIterator<String> iterator = linkList.listIterator(linkList.size());

        while (iterator.hasPrevious()) {
            System.out.println(iterator.previous());
        }
    }
}
