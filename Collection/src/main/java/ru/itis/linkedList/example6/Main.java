package ru.itis.linkedList.example6;

import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        LinkedList<String> linkList = new LinkedList<>();
        linkList.add("red");
        linkList.add("green");
        linkList.add("black");
        System.out.println("Original list: " + linkList);
        linkList.addFirst("white");
        linkList.addLast("Pink");
        System.out.println("Modify list: " + linkList);

    }
}
