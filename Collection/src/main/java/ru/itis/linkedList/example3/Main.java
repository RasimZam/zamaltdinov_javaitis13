package ru.itis.linkedList.example3;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> linkList = new LinkedList<>();
        linkList.add("red");
        linkList.add("green");
        linkList.add("black");
        linkList.add("yellow");
        Iterator iterator = linkList.listIterator(1);

        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
