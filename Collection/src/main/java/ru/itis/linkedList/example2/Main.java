package ru.itis.linkedList.example2;

import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> linkList = new LinkedList<>();
        linkList.add("red");
        linkList.add("green");
        linkList.add("black");
        linkList.add("yellow");
        for (String element: linkList) {
            System.out.println(element);
        }
    }
}
