package ru.itis.webclientdemo.exception;

import org.springframework.core.NestedRuntimeException;

public class ApiWebClientException extends NestedRuntimeException {
    public ApiWebClientException(String msg) {
        super(msg);
    }
}
