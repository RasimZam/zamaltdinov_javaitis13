package ru.itis.webclientdemo.client;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import ru.itis.webclientdemo.model.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.tcp.TcpClient;
import ru.itis.webclientdemo.exception.ApiWebClientException;
import ru.itis.webclientdemo.model.ClientRequest;
import ru.itis.webclientdemo.properties.ClientProperties;

import java.time.Duration;

@Slf4j
@Component
public class ApiWebClient {

    private final ClientProperties.ClientConfig clientProperties;

    public ApiWebClient(ClientProperties clientProperties) {
        this.clientProperties = clientProperties.getConfigName("demoService");
    }

    public ClientResponse createClient(ClientRequest request) {
        TcpClient tcpClient = TcpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 3000)
                .doOnConnected(connection ->
                        connection.addHandlerLast(new ReadTimeoutHandler(clientProperties.getReadTimeout()))
                                .addHandlerLast(new WriteTimeoutHandler(clientProperties.getWriteTimeout())));

        WebClient webClient = WebClient.builder()
                .baseUrl(clientProperties.getUrl())
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .clientConnector(new ReactorClientHttpConnector(HttpClient.from(tcpClient)))
                .build();

        ClientResponse response = webClient
                .post()
                .uri("/accounts")
                .body(Mono.just(request), ClientRequest.class)
                .retrieve()
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("Error endpoint with status code {}", clientResponse.statusCode());
                    throw new ApiWebClientException("HTTP status 500 error");
                })
                .bodyToMono(ClientResponse.class)
                .block();

        return response;
    }

    public ClientResponse createClientWithDuration(ClientRequest request) {
        WebClient webClient = WebClient.builder()
                .baseUrl(clientProperties.getUrl())
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .build();

        ClientResponse response = webClient
                .post()
                .uri("/accounts")
                .body(Mono.just(request), ClientRequest.class)
                .retrieve()
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("Error endpoint with status code {}", clientResponse.statusCode());
                    throw new ApiWebClientException("HTTP status 500 error");
                })
                .bodyToMono(ClientResponse.class)
                .timeout(Duration.ofSeconds(3))
                .block();

        return response;
    }

}
