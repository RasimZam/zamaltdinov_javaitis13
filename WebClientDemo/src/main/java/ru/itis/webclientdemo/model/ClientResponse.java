package ru.itis.webclientdemo.model;

import lombok.Data;

@Data
public class ClientResponse {
    private String name;
    private String email;
}
