package ru.itis.webclientdemo.model;

import lombok.Data;

@Data
public class ClientRequest {
    private String name;
    private String email;
}
