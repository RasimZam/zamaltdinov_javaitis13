package ru.itis.webclientdemo.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ConfigurationProperties(prefix = "client")
@Data
public class ClientProperties {

    private Map<String, ClientConfig> configs;

    public ClientConfig getConfigName(String name) {
        return configs.get(name);
    }

    @Data
    public static class ClientConfig {
        private String url;
        private Integer readTimeout;
        private Integer writeTimeout;
    }
}
