package ru.itis.webclientdemo.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import ru.itis.webclientdemo.model.ClientResponse;
import ru.itis.webclientdemo.client.ApiWebClient;
import ru.itis.webclientdemo.model.ClientRequest;

import java.util.concurrent.TimeUnit;

public class ClientServiceTest {

    @Autowired
    private ApiWebClient webClient;

    private ClientAndServer mockServer;

    @BeforeEach
    void startServer() {
        mockServer = ClientAndServer.startClientAndServer(8899);
    }

    @AfterEach
    void stopServer() {
        mockServer.stop();
    }

    void createShouldReturnResponse() {
        mockServer.when(HttpRequest.request().withMethod("POST")
                .withPath("/accounts"))
                .respond(HttpResponse.response()
                        .withBody("{ \"name\": \"Frank\", \"email\": \"frank@mail.com\"}")
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withDelay(TimeUnit.MILLISECONDS, 1000));

        ClientRequest clientRequest = new ClientRequest();
        clientRequest.setName("Frank");
        clientRequest.setName("frank@mail.com");

        ru.itis.webclientdemo.model.ClientResponse response = webClient.createClient(clientRequest);

        Assertions.assertNotNull(response);
        Assertions.assertEquals("Frank", response.g);
    }

}
