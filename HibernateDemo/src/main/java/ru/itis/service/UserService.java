package ru.itis.service;

import org.hibernate.Session;
import org.hibernate.query.Query;
import ru.itis.entity.User;
import ru.itis.util.HibernateUtil;

import java.util.List;

public interface UserService {

    List<User> findAll();
    List<User> findAll(String email);
    User get(Long id);
    void update(User obj);
    void delete(Long id);
    void add(User obj);
    User getByEmail(String email);
}
