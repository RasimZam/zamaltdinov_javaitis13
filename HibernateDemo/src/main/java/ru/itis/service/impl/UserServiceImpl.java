package ru.itis.service.impl;

import ru.itis.dao.interfaces.objects.UserDAO;
import ru.itis.entity.User;
import ru.itis.service.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {

    private final UserDAO userDaoImpl;

    public UserServiceImpl(UserDAO userDaoImpl) {
        this.userDaoImpl = userDaoImpl;
    }

    @Override
    public List<User> findAll() {
        return userDaoImpl.findAll();
    }

    @Override
    public List<User> findAll(String email) {
        return userDaoImpl.findAll(email);
    }

    @Override
    public User get(Long id) {
        return userDaoImpl.get(id);
    }

    @Override
    public void update(User obj) {
        userDaoImpl.update(obj);
    }

    @Override
    public void delete(Long id) {
        userDaoImpl.delete(id);
    }

    @Override
    public void add(User obj) {
        userDaoImpl.add(obj);
    }

    @Override
    public User getByEmail(String email) {
        return userDaoImpl.getByEmail(email);
    }
}
