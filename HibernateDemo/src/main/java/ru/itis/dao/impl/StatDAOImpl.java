package ru.itis.dao.impl;

import org.hibernate.Session;
import org.hibernate.query.Query;
import ru.itis.dao.interfaces.objects.StatDAO;
import ru.itis.entity.Stat;
import ru.itis.entity.User;
import ru.itis.util.HibernateUtil;


public class StatDAOImpl implements StatDAO {

    @Override
    public Stat getByUser(String email) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query<Stat> query = session.createQuery("FROM Stat where user.email like :email");
        query.setParameter("email", "%" + email + "%");
        Stat stat = query.uniqueResult();
        session.close();
        return stat;
    }

    @Override
    public Stat getByUser(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query<Stat> query = session.createQuery("FROM Stat where user.email like :email");
        query.setParameter("email", "%" + user.getEmail() + "%");
        Stat stat = query.uniqueResult();
        session.close();
        return stat;
    }
}
