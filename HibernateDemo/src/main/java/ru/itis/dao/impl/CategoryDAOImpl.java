package ru.itis.dao.impl;

import org.hibernate.Session;
import org.hibernate.query.Query;
import ru.itis.dao.interfaces.objects.CategoryDAO;
import ru.itis.entity.Category;
import ru.itis.util.HibernateUtil;

import java.util.List;

public class CategoryDAOImpl implements CategoryDAO {

    @Override
    public Category get(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Category priority = session.get(Category.class, id);
        session.close();
        return priority;
    }

    @Override
    public void update(Category obj) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(obj);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void delete(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Category category = new Category();
        category.setId(id);
        session.delete(category);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void add(Category obj) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(obj);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public List<Category> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query<Category> query = session.createQuery("FROM Category");
        List<Category> list = query.getResultList();
        session.close();
        return list;
    }

    @Override
    public List<Category> findAll(String email) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query<Category> query = session.createQuery("FROM Category p WHERE p.user.email=:email");
        query.setParameter("email", "%" + email + "%");
        List<Category> list = query.getResultList();
        session.close();
        return list;
    }
}
