package ru.itis.dao.interfaces.objects;

import ru.itis.dao.interfaces.CommonDAO;
import ru.itis.dao.interfaces.FindDAO;
import ru.itis.entity.Priority;

public interface PriorityDAO extends CommonDAO<Priority>, FindDAO<Priority> {

}
