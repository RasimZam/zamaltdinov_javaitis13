package ru.itis.dao.interfaces.objects;

import ru.itis.dao.interfaces.CommonDAO;
import ru.itis.dao.interfaces.FindDAO;
import ru.itis.entity.Task;

import java.util.List;

public interface TaskDAO extends CommonDAO<Task>, FindDAO<Task> {

    List<Task> find(boolean completed, String email);
}
