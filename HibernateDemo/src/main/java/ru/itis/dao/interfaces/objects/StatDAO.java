package ru.itis.dao.interfaces.objects;

import ru.itis.entity.Stat;
import ru.itis.entity.User;

public interface StatDAO {

    Stat getByUser(String email);
    Stat getByUser(User user);
}
