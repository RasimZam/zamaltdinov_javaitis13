package ru.itis.dao.interfaces.objects;

import ru.itis.dao.interfaces.CommonDAO;
import ru.itis.dao.interfaces.FindDAO;
import ru.itis.entity.Category;

public interface CategoryDAO extends CommonDAO<Category>, FindDAO<Category> {

}
