package ru.itis.hqladditional;

import lombok.extern.log4j.Log4j2;
import ru.itis.dao.impl.ActivityDAOImpl;
import ru.itis.dao.impl.UserDAOImpl;
import ru.itis.entity.Activity;
import ru.itis.entity.User;
import ru.itis.util.HibernateUtil;


@Log4j2
public class Main {
    public static void main(String[] args) {
        log.info("Hibernate tutorial started");

//        Session session = HibernateUtil.getSessionFactory().openSession();
//        User user = session.get(User.class, 10025L);
//        log.info(user);
//        session.close();
//
//        session = HibernateUtil.getSessionFactory().openSession();
//        User user1 = session.get(User.class, 10025L); //должен получить из кэша L2C
//        log.info(user1);

//        Query query = session.createQuery("from User");
//        query.setMaxResults(2);
//        List<User> users = query.getResultList();
//        log.info(users.get(0).getRoles());

//        Task t1 = session.get(Task.class,124039L);

//        session.close();
        UserDAOImpl userDAO = new UserDAOImpl();

        User user = new User();
        user.setUserpassword("testuser");
        user.setUsername("testuser");
        user.setEmail("testuser@gmail.com");
        userDAO.add(user);

        ActivityDAOImpl activityDAO = new ActivityDAOImpl();
        Activity activity = activityDAO.getByUser(user);
        activity.setActivated(true);
        activityDAO.update(activity);


        HibernateUtil.close();
    }
}
