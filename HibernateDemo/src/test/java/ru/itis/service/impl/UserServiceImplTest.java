package ru.itis.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.itis.dao.impl.UserDAOImpl;
import ru.itis.entity.User;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserDAOImpl userDAOImpl;

    private UserServiceImpl userServiceImpl;
    private User user;

    @BeforeEach
    void setUp() {
        userServiceImpl = new UserServiceImpl(userDAOImpl);
    }


    @Test
    @DisplayName("Test should pass when find all users")
    void findAll() {
    }

    @Test
    void testFindAll() {
    }

    @Test
    void get() {
    }

    @Test
    void update() {
    }

    @Test
    void delete() {
    }

    @Test
    void add() {
    }

    @Test
    @DisplayName("Should get user by email")
    void getByEmail() {
       String email = "ivanov@gmail.com";

    }
}