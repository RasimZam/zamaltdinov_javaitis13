package ru.itis.structural.decorator.sample1;

public interface Developer {
    public String makeJob();
}
