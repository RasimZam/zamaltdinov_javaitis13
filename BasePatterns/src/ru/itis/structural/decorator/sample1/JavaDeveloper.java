package ru.itis.structural.decorator.sample1;

public class JavaDeveloper implements Developer {
    @Override
    public String makeJob() {
        return "Write Java code.";
    }
}
