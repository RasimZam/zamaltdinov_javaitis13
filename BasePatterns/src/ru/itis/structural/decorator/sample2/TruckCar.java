package ru.itis.structural.decorator.sample2;

public class TruckCar implements Car {

    private Car car;

    public TruckCar(Car car) {
        this.car = car;
    }

    @Override
    public int getSpeed() {
        return this.car.getSpeed();
    }

    @Override
    public int getBaggageWeight() {
        return this.car.getBaggageWeight() + 100;
    }
}
