package ru.itis.structural.decorator.sample2;

public interface Car {
    int getSpeed();
    int getBaggageWeight();

}
