package ru.itis.creational.abstractFactory;

import java.time.LocalDate;

public class DocumentProducerWithDateImpl implements DocumentProducer{

    private static class ActImpl implements Act {

        private LocalDate date;

        public ActImpl(LocalDate date) {
            this.date = date;
        }

        @Override
        public String getDescription() {
            return "Act[date = " + date + " ]";
        }
    }

    private static class StatementImpl implements Statement {
        private LocalDate date;

        public StatementImpl(LocalDate date) {
            this.date = date;
        }

        @Override
        public String getDescription() {
            return "Statement[date = " + date + " ]";
        }
    }

    @Override
    public Act createAct() {
        return new ActImpl(LocalDate.now());
    }

    @Override
    public Statement createStatement() {
        return new StatementImpl(LocalDate.now());
    }
}
