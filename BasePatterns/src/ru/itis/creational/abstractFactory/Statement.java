package ru.itis.creational.abstractFactory;

public interface Statement {
    String getDescription();
}
