package ru.itis.creational.abstractFactory;

public interface DocumentProducer {
    Act createAct();
    Statement createStatement();
}
