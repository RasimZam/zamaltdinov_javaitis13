package ru.itis.creational.abstractFactory;

public interface Act {
    String getDescription();
}
