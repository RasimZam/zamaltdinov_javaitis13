package ru.itis.creational.builder;

public class Main {
    public static void main(String[] args) {
        User user = User.builder()
                .firstName("Максим")
                .lastName("Сидоров")
                .build();

        System.out.println(user);

    }
}
