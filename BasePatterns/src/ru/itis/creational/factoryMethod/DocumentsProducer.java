package ru.itis.creational.factoryMethod;

public interface DocumentsProducer {

    Document create(Information information);
}
