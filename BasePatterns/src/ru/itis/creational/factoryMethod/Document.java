package ru.itis.creational.factoryMethod;

public interface Document {
    String getType();
    String getFrom();
}
