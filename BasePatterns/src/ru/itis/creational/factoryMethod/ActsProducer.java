package ru.itis.creational.factoryMethod;

public class ActsProducer implements DocumentsProducer{

    @Override
    public Document create(Information information) {
        FromAndDateTime fromAndDateTime = (FromAndDateTime)information;
        return new Act(fromAndDateTime.getDateTime(), fromAndDateTime.getFrom());
    }
}
