package ru.itis.purchase.order.service.async.service;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.itis.purchase.order.service.async.dto.Order;

@Service
@Slf4j
public class PaymentService {

    @SneakyThrows
    public void processPayment(Order order) {
      log.info("initiate payment for order " + order.getProductId());
      Thread.sleep(2000L);
      log.info("completed payment for order " + order.getProductId());
    }
}
