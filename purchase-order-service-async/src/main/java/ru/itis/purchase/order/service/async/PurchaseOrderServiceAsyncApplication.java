package ru.itis.purchase.order.service.async;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PurchaseOrderServiceAsyncApplication {

    public static void main(String[] args) {
        SpringApplication.run(PurchaseOrderServiceAsyncApplication.class, args);
    }

}
