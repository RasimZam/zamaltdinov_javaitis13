package ru.itis.purchase.order.service.async.controller;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.purchase.order.service.async.dto.Order;
import ru.itis.purchase.order.service.async.service.OrderFulfilmentService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/orders")
public class OrderFulfilmentController {

    private final OrderFulfilmentService service;

    @SneakyThrows
    @PostMapping
    public ResponseEntity<Order> processOrder(@RequestBody Order order) {
        System.out.println("main thread " + Thread.currentThread().getName());
        service.processOrder(order);
        service.notifyUser(order);
        service.assignVendor(order);
        service.packaging(order);
        service.assignDeliveryPartner(order);
        service.assignTrailerAndDispatch(order);
        return ResponseEntity.ok(order);
    }
}
