package ru.itis.example2;

public enum Note {
    MIDDLE_C,
    C_SHARP,
    B_FLAT;
}
