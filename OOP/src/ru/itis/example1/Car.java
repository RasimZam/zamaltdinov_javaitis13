package ru.itis.example1;

abstract class Car implements Vehicle {

    private String name;
    private int speed;

    public Car(String name, int speed) {
        this.name = name;
        this.speed = speed;
    }

    @Override
    public void gas(){
        System.out.println("Car goes");
    }

    @Override
    public void brake() {
        System.out.println(" Car has stopped ");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
