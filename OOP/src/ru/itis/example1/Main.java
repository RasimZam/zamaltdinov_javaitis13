package ru.itis.example1;

public class Main {
    public static void main(String[] args) {
        Car ferrari = new FerrariCar("ferrari", 300);
        ferrari.gas();
        ferrari.brake();
    }
}
