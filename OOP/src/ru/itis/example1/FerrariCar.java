package ru.itis.example1;

public class FerrariCar extends Car {

    public FerrariCar(String name, int speed) {
        super(name, speed);
    }

    @Override
    public void gas(){
        System.out.println("Car " + getName() + " speed " + getSpeed());
    }

    @Override
    public void brake() {
        System.out.println("Car " + getName() +  " has stopped");
    }


}
