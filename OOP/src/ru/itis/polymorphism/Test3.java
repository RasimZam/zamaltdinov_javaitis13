package ru.itis.polymorphism;

public class Test3 {
    public static void main(String[] args) {
        Employee1 teacher1 = new Teacher1();
        teacher1.eat();
    }
}

class Employee1 {
    double salary = 100;
    String name = "Kolya";
    int age;
    int experience;

    void eat() {
        System.out.println("eating worker");
    }

    void sleep() {
        System.out.println("sleep");
    }
}

class Teacher1 extends Employee1 {
    int numberOfPupils;

    void eat() {
        System.out.println("eating teacher");
    }
    void uchit() {
        System.out.println("uchit");
    }

}
