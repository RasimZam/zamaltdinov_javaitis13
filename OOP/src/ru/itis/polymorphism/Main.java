package ru.itis.polymorphism;

public class Main {
    public static void main(String[] args) {
        Employee emp1 = new Doctor();
        System.out.println(emp1.salary);
        System.out.println(emp1.name);
        System.out.println(emp1.age);
        emp1.sleep();
        emp1.eat();

        Employee emp2 = new Teacher();
        Employee emp3 = new Driver();
    }
}
