package ru.itis.polymorphism;

public class Employee {
    double salary = 100;
    String name = "Kolya";
    int age;
    int experience;

    void eat() {
        System.out.println("eating");
    }

    void sleep() {
        System.out.println("sleep");
    }
}
