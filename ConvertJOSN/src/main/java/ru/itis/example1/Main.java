package ru.itis.example1;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;

public class Main {
    public static void main(String[] args) throws IOException {
        Employee employee = new Employee();
        employee.setId(100L);
        employee.setName("Elon");
        employee.setAge(44);
        employee.setDateOfJoining(new Date());
        employee.setLocation(new Location(101L,"California"));

        // create object mapper class object
        ObjectMapper mapper = new ObjectMapper();
        // print string in JSON format
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        // print json String
        String empJson = mapper.writeValueAsString(employee);
        System.out.println(empJson);

        // convert JSON String into Employee object
        // use mapper object
        Employee emp = mapper.readValue(empJson.getBytes(StandardCharsets.UTF_8), Employee.class);
        // print values from emp object
        System.out.println();
        System.out.println(emp.getId());
        System.out.println(emp.getName());
        System.out.println(emp.getAge());
        System.out.println(emp.getDateOfJoining());
        System.out.println(emp.getLocation());
    }
}
