package ru.itis.example1;

import java.util.Date;

public class Employee {
    private Long id;
    private String name;
    private Integer age;
    private Date dateOfJoining;
    private Location location;

    public Employee() {
        super();
    }

    public Employee(Long id, String name, Integer age, Date dateOfJoining, Location location) {
        super();
        this.id = id;
        this.name = name;
        this.age = age;
        this.dateOfJoining = dateOfJoining;
        this.location = location;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    public Date getDateOfJoining() {
        return dateOfJoining;
    }

    public void setDateOfJoining(Date dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Employee { " +
                "id= " + id +
                ", name=' " + name + '\'' +
                ", dateOfJoining= " + dateOfJoining +
                ", location= " + location +
                '}';
    }
}
