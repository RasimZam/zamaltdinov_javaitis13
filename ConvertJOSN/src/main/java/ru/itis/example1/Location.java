package ru.itis.example1;

public class Location {
    private Long id;
    private String name;

    public Location() {
        super();
    }

    public Location(Long id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Location {" +
                "id= " + id +
                ", name=' " + name + '\'' +
                '}';
    }
}
