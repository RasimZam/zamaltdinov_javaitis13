package ru.itis.example2;

public class UserBean {
    public int id;
    private String firstName;

    public UserBean(int id, String firstName) {
        this.id = id;
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }
}
