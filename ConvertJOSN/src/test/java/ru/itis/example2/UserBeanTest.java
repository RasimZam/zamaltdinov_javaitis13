package ru.itis.example2;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

public class UserBeanTest {

    @Test
    public void testUserBeanForModifiers() throws JsonProcessingException {
        UserBean user = new UserBean(1, "John");

        String resultJSON = new ObjectMapper().writeValueAsString(user);
        System.out.println(resultJSON);
        assertThat(resultJSON, containsString("id"));
        assertThat(resultJSON, containsString("firstName"));
    }
}