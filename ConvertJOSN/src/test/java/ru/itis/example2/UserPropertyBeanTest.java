package ru.itis.example2;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

public class UserPropertyBeanTest {

    @Test
    public void testUserPropertyBean() throws JsonProcessingException {
        UserPropertyBean user = new UserPropertyBean(1, "Michal");

        String jsonResult = new ObjectMapper().writeValueAsString(user);
        System.out.println(jsonResult);

        assertThat(jsonResult, containsString("id"));
        assertThat(jsonResult, containsString("name"));
    }

}