package ru.itis.example2;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

public class UserGetterBeanTest {

    @Test
    public void testUserGetterBean() throws JsonProcessingException {
        UserGetterBean userGetterBean = new UserGetterBean(1, "Elon");

        String jsonResult = new ObjectMapper().writeValueAsString(userGetterBean);
        System.out.println(jsonResult);

        assertThat(jsonResult, containsString("id"));
        assertThat(jsonResult, containsString("name"));
    }

}