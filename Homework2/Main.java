public class Main {
    public static void main(String[] args) {
        String carNumber = "А010HC 116 RUS";
        String str = getNext(carNumber);
        System.out.println(str);


    }

    public static String getNext( String carNumber) {
        String oldNumber = carNumber;
        String str = carNumber.substring(1,4);
        int parseNumber = Integer.parseInt(str);
        parseNumber++;

        if (parseNumber == 999) {
            parseNumber = 000;
        }
        String str1 = oldNumber.substring(0,1);
        String str2 = oldNumber.substring(4,14);

        if (parseNumber < 100) {
            return str1 + "0" + parseNumber + str2;
        }

        return str1 + parseNumber + str2;
    }
}
