package semaphore;

import java.util.concurrent.Semaphore;

public class Person extends Thread{

    private String name;
    private Semaphore callBox;

    public Person(String name, Semaphore callBox) {
        this.name = name;
        this.callBox = callBox;
        this.start();
    }

    public void run() {
        System.out.println(name + " ждет...");
        try {
            callBox.acquire();
            System.out.println(name + " пользуется телефоном");
            sleep(2000);
            System.out.println(name + " завершил(а) звонок");
        } catch (InterruptedException e) {

        } finally {
            callBox.release();
        }
    }
}
