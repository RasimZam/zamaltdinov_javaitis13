package callable;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableFactorial {

    static int factorialResult;

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Factorial factorial = new Factorial(5);
        //для Callable вызываем метод submit и executorService возращает нам значение и
        // этот результат хранится в объекте Future
        Future<Integer> future = executorService.submit(factorial);
        //получаем значение из нашего объекта Future
        try {
            factorialResult = future.get();
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {
            System.out.println(e.getCause());
        } finally {
            executorService.shutdown();
        }
        System.out.println(factorialResult);

    }
}
