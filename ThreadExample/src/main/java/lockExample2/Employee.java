package lockExample2;

import java.util.concurrent.locks.Lock;

public class Employee extends Thread {

    private String name;
    private Lock lock;

    public Employee(String name, Lock lock) {
        this.name = name;
        this.lock = lock;
        this.start();
    }

    public  void run() {

        try {
            System.out.println(name + " ждет ...");
            lock.lock();
            System.out.println(name + " пользуется бакоматом");
            Thread.sleep(2000);
            System.out.println(name + " завершил(а) свои дела");
        } catch (InterruptedException e) {

        } finally {
            lock.unlock();
        }
    }

}
