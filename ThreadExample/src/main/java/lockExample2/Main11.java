package lockExample2;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main11 {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        new Employee("Расим", lock);
        new Employee("Олег", lock);
        new Employee("Роман", lock);
        new Employee("Максим", lock);
    }
}
