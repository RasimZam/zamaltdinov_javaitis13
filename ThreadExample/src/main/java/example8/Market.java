package example8;

public class Market {

    private int goodCount = 0;

    public synchronized void getGood() {
        while (goodCount < 1) {
            try {
                wait();
            } catch (InterruptedException e) {

            }
        }
        goodCount--;
        System.out.println("Потребитель купил 1 товар");
        System.out.println("Количество товара в магазине = " + goodCount);
        notify();
    }

    public synchronized void putGood() {
        while(goodCount >= 5) {
            try {
                wait();
            } catch (InterruptedException e) {

            }
        }
        goodCount++;
        System.out.println("Производитель добавил 1 товар");
        System.out.println("Количество товара в магазине = " + goodCount);
        notify();
    }
}
