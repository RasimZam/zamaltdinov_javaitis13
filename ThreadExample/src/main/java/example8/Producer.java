package example8;

public class Producer implements Runnable{

    private Market market;

    public Producer(Market market) {
        this.market = market;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            market.putGood();
        }
    }
}
