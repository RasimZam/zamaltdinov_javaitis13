package exchanger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Exchanger;

public class ExchangerEx {
    public static void main(String[] args) {
        Exchanger<Action> exchanger = new Exchanger<>();
        List<Action> firstFriendAction = new ArrayList<>();
        firstFriendAction.add(Action.NOJNICI);
        firstFriendAction.add(Action.KAMEN);
        firstFriendAction.add(Action.NOJNICI);
        List<Action> secondFriendAction = new ArrayList<>();
        secondFriendAction.add(Action.BUMAGA);
        secondFriendAction.add(Action.KAMEN);
        secondFriendAction.add(Action.KAMEN);
        new BestFriend("Rasim",firstFriendAction,exchanger);
        new BestFriend("Roman",secondFriendAction,exchanger);
    }
}
