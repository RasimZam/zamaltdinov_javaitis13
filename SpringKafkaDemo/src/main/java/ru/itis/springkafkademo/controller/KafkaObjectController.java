package ru.itis.springkafkademo.controller;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.springkafkademo.dto.Address;
import ru.itis.springkafkademo.dto.UserDto;

@RestController
public class KafkaObjectController {
    private final KafkaTemplate<Long, UserDto> kafkaTemplate;

    public KafkaObjectController(KafkaTemplate<Long, UserDto> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @PostMapping("/msg")
    public void sendMsg(Long msgId, UserDto msg) {
        msg.setAddress(new Address("RUS", "MSK", "Lenina", 1L, 1L));
        ListenableFuture<SendResult<Long, UserDto>> future = kafkaTemplate.send("msg", msgId, msg);
        future.addCallback(System.out::println, System.err::println);
        kafkaTemplate.flush();
    }
}
