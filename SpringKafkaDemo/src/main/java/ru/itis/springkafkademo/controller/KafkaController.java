//package ru.itis.springkafkademo.controller;
//
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.kafka.support.SendResult;
//import org.springframework.util.concurrent.ListenableFuture;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//public class KafkaController {
//
//   private final KafkaTemplate<Long, String> kafkaTemplate;
//
//    public KafkaController(KafkaTemplate<Long, String> kafkaTemplate) {
//        this.kafkaTemplate = kafkaTemplate;
//    }
//
//    @PostMapping("/msgexample1")
//    public void sendOrder(Long msgId, String msg) {
//        kafkaTemplate.send("msg", msgId, msg);
//    }
//
//    @PostMapping("/msgexample2")
//    public void sendMsg(Long msgId, String msg) {
//        ListenableFuture<SendResult<Long, String>> future = kafkaTemplate.send("msg", msgId, msg);
//        future.addCallback(System.out::println, System.err::println);
//        kafkaTemplate.flush();
//    }
//}
