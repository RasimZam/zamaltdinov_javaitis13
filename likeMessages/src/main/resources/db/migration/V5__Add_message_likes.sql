create table message_likes (
    user_id bigserial not null references usr,
    message_id bigserial not null references message,
    primary key (user_id, message_id)
)