package ru.example.likeMessages.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.example.likeMessages.models.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    User findByActivationCode(String code);
}
