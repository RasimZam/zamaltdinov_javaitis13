package ru.itis.springreactivedemo.persistence.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.itis.springreactivedemo.persistence.entity.CarEntity;

public interface CarRepository extends ReactiveMongoRepository<CarEntity, String> {

    Flux<CarEntity> findByBrand(String brand);

    Mono<CarEntity> findByModel(String model);

    Mono<Boolean> existsByModel(String model);
}
