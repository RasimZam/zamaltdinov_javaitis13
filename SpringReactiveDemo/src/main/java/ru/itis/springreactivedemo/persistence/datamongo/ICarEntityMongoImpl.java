package ru.itis.springreactivedemo.persistence.datamongo;

import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.itis.springreactivedemo.persistence.ICar;
import ru.itis.springreactivedemo.persistence.entity.CarEntity;
import ru.itis.springreactivedemo.persistence.repository.CarRepository;

@Repository
public class ICarEntityMongoImpl implements ICar {
    private final CarRepository carRepository;

    public ICarEntityMongoImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    public Flux<CarEntity> findAll() {
        return carRepository.findAll();
    }

    @Override
    public Flux<CarEntity> findByBrand(String brand) {
        return carRepository.findByBrand(brand);
    }

    @Override
    public Mono<CarEntity> findById(String id) {
        return carRepository.findById(id);
    }

    @Override
    public Mono<CarEntity> findByModel(String model) {
        return carRepository.findByModel(model);
    }

    @Override
    public Mono<CarEntity> save(CarEntity carEntity) {
        return carRepository.save(carEntity);
    }

    @Override
    public Mono<Boolean> existsById(String id) {
        return carRepository.existsById(id);
    }

    @Override
    public Mono<Boolean> existsByModel(String model) {
        return carRepository.existsByModel(model);
    }

    @Override
    public Mono<Void> deleteById(String id) {
        return carRepository.deleteById(id);
    }
}
