package ru.itis.springreactivedemo.persistence;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.itis.springreactivedemo.persistence.entity.CarEntity;

public interface ICar {

    Flux<CarEntity> findAll();
    Flux<CarEntity> findByBrand(String id);
    Mono<CarEntity> findById(String id);
    Mono<CarEntity> findByModel(String id);
    Mono<CarEntity> save(CarEntity carEntity);
    Mono<Boolean> existsById(String id);

    Mono<Boolean> existsByModel(String model);
    Mono<Void> deleteById(String id);

}
