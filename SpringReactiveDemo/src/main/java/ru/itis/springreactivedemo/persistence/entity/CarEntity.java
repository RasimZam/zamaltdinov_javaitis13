package ru.itis.springreactivedemo.persistence.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class CarEntity {
    @Id
    private String id;
    private String brand;
    private String model;
}
