package ru.itis.springreactivedemo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.itis.springreactivedemo.persistence.ICar;
import ru.itis.springreactivedemo.persistence.entity.CarEntity;
import ru.itis.springreactivedemo.service.converter.DtoConverter;
import ru.itis.springreactivedemo.web.dto.CarDto;

import java.util.List;

@Slf4j
@Transactional
@Service
public class CarSimpleService {
    private final ICar iCar;
    private final DtoConverter dtoConverter;

    public CarSimpleService(ICar iCar, DtoConverter dtoConverter) {
        this.iCar = iCar;
        this.dtoConverter = dtoConverter;
    }

    public Flux<CarDto> findAllCars() {
        return iCar.findAll()
                .map(car -> dtoConverter.convert(car, CarDto.class));
    }

    public Mono<List<String>>  findAllCarBrands() {
        return iCar.findAll()
                .mapNotNull(CarEntity::getBrand)
                .collectList()
                .log();
    }

    public Mono<CarDto> findCarById(String id) {
        return iCar.findById(id)
                .switchIfEmpty(Mono.error(new RuntimeException("Not found")))
                .doOnError(ex -> log.error("Error: {}", ex.getMessage()))
                .mapNotNull(car -> dtoConverter.convert(car, CarDto.class));
    }

    public Mono<CarDto> createCar(CarDto carDto) {
        return Mono.just(carDto)
                .filterWhen(car -> iCar.existsByModel(car.getModel())
                        .map(exist -> !exist))
                .switchIfEmpty(Mono.error(new RuntimeException("Already exist")))
                .map(car -> dtoConverter.convert(car, CarEntity.class))
                .flatMap(iCar::save)
                .map(car -> dtoConverter.convert(car, CarDto.class));
    }

    public Mono<Void> deleteCar(String id) {
        return Mono.just(id)
                .filterWhen(iCar::existsById)
                .switchIfEmpty(Mono.error(new RuntimeException("Id not found")))
                .flatMap(iCar::deleteById);
    }
}
