package ru.itis.springreactivedemo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;
import ru.itis.springreactivedemo.persistence.ICar;
import ru.itis.springreactivedemo.persistence.entity.CarEntity;
import ru.itis.springreactivedemo.service.converter.DtoConverter;

@Transactional
@Slf4j
@Service
public class CarService {
    private final ICar iCar;
    private final DtoConverter dtoConverter;

    public CarService(ICar iCar, DtoConverter dtoConverter) {
        this.iCar = iCar;
        this.dtoConverter = dtoConverter;
    }


    public Mono<Void> frontMethod(String param) {
        return iCar.findByModel(param)
                .doOnNext(this::backMethod)
                        .log()
                        .then();
    }

    private void backMethod(CarEntity modelBefore) {
        iCar.findAll()
                .log()
                .subscribe();
    }
}
