package ru.itis.springreactivedemo.web.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.itis.springreactivedemo.service.CarSimpleService;
import ru.itis.springreactivedemo.web.dto.CarDto;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/v1/car")
public class CarRestController {
    private final CarSimpleService carSimpleService;

    public CarRestController(CarSimpleService carSimpleService) {
        this.carSimpleService = carSimpleService;
    }

    @GetMapping
    public Flux<CarDto> findAll() {
        return carSimpleService.findAllCars();
    }

    @GetMapping(path = "/id/{id}")
    public Mono<CarDto> findById(@PathVariable(name = "id") String id) {
        return carSimpleService.findCarById(id);
    }

    @GetMapping(path = "/types")
    public Mono<List<String>> findAllTypes() {
        return carSimpleService.findAllCarBrands();
    }

    @PostMapping
    public Mono<CarDto> create(@RequestBody CarDto carDto) {
        return carSimpleService.createCar(carDto);
    }

    @DeleteMapping(path = "/{id}")
    public Mono<Void> create(@PathVariable(name = "id") String id) {
        return carSimpleService.deleteCar(id);
    }
}
