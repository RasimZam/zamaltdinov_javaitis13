package ru.itis.springreactivedemo.web.handler;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import ru.itis.springreactivedemo.service.CarService;

@Component
public class CarHandler {

    private final CarService carService;

    public CarHandler(CarService carService) {
        this.carService = carService;
    }

    public Mono<ServerResponse> sampleMethod(ServerRequest serverRequest) {
        String param = serverRequest.queryParam("param")
                .orElseThrow(() -> new RuntimeException("Param doesn't exist"));

        Mono<ServerResponse> notFound = ServerResponse
                .notFound()
                .build();
        Mono<Void> result = carService.frontMethod(param);
        return result.flatMap(r -> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .build()

        );
    }
}
