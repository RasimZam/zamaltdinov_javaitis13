package ru.itis.springreactivedemo.web.dto;

import lombok.Data;

@Data
public class CarDto {
    private String id;
    private String brand;
    private String model;
}
