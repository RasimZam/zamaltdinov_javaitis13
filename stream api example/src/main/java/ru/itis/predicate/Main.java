package ru.itis.predicate;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;


public class Main {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Umka", 12);
        Cat cat2 = new Cat("Lushka", 5);
        Cat cat3 = new Cat("Umka", 8);
        Cat cat4 = new Cat("Timka", 4);
        Cat cat5 = new Cat("Kuzia", 2);

        List<Cat> cats = new ArrayList<>();
        cats.add(cat1);
        cats.add(cat2);
        cats.add(cat3);
        cats.add(cat4);
        cats.add(cat5);

        Predicate<Cat> pr1 = it -> it.getAge() < 5;
        Predicate<Cat> pr2 = it -> it.getName().startsWith("K");
        cats.removeIf(pr1.and(pr2));
        System.out.println(cats);
    }

    public static boolean testCat(Cat cat) {
        return cat.getAge() < 6;
    }
}
