package ru.itis.lambdaFunction.sample2;

public class SimpleClass {
    private int[] arr;

    public SimpleClass(int[] arr) {
        this.arr = arr;
    }
    private Summator sm = () -> {
        int sum = 0;
        for (int j : arr) {
            sum += j;
        }
        return sum;
    };

    public Summator getSummatorInstance() {
        return this.sm;
    }
}
