package ru.itis.lambdaFunction.sample2;

@FunctionalInterface
public interface Summator {
    int getSum();
}
