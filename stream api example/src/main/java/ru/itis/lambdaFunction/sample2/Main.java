package ru.itis.lambdaFunction.sample2;

//Пример использования лямбда выражения. В данном примере лямбда в классе SimpleClass является полем класса
public class Main {
    public static void main(String[] args) {
        SimpleClass simpleClass = new SimpleClass(new int[] {1, 2, 3});
        Summator sm = simpleClass.getSummatorInstance();
        System.out.println(sm.getSum());
    }
}
