package ru.itis.lambdaFunction.sample1;

// Многострочный или блочный лямбда выражения
public class Main1 {
    public static void main(String[] args) {
        StringModificator sm = (text) -> {
            String result = "";
            for (char let: text.toCharArray()) {
                if (Character.isLetter(let) || let == ' ') {
                    result += let;
                }
            }
            return result;
        };
        System.out.println(sm.getString("Hello123456 world"));
    }
}
