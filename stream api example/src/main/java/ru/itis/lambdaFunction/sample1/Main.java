package ru.itis.lambdaFunction.sample1;

//Однострочная лямбда выражения
public class Main {
    public static void main(String[] args) {
        StringModificator sm = (text) -> text.toUpperCase();
        System.out.println(sm.getString("hello"));
    }
}
