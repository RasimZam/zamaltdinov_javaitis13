package ru.itis.lambdaFunction.sample1;

@FunctionalInterface
public interface StringModificator {
    String getString(String text);
}
