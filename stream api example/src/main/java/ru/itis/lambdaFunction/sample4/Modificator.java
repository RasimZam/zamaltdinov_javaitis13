package ru.itis.lambdaFunction.sample4;

@FunctionalInterface
public interface Modificator<T> {
    T modification(T element);
}
