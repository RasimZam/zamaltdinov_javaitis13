package ru.itis.lambdaFunction.sample4;

// Обобщенный лямбда выражение
public class Main {
    public static void main(String[] args) {
        Modificator<String> mod = (text) -> text.toUpperCase();
        System.out.println(mod.modification("hello"));
    }
}
