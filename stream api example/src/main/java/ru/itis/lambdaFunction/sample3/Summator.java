package ru.itis.lambdaFunction.sample3;

@FunctionalInterface
public interface Summator {
    int getSum();
}
