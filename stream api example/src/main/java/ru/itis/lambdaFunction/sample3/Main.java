package ru.itis.lambdaFunction.sample3;

public class Main {
    public static void main(String[] args) {
        Summator sm = getIntegerSummator(new int[] {1, 2, 3});
        System.out.println(sm.getSum());
    }

    public static Summator getIntegerSummator(int[] array) {
        return () -> {
            int sum = 0;
            for (int element : array) {
                sum += element;
            }
            return sum;
        };
    }
}
