package ru.itis.functionalInterface.sample3;

@FunctionalInterface
public interface Generator1 {
    Object createNewObject(int size);
}
