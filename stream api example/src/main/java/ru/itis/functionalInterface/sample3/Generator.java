package ru.itis.functionalInterface.sample3;

@FunctionalInterface
public interface Generator {
    Object createNewObject();
}
