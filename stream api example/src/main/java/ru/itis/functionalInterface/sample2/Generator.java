package ru.itis.functionalInterface.sample2;

@FunctionalInterface
public interface Generator {
    int getNextElement(IntGenerator gen);
}
