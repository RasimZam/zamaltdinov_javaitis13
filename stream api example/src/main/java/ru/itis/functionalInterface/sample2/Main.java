package ru.itis.functionalInterface.sample2;

//Ссылка на не статический метод любого объекта
public class Main {
    public static void main(String[] args) {
        Generator gen1 = IntGenerator::next;
        IntGenerator a = new IntGenerator();
        System.out.println(gen1.getNextElement(a));
    }
}
