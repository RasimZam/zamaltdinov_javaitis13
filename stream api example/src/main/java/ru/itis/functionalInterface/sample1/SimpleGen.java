package ru.itis.functionalInterface.sample1;

public class SimpleGen {
    private int number;

    public SimpleGen(int number) {
        this.number = number;
    }

    public SimpleGen() {

    }

    public int getNumber() {
        int temp = number;
        number++;
        return temp;
    }

    public static int getRandomNumber() {
        return (int) (Math.random() * 10);
    }
}
