package ru.itis.functionalInterface.sample1;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> lists = new ArrayList<>();
        lists.add("Hello");
        lists.add("Cat");
        lists.add("Bag");
        lists.add("Goodbye");

        lists.removeIf(word -> word.length() > 3);
        System.out.println(lists);
    }
}
