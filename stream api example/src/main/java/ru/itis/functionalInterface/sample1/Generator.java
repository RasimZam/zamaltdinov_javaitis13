package ru.itis.functionalInterface.sample1;

@FunctionalInterface
public interface Generator {
    int getNextElement();
}
