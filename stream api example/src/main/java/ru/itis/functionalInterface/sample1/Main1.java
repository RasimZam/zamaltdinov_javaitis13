package ru.itis.functionalInterface.sample1;

public class Main1 {
    public static void main(String[] args) {
        //Ссылка на не статический метод конкретного объекта
        SimpleGen sg = new SimpleGen(5);
        Generator gen1 = sg::getNumber;
        int temp = gen1.getNextElement();
        System.out.println(temp);

        //Ссылка на статический метод
        Generator gen2 = SimpleGen::getRandomNumber;
        int temp1 = gen2.getNextElement();
        System.out.println(temp1);
    }
}
