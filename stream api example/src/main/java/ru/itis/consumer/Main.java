package ru.itis.consumer;

import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {
        Consumer<String> consumer = new HashCodePrinter<>();
        consumer.accept("Hello");
    }

    static class HashCodePrinter<T> implements Consumer<T> {

        @Override
        public void accept(T t) {
            System.out.println(t.hashCode());
        }
    }
}
