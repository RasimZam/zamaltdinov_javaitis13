package ru.itis.supplier;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        String[] names = new String[]{"Luke", "Darth", "Obi-Wan", "R2D2"};
        String firstLetter = "L";

        String result = Optional.ofNullable(findNameByFirstLetter(names, firstLetter))
                .orElseGet(() -> "Name not found. My the force be with you.");

        System.out.println(result);
    }

    public static String findNameByFirstLetter(String[] names, String firstLetter) {
        for (String name : names) {
            if (name.startsWith(firstLetter)) {
                return name;
            }
        }
        return null;
    }
}
