package ru.itis.referenceMethod;

@FunctionalInterface
public interface Generator {
    public int getNextElement();
}
