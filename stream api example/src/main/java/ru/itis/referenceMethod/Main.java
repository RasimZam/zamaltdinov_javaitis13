package ru.itis.referenceMethod;

public class Main {
    public static void main(String[] args) {
//        SimpleGen sg = new SimpleGen(5);
//        Generator gen = sg::getNumber;
//        int tmp = gen.getNextElement();
//        System.out.println(tmp);

        Generator gen = SimpleGen::getRandomNumber;
        int tmp = gen.getNextElement();
        System.out.println(tmp);

    }
}
