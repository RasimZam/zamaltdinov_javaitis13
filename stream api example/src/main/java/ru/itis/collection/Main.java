package ru.itis.collection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Integer> listNumber = new ArrayList<>();
        listNumber.add(1);
        listNumber.add(2);
        listNumber.add(3);
        listNumber.add(4);
        listNumber.add(5);

        Map<Integer, String> result = listNumber.stream()
                .collect(Collectors.toMap(Function.identity(), a -> (a % 2 == 0) ? "even" : "odd"));
        System.out.println(result);

        Map<String, List<Integer>> resultMap = listNumber.stream()
                .collect(Collectors.groupingBy(a -> (a % 2 == 0) ? "even" : "odd"));
        System.out.println(resultMap);
    }
}
