package ru.itis.optional;

import java.util.Optional;

// Optional который хранит внутри себя либо значение, либо null.
public class Main {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Vaska", "black");
        Cat cat2 = new Cat("Umka", "black");
        Cat cat3 = new Cat("Vaska", "black");

        Cat[] cats = new Cat[]{cat1, cat2, cat3};
        Optional<Cat> result = findCatByNameOptional("Vaska", cats);
        String catColor = result.map(it -> it.getName()).get();
        System.out.println(catColor);
    }

    public static Optional<Cat> findCatByNameOptional(String name, Cat[] cats) {
        Cat result = null;
        for (Cat cat : cats) {
            if (cat.getName().equals(name)) {
                 result = cat;
                 break;
            }
        }
        return Optional.ofNullable(result);
    }

}
