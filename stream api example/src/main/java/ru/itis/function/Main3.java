package ru.itis.function;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class Main3 {
    public static void main(String[] args) {
        Map<Integer, String> numbers = new HashMap<>();
        numbers.put(1, "one");
        numbers.put(5, "five");
        numbers.put(10, "ten");

        Integer num = 2;
        String result = numbers.computeIfAbsent(num, n -> "number_" + n);
        System.out.println(numbers);
        System.out.println(result);

        Function<Integer, Integer> fun = Function.identity();
        System.out.println(fun.apply(100));
    }
}
