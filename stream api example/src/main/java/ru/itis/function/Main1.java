package ru.itis.function;

import java.util.function.Function;

public class Main1 {
    public static void main(String[] args) {
        String text = "Java the best!";
        Function<String, Integer> fun = Main1::countWhiteSpace;

        System.out.println(applyToStr(text, fun));
    }

    public static Integer applyToStr(String txt, Function<String, Integer> fun) {
        return fun.apply(txt);
    }

    public static Integer countWhiteSpace(String text) {
        int n = 0;
        char[] s = text.toCharArray();
        for (int i = 0; i < s.length; i++) {
            if (s[i] == ' ') {
                n++;
            }
        }
        return n;
    }
}
