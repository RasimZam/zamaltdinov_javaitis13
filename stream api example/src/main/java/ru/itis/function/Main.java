package ru.itis.function;

import java.util.function.Function;

public class Main {
    public static void main(String[] args) {
        Function<String, Integer> fun = new StrLength();
        System.out.println(fun.apply("Hello"));

        Function<String, Integer> fun1 = a -> a.length();
        System.out.println(fun1.apply("Possible"));

        Function<String, Integer> fun2 = String::length;
        System.out.println(fun1.apply("Impossible"));

    }

    static class StrLength implements Function<String, Integer> {

        @Override
        public Integer apply(String s) {
            return s.length();
        }
    }
}
