package ru.itis.function;

import java.util.function.BiFunction;

public class Main4 {
    public static void main(String[] args) {
        BiFunction<String, Character, Integer> countLetter = Main4::count;

        System.out.println(countLetter.apply("Hello" , 'l'));
    }

    private static Integer count(String letter, Character chr) {
        int result = 0;
        char[] ls = letter.toCharArray();
        for (int i = 0; i < ls.length; i++) {
            if (ls[i] == chr) {
                result++;
            }
        }
        return result;
    }
}
