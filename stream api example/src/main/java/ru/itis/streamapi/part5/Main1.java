package ru.itis.streamapi.part5;

import java.util.List;
import java.util.Optional;

public class Main1 {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(0, 8, 4, 6, 3, 10, 5);
        Optional<Integer> result = numbers.stream()
                .filter(n -> n % 2 == 1)
                .findFirst();
        result.ifPresent(System.out::println);
    }
}
