package ru.itis.streamapi.part3;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main1 {
    public static void main(String[] args) {
        Singer singer1 = new Singer("Freddie Mercury", new String[]{"We are the champions", "Somebody to Love"});
        Singer singer2 = new Singer("David Bowie", new String[]{"Space Oddity", "Let Me Sleep Beside You", "Suffragette City"});
        Singer singer3 = new Singer("James Paul McCartney", new String[]{"Can't me by Love", "Another girl"});

        Singer[] rockStars = new Singer[]{singer1, singer2, singer3};

        List<String> song = Arrays.stream(rockStars)
                .flatMap(n -> Arrays.stream(n.getSongs()))
                .collect(Collectors.toList());
        System.out.println(song);
    }
}
