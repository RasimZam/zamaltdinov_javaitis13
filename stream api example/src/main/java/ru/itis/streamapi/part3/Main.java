package ru.itis.streamapi.part3;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<String> list = List.of("Java", "Python", "Fortran", "C");
        list.stream()
                .map(n -> n.length())
                .forEach(System.out::println);

        System.out.println("-------------------------------------");

        Cat cat1 = new Cat("Luska", 5, "White");
        Cat cat2 = new Cat("Masha", 4, "Green");
        Cat cat3 = new Cat("Aika", 3, "Black");
        Cat cat4 = new Cat("Barsic", 2, "Red");
        Cat cat5 = new Cat("Luska", 3, "Grey");

        Cat[] cats = new Cat[]{cat1, cat2, cat3, cat4, cat5};
        List<String> result = Arrays.stream(cats)
                .filter(cat -> cat.getWeight()  < 5)
                .map(Cat::getName)
                .sorted(Comparator.comparingInt(String::length))
                .collect(Collectors.toList());
        System.out.println(result);

    }
}
