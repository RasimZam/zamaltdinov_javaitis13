package ru.itis.streamapi.part3;

import java.util.List;
import java.util.stream.Stream;

public class Main3 {
    public static void main(String[] args) {
        List<Integer> list1 = List.of(0, 2, 4, 6);
        List<Integer> list2 = List.of(1, 3, 5, 7);

        Stream<Integer> concatStream = Stream.concat(list1.stream(),list2.stream());

        concatStream.forEach(System.out::println);
    }
}
