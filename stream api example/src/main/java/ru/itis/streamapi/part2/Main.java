package ru.itis.streamapi.part2;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(4);
        list.add(2);
        list.add(-2);
        list.add(1);
        list.add(3);
        list.add(1);
        list.add(8);

        list.stream()
                .distinct()
                .forEach(System.out::println);

        Cat cat1 = new Cat("Luska", 5, "White");
        Cat cat2 = new Cat("Luska", 3, "Grey");
        Cat cat3 = new Cat("Barsic", 2, "Red");
        Cat cat4 = new Cat("Luska", 3, "Grey");

        List<Cat> catList = List.of(cat1, cat2, cat3, cat4);

        catList.stream()
                .distinct()
                .forEach(System.out::println);
    }
}
