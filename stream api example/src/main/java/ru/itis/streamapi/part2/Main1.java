package ru.itis.streamapi.part2;

import java.util.List;

public class Main1 {
    public static void main(String[] args) {
        List<Integer> list = List.of(0, 5, -2, 0, 3, 1, 1, -4, 7);
        list.stream()
                .dropWhile(n -> n >= 0)
                .forEach(System.out::println);

        System.out.println("___________________");

        list.stream()
                .takeWhile(n -> n >= 0)
                .forEach(System.out::println);
    }
}
