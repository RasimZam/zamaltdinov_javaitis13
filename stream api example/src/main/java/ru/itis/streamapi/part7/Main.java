package ru.itis.streamapi.part7;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3, 4);
        List<Integer> even = list.stream()
                .filter(a -> a % 2 == 0)
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
        System.out.println(even);

        System.out.println("---------------------------------------------------");

        List<Goods> goodsList = List.of(new Goods("Apple", 50),
                new Goods("Orange", 70),
                new Goods("Pear", 65),
                new Goods("Cherry", 75));

        List<String> goodsName = goodsList.stream()
                .filter(a -> a.getPrice() > 50)
                .collect(ArrayList::new, (a, b) -> a.add(b.getName()), ArrayList::addAll);

        System.out.println(goodsName);
    }
}
