package ru.itis.streamapi.part1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Luska", 5, "White");
        Cat cat2 = new Cat("Umka", null, "Black");
        Cat cat3 = new Cat("Barsic", 2, "Red");
        Cat cat4 = new Cat("Luska", 3, "Grey");

        List<Cat> catList = new ArrayList<>();
        catList.add(cat1);
        catList.add(cat2);
        catList.add(cat3);
        catList.add(cat4);

        Cat[] str = catList.stream()
                .filter(value -> Objects.nonNull(value.getWeight()))
                .filter(value -> value.getWeight() < 5)
                .collect(Collectors.toList())
                .toArray(new Cat[]{});

        catList.stream()
                .filter(value -> Objects.nonNull(value.getWeight()))
                .peek(it -> it.setName("_" + it.getName()))
                .forEach(System.out::println);


        System.out.println(Arrays.toString(str));
    }
}
