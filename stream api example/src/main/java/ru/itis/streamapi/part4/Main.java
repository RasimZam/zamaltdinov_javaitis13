package ru.itis.streamapi.part4;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list1 = List.of("A1", "A2", "A3");

        list1.stream()
                .filter(n -> {
                    System.out.println("Filter " + n);
                    return n.length() <= 2;
                }).map(n -> {
                    System.out.println("Map " + n);
                    return "_" + n;
                }).forEach(n -> System.out.println("forEach " + n));

        System.out.println("----------------------------------------------");

        List<String> list2 = List.of("A1", "C1", "B1");
        list2.stream()
                .sorted((a, b) -> {
                    System.out.println("Sorted " + a + " " + b);
                    return a.compareTo(b);
                }).filter(n -> {
                    System.out.println("Filter " + n);
                    return n.startsWith("B");
                }).map(n -> {
                    System.out.println("Map " + n);
                    return "_" + n;
                }).forEach(n -> System.out.println("forEach " + n));

    }
}
