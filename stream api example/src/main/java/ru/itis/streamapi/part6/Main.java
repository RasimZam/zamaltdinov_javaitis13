package ru.itis.streamapi.part6;

import java.util.List;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7, 8);

        Optional<Integer> sum = list.stream()
                .filter(a -> a % 2 == 0)
                .reduce(Integer::sum);

        System.out.println(sum.get());

    }
}
