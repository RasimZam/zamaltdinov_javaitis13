package ru.itis.streamapi.part6;

import java.util.List;
import java.util.Optional;

public class Main1 {
    public static void main(String[] args) {
        List<String> worlds = List.of("Java", "Fortran", "Python", "C++");

        Optional<String> result = worlds.stream()
                .reduce((a, b) -> a.length() > b.length() ? a : b);
        System.out.println(result.get());
    }
}
