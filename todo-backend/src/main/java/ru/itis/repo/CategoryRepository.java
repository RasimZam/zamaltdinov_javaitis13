package ru.itis.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.entity.Category;

import java.util.List;


public interface CategoryRepository extends JpaRepository<Category, Long> {

    List<Category> findByUserEmailOrderByTitleAsc(String email);
}
