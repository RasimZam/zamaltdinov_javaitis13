package ru.itis.tasklist.web.mappers;

import org.mapstruct.Mapper;
import ru.itis.tasklist.domain.user.User;
import ru.itis.tasklist.web.dto.user.UserDto;

@Mapper(componentModel = "spring")
public interface UserMapper extends Mappable<User, UserDto> {
}
