package ru.itis.tasklist.web.mappers;

import java.util.List;

public interface Mappable<E, D> {

    D toDto(E entity);

    List<D> toDto(List<E> entity);

    E toEntity(D entity);

    List<E> toEntity(List<D> dtos);
}
