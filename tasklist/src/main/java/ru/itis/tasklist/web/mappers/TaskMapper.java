package ru.itis.tasklist.web.mappers;

import org.mapstruct.Mapper;
import ru.itis.tasklist.domain.task.Task;
import ru.itis.tasklist.web.dto.task.TaskDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaskMapper extends Mappable<Task, TaskDto> {
}
