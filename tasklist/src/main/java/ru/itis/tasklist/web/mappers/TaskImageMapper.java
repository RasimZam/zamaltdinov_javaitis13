package ru.itis.tasklist.web.mappers;

import org.mapstruct.Mapper;
import ru.itis.tasklist.domain.task.Task;
import ru.itis.tasklist.domain.task.TaskImage;
import ru.itis.tasklist.web.dto.task.TaskDto;
import ru.itis.tasklist.web.dto.task.TaskImageDto;

@Mapper(componentModel = "spring")
public interface TaskImageMapper extends Mappable<TaskImage, TaskImageDto> {
}
