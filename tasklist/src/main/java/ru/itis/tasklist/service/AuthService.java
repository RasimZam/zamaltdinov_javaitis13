package ru.itis.tasklist.service;

import ru.itis.tasklist.web.dto.auth.JwtRequest;
import ru.itis.tasklist.web.dto.auth.JwtResponse;

public interface AuthService {
    JwtResponse login(JwtRequest loginRequest);
    JwtResponse refresh(String refreshToken);
}
