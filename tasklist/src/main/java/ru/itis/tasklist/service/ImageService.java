package ru.itis.tasklist.service;

import ru.itis.tasklist.domain.task.TaskImage;

public interface ImageService {

    String upload(TaskImage image);
}
