package ru.itis.tasklist.domain.exception;

public class AccessDeniedException extends RuntimeException {
    public AccessDeniedException() {
        super();
    }
}
