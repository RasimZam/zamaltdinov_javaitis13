package ru.itis.resttemplate;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.itis.entity.User;

@Component
public class UserRestBuilder {

    private static final String BASEURL = "http://localhost:8765/planner-users/user/";
    // проверка - существует ли пользователь
    public boolean userExists(Long userId) {
        // для примера - как использовать RestTemplate (но он уже deprecated)
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Long> request = new HttpEntity(userId);
        ResponseEntity<User> response = null;
        // если нужно получить объект - просто вызываете response.getBody() и произойдет автоматическая конвертация из JSON в POJO
        // в текущем вызове нам не нужен объект, т.к. мы просто проверяем, есть ли такой пользователь
        try {
            // вызов сервисы
            response = restTemplate.exchange(BASEURL +"/id", HttpMethod.POST, request, User.class);
            if (response.getStatusCode() == HttpStatus.OK) { // если статус был 200
                return true;
            }
        } catch (Exception e){
            throw new IllegalArgumentException("Doesn't find");
        }
        return false; // если статус не был 200
    }
}
