package ru.itis.service;

import org.springframework.stereotype.Service;
import ru.itis.entity.Priority;
import ru.itis.entity.Stat;
import ru.itis.repo.PriorityRepository;
import ru.itis.repo.StatRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class StatService {

    private final StatRepository statRepository;

    public StatService(StatRepository statRepository) {
        this.statRepository = statRepository;
    }

    public Stat findStat(Long userId) {
        return statRepository.findByUserId(userId);
    }
}
