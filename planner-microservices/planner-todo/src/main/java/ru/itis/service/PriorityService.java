package ru.itis.service;

import org.springframework.stereotype.Service;
import ru.itis.entity.Category;
import ru.itis.entity.Priority;
import ru.itis.repo.CategoryRepository;
import ru.itis.repo.PriorityRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class PriorityService {

    private final PriorityRepository priorityRepository;

    public PriorityService(PriorityRepository priorityRepository) {
        this.priorityRepository = priorityRepository;
    }

    public Optional<Priority> findById(Long id) {
        return Optional.ofNullable(priorityRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Doesn't find")));
    }

    public List<Priority> findAll(Long userId) {
        return priorityRepository.findByUserIdOrderByTitleAsc(userId);
    }

    public Priority add(Priority priority) {
        return priorityRepository.save(priority);
    }

    public void update(Priority priority) {
        priorityRepository.save(priority);
    }

    public void deleteById(Long id) {
        priorityRepository.deleteById(id);
    }

    public List<Priority> findByTitle(String title, Long userId) {
        return priorityRepository.findByTitle(title, userId);
    }
}
