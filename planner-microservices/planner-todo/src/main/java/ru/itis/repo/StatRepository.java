package ru.itis.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itis.entity.Stat;

@Repository
public interface StatRepository extends JpaRepository<Stat, Long> {

    Stat findByUserId(Long id);
}
