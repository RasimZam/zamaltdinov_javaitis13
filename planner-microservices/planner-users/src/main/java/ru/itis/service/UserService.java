package ru.itis.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.itis.entity.User;
import ru.itis.repo.UserRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User add(User user) {
        return userRepository.save(user);
    }

    public void deleteByUserId(Long id) {
        userRepository.deleteById(id);
    }

    public void deleteByUserEmail(String email) {
        userRepository.deleteByEmail(email);
    }

    public Optional<User> findById(Long id) {
       return userRepository.findById(id);
    }

    public Page<User> findByParams(String email, String username, PageRequest paging) {
        return userRepository.findAllByParams(email, username, paging);
    }

    public void update(User user) {
        userRepository.save(user);
    }
}
