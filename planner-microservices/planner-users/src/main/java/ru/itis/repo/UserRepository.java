package ru.itis.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.itis.entity.User;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);
    void deleteAllByEmail(String email);

    @Query("SELECT u from User u where" +
            ":email is null or :email='' or lower(u.email) like lower(concat('%', :email, '%')) or" +
            "(:username is null or :username='' or lower(u.username) like lower(concat('%', :username, '%')))")
    Page<User> findAllByParams(@Param("email") String email,
                               @Param("username") String username,
                                Pageable pageable);

    void deleteByEmail(String email);
}
