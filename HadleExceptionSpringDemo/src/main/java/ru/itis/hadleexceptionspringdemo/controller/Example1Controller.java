package ru.itis.hadleexceptionspringdemo.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.hadleexceptionspringdemo.exception.BusinessException;
import ru.itis.hadleexceptionspringdemo.model.Response;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class Example1Controller {

    @GetMapping(value = "/testExceptionHandler", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> testExceptionHandler(@RequestParam(required = false, defaultValue = "false") boolean exception) throws BusinessException {
       if (exception) {
           throw new BusinessException("BusinessException in testExceptionHandler");
       }
       return ResponseEntity.ok(new Response("ok"));
    }

    @ExceptionHandler(BusinessException.class)
    public Response handleException(BusinessException e) {
        return new Response(e.getMessage());
    }
}
