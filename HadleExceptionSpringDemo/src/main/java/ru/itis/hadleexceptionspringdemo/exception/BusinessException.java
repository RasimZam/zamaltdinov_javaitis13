package ru.itis.hadleexceptionspringdemo.exception;

public class BusinessException extends RuntimeException {

    public BusinessException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "BusinessException{}";
    }
}
