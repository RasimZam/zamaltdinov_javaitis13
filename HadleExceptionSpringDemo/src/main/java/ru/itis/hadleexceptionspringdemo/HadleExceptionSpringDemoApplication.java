package ru.itis.hadleexceptionspringdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HadleExceptionSpringDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(HadleExceptionSpringDemoApplication.class, args);
    }

}
