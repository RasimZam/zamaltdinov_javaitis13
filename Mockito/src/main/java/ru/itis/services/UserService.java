package ru.itis.services;

import ru.itis.dao.UserDao;
import ru.itis.model.User;

public class UserService {

    private UserDao userDao;

    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public boolean checkUserPresence(User user) throws Exception {
        User userFromStorage = userDao.getUserByUsername(user.getUsername());
        return userFromStorage != null;
    }

}
