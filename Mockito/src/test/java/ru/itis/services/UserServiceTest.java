package ru.itis.services;

import com.sun.org.apache.xpath.internal.Arg;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ru.itis.dao.UserDao;
import ru.itis.model.User;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

public class UserServiceTest {

    @Mock
    private UserDao userDao;

    private UserService userService;

    public UserServiceTest() {
        MockitoAnnotations.initMocks(this);
        this.userService = new UserService(userDao);
    }

    @Test
    public void checkUserPresence_should_return_true() throws Exception {
        given(userDao.getUserByUsername("anton@gmail.com")).willReturn(
                new User("anton@gmail.com"));

        boolean userExists = userService.checkUserPresence(
                new User("anton@gmail.com"));
        assertThat(userExists).isTrue();

        verify(userDao).getUserByUsername("anton@gmail.com");
    }

    @Test
    public void checkUserPresence_should_return_null() throws Exception {
        given(userDao.getUserByUsername("anton@gmail.com")).willReturn(
                null);

        boolean userExists = userService.checkUserPresence(
                new User("anton@gmail.com"));
        assertThat(userExists).isFalse();
    }

    @Test(expected = Exception.class)
    public void checkUserPresence_should_return_throw_exception() throws Exception {
        given(userDao.getUserByUsername(anyString())).willThrow(Exception.class);

        userService.checkUserPresence(
                new User("anton@gmail.com"));
    }

    @Test
    public void testCaptor() throws Exception{
        given(userDao.getUserByUsername(anyString())).willReturn(
                new User("anton@gmail.com")
        );

        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);

        boolean b = userService.checkUserPresence(new User("anton@gmail.com"));
        verify(userDao).getUserByUsername(captor.capture());
        String argument = captor.getValue();

        assertThat(argument).isEqualTo("anton@gmail.com");
    }
}