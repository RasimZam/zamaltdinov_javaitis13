package ru.itis.test.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.itis.test.models.Test;

import java.util.List;

public interface TestRepository extends JpaRepository<Test, Long> {

    @Query("from Test t where t.tag = 'физика'")
    List<Test> findByQuestionsPhysic();

    @Query("from Test t where t.tag = 'история'")
    List<Test> findByQuestionsHistory();

}
