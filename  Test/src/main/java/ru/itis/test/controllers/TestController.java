package ru.itis.test.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.test.models.Test;
import ru.itis.test.models.User;
import ru.itis.test.security.details.AccountUserDetails;
import ru.itis.test.service.TestService;
import ru.itis.test.service.UserService;

import java.util.Map;
import java.util.Set;

@Controller
public class TestController {

    @Autowired
    private UserService userService;

    @Autowired
    private TestService testService;

    @GetMapping("/test")
    public String testPage(@AuthenticationPrincipal AccountUserDetails currentUser, Model model) {
        findUser(currentUser, model);
        return "test";
    }

    private void findUser (AccountUserDetails currentUser, Model model) {
        User user = userService.findByUser(currentUser.getUsername());
        model.addAttribute("user", user);
    }

    @GetMapping("/test/testPhysics")
    public String testPagePhysic(Model model, @AuthenticationPrincipal AccountUserDetails currentUser) {
        Set<Test> testPhysics = testService.findByQuestionsPhysic();
        findUser(currentUser, model);
        model.addAttribute("tests", testPhysics);

        return "testPhysics";
    }

    @PostMapping("/test")
    public String testPagePost(@AuthenticationPrincipal AccountUserDetails currentUser,
                               Model model,
                               @RequestParam("name") String test) {
        findUser(currentUser, model);

        if (test.equalsIgnoreCase("физика")) {
            return "redirect:/test/testPhysics";
        }
            return "redirect:/test/testHistory";
    }

    @GetMapping("/test/testHistory")
    public String testPageHistory(Model model, @AuthenticationPrincipal AccountUserDetails currentUser) {
        Set<Test> testHistory = testService.findByQuestionsHistory();
        findUser(currentUser, model);
        model.addAttribute("tests", testHistory);

        return "testHistory";
    }

    @PostMapping("/test/testHistory")
    public String testHandleRadioHistory(Model model, @AuthenticationPrincipal AccountUserDetails currentUser,
                                         @ModelAttribute("history") Map<String, String> history) {
//        Set<Test> testHistory = testService.findByQuestionsHistory();
        int i = 0;
        findUser(currentUser, model);
//        model.addAttribute("tests", testHistory);

        return "testHistory";
    }
}
