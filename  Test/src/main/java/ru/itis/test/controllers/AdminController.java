package ru.itis.test.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.test.models.User;
import ru.itis.test.security.details.AccountUserDetails;
import ru.itis.test.service.TestService;
import ru.itis.test.service.UserService;

@Controller
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private TestService testService;

    @GetMapping("/admin")
    public String pageAdmin() {
        return "admin";
    }

    @GetMapping("/admin/testAdd")
    public String testList(Model model, @AuthenticationPrincipal AccountUserDetails currentUser) {
        User user = userService.findByUser(currentUser.getUsername());
        model.addAttribute("user", user);
        return "testAdd";
    }

    @PostMapping("/admin/testAdd")
    public String addTest(Model model, @AuthenticationPrincipal AccountUserDetails currentUser,
                          @RequestParam("nameQuestion") String question,
                          @RequestParam("firstResponse") String firstResponse,
                          @RequestParam("secondResponse") String secondResponse,
                          @RequestParam("thirdResponse") String thirdResponse,
                          @RequestParam("fourthResponse") String fourthResponse,
                          @RequestParam("correctAnswer") String correctAnswer,
                          @RequestParam("tag") String tag) {
        User user = userService.findByUser(currentUser.getUsername());
        model.addAttribute("user", user);
        testService.save(question, firstResponse, secondResponse,
                thirdResponse, fourthResponse, tag, correctAnswer);

        if (tag.equalsIgnoreCase("физика")) {
            return "redirect:/test/testPhysics";
        }
            return "redirect:/test/testHistory";
    }

}
