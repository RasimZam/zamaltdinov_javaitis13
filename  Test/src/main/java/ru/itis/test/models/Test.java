package ru.itis.test.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "test")
public class Test {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name_question")
    private String nameQuestion;

    @Column(name = "tag")
    private String tag;

    @Column(name = "first_response")
    private String firstResponse;

    @Column(name = "second_response")
    private String secondResponse;

    @Column(name = "third_response")
    private String thirdResponse;

    @Column(name = "fourth_response")
    private String fourthResponse;

    @Column(name = "correct_answer")
    private String correctAnswer;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameQuestion() {
        return nameQuestion;
    }

    public void setNameQuestion(String nameQuestion) {
        this.nameQuestion = nameQuestion;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFirstResponse() {
        return firstResponse;
    }

    public void setFirstResponse(String firstResponse) {
        this.firstResponse = firstResponse;
    }

    public String getSecondResponse() {
        return secondResponse;
    }

    public void setSecondResponse(String secondResponse) {
        this.secondResponse = secondResponse;
    }

    public String getThirdResponse() {
        return thirdResponse;
    }

    public void setThirdResponse(String thirdResponse) {
        this.thirdResponse = thirdResponse;
    }

    public String getFourthResponse() {
        return fourthResponse;
    }

    public void setFourthResponse(String fourthResponse) {
        this.fourthResponse = fourthResponse;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Test test = (Test) o;
        return Objects.equals(nameQuestion, test.nameQuestion) && Objects.equals(tag, test.tag) && Objects.equals(firstResponse, test.firstResponse) && Objects.equals(secondResponse, test.secondResponse) && Objects.equals(thirdResponse, test.thirdResponse) && Objects.equals(fourthResponse, test.fourthResponse) && Objects.equals(correctAnswer, test.correctAnswer) && Objects.equals(user, test.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameQuestion, tag, firstResponse, secondResponse, thirdResponse, fourthResponse, correctAnswer, user);
    }
}
