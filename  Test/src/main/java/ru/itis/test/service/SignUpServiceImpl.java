package ru.itis.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.test.forms.SignUpForm;
import ru.itis.test.models.Role;
import ru.itis.test.models.User;
import ru.itis.test.repositories.UserRepository;

import java.util.Collections;
import java.util.Date;

@Service
public class SignUpServiceImpl implements SignUpService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void signUp(SignUpForm form) {
        User user = new User();

        user.setUsername(form.getUsername());
        user.setEmail(form.getEmail());
        user.setPassword(passwordEncoder.encode(form.getPassword()));
        user.setRoles(Collections.singleton(Role.USER));
        user.setDateRegistration(new Date());
        userRepository.save(user);
    }
}
