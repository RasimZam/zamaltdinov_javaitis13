package ru.itis.test.service;

import ru.itis.test.models.Test;

import java.util.Set;

public interface TestService {
    Set<Test> findByQuestionsPhysic();

    void save(String question, String firstResponse, String secondResponse, String thirdResponse, String fourthResponse, String tag, String correctAnswer);

    Set<Test> findByQuestionsHistory();
}
