package ru.itis.test.service;

import ru.itis.test.forms.SignUpForm;

public interface SignUpService {

    void signUp(SignUpForm form);
}
