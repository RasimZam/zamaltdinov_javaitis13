package ru.itis.test.service;

import ru.itis.test.models.User;

public interface UserService {
    User findByUser(String username);

}
