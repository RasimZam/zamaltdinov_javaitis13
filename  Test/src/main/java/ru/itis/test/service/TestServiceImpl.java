package ru.itis.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.test.models.Test;
import ru.itis.test.repositories.TestRepository;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private TestRepository testRepository;

    @Override
    public Set<Test> findByQuestionsPhysic() {

        List<Test> tests = testRepository.findByQuestionsPhysic();
        return getTests(tests);
    }


    @Override
    public void save(String question, String firstResponse, String secondResponse,
                     String thirdResponse, String fourthResponse, String tag, String correctAnswer) {
        Test newTest = new Test();
        newTest.setNameQuestion(question);
        newTest.setFirstResponse(firstResponse);
        newTest.setSecondResponse(secondResponse);
        newTest.setThirdResponse(thirdResponse);
        newTest.setFourthResponse(fourthResponse);
        newTest.setTag(tag);
        newTest.setCorrectAnswer(correctAnswer);
        testRepository.save(newTest);
    }

    @Override
    public Set<Test> findByQuestionsHistory() {
        List<Test> tests = testRepository.findByQuestionsHistory();
        return getTests(tests);
    }

    private Set<Test> getTests(List<Test> tests) {
        Set<Test> uniqueTest = new HashSet<>();
        Collections.shuffle(tests);
        for (int i = 0; i < tests.size(); i++) {
            if(!uniqueTest.contains(tests.get(i)))
            uniqueTest.add(tests.get(i));
        }
        return uniqueTest;
    }

}
