package ru.itis.webflux.demo;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

class MonoFluxTest {

    @Test
    void testMono() {
        Mono<?> monoString = Mono.just("example")
                .then(Mono.error(new RuntimeException("exception occured")))
                .log();
        monoString.subscribe(System.out::println, (ex) -> System.out.println(ex.getMessage()));
    }

    @Test
    void testFlux() {
        Flux<String> fluxString = Flux.just("Spring", "Spring boot", "example")
                .concatWithValues("AWS")
                .concatWith(Flux.error(new RuntimeException("Exception occured is Flux")))
                .log();
        fluxString.subscribe(System.out::println, (ex) -> System.out.println(ex.getMessage()));
    }
}
