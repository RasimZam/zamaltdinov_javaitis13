package ru.itis.webflux.demo.service;

import ru.itis.webflux.demo.model.Revenue;

import static ru.itis.webflux.demo.util.CommonUtil.delay;


public class RevenueService {

    public Revenue getRevenue(Long movieId){
        delay(1000); // simulating a network call ( DB or Rest call)
        return Revenue.builder()
                .movieInfoId(movieId)
                .budget(1000000)
                .boxOffice(5000000)
                .build();

    }
}
