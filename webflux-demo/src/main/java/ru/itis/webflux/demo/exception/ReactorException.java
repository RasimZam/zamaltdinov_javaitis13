package ru.itis.webflux.demo.exception;

public class ReactorException extends Throwable {
    private Throwable exception;
    private String message;

    public ReactorException(Throwable exception, String message) {
        super(message, exception);
        this.exception = exception;
        this.message = message;
    }
}
