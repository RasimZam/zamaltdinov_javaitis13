package com.example.reactordemo;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class ReactorDemoApplicationTests {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReactorDemoApplicationTests.class);

    @Test
    void simpleFluxExample() {
        Flux<String> fluxColors = Flux.just("red", "green", "blue");
        fluxColors.log().subscribe(System.out::println);
    }

    @Test
    void mapExample() {
        Flux<String> fluxColors = Flux.just("red", "green", "blue");
        fluxColors.log().map(color -> color.charAt(0)).subscribe(System.out::println);
    }

    @Test
    void zipExample() {
        Flux<String> fluxFruits = Flux.just("apple", "pear", "plum");
        Flux<String> fluxColors = Flux.just("red", "green", "blue");
        Flux<Integer> fluxAmounts = Flux.just(10, 20, 30);
        Flux.zip(fluxFruits, fluxColors, fluxAmounts).log().subscribe(System.out::println);
    }

    @Test
    void onErrorExample() {
        Flux<String> fluxCalc = Flux.just(-1, 0, 1)
                .map(i -> "10 / " + i + " = " + (10 / i));

        fluxCalc.log().subscribe(value -> System.out.println("Next: " + value),
                error -> System.err.println("Error: " + error));
    }

    @Test
    void onErrorReturnExample() {
        Flux<String> fluxCalc = Flux.just(-1, 0, 1)
                .map(i -> "10 / " + i + " = " + (10 / i))
                .onErrorReturn(ArithmeticException.class, "Division by 0 nota allowed");

        fluxCalc.log().subscribe(value -> System.out.println("Next: " + value),
                error -> System.err.println("Error: " + error));
    }

    @Test
    void stepVerifierTest() {
        Flux<String> fluxCalc = Flux.just(-1, 0, 1)
                .map(i -> "10 / " + i + " = " + (10 / i));

        StepVerifier.create(fluxCalc)
                .expectNextCount(1)
                .expectError(ArithmeticException.class)
                .verify();
    }

    @Test
    void backpressureExample() {
        Flux.range(1, 5)
                .subscribe(new Subscriber<Integer>() {
                    private Subscription s;
                    int counter;

                    @Override
                    public void onSubscribe(Subscription s) {
                        System.out.println("onSubscibe");
                        this.s = s;
                        System.out.println("Requesting 2 emissions");
                        s.request(2);
                    }

                    @Override
                    public void onNext(Integer i) {
                        System.out.println("onNext " + i);
                        counter++;
                        if (counter % 2 == 0) {
                            System.out.println("Requesting 2 emissions");
                            s.request(2);
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        System.err.println("onError");
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("onComplete");
                    }
                });
    }

    @Test
    void monoSubscriberConsumerComplete() {
        String name = "William Suane";
        Mono<String> mono = Mono.just(name)
                .log()
                .map(String::toUpperCase);

        mono.subscribe(s -> LOGGER.info("Value {}", s), Throwable::printStackTrace,
                () -> LOGGER.info("FINISHED!"));
        LOGGER.info("-------------------------------");

        StepVerifier.create(mono)
                .expectNext(name.toUpperCase())
                .verifyComplete();
    }

    @Test
    void monoSubscriberConsumerSubscription() {
        String name = "William Suane";
        Mono<String> mono = Mono.just(name)
                .log()
                .map(String::toUpperCase);

        mono.subscribe(s -> LOGGER.info("Value {}", s), Throwable::printStackTrace,
                () -> LOGGER.info("FINISHED!"), Subscription::cancel);

        LOGGER.info("-------------------------------");

        StepVerifier.create(mono)
                .expectNext(name.toUpperCase())
                .verifyComplete();
    }

    @Test
    void monoDoOnMethods() {
        String name = "William Suane";
        Mono<Object> mono = Mono.just(name)
                .log()
                .map(String::toUpperCase)
                .doOnSubscribe(subscription -> LOGGER.info("Subscribed"))
                .doOnRequest(longNumber -> LOGGER.info("Request Received, starting doing something..."))
                .doOnNext(s -> LOGGER.info("Value is here. Executing doOnNext {}", s))
                .flatMap(s -> Mono.empty())
                .doOnNext(s -> LOGGER.info("Value is here. Executing doOnNext {}", s))
                .doOnSuccess(s -> LOGGER.info("doOnSuccess executed"));

        mono.subscribe(s -> LOGGER.info("Value {}", s), Throwable::printStackTrace,
                () -> LOGGER.info("FINISHED!"));
        LOGGER.info("-------------------------------");
    }

    @Test
    void monoDoOnError() {
        Mono<Object> error = Mono.error(new IllegalArgumentException("Illegal Argument exception"))
                .doOnError(e -> LOGGER.error("Error message: {}", e.getMessage()))
                .doOnNext(s -> LOGGER.info("Executing this doOnNext"))
                .log();

        StepVerifier.create(error)
                .expectError(IllegalArgumentException.class)
                .verify();
    }

    @Test
    void monoOnErrorResume() {
        String name = "William Suane";
        Mono<Object> error = Mono.error(new IllegalArgumentException("Illegal Argument exception"))
                .doOnError(e -> LOGGER.error("Error message: {}", e.getMessage()))
                .onErrorResume(s -> {
                    LOGGER.info("Inside On Error Resume");
                    return Mono.just(name);
                })
                .log();

        StepVerifier.create(error)
                .expectNext(name)
                .verifyComplete();
    }

    @Test
    void monoOnErrorReturn() {
        String name = "William Suane";
        Mono<Object> error = Mono.error(new IllegalArgumentException("Illegal Argument exception"))
                .onErrorReturn("EMPTY")
                .onErrorResume(s -> {
                    LOGGER.info("Inside On Error Resume");
                    return Mono.just(name);
                })
                .doOnError(e -> LOGGER.error("Error message: {}", e.getMessage()))
                .log();

        StepVerifier.create(error)
                .expectNext("EMPTY")
                .verifyComplete();
    }
}
